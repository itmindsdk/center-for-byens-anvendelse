﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DomainModel
{
    public class CouncilPriority : IEntity
    {
        public int Id { get; set; }
        public string Priority { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
