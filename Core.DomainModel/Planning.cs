﻿#region Head
// <copyright file="Planning.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    /// <summary>
    /// Represents a plan for a <see cref="Project"/>.
    /// </summary>
    public class Planning : IEntity
    {
        public int Id { get; set; }
        public string Plan { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
