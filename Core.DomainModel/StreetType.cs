﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents a type of street.
    /// </summary>
    public class StreetType : IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
