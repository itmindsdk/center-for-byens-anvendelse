﻿using System;

namespace Core.DomainModel
{
    /// <summary>
    /// Defines a date for which the object was created.
    /// </summary>
    public interface ICreatedOn
    {
        DateTime CreatedOn { get; set; }
    }
}
