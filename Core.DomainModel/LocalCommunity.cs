﻿#region Head
// <copyright file="LocalCommunity.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    public class LocalCommunity : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
