﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents an area within Denmark.
    /// </summary>
    public enum AreaType
    {
        Midt,
        Nord,
        Vest,
        Syd
    }
}
