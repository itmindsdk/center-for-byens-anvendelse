﻿#region Head
// <copyright file="ProjectCategory.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    /// <summary>
    /// Represents a category for a <see cref="Project"/>.
    /// </summary>
    public class ProjectCategory : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public bool IsDeleted { get; set; }
    }
}
