﻿#region Head
// <copyright file="InvitedPerson.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    /// <summary>
    /// Represents the default invited people for a <see cref="Meeting"/>.
    /// </summary>
    public class InvitedPerson : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
