﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DomainModel
{
    public class PartnerDiscussion : IEntity
    {
        public int Id { get; set; }
        public bool Discussed { get; set; }
        public string DiscussedWith { get; set; }
        public string Description { get; set; }
    }
}
