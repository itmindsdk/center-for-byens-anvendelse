﻿#region Head
// <copyright file="AgendaPoint.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace Core.DomainModel
{
    /// <summary>
    /// Represents an agenda point in a <see cref="MeetingAgendaPoint"/> (færdselsmødepunkter).
    /// </summary>
    public class AgendaPoint : Logger, IEntity
    {
        public AgendaPoint()
        {
            PreviousMeetings = new HashSet<Meeting>();
            Documents = new HashSet<AgendaPointDocument>();
        }

        public int Id { get; set; }
        public DbGeometry AddressLocation { get; set; }
        public string AddressStreetName { get; set; }
        public string AddressNumber { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressCity { get; set; }
        public virtual ProjectLeader ProjectLeader { get; set; }
        public int? ProjectLeaderId { get; set; }
        public bool TemporaryAddress { get; set; }
        public string TemporaryAddressName { get; set; }
        public int? StreetTypeId { get; set; }
        public virtual StreetType StreetType { get; set; }
        public int? ProjectId { get; set; }
        public virtual Project Project { get; set; }
        public virtual MeetingAgendaPoint MeetingAgendaPoint { get; set; }
        public string Description { get; set; }
        public string Conclusion { get; set; }
        public string Agreement { get; set; }
        public virtual ICollection<Meeting> PreviousMeetings { get; set; }
        public virtual ICollection<AgendaPointDocument> Documents { get; set; }
        public int? TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public int? AgendaPointStatusId { get; set; }
        public virtual AgendaPointStatus AgendaPointStatus { get; set; }
        public AreaType Area { get; set; }
        public virtual PartnerDiscussion CbmDiscussion { get; set; }
        public virtual PartnerDiscussion CbaDiscussion { get; set; }
    }
}
