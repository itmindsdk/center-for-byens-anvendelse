﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents the type of agenda point field a default phrase is assoiated with.
    /// </summary>
    public enum DefaultPhraseType
    {
        Conclusion,
        Agreement      
    }
}
