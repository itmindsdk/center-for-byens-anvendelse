﻿#region Head
// <copyright file="Team.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Collections.Generic;

namespace Core.DomainModel
{
    public class Team : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<ProjectLeader> ProjectLeaders { get; set; } = new HashSet<ProjectLeader>();
    }
}
