﻿#region Head
// <copyright file="Project.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace Core.DomainModel
{
    /// <summary>
    /// Represents a project.
    /// </summary>
    public class Project : Logger, IEntity
    {
        public int Id { get; set; }
        public int? TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public DateTime ExpectedStartDate { get; set; }
        public DateTime ExpectedEndDate { get; set; }
        public DateTime OneYearReview { get; set; }
        public DateTime FiveYearReview { get; set; }
        public string EDoc { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int? AdvisorId { get; set; }
        public virtual Advisor Advisor { get; set; }
        public int? StreetTypeId { get; set; }
        public virtual StreetType StreetType { get; set; }
        public virtual ProjectLeader ProjectLeader { get; set; }
        public int? ProjectLeaderId { get; set; }
        public string CouncilName { get; set; }
        public int? ProjectCategoryId { get; set; }
        public virtual ProjectCategory ProjectCategory { get; set; }
        public DbGeometry AddressLocation { get; set; }
        public int AddressLocationSRID { get; set; }
        public string AddressStreetName { get; set; }
        public string AddressNumber { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressCity { get; set; }
        public bool TemporaryAddress { get; set; }
        public string TemporaryAddressName { get; set; }
        public int? LocalCommunityId { get; set; }
        public virtual LocalCommunity LocalCommunity { get; set; }
        public int Budget { get; set; }
        public int? ConstructionProgramId { get; set; }
        public virtual ConstructionProgram ConstructionProgram { get; set; }
        public int? CouncilId { get; set; }
        public virtual Council Council { get; set; }
        public virtual ICollection<ProjectDocument> Documents { get; set; }
        public int? CouncilPriorityId { get; set; }
        public virtual CouncilPriority CouncilPriority { get; set; }
        public int? MunicipalityPriorityId { get; set; }
		public virtual MunicipalityPriority MunicipalityPriority { get; set; }
        public int ProjectStatusId { get; set; }
        public virtual ProjectStatus ProjectStatus { get; set; }
        public int PlanningId { get; set; }
        public virtual Planning Planning { get; set; }
        public int? EntrepreneurId { get; set; }
        public virtual Entrepreneur Entrepreneur { get; set; }
        public string Log { get; set; }
        public int? RequestorId { get; set; }
        public virtual Requestor Requestor { get; set; }
        public int? InconvenienceId { get; set; }
        public virtual Inconvenience Inconvenience { get; set; }
        public int? TrafficInconvenienceId { get; set; }
        public virtual TrafficInconvenience TrafficInconvenience { get; set; }

        public Project()
        {
            ExpectedStartDate = DateTime.Now;
            ExpectedEndDate = DateTime.Now;
            OneYearReview = DateTime.Now;
            OneYearReview.AddYears(1);
            FiveYearReview = DateTime.Now;
            FiveYearReview.AddYears(5);
        }
    }
}
