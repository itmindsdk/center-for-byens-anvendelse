﻿namespace Core.DomainModel
{
    /// <summary>
    /// Defines a primary key identifier.
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}
