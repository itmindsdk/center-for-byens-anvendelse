﻿namespace Core.DomainModel
{
    /// <summary>
    /// Represents the internal status types used by the application logic, not stored in the database.
    /// </summary>
    public enum ProjectStatusType
    {
        Awaiting,
        Ongoing,
        Accepted,
        Rejected 
    }
}
