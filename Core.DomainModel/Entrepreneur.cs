﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DomainModel
{
    public class Entrepreneur : CbaListProperties, IEntity
    {
        public int Id { get; set; }
        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }
        public string ResponsiblePersonName { get; set; }
        public string ResponsiblePersonPhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
    }
}
