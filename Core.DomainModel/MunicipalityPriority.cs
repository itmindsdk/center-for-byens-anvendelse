﻿#region Head
// <copyright file="MunicipalityPriority.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
# endregion
namespace Core.DomainModel
{
	/// <summary>
	/// Represents a priority for a <see cref="Project"/>.
	/// </summary>
	public class MunicipalityPriority : IEntity
	{
		public int Id { get; set; }
		public string Priority { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsActive { get; set; }
	}
}
