#region Head
// <copyright file="ConstructionProgram.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainModel
{
    /// <summary>
    /// Represents a year in which the <see cref="Project"/> is involved. (Anlægsprogram)
    /// </summary>
    public class ConstructionProgram : CbaListProperties
    {
        public int Id { get; set; }
        public int YearOfProjectInvolvement{ get; set; }
        public bool IsDeleted { get; set; }
    }
}
