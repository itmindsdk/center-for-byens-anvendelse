﻿#region Head
// <copyright file="Meeting.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;

namespace Core.DomainModel
{
    /// <summary>
    /// Represents a meeting. (færdselsmøde)
    /// </summary>
    public class Meeting : Logger, IEntity
    {
        public Meeting()
        {
            MeetingAgendaPoints = new HashSet<MeetingAgendaPoint>();
            Date = DateTime.Now.ToString();
        }

        public int Id { get; set; }
        public virtual ICollection<MeetingAgendaPoint> MeetingAgendaPoints { get; set; }
        public string Date { get; set; }
        public string Location { get; set; }
        public bool IsSummaryLocked { get; set; }
        public string EDocsLink { get; set; }
        public DateTime? SummarySendDate { get; set; }
        public string Summary { get; set; }
        public string InvitedPeople { get; set; }
        public Person Reviewer { get; set; }
        public DateTime? AcceptedOn { get; set; }
        public string NextMeeting { get; set; }
        public virtual ICollection<AgendaPoint> PreviousAgendaPoints { get; set; }
    }
}
