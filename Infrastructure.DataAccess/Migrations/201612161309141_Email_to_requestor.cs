namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Email_to_requestor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requestors", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requestors", "Email");
        }
    }
}
