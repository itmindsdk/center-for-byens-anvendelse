namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddressIdRemoved : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AgendaPoints", "AddressZipCode", c => c.String());
            AddColumn("dbo.AgendaPoints", "AddressCity", c => c.String());
            AddColumn("dbo.Projects", "AddressZipCode", c => c.String());
            AddColumn("dbo.Projects", "AddressCity", c => c.String());
            DropColumn("dbo.AgendaPoints", "AddressId");
            DropColumn("dbo.Projects", "AddressId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "AddressId", c => c.Guid(nullable: false));
            AddColumn("dbo.AgendaPoints", "AddressId", c => c.Guid(nullable: false));
            DropColumn("dbo.Projects", "AddressCity");
            DropColumn("dbo.Projects", "AddressZipCode");
            DropColumn("dbo.AgendaPoints", "AddressCity");
            DropColumn("dbo.AgendaPoints", "AddressZipCode");
        }
    }
}
