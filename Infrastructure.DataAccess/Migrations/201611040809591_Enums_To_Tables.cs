namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Enums_To_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgendaPointStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Status = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            // Insert keys required for conversion of old data.
            Sql("INSERT INTO dbo.AgendaPointStatus ( Id, Status, IsDeleted, IsActive) VALUES (0, 'Under udarbejdelse', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.AgendaPointStatus ( Id, Status, IsDeleted, IsActive) VALUES (1, 'Klar til dagsorden', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.AgendaPointStatus ( Id, Status, IsDeleted, IsActive) VALUES (2, 'Ikke godkendt', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.AgendaPointStatus ( Id, Status, IsDeleted, IsActive) VALUES (3, 'Godkendt', 'FALSE', 'TRUE')");

            CreateTable(
                "dbo.ProjectStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Status = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            // Insert keys required for conversion of old data.
            Sql("INSERT INTO dbo.ProjectStatus ( Id, Status, IsDeleted, IsActive) VALUES (0, '�nsker', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.ProjectStatus ( Id, Status, IsDeleted, IsActive) VALUES (1, 'Under udf�relse', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.ProjectStatus ( Id, Status, IsDeleted, IsActive) VALUES (2, 'Udf�rt', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.ProjectStatus ( Id, Status, IsDeleted, IsActive) VALUES (3, 'Udg�et', 'FALSE', 'TRUE')");

            CreateTable(
                "dbo.StreetTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Type = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            // Insert keys required for conversion of old data.
            Sql("INSERT INTO dbo.StreetTypes ( Id, Type, IsDeleted, IsActive) VALUES (0, 'Offentlig vej', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.StreetTypes ( Id, Type, IsDeleted, IsActive) VALUES (1, 'Privat vej', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.StreetTypes ( Id, Type, IsDeleted, IsActive) VALUES (2, 'Privat/f�lles vej', 'FALSE', 'TRUE')");
            Sql("INSERT INTO dbo.StreetTypes ( Id, Type, IsDeleted, IsActive) VALUES (3, 'Privat vej/privat omr�de', 'FALSE', 'TRUE')");

            AddColumn("dbo.AgendaPoints", "StreetTypeId", c => c.Int());
            AddColumn("dbo.AgendaPoints", "AgendaPointStatusId", c => c.Int());
            AddColumn("dbo.Projects", "StreetTypeId", c => c.Int());
            AddColumn("dbo.Projects", "ProjectStatusId", c => c.Int());

            // Copy project status data from the old enum column to the new status column. (reordering of enum values)
            Sql("UPDATE dbo.Projects SET ProjectStatusId = 0 WHERE Status = 0");
            Sql("UPDATE dbo.Projects SET ProjectStatusId = 1 WHERE Status = 3");
            Sql("UPDATE dbo.Projects SET ProjectStatusId = 2 WHERE Status = 1");
            Sql("UPDATE dbo.Projects SET ProjectStatusId = 3 WHERE Status = 2");

            // Copy agenda point status data from the old enum column to the new status column. (reordering of enum values)
            Sql("UPDATE dbo.AgendaPoints SET AgendaPointStatusId = 0 WHERE Status = 0");
            Sql("UPDATE dbo.AgendaPoints SET AgendaPointStatusId = 1 WHERE Status = 1");
            Sql("UPDATE dbo.AgendaPoints SET AgendaPointStatusId = 2 WHERE Status = 3");
            Sql("UPDATE dbo.AgendaPoints SET AgendaPointStatusId = 3 WHERE Status = 2");

            // Migrate street type data to new columns.
            Sql("UPDATE dbo.AgendaPoints SET StreetTypeId = 0 WHERE StreetType = 1");
            Sql("UPDATE dbo.AgendaPoints SET StreetTypeId = 1 WHERE StreetType = 0");
            Sql("UPDATE dbo.AgendaPoints SET StreetTypeId = 2 WHERE StreetType = 2");
            Sql("UPDATE dbo.AgendaPoints SET StreetTypeId = 3 WHERE StreetType = 3");
            Sql("UPDATE dbo.Projects SET StreetTypeId = 0 WHERE StreetType = 1");
            Sql("UPDATE dbo.Projects SET StreetTypeId = 1 WHERE StreetType = 0");
            Sql("UPDATE dbo.Projects SET StreetTypeId = 2 WHERE StreetType = 2");
            Sql("UPDATE dbo.Projects SET StreetTypeId = 3 WHERE StreetType = 3");

            CreateIndex("dbo.AgendaPoints", "StreetTypeId");
            CreateIndex("dbo.AgendaPoints", "AgendaPointStatusId");
            CreateIndex("dbo.Projects", "StreetTypeId");
            CreateIndex("dbo.Projects", "ProjectStatusId");
            AddForeignKey("dbo.AgendaPoints", "AgendaPointStatusId", "dbo.AgendaPointStatus", "Id");
            AddForeignKey("dbo.Projects", "ProjectStatusId", "dbo.ProjectStatus", "Id");
            AddForeignKey("dbo.Projects", "StreetTypeId", "dbo.StreetTypes", "Id");
            AddForeignKey("dbo.AgendaPoints", "StreetTypeId", "dbo.StreetTypes", "Id");
            DropColumn("dbo.AgendaPoints", "StreetType");
            DropColumn("dbo.AgendaPoints", "Status");
            DropColumn("dbo.Projects", "StreetType");
            DropColumn("dbo.Projects", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "Status", c => c.Int());
            AddColumn("dbo.Projects", "StreetType", c => c.Int());
            AddColumn("dbo.AgendaPoints", "Status", c => c.Int());
            AddColumn("dbo.AgendaPoints", "StreetType", c => c.Int());

            // Copy project status data from the status column to the old enum column. (reordering of enum values)
            Sql("UPDATE dbo.Projects SET Status = 0 WHERE ProjectStatusId = 0");
            Sql("UPDATE dbo.Projects SET Status = 3 WHERE ProjectStatusId = 1");
            Sql("UPDATE dbo.Projects SET Status = 1 WHERE ProjectStatusId = 2");
            Sql("UPDATE dbo.Projects SET Status = 2 WHERE ProjectStatusId = 3");

            // Copy agenda point status data from the status column to the old enum column. (reordering of enum values)
            Sql("UPDATE dbo.AgendaPoints SET Status = 0 WHERE AgendaPointStatusId = 0");
            Sql("UPDATE dbo.AgendaPoints SET Status = 1 WHERE AgendaPointStatusId = 1");
            Sql("UPDATE dbo.AgendaPoints SET Status = 2 WHERE AgendaPointStatusId = 3");
            Sql("UPDATE dbo.AgendaPoints SET Status = 3 WHERE AgendaPointStatusId = 2");

            // Migrate street type data to new columns.
            Sql("UPDATE dbo.AgendaPoints SET StreetType = 0 WHERE StreetTypeId = 1");
            Sql("UPDATE dbo.AgendaPoints SET StreetType = 1 WHERE StreetTypeId = 0");
            Sql("UPDATE dbo.AgendaPoints SET StreetType = 2 WHERE StreetTypeId = 2");
            Sql("UPDATE dbo.AgendaPoints SET StreetType = 3 WHERE StreetTypeId = 3");
            Sql("UPDATE dbo.Projects SET StreetType = 0 WHERE StreetTypeId = 1");
            Sql("UPDATE dbo.Projects SET StreetType = 1 WHERE StreetTypeId = 0");
            Sql("UPDATE dbo.Projects SET StreetType = 2 WHERE StreetTypeId = 2");
            Sql("UPDATE dbo.Projects SET StreetType = 3 WHERE StreetTypeId = 3");

            AlterColumn("dbo.Projects", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.AgendaPoints", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.Projects", "StreetType", c => c.Int(nullable: false));
            AlterColumn("dbo.AgendaPoints", "StreetType", c => c.Int(nullable: false));
            DropForeignKey("dbo.AgendaPoints", "StreetTypeId", "dbo.StreetTypes");
            DropForeignKey("dbo.Projects", "StreetTypeId", "dbo.StreetTypes");
            DropForeignKey("dbo.Projects", "ProjectStatusId", "dbo.ProjectStatus");
            DropForeignKey("dbo.AgendaPoints", "AgendaPointStatusId", "dbo.AgendaPointStatus");
            DropIndex("dbo.Projects", new[] { "ProjectStatusId" });
            DropIndex("dbo.Projects", new[] { "StreetTypeId" });
            DropIndex("dbo.AgendaPoints", new[] { "AgendaPointStatusId" });
            DropIndex("dbo.AgendaPoints", new[] { "StreetTypeId" });
            DropColumn("dbo.Projects", "ProjectStatusId");
            DropColumn("dbo.Projects", "StreetTypeId");
            DropColumn("dbo.AgendaPoints", "AgendaPointStatusId");
            DropColumn("dbo.AgendaPoints", "StreetTypeId");
            DropTable("dbo.StreetTypes");
            DropTable("dbo.ProjectStatus");
            DropTable("dbo.AgendaPointStatus");
        }
    }
}
