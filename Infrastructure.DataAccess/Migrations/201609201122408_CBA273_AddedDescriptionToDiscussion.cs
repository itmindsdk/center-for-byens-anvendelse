namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CBA273_AddedDescriptionToDiscussion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PartnerDiscussions", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PartnerDiscussions", "Description");
        }
    }
}
