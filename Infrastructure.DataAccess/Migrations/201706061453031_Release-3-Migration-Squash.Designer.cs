// <auto-generated />
namespace Infrastructure.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Release3MigrationSquash : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Release3MigrationSquash));
        
        string IMigrationMetadata.Id
        {
            get { return "201706061453031_Release-3-Migration-Squash"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
