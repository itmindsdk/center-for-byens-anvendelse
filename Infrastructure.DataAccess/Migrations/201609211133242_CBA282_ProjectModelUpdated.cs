namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CBA282_ProjectModelUpdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "EntrepreneurId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "EntrepreneurId");
        }
    }
}
