// <auto-generated />
namespace Infrastructure.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CBA288_CompanyType : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CBA288_CompanyType));
        
        string IMigrationMetadata.Id
        {
            get { return "201609290755313_CBA288_CompanyType"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
