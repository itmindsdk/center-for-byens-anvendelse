namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Log : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Log", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "Log");
        }
    }
}
