namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CBA282_ProjectModelUpdated1 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Projects", "EntrepreneurId");
            AddForeignKey("dbo.Projects", "EntrepreneurId", "dbo.Entrepreneurs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "EntrepreneurId", "dbo.Entrepreneurs");
            DropIndex("dbo.Projects", new[] { "EntrepreneurId" });
        }
    }
}
