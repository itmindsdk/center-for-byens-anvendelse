#region Head
// <copyright file="Configuration.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Core.DomainModel;

namespace Infrastructure.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    [SuppressMessage("Microsoft.Performance", "CA1812", Justification = "Internal ASP.NET MVC class")]
    internal sealed class Configuration : DbMigrationsConfiguration<CbaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CbaContext context)
        {
            var appSettings = new List<AppSetting>
            {
                new AppSetting {Key = "appName", Value = "Center for Byens Anvendelse"},
                new AppSetting {Key = "AgendaPointZeroDescription", Value = "Godkendt uden bemærkninger"},
                new AppSetting {Key = "UploadPath", Value = @"D:\CBA_3DB"},
                new AppSetting {Key = "adAdminRoles", Value = "GG-Rolle-MTM-POFCBA-Admin"},
                new AppSetting {Key = "adUserRoles", Value = "GG-Rolle-MTM-POFCBA-RWUser"},
                new AppSetting {Key = "StatusExplanationFileName", Value = "statusbeskrivelse.txt"},
                new AppSetting {Key = "QuickCreateProject", Value = "Opret projekt.pdf"},
                new AppSetting {Key = "QuickAddAppendixToProject", Value = "Tilføj et bilag til eksisterende.pdf"},
                new AppSetting {Key = "QuickCreateAgendaPoint", Value = "Opret færdselsmødepunkt.pdf"},
                new AppSetting {Key = "QuickAddAppendixToAgendaPoint", Value = "Tilføj et bilag til eksisterende.pdf"},
                new AppSetting {Key = "QuickCreateDrawing", Value = "IngenVejledning.pdf"},
                new AppSetting {Key = "QuickAddAppendixToDrawing", Value = "IngenVejledning.pdf"}
            };
            foreach (var appSetting in appSettings)
            {
                context.AppSettings.AddIfNotExists(appSetting, x => x.Key == appSetting.Key);
            }

            var inconveniences = new List<Inconvenience>
            {
                new Inconvenience {Id = 1, Description = "Ikke relevant", IsActive = true},
                new Inconvenience {Id = 2, Description = "Spærret vej-/e", IsActive = true},
                new Inconvenience {Id = 3, Description = "Indsnævrede spor", IsActive = true},
                new Inconvenience {Id = 4, Description = "Hastighedsbegrænsning", IsActive = true},
                new Inconvenience {Id = 5, Description = "Omkørsel", IsActive = true},
                new Inconvenience {Id = 6, Description = "Midlertidig", IsActive = true},
                new Inconvenience {Id = 7, Description = "Påvirkning af kollektiv trafik", IsActive = true},
                new Inconvenience {Id = 8, Description = "Reduceret frihøjde", IsActive = true}
            };
            foreach (var inconvenience in inconveniences)
            {
                context.Inconveniences.AddIfNotExists(inconvenience, x => x.Id == inconvenience.Id);
            }

            var traffuiInconveniences = new List<TrafficInconvenience>
            {
                new TrafficInconvenience { Id = 1, Description = "Ikke relevant", IsActive = true },
                new TrafficInconvenience { Id = 2, Description = "Strategisk vejnet", IsActive = true },
                new TrafficInconvenience { Id = 3, Description = "Kritisk vejnet", IsActive = true },
                new TrafficInconvenience { Id = 4, Description = "Lokalveje", IsActive = true }
            };
            foreach (var trafficInconvenience in traffuiInconveniences)
            {
                context.TrafficInconveniences.AddIfNotExists(trafficInconvenience, x => x.Id == trafficInconvenience.Id);
            }

            context.SaveChanges();
        }
    }
}