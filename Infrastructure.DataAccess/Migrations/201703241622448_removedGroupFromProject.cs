namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RemovedGroupFromProject : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Projects", "GroupId", "dbo.Groups");
            DropIndex("dbo.Projects", new[] { "GroupId" });
            DropColumn("dbo.Projects", "GroupId");
        }

        public override void Down()
        {
            AddColumn("dbo.Projects", "GroupId", c => c.Int());
            CreateIndex("dbo.Projects", "GroupId");
            AddForeignKey("dbo.Projects", "GroupId", "dbo.Groups", "Id");
        }
    }
}
