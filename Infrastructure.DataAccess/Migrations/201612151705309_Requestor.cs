namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Requestor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Requestors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Projects", "RequestorId", c => c.Int());
            CreateIndex("dbo.Projects", "RequestorId");
            AddForeignKey("dbo.Projects", "RequestorId", "dbo.Requestors", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "RequestorId", "dbo.Requestors");
            DropIndex("dbo.Projects", new[] { "RequestorId" });
            DropColumn("dbo.Projects", "RequestorId");
            DropTable("dbo.Requestors");
        }
    }
}
