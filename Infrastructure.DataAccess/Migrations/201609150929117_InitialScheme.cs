namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialScheme : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advisors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        ResponsiblePersonName = c.String(),
                        ResponsiblePersonPhoneNumber = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AgendaPoints",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SP_GEOMETRY = c.Geometry(),
                        AddressId = c.Guid(nullable: false),
                        AddressStreetName = c.String(),
                        AddressNumber = c.String(),
                        ProjectLeaderId = c.Int(),
                        TemporaryAddress = c.Boolean(nullable: false),
                        StreetType = c.Int(nullable: false),
                        ProjectId = c.Int(),
                        Description = c.String(),
                        Conclusion = c.String(),
                        Agreement = c.String(),
                        TopicId = c.Int(),
                        Status = c.Int(nullable: false),
                        Area = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                        CbaDiscussion_Id = c.Int(),
                        CbmDiscussion_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PartnerDiscussions", t => t.CbaDiscussion_Id)
                .ForeignKey("dbo.PartnerDiscussions", t => t.CbmDiscussion_Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .ForeignKey("dbo.ProjectLeaders", t => t.ProjectLeaderId)
                .ForeignKey("dbo.Topics", t => t.TopicId)
                .Index(t => t.ProjectLeaderId)
                .Index(t => t.ProjectId)
                .Index(t => t.TopicId)
                .Index(t => t.CbaDiscussion_Id)
                .Index(t => t.CbmDiscussion_Id);
            
            CreateTable(
                "dbo.PartnerDiscussions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Discussed = c.Boolean(nullable: false),
                        DiscussedWith = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Guid = c.Guid(nullable: false),
                        FileName = c.String(),
                        FileSize = c.Single(nullable: false),
                        Comment = c.String(),
                        AgendaPointId = c.Int(),
                        ProjectId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Guid)
                .ForeignKey("dbo.AgendaPoints", t => t.AgendaPointId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .Index(t => t.AgendaPointId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.MeetingAgendaPoints",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MeetingId = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AgendaPoints", t => t.Id)
                .ForeignKey("dbo.Meetings", t => t.MeetingId, cascadeDelete: true)
                .Index(t => new { t.Id, t.MeetingId, t.Number }, unique: true, name: "IX_UniqueMeetingAgendaPoint");
            
            CreateTable(
                "dbo.Meetings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.String(),
                        Location = c.String(),
                        IsSummaryLocked = c.Boolean(nullable: false),
                        EDocsLink = c.String(),
                        SummarySendDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Summary = c.String(),
                        InvitedPeople = c.String(),
                        AcceptedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        NextMeeting = c.String(),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                        Reviewer_Id = c.Int(),
                        AgendaPoint_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.People", t => t.Reviewer_Id)
                .ForeignKey("dbo.AgendaPoints", t => t.AgendaPoint_Id)
                .Index(t => t.Reviewer_Id)
                .Index(t => t.AgendaPoint_Id);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(),
                        ExpectedStartDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExpectedEndDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        OneYearReview = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        FiveYearReview = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Description = c.String(),
                        Note = c.String(),
                        EDoc = c.String(),
                        AdvisorId = c.Int(),
                        StreetType = c.Int(nullable: false),
                        ProjectLeaderId = c.Int(),
                        CouncilPriority = c.Int(nullable: false),
                        MunicipalityPriority = c.Int(nullable: false),
                        CouncilName = c.String(),
                        ProjectCategoryId = c.Int(),
                        SP_GEOMETRY = c.Geometry(),
                        AddressId = c.Guid(nullable: false),
                        AddressStreetName = c.String(),
                        AddressNumber = c.String(),
                        TemporaryAddress = c.Boolean(nullable: false),
                        LocalCommunityId = c.Int(),
                        Budget = c.Int(nullable: false),
                        ConstructionProgramId = c.Int(),
                        GroupId = c.Int(),
                        CouncilId = c.Int(),
                        Status = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advisors", t => t.AdvisorId)
                .ForeignKey("dbo.ConstructionPrograms", t => t.ConstructionProgramId)
                .ForeignKey("dbo.Councils", t => t.CouncilId)
                .ForeignKey("dbo.Groups", t => t.GroupId)
                .ForeignKey("dbo.LocalCommunities", t => t.LocalCommunityId)
                .ForeignKey("dbo.ProjectCategories", t => t.ProjectCategoryId)
                .ForeignKey("dbo.ProjectLeaders", t => t.ProjectLeaderId)
                .ForeignKey("dbo.Topics", t => t.TopicId)
                .Index(t => t.TopicId)
                .Index(t => t.AdvisorId)
                .Index(t => t.ProjectLeaderId)
                .Index(t => t.ProjectCategoryId)
                .Index(t => t.LocalCommunityId)
                .Index(t => t.ConstructionProgramId)
                .Index(t => t.GroupId)
                .Index(t => t.CouncilId);
            
            CreateTable(
                "dbo.ConstructionPrograms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        YearOfProjectInvolvement = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Councils",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        GroupId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.GroupId);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LocalCommunities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProjectLeaders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Initials = c.String(),
                        Email = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        TeamId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.TeamId);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DefaultPhrases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Phrase = c.String(),
                        Type = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InvitedPersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgendaPoints", "TopicId", "dbo.Topics");
            DropForeignKey("dbo.AgendaPoints", "ProjectLeaderId", "dbo.ProjectLeaders");
            DropForeignKey("dbo.AgendaPoints", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "TopicId", "dbo.Topics");
            DropForeignKey("dbo.Projects", "ProjectLeaderId", "dbo.ProjectLeaders");
            DropForeignKey("dbo.ProjectLeaders", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.Projects", "ProjectCategoryId", "dbo.ProjectCategories");
            DropForeignKey("dbo.Projects", "LocalCommunityId", "dbo.LocalCommunities");
            DropForeignKey("dbo.Projects", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.Documents", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "CouncilId", "dbo.Councils");
            DropForeignKey("dbo.Councils", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.Projects", "ConstructionProgramId", "dbo.ConstructionPrograms");
            DropForeignKey("dbo.Projects", "AdvisorId", "dbo.Advisors");
            DropForeignKey("dbo.Meetings", "AgendaPoint_Id", "dbo.AgendaPoints");
            DropForeignKey("dbo.Meetings", "Reviewer_Id", "dbo.People");
            DropForeignKey("dbo.MeetingAgendaPoints", "MeetingId", "dbo.Meetings");
            DropForeignKey("dbo.MeetingAgendaPoints", "Id", "dbo.AgendaPoints");
            DropForeignKey("dbo.Documents", "AgendaPointId", "dbo.AgendaPoints");
            DropForeignKey("dbo.AgendaPoints", "CbmDiscussion_Id", "dbo.PartnerDiscussions");
            DropForeignKey("dbo.AgendaPoints", "CbaDiscussion_Id", "dbo.PartnerDiscussions");
            DropForeignKey("dbo.Advisors", "CompanyId", "dbo.Companies");
            DropIndex("dbo.ProjectLeaders", new[] { "TeamId" });
            DropIndex("dbo.Councils", new[] { "GroupId" });
            DropIndex("dbo.Projects", new[] { "CouncilId" });
            DropIndex("dbo.Projects", new[] { "GroupId" });
            DropIndex("dbo.Projects", new[] { "ConstructionProgramId" });
            DropIndex("dbo.Projects", new[] { "LocalCommunityId" });
            DropIndex("dbo.Projects", new[] { "ProjectCategoryId" });
            DropIndex("dbo.Projects", new[] { "ProjectLeaderId" });
            DropIndex("dbo.Projects", new[] { "AdvisorId" });
            DropIndex("dbo.Projects", new[] { "TopicId" });
            DropIndex("dbo.Meetings", new[] { "AgendaPoint_Id" });
            DropIndex("dbo.Meetings", new[] { "Reviewer_Id" });
            DropIndex("dbo.MeetingAgendaPoints", "IX_UniqueMeetingAgendaPoint");
            DropIndex("dbo.Documents", new[] { "ProjectId" });
            DropIndex("dbo.Documents", new[] { "AgendaPointId" });
            DropIndex("dbo.AgendaPoints", new[] { "CbmDiscussion_Id" });
            DropIndex("dbo.AgendaPoints", new[] { "CbaDiscussion_Id" });
            DropIndex("dbo.AgendaPoints", new[] { "TopicId" });
            DropIndex("dbo.AgendaPoints", new[] { "ProjectId" });
            DropIndex("dbo.AgendaPoints", new[] { "ProjectLeaderId" });
            DropIndex("dbo.Advisors", new[] { "CompanyId" });
            DropTable("dbo.InvitedPersons");
            DropTable("dbo.DefaultPhrases");
            DropTable("dbo.Topics");
            DropTable("dbo.Teams");
            DropTable("dbo.ProjectLeaders");
            DropTable("dbo.ProjectCategories");
            DropTable("dbo.LocalCommunities");
            DropTable("dbo.Groups");
            DropTable("dbo.Councils");
            DropTable("dbo.ConstructionPrograms");
            DropTable("dbo.Projects");
            DropTable("dbo.People");
            DropTable("dbo.Meetings");
            DropTable("dbo.MeetingAgendaPoints");
            DropTable("dbo.Documents");
            DropTable("dbo.PartnerDiscussions");
            DropTable("dbo.AgendaPoints");
            DropTable("dbo.Companies");
            DropTable("dbo.Advisors");
        }
    }
}
