namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CBA284_AddedTemporaryAddressName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AgendaPoints", "TemporaryAddressName", c => c.String());
            AddColumn("dbo.Projects", "TemporaryAddressName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "TemporaryAddressName");
            DropColumn("dbo.AgendaPoints", "TemporaryAddressName");
        }
    }
}
