namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedNextMeetingToAgendaPoints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AgendaPoints", "NextMeetingId", c => c.Int());
            CreateIndex("dbo.AgendaPoints", "NextMeetingId");
            AddForeignKey("dbo.AgendaPoints", "NextMeetingId", "dbo.NextMeetings", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgendaPoints", "NextMeetingId", "dbo.NextMeetings");
            DropIndex("dbo.AgendaPoints", new[] { "NextMeetingId" });
            DropColumn("dbo.AgendaPoints", "NextMeetingId");
        }
    }
}
