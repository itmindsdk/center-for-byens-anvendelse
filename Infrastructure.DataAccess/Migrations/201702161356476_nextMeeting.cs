namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nextMeeting : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AgendaPoints", "NextMeetingId", "dbo.NextMeetings");
            DropIndex("dbo.AgendaPoints", new[] { "NextMeetingId" });
            DropColumn("dbo.AgendaPoints", "NextMeetingId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AgendaPoints", "NextMeetingId", c => c.Int());
            CreateIndex("dbo.AgendaPoints", "NextMeetingId");
            AddForeignKey("dbo.AgendaPoints", "NextMeetingId", "dbo.NextMeetings", "Id");
        }
    }
}
