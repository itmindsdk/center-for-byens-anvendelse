namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Release3MigrationSquash : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Inconveniences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.TrafficInconveniences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.AppSettings",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 128),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Key);

            AddColumn("dbo.Projects", "InconvenienceId", c => c.Int());
            AddColumn("dbo.Projects", "TrafficInconvenienceId", c => c.Int());
            CreateIndex("dbo.Projects", "InconvenienceId");
            CreateIndex("dbo.Projects", "TrafficInconvenienceId");
            AddForeignKey("dbo.Projects", "InconvenienceId", "dbo.Inconveniences", "Id");
            AddForeignKey("dbo.Projects", "TrafficInconvenienceId", "dbo.TrafficInconveniences", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Projects", "TrafficInconvenienceId", "dbo.TrafficInconveniences");
            DropForeignKey("dbo.Projects", "InconvenienceId", "dbo.Inconveniences");
            DropIndex("dbo.Projects", new[] { "TrafficInconvenienceId" });
            DropIndex("dbo.Projects", new[] { "InconvenienceId" });
            DropColumn("dbo.Projects", "TrafficInconvenienceId");
            DropColumn("dbo.Projects", "InconvenienceId");
            DropTable("dbo.AppSettings");
            DropTable("dbo.TrafficInconveniences");
            DropTable("dbo.Inconveniences");
        }
    }
}
