namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Projects", "ProjectStatusId", "dbo.ProjectStatus");
            DropIndex("dbo.Projects", new[] { "ProjectStatusId" });
            CreateTable(
                "dbo.CouncilPriorities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Priority = c.String(),
                        IsDeleted = c.Boolean(nullable: false, defaultValue: false),
                        IsActive = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.Id);

            Sql("INSERT INTO dbo.CouncilPriorities ( [Priority] ) VALUES ('Lav')");
            Sql("INSERT INTO dbo.CouncilPriorities ( [Priority] ) VALUES ('Mellem')");
            Sql("INSERT INTO dbo.CouncilPriorities ( [Priority] ) VALUES ('H�j')");

            CreateTable(
                "dbo.MunicipalityPriorities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Priority = c.String(),
                        IsDeleted = c.Boolean(nullable: false, defaultValue: false),
                        IsActive = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.Id);

            Sql("INSERT INTO dbo.MunicipalityPriorities ( [Priority] ) VALUES ('Lav')");
            Sql("INSERT INTO dbo.MunicipalityPriorities ( [Priority] ) VALUES ('Mellem')");
            Sql("INSERT INTO dbo.MunicipalityPriorities ( [Priority] ) VALUES ('H�j')");

            CreateTable(
                "dbo.Plannings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Plan = c.String(),
                        IsDeleted = c.Boolean(nullable: false, defaultValue: false),
                        IsActive = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.Id);

            Sql("INSERT INTO dbo.Plannings ( [Plan] ) VALUES ('Ikke relevant')");
            Sql("INSERT INTO dbo.Plannings ( [Plan] ) VALUES ('Klar til koordinering')");
            Sql("INSERT INTO dbo.Plannings ( [Plan] ) VALUES ('Godkendt')");
            Sql("INSERT INTO dbo.Plannings ( [Plan] ) VALUES ('Ikke godkendt')");

            //AddColumn("dbo.Projects", "EDoc", c => c.String());
            AddColumn("dbo.Projects", "CouncilPriorityId", c => c.Int());
            AddColumn("dbo.Projects", "MunicipalityPriorityId", c => c.Int());
            AddColumn("dbo.Projects", "PlanningId", c => c.Int(nullable: false, defaultValue: 1));

            Sql("UPDATE dbo.Projects SET ProjectStatusId = 0 WHERE ProjectStatusId IS NULL");

            AlterColumn("dbo.Projects", "ProjectStatusId", c => c.Int(nullable: false, defaultValue: 0));
            CreateIndex("dbo.Projects", "CouncilPriorityId");
            CreateIndex("dbo.Projects", "MunicipalityPriorityId");
            CreateIndex("dbo.Projects", "ProjectStatusId");
            CreateIndex("dbo.Projects", "PlanningId");
            AddForeignKey("dbo.Projects", "CouncilPriorityId", "dbo.CouncilPriorities", "Id");
            AddForeignKey("dbo.Projects", "MunicipalityPriorityId", "dbo.MunicipalityPriorities", "Id");
            AddForeignKey("dbo.Projects", "PlanningId", "dbo.Plannings", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Projects", "ProjectStatusId", "dbo.ProjectStatus", "Id", cascadeDelete: true);
            
            Sql("UPDATE dbo.Projects SET CouncilPriorityId = 1");
            Sql("UPDATE dbo.Projects SET MunicipalityPriorityId = 1");
            
            DropColumn("dbo.Projects", "CouncilPriority");
            DropColumn("dbo.Projects", "MunicipalityPriority");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "MunicipalityPriority", c => c.Int(nullable: false));
            AddColumn("dbo.Projects", "CouncilPriority", c => c.Int(nullable: false));
            DropForeignKey("dbo.Projects", "ProjectStatusId", "dbo.ProjectStatus");
            DropForeignKey("dbo.Projects", "PlanningId", "dbo.Plannings");
            DropForeignKey("dbo.Projects", "MunicipalityPriorityId", "dbo.MunicipalityPriorities");
            DropForeignKey("dbo.Projects", "CouncilPriorityId", "dbo.CouncilPriorities");
            DropIndex("dbo.Projects", new[] { "PlanningId" });
            DropIndex("dbo.Projects", new[] { "ProjectStatusId" });
            DropIndex("dbo.Projects", new[] { "MunicipalityPriorityId" });
            DropIndex("dbo.Projects", new[] { "CouncilPriorityId" });
            AlterColumn("dbo.Projects", "ProjectStatusId", c => c.Int());

            Sql("UPDATE dbo.Projects SET CouncilPriority = 0");
            Sql("UPDATE dbo.Projects SET MunicipalityPriority = 0");

            DropColumn("dbo.Projects", "PlanningId");
            DropColumn("dbo.Projects", "MunicipalityPriorityId");
            DropColumn("dbo.Projects", "CouncilPriorityId");
            DropColumn("dbo.Projects", "EDoc");
            DropTable("dbo.Plannings");
            DropTable("dbo.MunicipalityPriorities");
            DropTable("dbo.CouncilPriorities");
            CreateIndex("dbo.Projects", "ProjectStatusId");
            AddForeignKey("dbo.Projects", "ProjectStatusId", "dbo.ProjectStatus", "Id");
        }
    }
}
