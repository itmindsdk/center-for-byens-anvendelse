namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CBA288_CompanyType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "Type", c => c.Int(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Type");
        }
    }
}
