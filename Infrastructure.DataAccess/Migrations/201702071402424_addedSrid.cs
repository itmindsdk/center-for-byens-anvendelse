namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedSrid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "AddressLocationSRID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "AddressLocationSRID");
        }
    }
}
