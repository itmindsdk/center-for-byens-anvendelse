namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AgendapointPreviousmeetingsmmRelationship : DbMigration
    {
        public override void Up()
        {
            // Creates the new many-many relationship table
            CreateTable(
                "dbo.AgendaPointPreviousMeetings",
                c => new
                {
                    AgendaPointId = c.Int(nullable: false),
                    MeetingId = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.AgendaPointId, t.MeetingId })
                .ForeignKey("dbo.AgendaPoints", t => t.AgendaPointId, cascadeDelete: true)
                .ForeignKey("dbo.Meetings", t => t.MeetingId, cascadeDelete: true)
                .Index(t => t.AgendaPointId)
                .Index(t => t.MeetingId);

            // Moves all entries in the 1-M relationship from agendapoints to meetings to the new m-m relationship
            Sql("INSERT INTO dbo.AgendaPointPreviousMeetings (AgendaPointId, MeetingId) SELECT AgendaPoint_Id, Id FROM dbo.Meetings WHERE AgendaPoint_ID IS NOT NULL");

            DropForeignKey("dbo.Meetings", "AgendaPoint_Id", "dbo.AgendaPoints");
            DropIndex("dbo.Meetings", new[] { "AgendaPoint_Id" });

            DropColumn("dbo.Meetings", "AgendaPoint_Id");
        }

        public override void Down()
        {
            AddColumn("dbo.Meetings", "AgendaPoint_Id", c => c.Int());
            DropForeignKey("dbo.AgendaPointPreviousMeetings", "MeetingId", "dbo.Meetings");
            DropForeignKey("dbo.AgendaPointPreviousMeetings", "AgendaPointId", "dbo.AgendaPoints");
            DropIndex("dbo.AgendaPointPreviousMeetings", new[] { "MeetingId" });
            DropIndex("dbo.AgendaPointPreviousMeetings", new[] { "AgendaPointId" });
            DropTable("dbo.AgendaPointPreviousMeetings");
            CreateIndex("dbo.Meetings", "AgendaPoint_Id");
            AddForeignKey("dbo.Meetings", "AgendaPoint_Id", "dbo.AgendaPoints", "Id");
        }
    }
}
