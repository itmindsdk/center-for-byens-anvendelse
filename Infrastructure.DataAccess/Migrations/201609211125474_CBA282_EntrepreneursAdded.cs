namespace Infrastructure.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CBA282_EntrepreneursAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Entrepreneurs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        ResponsiblePersonName = c.String(),
                        ResponsiblePersonPhoneNumber = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedOn = c.DateTime(precision: 7, storeType: "datetime2"),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entrepreneurs", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Entrepreneurs", new[] { "CompanyId" });
            DropTable("dbo.Entrepreneurs");
        }
    }
}
