﻿#region Head
// <copyright file="DawaRepository.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Linq;

namespace Infrastructure.Dawa
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Core.DomainModel;
    using Core.DomainServices;
    using Newtonsoft.Json;

    /// <summary>
    /// Repository for "Danmarks Adressers WebAPI"
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Abbreviation.")]
    [SuppressMessage("Microsoft.Naming", "CA1704", Justification = "Abbreviation.")]
    public class DawaRepository : IDawaRepository
    {
        /// <summary>
        /// Base URL for the DAWA WebAPI
        /// </summary>
        private const string BaseUrl = "http://dawa.aws.dk";

        /// <summary>
        /// <see cref="HttpClient"/> to access the WebAPI
        /// </summary>
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="DawaRepository"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public DawaRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.Timeout = new TimeSpan(0, 0, 10);
            _httpClient.BaseAddress = new Uri(BaseUrl);
        }

        /// <summary>
        /// Request a single Address from the repository
        /// </summary>
        /// <param name="id">GUID of address to request</param>
        /// <returns>Requested address</returns>
        public Address GetAddress(Guid id)
        {
            var url = "adresser/" + id.ToString();

            var response = _httpClient.GetAsync(url).Result;
            response.EnsureSuccessStatusCode();

            var responseBody = response.Content.ReadAsStringAsync().Result;
            var jsonSettings = new JsonSerializerSettings
            {
                ContractResolver = new DawaAddressResolver()
            };

            return JsonConvert.DeserializeObject<Address>(responseBody, jsonSettings);
        }

        /// <summary>
        /// Get adresses. See http://dawa.aws.dk/adressedok for options.
        /// </summary>
        /// <param name="query">Options: "q" for mixed search, "fuzzy" to enable fuzzy search.
        /// See http://dawa.aws.dk/adressedok for options.</param>
        /// <returns>List of found addresses</returns>
        /// <exception cref="HttpRequestException">Thrown when HTTP request times out.</exception>
        [SuppressMessage("Microsoft.Usage", "CA2234", Justification = "Not a Uri but an URL query.")]
        public IEnumerable<Address> GetAddresses(string query)
        {
            var url = "adresser/" + (query.Length == 0 ? String.Empty : "?") + query;

            var response = _httpClient.GetAsync(url).Result;
            response.EnsureSuccessStatusCode();

            var responseBody = response.Content.ReadAsStringAsync().Result;
            var jsonSettings = new JsonSerializerSettings
            {
                ContractResolver = new DawaAddressResolver()
            };

            var addresses = JsonConvert.DeserializeObject<IEnumerable<Address>>(responseBody, jsonSettings);

            return addresses;
        }

        /// <summary>
        /// Get adresses async. See http://dawa.aws.dk/adressedok for options.
        /// </summary>
        /// <param name="query">Options: "q" for mixed search, "fuzzy" to enable fuzzy search.
        /// See http://dawa.aws.dk/adressedok for options.</param>
        /// <returns>List of found addresses</returns>
        /// <exception cref="HttpRequestException">Thrown when HTTP request times out.</exception>
        public async Task<IEnumerable<Address>> GetAddressesAsync(string query)
        {
            var url = "adresser/" + (query.Length == 0 ? String.Empty : "?") + query;

            var response = await _httpClient.GetAsync(url);
            response.EnsureSuccessStatusCode();

            var responseBody = await response.Content.ReadAsStringAsync();
            var jsonSettings = new JsonSerializerSettings
            {
                ContractResolver = new DawaAddressResolver()
            };

            var addresses = JsonConvert.DeserializeObject<IEnumerable<Address>>(responseBody, jsonSettings);

            return addresses;
        }
    }
}
