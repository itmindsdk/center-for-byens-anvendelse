﻿#region Head
// <copyright file="DawaAddressResolver.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Infrastructure.Dawa
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Core.DomainModel;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    /// Resolve DAWA address result to <see cref="Address"/> class.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704", Justification = "Name.")]
    public class DawaAddressResolver : DefaultContractResolver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DawaAddressResolver"/> class.
        /// </summary>
        public DawaAddressResolver()
        {
            PropertyMappings = new Dictionary<string, string>
            {
                {
                    "Id", "id"
                },
                {
                    "AddressName", "adressebetegnelse"
                }
            };
        }

        /// <summary>
        /// Gets or sets property mapping.
        /// </summary>
        private Dictionary<string, string> PropertyMappings { get; set; }

        /// <summary>
        /// Resolve property name from string.
        /// </summary>
        /// <param name="propertyName">The property name to resolve.</param>
        /// <returns>The resolved name.</returns>
        protected override string ResolvePropertyName(string propertyName)
        {
            string resolvedName;
            var resolved = PropertyMappings.TryGetValue(propertyName, out resolvedName);
            return resolved ? resolvedName : base.ResolvePropertyName(propertyName);
        }
    }
}
