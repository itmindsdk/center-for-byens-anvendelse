ECHO OFF
CLS
					 
SET COVERAGE_FILTERS=+[Core.ApplicationServices]* -[Core.ApplicationServices]Core.ApplicationServices.ExpressionHelpers +[Core.DomainModel]* -[Core.DomainModel]ApplicationUser +[Core.DomainServices]* +[Infrastructure.DataAccess]* +[Infrastructure.Dawa]* +[Presentation.Web]* -[Presentation.Web]Presentation.Web.App_Start* -[Presentation.Web]Presentation.Web.MvcApplication  -[Presentation.Web]Presentation.Web.Startup -[Presentation.Web]Presentation.Web.Models.Search.ResultsViewModel -[Presentation.Web]Presentation.Web.Helpers.AdHelper -[Presentation.Web]Presentation.Web.Helpers.AppSettings -[Presentation.Web]Presentation.Web.Controllers.BaseController -[Presentation.Web]Presentation.Web.Models.CustomDateTime
SET TEST_ASSEMBLIES=Presentation.Web.UnitTests\bin\Debug\Presentation.Web.UnitTests.dll Presentation.Web.IntegrationTests\bin\Debug\Presentation.Web.IntegrationTests.dll Core.ApplicationServices.UnitTests\bin\Debug\Core.ApplicationServices.UnitTests.dll

packages\OpenCover.4.6.519\tools\OpenCover.Console.exe -target:packages\xunit.runner.console.2.1.0\tools\xunit.console.exe -targetargs:"%TEST_ASSEMBLIES% -noshadow -appveyor -nologo" -register:user -filter:"%COVERAGE_FILTERS%"
packages\ReportGenerator.2.4.4.0\tools\ReportGenerator.exe -reports:results.xml -targetdir:coverage

START .\coverage\index.htm
