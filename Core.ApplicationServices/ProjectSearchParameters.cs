#region Head
// <copyright file="ProjectSearchParameters.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;

namespace Core.ApplicationServices
{
    using System;
    using DomainModel;

    /// <summary>
    /// Search parameters for projects.
    /// </summary>
    public class ProjectSearchParameters
    {
        /// <summary>
        /// Gets or sets agenda point address
        /// </summary>
        public string Address { get; set; }

        public string TemporaryAddressName { get; set; }

        /// <summary>
        /// Gets or sets associated project id
        /// </summary>
        public int? ProjectId { get; set; }

        /// <summary>
        /// Gets or sets project status
        /// </summary>
        public int? ProjectStatusId { get; set; }

        /// <summary>
        /// Gets or sets associated council id
        /// </summary>
        public int? CouncilId { get; set; }

        /// <summary>
        /// Gets or sets associated group id
        /// </summary>
        public int? GroupId { get; set; }

        /// <summary>
        /// Gets or sets associated ProjectCategory id
        /// </summary>
        public int? ProjectCategoryId { get; set; }

        /// <summary>
        /// Gets or sets associated advisor company id
        /// </summary>
        public int? CompanyId { get; set; }

        /// <summary>
        /// Gets or sets associated entrepreneur company id
        /// </summary>
        public int? EntrepreneurCompanyId { get; set; }

        /// <summary>
        /// Gets or sets associated entrepreneur company id
        /// </summary>
        public int? RequestorId { get; set; }

        /// <summary>
        /// Gets or sets date interval beginning. Search is including this date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets date interval end. Search is including this date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the type of date search intended.
        /// </summary>
        public DateSearchType DateSearchType { get; set; }

        /// <summary>
        /// Gets or sets associated topic ids.
        /// </summary>
        public IEnumerable<int> TopicIds { get; set; }

        public int? LocalCommunityId { get; set; }
    }
}
