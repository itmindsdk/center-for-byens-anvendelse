﻿#region Head
// <copyright file="SearchService.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Linq.Expressions;

namespace Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DomainModel;
    using DomainServices;

    /// <summary>
    /// Search for entities in local repositories and the DAWA API
    /// </summary>
    public class SearchService : ISearchService
    {
        /// <summary>
        /// <see cref="Project"/> repository
        /// </summary>
        private readonly IGenericRepository<Project> _projectRepo;

        /// <summary>
        /// <see cref="AgendaPoint"/> repository
        /// </summary>
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchService"/> class.
        /// </summary>
        /// <param name="projectRepo">The <see cref="Project"/> repository.</param>
        /// <param name="agendaPointRepo">The <see cref="AgendaPoint"/> repository.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "dawa", Justification = "Abbreviation.")]
        public SearchService(IGenericRepository<Project> projectRepo, IGenericRepository<AgendaPoint> agendaPointRepo)
        {
            _projectRepo = projectRepo;
            _agendaPointRepo = agendaPointRepo;
        }

        #region Projects

        /// <summary>
        /// Get <see cref="Project"/>s from repository.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="Project"/>s.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        public IEnumerable<Project> GetProjects(ProjectSearchParameters searchParameters)
        {
            if (searchParameters == null)
                throw new ArgumentNullException("searchParameters");

            var query = _projectRepo.AsQueryable();
            var isFiltered = false;

            if (!String.IsNullOrWhiteSpace(searchParameters.Address))
            {
                query = query.Where(t => t.AddressStreetName.Contains(searchParameters.Address));
                isFiltered = true;
            }

            if (!String.IsNullOrWhiteSpace(searchParameters.TemporaryAddressName))
            {
                query = query.Where(t => t.TemporaryAddressName.Contains(searchParameters.TemporaryAddressName));
                isFiltered = true;
            }

            if (searchParameters.ProjectId.HasValue)
            {
                query = query.Where(p => p.Id == searchParameters.ProjectId.Value);
                isFiltered = true;
            }

            if (searchParameters.ProjectStatusId.HasValue)
            {
                query = query.Where(p => p.ProjectStatusId == searchParameters.ProjectStatusId.Value);
                isFiltered = true;
            }

            if (searchParameters.CouncilId.HasValue)
            {
                query = query.Where(p => p.CouncilId == searchParameters.CouncilId.Value);
                isFiltered = true;
            }

            if (searchParameters.ProjectCategoryId.HasValue)
            {
                query = query.Where(p => p.ProjectCategoryId == searchParameters.ProjectCategoryId.Value);
                isFiltered = true;
            }

            if (searchParameters.RequestorId.HasValue)
            {
                query = query.Where(p => p.RequestorId == searchParameters.RequestorId.Value);
                isFiltered = true;
            }

            if (searchParameters.GroupId.HasValue)
            {
                query = query.Where(p => p.Council.GroupId == searchParameters.GroupId.Value);
                isFiltered = true;
            }

            if (searchParameters.CompanyId.HasValue)
            {
                query = query.Where(p => p.Advisor.CompanyId == searchParameters.CompanyId.Value);
                isFiltered = true;
            }

            if (searchParameters.EntrepreneurCompanyId.HasValue)
            {
                query = query.Where(p => p.Entrepreneur.CompanyId == searchParameters.EntrepreneurCompanyId.Value);
                isFiltered = true;
            }

            if (searchParameters.TopicIds != null && searchParameters.TopicIds.Count() != 0)
            {
                var compositeFilter = ConstructTopicIdFilterExpression<Project>(searchParameters.TopicIds);

                query = query.Where(compositeFilter);
                isFiltered = true;
            }

            if (searchParameters.StartDate.HasValue && searchParameters.EndDate.HasValue)
            {
                // Sanitizing dates to secure results on dates with time set
                // Start: 26/10/2015 14:00:22 -> 26/10/2015 00:00:00
                // End: 26/10/2015 14:00:22 -> 26/10/2015 23:59:59
                var sanitizedStartDate = searchParameters.StartDate.Value.Date;
                var sanitizedEndDate = searchParameters.EndDate.Value.Date.Add(new TimeSpan(23, 59, 59));

                switch (searchParameters.DateSearchType)
                {
                    case DateSearchType.OneYearReview:
                        query =
                            query.Where(
                                p => (p.OneYearReview >= sanitizedStartDate && p.OneYearReview <= sanitizedEndDate));
                        break;
                    case DateSearchType.FiveYearReview:
                        query =
                            query.Where(
                                p => (p.FiveYearReview >= sanitizedStartDate && p.FiveYearReview <= sanitizedEndDate));
                        break;
                }

                isFiltered = true;
            }

            if (isFiltered)
                return query;
            else
                return null;
        }

        /// <summary>
        /// Get <see cref="Project"/>s from repository async.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="Project"/>s.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        public async Task<IEnumerable<Project>> GetProjectsAsync(ProjectSearchParameters searchParameters)
        {
            return GetProjects(searchParameters);
        }
        #endregion

        #region AgendaPoints
        /// <summary>
        /// Get <see cref="AgendaPoint"/>s from repository.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="AgendaPoint"/>s.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        public IEnumerable<AgendaPoint> GetAgendaPoints(AgendaPointSearchParameters searchParameters)
        {
            if (searchParameters == null)
                throw new ArgumentNullException("searchParameters");

            var query = _agendaPointRepo.AsQueryable();
            var isFiltered = false;

            // FIX: This is to filter out agenda points that are auto-created when creating a meeting
            query = query.Where(p => p.MeetingAgendaPoint.Number != 0);

            if (!String.IsNullOrWhiteSpace(searchParameters.Address))
            {
                query = query.Where(p => p.AddressStreetName.Contains(searchParameters.Address));
                isFiltered = true;
            }

            if (!String.IsNullOrWhiteSpace(searchParameters.TemporaryAddressName))
            {
                query = query.Where(t => t.TemporaryAddressName.Contains(searchParameters.TemporaryAddressName));
                isFiltered = true;
            }

            if (searchParameters.ProjectId.HasValue)
            {
                query = query.Where(p => p.ProjectId == searchParameters.ProjectId.Value);
                isFiltered = true;
            }

            if (searchParameters.MeetingId.HasValue)
            {
                query = query.Where(p => p.MeetingAgendaPoint.MeetingId == searchParameters.MeetingId.Value);
                isFiltered = true;
            }

            if (searchParameters.AgendaPointStatusId.HasValue)
            {
                query = query.Where(p => p.AgendaPointStatusId == searchParameters.AgendaPointStatusId.Value);
                isFiltered = true;
            }

            if (searchParameters.TopicIds != null && searchParameters.TopicIds.Count() != 0)
            {
                var compositeFilter = ConstructTopicIdFilterExpression<AgendaPoint>(searchParameters.TopicIds);

                query = query.Where(compositeFilter);
                isFiltered = true;
            }

            if (isFiltered)
                return query;
            else
                return null;
        }

        /// <summary>
        /// Get <see cref="AgendaPoint"/>s from repository async.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="AgendaPoint"/>s.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        public async Task<IEnumerable<AgendaPoint>> GetAgendaPointsAsync(AgendaPointSearchParameters searchParameters)
        {
            return GetAgendaPoints(searchParameters);
        }
        #endregion

        private Expression<Func<T, bool>> ConstructTopicIdFilterExpression<T>(IEnumerable<int> topicIds)
        {
            var param = Expression.Parameter(typeof(T), "x");
            var topicPropertyExpression = Expression.Property(param, "Topic");
            var topicIdPropertyExpression = Expression.Property(topicPropertyExpression, "Id");

            var firstTopicId = topicIds.First();
            var lastExpression = Expression.Equal(topicIdPropertyExpression, Expression.Constant(firstTopicId));

            var remainingTopicIds = topicIds.Skip(1);
            foreach (var topicId in remainingTopicIds)
            {
                var nextOr = Expression.Equal(topicIdPropertyExpression, Expression.Constant(topicId));
                lastExpression = Expression.OrElse(lastExpression, nextOr);
            }
            return Expression.Lambda<Func<T, bool>>(lastExpression, param);
        }
    }
}
