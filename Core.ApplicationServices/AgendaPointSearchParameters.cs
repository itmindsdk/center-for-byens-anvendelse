#region Head
// <copyright file="AgendaPointSearchParameters.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;

namespace Core.ApplicationServices
{
    using System;
    using DomainModel;

    /// <summary>
    /// Search parameters for <see cref="AgendaPoint"/>.
    /// </summary>
    public class AgendaPointSearchParameters
    {
        /// <summary>
        /// Gets or sets address.
        /// </summary>
        public string Address { get; set; }

        public string TemporaryAddressName { get; set; }

        /// <summary>
        /// Gets or sets associated project id.
        /// </summary>
        public int? ProjectId { get; set; }

        /// <summary>
        /// Gets or sets associated meeting id
        /// </summary>
        public int? MeetingId { get; set; }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        public int? AgendaPointStatusId { get; set; }

        /// <summary>
        /// Gets or sets associated topic ids.
        /// </summary>
        public IEnumerable<int> TopicIds { get; set; }

        /// <summary>
        /// Gets or sets date interval beginning. Search is including this date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets date interval end. Search is including this date.
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}
