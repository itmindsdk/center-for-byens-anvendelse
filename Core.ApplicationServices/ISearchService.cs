﻿#region Head
// <copyright file="ISearchService.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DomainModel;

    /// <summary>
    /// Search for entities in local repositories and the DAWA API.
    /// </summary>
    public interface ISearchService
    {
        /// <summary>
        /// Get projects from repository.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="Project"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        IEnumerable<Project> GetProjects(ProjectSearchParameters searchParameters);

        /// <summary>
        /// Get projects from repository async.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="Project"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        Task<IEnumerable<Project>> GetProjectsAsync(ProjectSearchParameters searchParameters);

        /// <summary>
        /// Get agendaPoints from repository.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="AgendaPoint"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        IEnumerable<AgendaPoint> GetAgendaPoints(AgendaPointSearchParameters searchParameters);

        /// <summary>
        /// Get agendaPoints from repository async.
        /// </summary>
        /// <param name="searchParameters">The search parameters.</param>
        /// <returns>List of found <see cref="AgendaPoint"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when an argument is null.</exception>
        Task<IEnumerable<AgendaPoint>> GetAgendaPointsAsync(AgendaPointSearchParameters searchParameters);
    }
}
