﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Owin;
using Owin;
using Presentation.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace Presentation.Web
{
    public class Startup
    {
        [SuppressMessage("Microsoft.Performance", "CA1822", Justification = "Internal ASP.NET MVC method")]
        [SuppressMessage("Microsoft.Usage", "CA1801", Justification = "Internal ASP.NET MVC class")]
        public void Configuration(IAppBuilder app)
        {
        }
    }
}
