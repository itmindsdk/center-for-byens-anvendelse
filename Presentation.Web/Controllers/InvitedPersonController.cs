﻿#region Head
// <copyright file="InvitedPersonController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.InvitedPerson;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class InvitedPersonController : BaseController
    {
        private readonly IGenericRepository<InvitedPerson> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public InvitedPersonController(IGenericRepository<InvitedPerson> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: InvitedPeople
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult InvitedPerson_Read([DataSourceRequest] DataSourceRequest request)
        {
            var people = _repo.AsQueryable().Where(p => !p.IsDeleted);

            var vms = people.ProjectTo<InvitedPersonViewModel>();

            var peopleVm = vms.ToDataSourceResult(request);
            return Json(peopleVm);
        }

        // GET: InvitedPeople/Create
        public ActionResult Create()
        {
            var vm = new InvitedPersonViewModel();
            return View(vm);
        }

        // POST: InvitedPeople/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InvitedPersonViewModel invitedPersonViewModel)
        {
            if (!ModelState.IsValid) return View(invitedPersonViewModel);

            var invitedPerson = _mapper.Map<InvitedPerson>(invitedPersonViewModel);
            invitedPerson.CreatedOn = DateTime.Now;
            invitedPerson.CreatedBy = _adHelper.Name;

            _repo.Insert(invitedPerson);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: InvitedPeople/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invitedPerson = _repo.GetByKey(id);
            if (invitedPerson == null || invitedPerson.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(invitedPerson);

            var vm = _mapper.Map<InvitedPersonViewModel>(invitedPerson);

            return View(vm);
        }

        // POST: InvitedPeople/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InvitedPersonViewModel invitedPersonViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(invitedPersonViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(invitedPersonViewModel);
            }

            var invitedPerson = _mapper.Map<InvitedPerson>(invitedPersonViewModel);
            invitedPerson.ModifiedBy = _adHelper.Name;
            invitedPerson.ModifiedOn = DateTime.Now;

            _repo.Update(invitedPerson);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: InvitedPeople/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var invitedPerson = _repo.GetByKey(id);
            if (invitedPerson == null || invitedPerson.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(invitedPerson);

            var vm = _mapper.Map<InvitedPersonViewModel>(invitedPerson);

            return View(vm);
        }

        // POST: InvitedPeople/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var person = _repo.GetByKey(id);
            if (person == null || person.IsDeleted) return HttpNotFound();

            person.IsDeleted = true;
            _repo.Update(person);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

    }
}
