﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Requestor;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class RequestorController : BaseController
    {
        private readonly IGenericRepository<Requestor> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public RequestorController(IGenericRepository<Requestor> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Requestor_Read([DataSourceRequest] DataSourceRequest request)
        {
            var requestors = _repo.AsQueryable().Where(r => !r.IsDeleted);

            var vms = requestors.ProjectTo<IndexRequestorViewModel>();

            var requestorsVm = vms.ToDataSourceResult(request);
            return Json(requestorsVm);
        }

        public ActionResult Create()
        {
            var vm = new CreateRequestorViewModel();
            return View(vm);
        }

        // POST: Requestor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateRequestorViewModel requestorViewModel)
        {
            if (!ModelState.IsValid) return View(requestorViewModel);

            var requestor = _mapper.Map<Requestor>(requestorViewModel);
            requestor.CreatedOn = DateTime.Now;
            requestor.CreatedBy = _adHelper.Name;

            _repo.Insert(requestor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Requestor/Edit
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var requestor = _repo.GetByKey(id);
            if (requestor == null || requestor.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(requestor);

            var vm = _mapper.Map<EditRequestorViewModel>(requestor);

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditRequestorViewModel requestorViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(requestorViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(requestorViewModel);
            }

            var requestor = _mapper.Map<Requestor>(requestorViewModel);
            requestor.ModifiedBy = _adHelper.Name;
            requestor.ModifiedOn = DateTime.Now;

            _repo.Update(requestor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var requestor = _repo.GetByKey(id);
            if (requestor == null || requestor.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(requestor);

            var vm = _mapper.Map<DeleteRequestorViewModel>(requestor);

            return View(vm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var requestor = _repo.GetByKey(id);
            if (requestor == null || requestor.IsDeleted) return HttpNotFound();

            requestor.IsDeleted = true;
            _repo.Update(requestor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}