﻿#region Head
// <copyright file="BaseController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Security.Principal;
using System.Web.Mvc;
using Core.DomainModel;

namespace Presentation.Web.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Mock user identity in debug mode.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        #if DEBUG
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            string[] roles = { "GG-Rolle-MTM-POFCBA-Admin" };
            HttpContext.User = new GenericPrincipal(new GenericIdentity("Test Admin"), roles);

            base.OnAuthorization(filterContext);
        }
        #endif

        /// <summary>
        /// Set Created and Modified information in ViewBag.
        /// </summary>
        /// <param name="logger">Respresents a domain class object which inherits from logger</param>
        protected void SetCreatedModifiedInViewBag(Logger logger)
        {
            if (logger == null)
            {
                return;
            }

            ViewBag.CreatedOn = logger.CreatedOn;
            ViewBag.CreatedBy = logger.CreatedBy;
            ViewBag.ModifiedOn = logger.ModifiedOn;
            ViewBag.ModifiedBy = logger.ModifiedBy;
        }
    }
}
