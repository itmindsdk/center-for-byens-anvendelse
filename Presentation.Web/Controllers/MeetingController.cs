﻿#region Head
// <copyright file="MeetingController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Meeting;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.MeetingAgendaPoint;
using WebGrease.Css.Extensions;
using DetailsMeetingViewModel = Presentation.Web.Models.Meeting.DetailsMeetingViewModel;

namespace Presentation.Web.Controllers
{
    public class MeetingController : BaseController
    {
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<MeetingAgendaPoint> _meetingAgendaPointRepo;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDateTime _dateTime;
        private readonly IAdHelper _adHelper;
        private readonly IAppSettings _appSettings;

        public MeetingController(
            IGenericRepository<Meeting> meetingRepo,
            IGenericRepository<MeetingAgendaPoint> meetingAgendaPointRepo,
            IGenericRepository<AgendaPoint> agendaPointRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IDateTime dateTime,
            IAppSettings appSettings,
            IAdHelper adHelper)
        {
            _meetingRepo = meetingRepo;
            _meetingAgendaPointRepo = meetingAgendaPointRepo;
            _agendaPointRepository = agendaPointRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _dateTime = dateTime;
            _appSettings = appSettings;
            _adHelper = adHelper;
        }

        // GET: Meeting
        public ActionResult Index()
        {
            //Update server cookie with new client values.
            Response.Cookies["onlyNewPointsMeeting"].Value = Request.Cookies["onlyNewPointsMeeting"].Value ?? "true";

            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Meeting_Read([DataSourceRequest] DataSourceRequest request)
        {
            var meetings = _meetingRepo.AsQueryable();

            //If set, only show points that are less than a year old.

            if (Request.Cookies["onlyNewPointsMeeting"] != null &&
                Request.Cookies["onlyNewPointsMeeting"].Value == "true")
            {
                DateTime oneYearBack = _dateTime.Now.AddYears(-1);
                meetings = meetings.Where(a => a.CreatedOn > oneYearBack);
            }

            var meetingsViewModels = meetings.ProjectTo<IndexMeetingViewModel>();
            var meetingsVm = meetingsViewModels.ToDataSourceResult(request);
            return Json(meetingsVm);
        }

        // GET: Meeting/Create
        [PopulateInvitedPeople]
        public ActionResult Create()
        {
            var openAgendaPoints = _agendaPointRepository.Get(a => a.MeetingAgendaPoint == null &&
                                                                   a.AgendaPointStatusId == (int)AgendaPointStatusType.Ready);

            var openAgendaPointVMs = _mapper.Map<IList<AgendaPointViewModel>>(openAgendaPoints);
            var meetingAgendaPoints = new List<MeetingAgendaPointViewModel>();

            foreach (var vm in openAgendaPointVMs)
            {
                var meetingAgendaPointVM = new MeetingAgendaPointViewModel
                {
                    Number = 1,
                    AgendaPoint = vm,
                    AgendaPointId = vm.Id
                };
                meetingAgendaPointVM.AgendaPoint.AgendaPointStatusId = vm.AgendaPointStatusId;
                meetingAgendaPoints.Add(meetingAgendaPointVM);
            }

            // Sort meeting agenda points by area
            var sortedMeetingAgendaPoints = meetingAgendaPoints.OrderBy(o => o.AgendaPoint.Area).ToList();

            var meetingViewModel = new CreateMeetingViewModel
            {
                MeetingAgendaPoints = sortedMeetingAgendaPoints,
                Date = _dateTime.Now.ToString()
            };

            return View(meetingViewModel);
        }

        // POST: Meeting/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateInvitedPeople]
        public ActionResult Create(CreateMeetingViewModel meetingViewModel)
        {
            if (meetingViewModel == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid) return View(meetingViewModel);

            var meeting = _mapper.Map<Meeting>(meetingViewModel);

            var selectedAgendaPointIds = new List<int>();

            if (meetingViewModel.MeetingAgendaPoints.Any())
                selectedAgendaPointIds = meetingViewModel.MeetingAgendaPoints.Where(o => o.AgendaPoint.IsAssignedToMeeting).Select(x => x.AgendaPoint.Id).ToList();
            // Using this as proxy to the actual created meeting for id reference
            var createdMeeting = _meetingRepo.Create();

            // copying vm data
            createdMeeting.Location = meeting.Location;
            createdMeeting.Date = meeting.Date;
            createdMeeting.InvitedPeople = meeting.InvitedPeople;
            createdMeeting.CreatedOn = _dateTime.Now;
            createdMeeting.CreatedBy = _adHelper.Name;

            // Add meetingpointzero
            var agendaPointZero = new AgendaPoint
            {
                Description = _appSettings.AgendaPointZeroDescription
            };
            var meetingAgendaPointZero = new MeetingAgendaPoint
            {
                MeetingId = createdMeeting.Id,
                AgendaPoint = agendaPointZero,
                Number = 0
            };
            createdMeeting.MeetingAgendaPoints.Add(meetingAgendaPointZero);

            // insert meeting
            _meetingRepo.Insert(createdMeeting);

            // insert all meetingagendapoints in db
            var number = 1;
            foreach (var selectedId in selectedAgendaPointIds)
            {
                var meetingAgendaPoint = new MeetingAgendaPoint
                {
                    Id = selectedId,
                    MeetingId = createdMeeting.Id,
                    Number = number++
                };
                _meetingAgendaPointRepo.Insert(meetingAgendaPoint);

                // Update the status of the associated agendapoint to be disapproved.
                var agendaPoint = _agendaPointRepository.GetByKey(selectedId);
                agendaPoint.AgendaPointStatusId = (int)AgendaPointStatusType.Disapproved;
                _agendaPointRepository.Update(meetingAgendaPoint.AgendaPoint);
            }
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Meeting/Details/5
        [HttpGet]
        [PopulateInvitedPeople]
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var meeting = _meetingRepo.GetByKey(id);
            if (meeting == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(meeting);
            var meetingViewModel = _mapper.Map<DetailsMeetingViewModel>(meeting);

            if (meetingViewModel.MeetingAgendaPoints != null)
            {
                // sort meetingagendapoints by number and then by area
                var sortedMeetingAgendaPointsByNumber = meetingViewModel.MeetingAgendaPoints.OrderBy(o => o.Number).ThenBy(o => o.AgendaPoint.Area);
                meetingViewModel.MeetingAgendaPoints = sortedMeetingAgendaPointsByNumber.ToList();
            }

            return View(meetingViewModel);
        }

        // GET: Meeting/Edit/5
        [HttpGet]
        [PopulateInvitedPeople]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var meeting = _meetingRepo.GetByKey(id);
            if (meeting == null) return HttpNotFound();
            if (meeting.IsSummaryLocked) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            SetCreatedModifiedInViewBag(meeting);
            var editMeetingViewModel = _mapper.Map<EditMeetingViewModel>(meeting);

            // Get all open agendapoints
            var openAgendaPoints = _agendaPointRepository.Get(a => a.MeetingAgendaPoint == null &&
                                                                   a.AgendaPointStatusId == (int)AgendaPointStatusType.Ready);
            var openAgendaPointVMs = _mapper.Map<IEnumerable<AgendaPointViewModel>>(openAgendaPoints);

            // Convert openAgendaPointsVM to meetingAgendaPoint
            var openMeetingAgendaPoints = openAgendaPointVMs.Select(openAgendaPointVM => new MeetingAgendaPointViewModel
            {
                AgendaPoint = openAgendaPointVM,
                Id = openAgendaPointVM.Id,
                Number = 1
            }).OrderBy(o => o.AgendaPoint.Area).ToList();

            // Dont show agenda point zero
            if (editMeetingViewModel.MeetingAgendaPoints != null)
            {
                foreach (var meetingAgendaPoint in editMeetingViewModel.MeetingAgendaPoints.ToList())
                {
                    if (meetingAgendaPoint.Number == 0)
                        editMeetingViewModel.MeetingAgendaPoints.Remove(meetingAgendaPoint);
                }

                // Sort meeting agenda points by number and then by area
                editMeetingViewModel.MeetingAgendaPoints =
                    editMeetingViewModel.MeetingAgendaPoints.OrderBy(o => o.Number).ThenBy(o => o.AgendaPoint.Area).ToList();

                // Add open agendapoints
                foreach (var openAgendaPoint in openMeetingAgendaPoints)
                {
                    editMeetingViewModel.MeetingAgendaPoints.Add(openAgendaPoint);
                }
            }

            return View(editMeetingViewModel);
        }

        // POST: Meeting/Edit/5
        // TODO: This method is absurdly insane
        [HttpPost]
        [PopulateInvitedPeople]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditMeetingViewModel meetingViewModel)
        {
            if (meetingViewModel == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var meeting = _meetingRepo.GetByKey(meetingViewModel.Id);
            if (meeting == null) return HttpNotFound();

            if (!ModelState.IsValid)
            {
                SetCreatedModifiedInViewBag(meeting);
                return View(meetingViewModel);
            }

            if (meetingViewModel.InvitedPeople == null) meetingViewModel.InvitedPeople = new List<string>();

            if (meeting.MeetingAgendaPoints.Any())
            {
                MeetingAgendaPoint meetingPointZero;
                try
                {
                    meetingPointZero = meeting.MeetingAgendaPoints.Single(o => o.Number == 0);
                }
                catch (InvalidOperationException e)
                {
                    return View("ZeroAgendaPointError", null);
                }
                var meetingPointZeroVm = _mapper.Map<MeetingAgendaPointViewModel>(meetingPointZero);
                meetingViewModel.MeetingAgendaPoints.Add(meetingPointZeroVm); // add meeting point zero

                // Check if any agendapoints has been removed from original meeting
                var originalMeetingAgendaPointIds = meeting.MeetingAgendaPoints.Where(o => o.Number != 0).Select(o => o.Id);
                var vmSelectedMeetingAgendaPointIds = new List<int>();
                if (meetingViewModel.MeetingAgendaPoints.Any())
                    vmSelectedMeetingAgendaPointIds =
                        meetingViewModel.MeetingAgendaPoints.Where(
                            o => o.AgendaPoint.IsAssignedToMeeting || o.MeetingId != 0)
                            .Select(o => o.Id)
                            .ToList(); // meetingId is 0 if an openMeetingAgendaPoint not attached to a meeting
                var meetingAgendaPointIdsToBeRemoved = originalMeetingAgendaPointIds.Except(vmSelectedMeetingAgendaPointIds);

                // Remove meetingAgendaPointToBeRemoved
                foreach (var idToRemove in meetingAgendaPointIdsToBeRemoved.ToList())
                {
                    // Update associated agenda point status to be ready
                    var meetingAgendaPoint = _meetingAgendaPointRepo.GetByKey(idToRemove);
                    meetingAgendaPoint.AgendaPoint.AgendaPointStatusId = (int)AgendaPointStatusType.Ready;
                    meetingAgendaPoint.AgendaPoint.Agreement = null;
                    meetingAgendaPoint.AgendaPoint.Conclusion = null;
                    _agendaPointRepository.Update(meetingAgendaPoint.AgendaPoint);

                    // Remove meeting agenda point
                    _meetingAgendaPointRepo.DeleteByKey(idToRemove);
                }
            }

            _mapper.Map(meetingViewModel, meeting);

            meeting.ModifiedOn = _dateTime.Now;
            meeting.ModifiedBy = _adHelper.Name;

            // Add attached meetingagendapoints
            if (meetingViewModel.MeetingAgendaPoints.Any())
            {
                var assignedMeetingAgendaPoints = meetingViewModel.MeetingAgendaPoints.Where(o => o.AgendaPoint.IsAssignedToMeeting).ToList();

                meeting.MeetingAgendaPoints.Clear();
                var index = 0;
                foreach (var meetingAgendaPoint in GetAttachedMeetingAgendaPoints(meetingViewModel.MeetingAgendaPoints))
                {
                    if (meetingAgendaPoint.Id == 0) // meeting agendapoint doesn't exist
                    {
                        meetingAgendaPoint.Id = assignedMeetingAgendaPoints[index].AgendaPoint.Id;
                        meetingAgendaPoint.MeetingId = assignedMeetingAgendaPoints[index].AgendaPoint.Id;
                        meetingAgendaPoint.Number = 1;
                    }
                    meetingAgendaPoint.Number = assignedMeetingAgendaPoints[index].Number;
                    meeting.MeetingAgendaPoints.Add(meetingAgendaPoint);

                    // Update the status of the associated agendapoint to be disapproved.
                    var agendaPoint = _agendaPointRepository.GetByKey(assignedMeetingAgendaPoints[index].AgendaPoint.Id);
                    agendaPoint.AgendaPointStatusId = (int)AgendaPointStatusType.Disapproved;
                    _agendaPointRepository.Update(meetingAgendaPoint.AgendaPoint);

                    index++;
                }
            }

            _meetingRepo.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Details", new { id = meeting.Id });
        }

        // GET: Meeting/CreateSummaries
        [PopulateDefaultAgreementPhrases]
        [PopulateDefaultConclusionPhrases]
        public ActionResult CreateSummaries(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var meeting = _meetingRepo.GetByKey(id);

            if (meeting == null)
            {
                return HttpNotFound();
            }

            var meetingVm = _mapper.Map<CreateSummariesMeetingViewModel>(meeting);

            SetCreatedModifiedInViewBag(meeting);

            if (meetingVm.MeetingAgendaPoints == null)
            {
                meetingVm.MeetingAgendaPoints = new List<MeetingAgendaPointSummariesViewModel>();
            }

            // sort meetingagendapoints by number and then by area
            var sortedMeetingAgendaPointsByNumber = meetingVm.MeetingAgendaPoints.OrderBy(o => o.Number).ThenBy(o => o.AgendaPoint.Area);
            meetingVm.MeetingAgendaPoints = sortedMeetingAgendaPointsByNumber.ToList();

            // set the vm's approved field based on the agenda point status
            meetingVm.MeetingAgendaPoints.Select(meetingAgendaPoint => meetingAgendaPoint.AgendaPoint).ForEach(agendaPoint =>
                agendaPoint.Approved = agendaPoint.AgendaPointStatusId == (int)AgendaPointStatusType.Approved);

            return View(meetingVm);
        }

        // POST: Meeting/CreateSummaries
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateDefaultAgreementPhrases]
        [PopulateDefaultConclusionPhrases]
        public ActionResult CreateSummaries(CreateSummariesMeetingViewModel createSummariesMeetingViewModel)
        {
            if (createSummariesMeetingViewModel == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return View(createSummariesMeetingViewModel);

            var storedMeeting = _meetingRepo.GetByKey(createSummariesMeetingViewModel.Id);

            if (storedMeeting != null)
            {
                storedMeeting.NextMeeting = createSummariesMeetingViewModel.NextMeeting;

                foreach (var meetingAgendaPoint in createSummariesMeetingViewModel.MeetingAgendaPoints)
                {
                    var ap = _agendaPointRepository.GetByKey(meetingAgendaPoint.AgendaPoint.Id);
                    if (ap == null) continue;

                    // Update the editable description field of agenda point zero.
                    if (meetingAgendaPoint.Number == 0)
                    {
                        ap.Description = meetingAgendaPoint.AgendaPoint.Description;
                    }

                    // Update the editable summary fields and agenda point status.
                    ap.Agreement = meetingAgendaPoint.AgendaPoint.Agreement;
                    ap.Conclusion = meetingAgendaPoint.AgendaPoint.Conclusion;
                    ap.AgendaPointStatusId = meetingAgendaPoint.AgendaPoint.Approved
                        ? (int)AgendaPointStatusType.Approved
                        : (int)AgendaPointStatusType.Disapproved;

                    _agendaPointRepository.Update(ap);
                }

                _meetingRepo.Update(storedMeeting);
                _unitOfWork.Save();
            }

            return RedirectToAction("Index");
        }

        // GET: Meeting/LockSummary/5
        [AuthorizeRoles(UserRole.Admin)]
        public ActionResult LockSummary(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var meeting = _meetingRepo.GetByKey(id);

            if (meeting == null)
            {
                return HttpNotFound();
            }

            meeting.IsSummaryLocked = true;

            _meetingRepo.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Meeting/UnlockSummary/5
        [AuthorizeRoles(UserRole.Admin)]
        public ActionResult UnlockSummary(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var meeting = _meetingRepo.GetByKey(id);

            if (meeting == null)
            {
                return HttpNotFound();
            }

            meeting.IsSummaryLocked = false;

            _meetingRepo.Update(meeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        #region Helpers
        /// <summary>
        /// Set Created and Modified information in ViewBag.
        /// </summary>
        /// <param name="meeting">The meeting.</param>
        private void SetCreatedModifiedInViewBag(Meeting meeting)
        {
            if (meeting == null)
            {
                return;
            }

            ViewBag.CreatedOn = meeting.CreatedOn.ToShortDateString();
            ViewBag.CreatedBy = meeting.CreatedBy;
            ViewBag.ModifiedOn = meeting.ModifiedOn;
            ViewBag.ModifiedBy = meeting.ModifiedBy;
            ViewBag.InvitedPeople = meeting.InvitedPeople;
        }

        /// <summary>
        /// Get attached meetingAgendaPoints from repository from a list of meetingAgendaPointViewModels.
        /// </summary>
        /// <param name="meetingAgendaPointsViewModels">List of MeetingAgendaPoints with IDs set</param>
        /// <returns>List of attached MeetingAgendaPoint found in repository</returns>
        private IEnumerable<MeetingAgendaPoint> GetAttachedMeetingAgendaPoints(IEnumerable<MeetingAgendaPointViewModel> meetingAgendaPointsViewModels)
        {
            var meetingAgendaPoints = new List<MeetingAgendaPoint>();

            foreach (var meetingAgendaPointViewModel in meetingAgendaPointsViewModels.Where(a => a.AgendaPoint.IsAssignedToMeeting))
            {
                var meetingAgendaPoint = _meetingAgendaPointRepo.GetByKey(meetingAgendaPointViewModel.Id) ??
                                         _meetingAgendaPointRepo.Create();

                meetingAgendaPoints.Add(meetingAgendaPoint);
            }

            return meetingAgendaPoints;
        }
        #endregion
    }
}
