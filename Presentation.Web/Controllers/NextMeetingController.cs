﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.NextMeeting;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class NextMeetingController : BaseController
    {
        private readonly IGenericRepository<NextMeeting> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public NextMeetingController(IGenericRepository<NextMeeting> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: NextMeeting
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult NextMeeting_Read([DataSourceRequest] DataSourceRequest request)
        {
            var nextMeetings = _repo.AsQueryable().Where(nm => !nm.IsDeleted);

            var vms = nextMeetings.ProjectTo<IndexNextMeetingViewModel>();

            var nextMeetingsVm = vms.ToDataSourceResult(request);
            return Json(nextMeetingsVm);
        }

        // GET: NextMeeting/Create
        public ActionResult Create()
        {
            var vm = new CreateNextMeetingViewModel();
            return View(vm);
        }

        // POST: NextMeeting/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateNextMeetingViewModel nextMeetingViewModel)
        {
            if (!ModelState.IsValid) return View(nextMeetingViewModel);

            var nextMeeting = _mapper.Map<NextMeeting>(nextMeetingViewModel);
            nextMeeting.CreatedOn = DateTime.Now;
            nextMeeting.CreatedBy = _adHelper.Name;

            _repo.Insert(nextMeeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: NextMeeting/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var nextMeeting = _repo.GetByKey(id);
            if (nextMeeting == null || nextMeeting.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(nextMeeting);

            var vm = _mapper.Map<EditNextMeetingViewModel>(nextMeeting);

            return View(vm);
        }

        // POST: NextMeeting/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditNextMeetingViewModel nextMeetingViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(nextMeetingViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(nextMeetingViewModel);
            }

            var nextMeeting = _mapper.Map<NextMeeting>(nextMeetingViewModel);
            nextMeeting.ModifiedBy = _adHelper.Name;
            nextMeeting.ModifiedOn = DateTime.Now;

            _repo.Update(nextMeeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: NextMeeting/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var nextMeeting = _repo.GetByKey(id);
            if (nextMeeting == null || nextMeeting.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(nextMeeting);

            var vm = _mapper.Map<DeleteNextMeetingViewModel>(nextMeeting);

            return View(vm);
        }

        // POST: NextMeeting/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var nextMeeting = _repo.GetByKey(id);
            if (nextMeeting == null || nextMeeting.IsDeleted) return HttpNotFound();

            nextMeeting.IsDeleted = true;
            _repo.Update(nextMeeting);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

    }
}

