﻿#region Head
// <copyright file="SearchController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Ninject.Infrastructure.Language;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Search;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.User, UserRole.Admin)]
    public class SearchController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ISearchService _searchService;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepository;

        public SearchController(IMapper mapper, ISearchService searchService, IGenericRepository<Project> projectRepository, IGenericRepository<AgendaPoint> agendaPointRepository)
        {
            _mapper = mapper;
            _searchService = searchService;
            _projectRepository = projectRepository;
            _agendaPointRepository = agendaPointRepository;
        }

        // GET: Search
        [PopulateTopics]
        [PopulateCouncilsRaw]
        [PopulateGroups]
        [PopulateCompanies(ShowInactives = true)]
        [PopulateEntrepreneurCompanies(ShowInactives = true)]
        [PopulateProjectStatuses(ShowInactives = true)]
        [PopulateMunicipalityPriorities(ShowInactives = true)]
        [PopulateCouncilPriorities(ShowInactives = true)]
        [PopulatePlannings(ShowInactives = true)]
        [PopulateAgendaPointStatuses(ShowInactives = true)]
        [PopulateRequestors(ShowInactives = true)]
        [PopulateLocalCommunities]
        [PopulateProjectCategories]
        public ActionResult Index(SearchViewModel viewModel)
        {
            if (viewModel == null)
            {
                viewModel = new SearchViewModel();
            }

            return View(viewModel);
        }

        // POST: Search
        [HttpPost, ActionName("Index")]
        [PopulateTopics]
        [PopulateCouncilsRaw]
        [PopulateGroups]
        [PopulateCompanies(ShowInactives = true)]
        [PopulateEntrepreneurCompanies(ShowInactives = true)]
        [PopulateProjectStatuses(ShowInactives = true)]
        [PopulateMunicipalityPriorities(ShowInactives = true)]
        [PopulateCouncilPriorities(ShowInactives = true)]
        [PopulatePlannings(ShowInactives = true)]
        [PopulateAgendaPointStatuses(ShowInactives = true)]
        [PopulateRequestors(ShowInactives = true)]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> IndexPost(SearchViewModel viewModel)
        {
            if (viewModel == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!viewModel.AnyParametersSet())
            {
                ViewBag.NoSearchParameters = true;
                return View(viewModel);
            }

            var results = new ResultsViewModel();
            if (viewModel.SearchInProjects)
            {
                var projectSearchParameters = _mapper.Map<ProjectSearchParameters>(viewModel);
                var projectsFilter = SearchHelper.ConstructFilterFuncProjectFromSearchParams(projectSearchParameters);

                results.ShowProjects = SearchHelper.ProjectFilterActuallyFilters(projectSearchParameters);
                results.ProjectFilter = projectsFilter;
                results.ProjectSearchParameters = projectSearchParameters;
            }
            if (viewModel.SearchInAgendaPoints)
            {
                var agendaPointSearchParameters = _mapper.Map<AgendaPointSearchParameters>(viewModel);
                var agendaPointsFilter = SearchHelper.ConstructFilterFuncAgendaPointFromSearchParams(agendaPointSearchParameters);

                results.ShowAgendaPoints = SearchHelper.AgendaPointFilterActuallyFilters(agendaPointSearchParameters);
                results.AgendaPointFilter = agendaPointsFilter;
                results.AgendaPointSearchParameters = agendaPointSearchParameters;
            }


            return View("Results", results);
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult AgendaPoint_Read([DataSourceRequest] DataSourceRequest request)
        {
            var agendaPoints = _agendaPointRepository.AsQueryable();

            var agendaPointsVm = agendaPoints.ProjectTo<AgendaPointViewModel>();
            var queryResultAgendaPoints = agendaPointsVm.ToDataSourceResult(request);
            return Json(queryResultAgendaPoints);
        }
        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Project_Read([DataSourceRequest] DataSourceRequest request)
        {
            var projects = _projectRepository.AsQueryable();

            var projectsVm = projects.ProjectTo<ProjectViewModel>();
            var queryResultAgendaPoints = projectsVm.ToDataSourceResult(request);
            return Json(queryResultAgendaPoints);
        }

        private string CouncilGroupNameOrNull(ProjectViewModel vm)
        {
            return vm.Council != null ? vm.Council.Group.Name : null;
        }

        public ActionResult PrintSearchResults(ProjectSearchParameters searchParams)
        {
            var projects = _searchService.GetProjects(searchParams);
            var vmEnumerable = _mapper.Map<IEnumerable<ProjectViewModel>>(projects);
            var vmDictionary = FormatProjectsForPrintDisplay(vmEnumerable);

            return View(vmDictionary);
        }

        public ActionResult PrintSearchResultsDetailed(ProjectSearchParameters searchParams)
        {
            var projects = _searchService.GetProjects(searchParams);
            var vmEnumerable = _mapper.Map<IEnumerable<ProjectViewModel>>(projects);
            var vmDictionary = FormatProjectsForPrintDisplay(vmEnumerable);

            return View(vmDictionary);
        }

        public ActionResult PrintAgendaPointResults(AgendaPointSearchParameters searchParams)
        {
            var projects = _searchService.GetAgendaPoints(searchParams);
            var vms = _mapper.Map<IEnumerable<AgendaPointViewModel>>(projects);

            return View(vms);
        }

        private Dictionary<string, IEnumerable<ProjectViewModel>> FormatProjectsForPrintDisplay(IEnumerable<ProjectViewModel> projects)
        {
            return projects.ToList()
                      .GroupBy(CouncilGroupNameOrNull)
                      .OrderBy(grp => grp.Key, new StringComparerNullLast())
                      .ToDictionary(
                          grp => grp.Key ?? "Projekter uden gruppe",
                          grp => grp.ToList()
                              .OrderBy(prj => prj.Council?.Name)
                              .ThenBy(prj => prj.Id)
                              .ToEnumerable()
                          );
        }
    }
}
