﻿#region Head
// <copyright file="ProjectController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Project;

namespace Presentation.Web.Controllers
{
    public class ProjectController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<ProjectDocument> _documentRepository;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IDocumentHelper _documentHelper;
        private readonly IDateTime _dateTime;
        private readonly IGeoCoder _geoCoder;
        private readonly IDawaRepository _dawaRepo;
        private readonly IAdHelper _adHelper;

        public ProjectController(IUnitOfWork unitOfWork,
            IGenericRepository<Project> projectRepository,
            IMapper mapper,
            IGenericRepository<ProjectDocument> documentRepository,
            IGenericRepository<Topic> topicRepo,
            IDocumentHelper documentHelper,
            IDateTime dateTime,
            IGeoCoder geoCoder,
            IDawaRepository dawaRepo,
            IAdHelper adHelper)
        {
            _unitOfWork = unitOfWork;
            _projectRepository = projectRepository;
            _mapper = mapper;
            _documentRepository = documentRepository;
            _topicRepo = topicRepo;
            _documentHelper = documentHelper;
            _dateTime = dateTime;
            _geoCoder = geoCoder;
            _dawaRepo = dawaRepo;
            _adHelper = adHelper;
        }

        // GET: Project
        [PopulateTopics]
        [PopulateStreetTypes]
        [PopulateMunicipalityPriorities]
        [PopulateCouncilPriorities]
        [PopulateProjectStatuses]
        public ActionResult Index()
        {
            //Update server cookie with new client values.
            Response.Cookies["select-awaiting"].Value = Request.Cookies["select-awaiting"].Value ?? "true";

            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Project_Read([DataSourceRequest] DataSourceRequest request)
        {
            var projects = _projectRepository.AsQueryable();

            if (Request.Cookies["select-awaiting"] != null && Request.Cookies["select-awaiting"].Value == "true")
                projects = projects.Where(p => p.ProjectStatusId == (int)ProjectStatusType.Awaiting || p.ProjectStatusId == (int)ProjectStatusType.Ongoing);

            var vms = projects.ProjectTo<IndexProjectViewModel>();
            var projectsVm = vms.ToDataSourceResult(request);
            return Json(projectsVm);
        }

        // GET: Project/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var project = _projectRepository.GetByKey(id);
            if (project == null)
            {
                return HttpNotFound();
            }

            SetCreatedModifiedInViewBag(project);
            var vm = _mapper.Map<IndexProjectViewModel>(project);

            return View(vm);
        }

        // GET: Project/Create
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateEntrepreneurs]
        [PopulateEntrepreneurCompanies]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectStatuses]
        [PopulateStreetTypes]
        [PopulatePlannings]
        [PopulateCouncilPriorities]
        [PopulateMunicipalityPriorities]
        [PopulateRequestors]
        [PopulateInconveniences]
        [PopulateTrafficInconveniences]
        [AddStatusExplanationUrl]
        public ActionResult Create()
        {
            var vm = new CreateProjectViewModel();
            return View(vm);
        }

        // POST: Project/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateEntrepreneurs]
        [PopulateEntrepreneurCompanies]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectStatuses]
        [PopulateStreetTypes]
        [PopulatePlannings]
        [PopulateCouncilPriorities]
        [PopulateMunicipalityPriorities]
        [PopulateRequestors]
        [PopulateInconveniences]
        [PopulateTrafficInconveniences]
        [AddStatusExplanationUrl]
        public ActionResult Create(CreateProjectViewModel createProjectViewModel)
        {
            if (!ValidateInconvenience(createProjectViewModel))
            {
                AddInconvenienceModelError();
            }

            if (!ValidateTrafficInconvenience(createProjectViewModel))
            {
                AddTrafficInconvenienceModelError();
            }

            if (!ModelState.IsValid)
            {
                return View(createProjectViewModel);
            }

            var project = _mapper.Map<Project>(createProjectViewModel);

            if (project.Documents != null)
            {
                var guidList = project.Documents.Select(o => o.Guid).ToList();
                project.Documents.Clear();

                // foreach guid in vm, get document from db and insert in created project
                foreach (var guid in guidList)
                {
                    var guid1 = guid;
                    var document = _documentRepository.Get(o => o.Guid == guid1).First();
                    project.Documents.Add(document);
                }
            }
            project.CreatedBy = _adHelper.Name;
            project.CreatedOn = _dateTime.Now;

            // lookup for topic
            var topic = _topicRepo.GetByKey(createProjectViewModel.TopicId);
            if (topic != null)
            {
                project.Topic = topic;
            }

            // add spatialobject
            var spatialObject = _geoCoder.GetSpatialObject(createProjectViewModel.AddressId);
            project.AddressLocation = spatialObject;
            project.AddressLocationSRID = spatialObject.CoordinateSystemId;

            // Fetch address details from server and save to local DB
            var addr = _dawaRepo.GetAddress(createProjectViewModel.AddressId);
            project.AddressStreetName = addr.StreetName;
            project.AddressNumber = addr.Number;
            project.AddressZipCode = addr.ZipCode;
            project.AddressCity = addr.City;

            _projectRepository.Insert(project);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Project/Edit/5
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateEntrepreneurs]
        [PopulateEntrepreneurCompanies]
        [PopulateGroups]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectStatuses]
        [PopulateStreetTypes]
        [PopulatePlannings]
        [PopulateCouncilPriorities]
        [PopulateMunicipalityPriorities]
        [PopulateRequestors]
        [PopulateInconveniences]
        [PopulateTrafficInconveniences]
        [AddStatusExplanationUrl]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var project = _projectRepository.GetByKey(id);
            if (project == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(project);
            var vm = _mapper.Map<EditProjectViewModel>(project);

            if (project.Advisor != null)
                vm.CompanyId = project.Advisor.CompanyId;

            if (project.Entrepreneur != null)
                vm.EntrepreneurCompanyId = project.Entrepreneur.CompanyId;

            if (project.ProjectLeader != null)
                vm.TeamId = project.ProjectLeader.TeamId;

            return View(vm);
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateAdvisors]
        [PopulateConstructionPrograms]
        [PopulateEntrepreneurs]
        [PopulateEntrepreneurCompanies]
        [PopulateGroups]
        [PopulateProjectCategories]
        [PopulateLocalCommunities]
        [PopulateCouncils]
        [PopulateProjectLeaders]
        [PopulateCompanies]
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectStatuses]
        [PopulateStreetTypes]
        [PopulatePlannings]
        [PopulateCouncilPriorities]
        [PopulateMunicipalityPriorities]
        [PopulateRequestors]
        [PopulateInconveniences]
        [PopulateTrafficInconveniences]
        [AddStatusExplanationUrl]
        public ActionResult Edit(EditProjectViewModel editProjectViewModel)
        {
            var project = _projectRepository.GetByKey(editProjectViewModel.Id);

            if (!ValidateInconvenience(editProjectViewModel))
            {
                AddInconvenienceModelError();
            }

            if (!ValidateTrafficInconvenience(editProjectViewModel))
            {
                AddTrafficInconvenienceModelError();
            }

            if (!ModelState.IsValid)
            {
                SetCreatedModifiedInViewBag(project);
                return View(editProjectViewModel);
            }

            if (editProjectViewModel.Documents != null && editProjectViewModel.Documents.Any())
            {
                var originalGuidsList = project.Documents.Select(o => o.Guid).ToList();
                var guidsList = editProjectViewModel.Documents.Where(o => o.Guid != Guid.Empty).Select(o => o.Guid).ToList();

                // get guids for document(s) to remove
                var exceptGuids = originalGuidsList.Except(guidsList).ToList();

                // Delete removed documents
                foreach (var exceptGuid in exceptGuids)
                {
                    project.Documents.Remove(project.Documents.First(d => d.Guid == exceptGuid));
                    _documentRepository.DeleteByKey(exceptGuid);
                }
                DeleteFilesFromServer(exceptGuids.ToList());
            }

            // Write properties
            var previousLocation = project.AddressLocation;
            _mapper.Map(editProjectViewModel, project);

            if (project.Documents != null)
            {
                var documentList = project.Documents.Where(o => o.Guid != Guid.Empty).ToList();
                project.Documents.Clear();

                // foreach guid in vm, get document from db and insert in project
                foreach (var doc in documentList)
                {
                    var guid1 = doc.Guid;
                    var document = _documentRepository.Get(o => o.Guid == guid1).First();
                    document.ProjectId = project.Id;
                    document.Comment = doc.Comment;

                    // update document through documentRepository, because EF marks navigation properties of parent as unmodified
                    _documentRepository.Update(document);

                    project.Documents.Add(document);
                }
            }

            project.ModifiedBy = _adHelper.Name;
            project.ModifiedOn = _dateTime.Now;

            // If address changed
            if (editProjectViewModel.AddressId != Guid.Empty)
            {
                // Fetch address details from server and save to local DB
                var addr = _dawaRepo.GetAddress(editProjectViewModel.AddressId);
                project.AddressStreetName = addr.StreetName;
                project.AddressNumber = addr.Number;
                project.AddressZipCode = addr.ZipCode;
                project.AddressCity = addr.City;

                // add spatialobject if previously null
                if (previousLocation == null)
                {
                    var spatialObject = _geoCoder.GetSpatialObject(editProjectViewModel.AddressId);
                    project.AddressLocation = spatialObject;
                    project.AddressLocationSRID = spatialObject.CoordinateSystemId;
                }
            }
            else
            {
                project.AddressLocation = previousLocation;
            }

            _projectRepository.Update(project);
            _unitOfWork.Save();
            return RedirectToAction("Details", new { id = project.Id });
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var project = _projectRepository.GetByKey(id);
            SetCreatedModifiedInViewBag(project);
            var vm = _mapper.Map<IndexProjectViewModel>(project);

            return View(vm);
        }

        // POST: Project/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var documents = _documentRepository.Get(o => o.ProjectId == id).ToList();
            // delete all attached documents from server
            DeleteFilesFromServer(documents.Select(o => o.Guid));

            // delete doc from db
            foreach (var doc in documents)
            {
                _documentRepository.DeleteByKey(doc.Guid);
            }

            _projectRepository.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Search project repository for projects.
        /// </summary>
        /// <param name="id">Project's id.</param>
        /// <returns>Json array with Project's matching id.</returns>
        public ActionResult GetProjectsById(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projects = _projectRepository.Get(p => p.Id == id);
            var select2Dtos = projects.Select(p => new ProjectDto { Id = p.Id });

            return Json(select2Dtos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveUploadedFile()
        {
            var fileObj = _documentHelper.UploadFile(Request, Server);
            if (fileObj == null) return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

            // create document
            var document = new ProjectDocument
            {
                Comment = Request.Form["comment"],
                Guid = fileObj.Guid,
                FileName = fileObj.File.FileName,
                FileSize = fileObj.File.ContentLength,
                ProjectId = fileObj.ForeignKeyId
            };

            _documentRepository.Insert(document);
            _unitOfWork.Save();

            return Json(new { Guid = fileObj.Guid});;
        }

        /// <summary>
        /// For downloading files
        /// </summary>
        /// <param name="guid">The file id.</param>
        /// <returns>The found file.</returns>
        public FileResult Download(Guid guid)
        {
            return _documentHelper.Download(Server, guid);
        }

        /// <summary>
        /// Delete file from server
        /// </summary>
        /// <param name="fileGuidsToDelete"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteFilesFromServer(IEnumerable<Guid> fileGuidsToDelete)
        {
            if (_documentHelper.DeleteFiles(fileGuidsToDelete, Server))
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        private static bool ValidateInconvenience<T>(T vm) where T: ISelectedInconvenience, ISelectedPlanning
        {
            return vm.PlanningId == 1 || vm.InconvenienceId > 1;
        }

        private void AddInconvenienceModelError()
        {
            ModelState.AddModelError("InconvenienceId",
                "Hvis feltet \"Planlægning af større anlægsopgaver\" er sat til andet end \"Ikke Relevant\" er gener påkrævet");
        }
        private static bool ValidateTrafficInconvenience<T>(T vm) where T : ISelectedTrafficInconvenience, ISelectedPlanning
        {
            return vm.PlanningId == 1 || vm.TrafficInconvenienceId > 1;
        }

        private void AddTrafficInconvenienceModelError()
        {
            ModelState.AddModelError("TrafficInconvenienceId",
                "Hvis feltet \"Planlægning af større anlægsopgaver\" er sat til andet end \"Ikke Relevant\" er traffik gener påkrævet");
        }
    }
}
