﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ConstructionProgram;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class ConstructionProgramController : BaseController
    {
        private readonly IGenericRepository<ConstructionProgram> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public ConstructionProgramController(IGenericRepository<ConstructionProgram> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: ConstructionProgram
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult ConstructionProgram_Read([DataSourceRequest] DataSourceRequest request)
        {
            var constructionPrograms = _repo.AsQueryable().Where(c => !c.IsDeleted);

            var vms = constructionPrograms.ProjectTo<IndexConstructionProgramViewModel>();

            var constructionProgramVm = vms.ToDataSourceResult(request);
            return Json(constructionProgramVm);
        }

        // GET: ConstructionProgram/Create
        public ActionResult Create()
        {
            var vm = new CreateConstructionProgramViewModel();
            return View(vm);
        }

        // POST: ConstructionProgram/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateConstructionProgramViewModel constructionProgramViewModel)
        {
            var cp =
                _repo.AsQueryable()
                    .Any(x => x.YearOfProjectInvolvement == constructionProgramViewModel.YearOfProjectInvolvement);

            if (cp)
                ModelState.AddModelError("YearOfProjectInvolvement", "Et anlægsprogram med dette årstal eksisterer allerede.");

            if (!ModelState.IsValid) return View(constructionProgramViewModel);

            var constructionProgram = _mapper.Map<ConstructionProgram>(constructionProgramViewModel);
            constructionProgram.CreatedOn = DateTime.Now;
            constructionProgram.CreatedBy = _adHelper.Name;

            _repo.Insert(constructionProgram);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ConstructionProgram/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var constructionProgram = _repo.GetByKey(id);
            if (constructionProgram == null || constructionProgram.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(constructionProgram);

            var vm = _mapper.Map<EditConstructionProgramViewModel>(constructionProgram);

            return View(vm);
        }

        // POST: ConstructionProgram/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditConstructionProgramViewModel constructionProgramViewModel)
        {
            var cp =
                _repo.AsQueryable()
                    .Any(x => x.YearOfProjectInvolvement == constructionProgramViewModel.YearOfProjectInvolvement &&
                              x.Id != constructionProgramViewModel.Id);

            if (cp)
                ModelState.AddModelError("YearOfProjectInvolvement", "Et anlægsprogram med dette årstal eksisterer allerede.");

            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(constructionProgramViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(constructionProgramViewModel);
            }

            var constructionProgram = _mapper.Map<ConstructionProgram>(constructionProgramViewModel);
            constructionProgram.ModifiedBy = _adHelper.Name;
            constructionProgram.ModifiedOn = DateTime.Now;

            _repo.Update(constructionProgram);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ConstructionProgram/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var constructionProgram = _repo.GetByKey(id);
            if (constructionProgram == null || constructionProgram.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(constructionProgram);

            var vm = _mapper.Map<DeleteConstructionProgramViewModel>(constructionProgram);

            return View(vm);
        }

        // POST: ConstructionProgram/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var constructionProgram = _repo.GetByKey(id);
            if (constructionProgram == null || constructionProgram.IsDeleted) return HttpNotFound();

            constructionProgram.IsDeleted = true;
            _repo.Update(constructionProgram);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
