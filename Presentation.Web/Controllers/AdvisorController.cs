﻿#region Head
// <copyright file="AdvisorController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Advisor;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class AdvisorController : BaseController
    {
        private readonly IGenericRepository<Advisor> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public AdvisorController(IGenericRepository<Advisor> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: Advisor
        [PopulateCompanies]
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Advisor_Read([DataSourceRequest] DataSourceRequest request)
        {
            var advisors = _repo.AsQueryable().Where(a => !a.IsDeleted);

            var vms = advisors.ProjectTo<IndexAdvisorViewModel>();

            var advisorVm = vms.ToDataSourceResult(request);
            return Json(advisorVm);
        }

        // GET: Advisor/:companyId
        // Returns all advisors with company id
        public ActionResult AdvisorsWithCompanyId(int? companyId)
        {
            var advisors = _repo.AsQueryable().Where(a => !a.IsDeleted && a.IsActive && a.Company.IsActive);
            if (companyId != null)
                advisors = advisors.Where(a => a.CompanyId == companyId);

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            // WARN make sure this order matches the order in PopulateAdvisorsFilter and visa versa
            var orderedAdvisors = advisors.ToList().OrderBy(x => x.ResponsiblePersonName, StringComparer.CurrentCulture);

            var advisorDTOs = _mapper.Map<IEnumerable<AdvisorDTO>>(orderedAdvisors);
            return Json(advisorDTOs, JsonRequestBehavior.AllowGet);
        }

        // GET: Advisor/Create
        [PopulateCompanies]
        public ActionResult Create()
        {
            var vm = new CreateAdvisorViewModel();
            return View(vm);
        }

        // POST: Advisor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateCompanies]
        public ActionResult Create(CreateAdvisorViewModel addAdvisorViewModel)
        {
            if (!ModelState.IsValid) return View(addAdvisorViewModel);

            var advisor = _mapper.Map<Advisor>(addAdvisorViewModel);
            advisor.CreatedOn = DateTime.Now;
            advisor.CreatedBy = _adHelper.Name;

            _repo.Insert(advisor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisor/Edit/{id}
        [PopulateCompanies]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var advisor = _repo.GetByKey(id);
            if (advisor == null || advisor.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(advisor);

            var vm = _mapper.Map<EditAdvisorViewModel>(advisor);

            return View(vm);
        }

        // POST: Advisors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateCompanies]
        public ActionResult Edit(EditAdvisorViewModel editAdvisorViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editAdvisorViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editAdvisorViewModel);
            }

            var advisor = _mapper.Map<Advisor>(editAdvisorViewModel);
            advisor.ModifiedOn = DateTime.Now;
            advisor.ModifiedBy = _adHelper.Name;

            _repo.Update(advisor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var advisor = _repo.GetByKey(id);
            if (advisor == null || advisor.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(advisor);

            var vm = _mapper.Map<DeleteAdvisorViewModel>(advisor);

            return View(vm);
        }

        // POST: Advisors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var advisor = _repo.GetByKey(id);
            if (advisor == null || advisor.IsDeleted) return HttpNotFound();

            advisor.IsDeleted = true;

            _repo.Update(advisor);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
