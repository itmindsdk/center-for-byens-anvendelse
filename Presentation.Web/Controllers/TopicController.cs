﻿#region Head
// <copyright file="TopicController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Topic;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class TopicController : BaseController
    {
        private readonly IGenericRepository<Topic> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public TopicController(IGenericRepository<Topic> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: Topic
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Topic_Read([DataSourceRequest] DataSourceRequest request)
        {
            var topics = _repo.AsQueryable().Where(t => !t.IsDeleted);

            var vms = topics.ProjectTo<IndexTopicViewModel>();

            var topicVm = vms.ToDataSourceResult(request);
            return Json(topicVm);
        }

        // GET: Topic/Create
        public ActionResult Create()
        {
            var vm = new CreateTopicViewModel();
            return View(vm);
        }

        // POST: Topic/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateTopicViewModel topicViewModel)
        {
            if (!ModelState.IsValid) return View(topicViewModel);

            var topic = _mapper.Map<Topic>(topicViewModel);
            topic.CreatedOn = DateTime.Now;
            topic.CreatedBy = _adHelper.Name;

            _repo.Insert(topic);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Topic/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var topic = _repo.GetByKey(id);
            if (topic == null || topic.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(topic);

            var vm = _mapper.Map<EditTopicViewModel>(topic);

            return View(vm);
        }

        // POST: Topic/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditTopicViewModel topicViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(topicViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(topicViewModel);
            }

            var topic = _mapper.Map<Topic>(topicViewModel);
            topic.ModifiedBy = _adHelper.Name;
            topic.ModifiedOn = DateTime.Now;

            _repo.Update(topic);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Topic/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var topic = _repo.GetByKey(id);
            if (topic == null || topic.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(topic);

            var vm = _mapper.Map<DeleteTopicViewModel>(topic);

            return View(vm);
        }

        // POST: Topic/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var topic = _repo.GetByKey(id);
            if (topic == null || topic.IsDeleted) return HttpNotFound();

            topic.IsDeleted = true;
            _repo.Update(topic);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
