﻿using System;
using System.Web.Mvc;
using Presentation.Web.Exceptions;

namespace Presentation.Web.Controllers
{
    public class ErrorPageController : Controller
    {
        public ActionResult Error(int statusCode, Exception exception)
        {
            Response.StatusCode = statusCode;
            ViewBag.StatusCode = statusCode;
            if (exception is ErrorMessageException)
            {
                ViewBag.Message = exception.Message;
            }
            return View();
        }
    }
}
