﻿#region Head
// <copyright file="ProjectCategoryController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Models.ProjectCategory;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class ProjectCategoryController : BaseController
    {
        private readonly IGenericRepository<ProjectCategory> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public ProjectCategoryController(IGenericRepository<ProjectCategory> repo, IUnitOfWork unitOfWork, IMapper mappingEngine, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mappingEngine;
            _adHelper = adHelper;
        }

        // GET: ProjectCategory
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult ProjectCategory_Read([DataSourceRequest] DataSourceRequest request)
        {
            var projectCategories = _repo.AsQueryable().Where(p => !p.IsDeleted);

            var vms = projectCategories.ProjectTo<IndexProjectCategoryViewModel>();

            var projectCategoriesVm = vms.ToDataSourceResult(request);
            return Json(projectCategoriesVm);
        }

        // GET: ProjectCategory/Create
        public ActionResult Create()
        {
            var vm = new CreateProjectCategoryViewModel();
            return View(vm);
        }

        // POST: ProjectCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateProjectCategoryViewModel projectCategoryViewModel)
        {
            if (!ModelState.IsValid) return View(projectCategoryViewModel);

            var projectCategory = _mapper.Map<ProjectCategory>(projectCategoryViewModel);
            projectCategory.CreatedOn = DateTime.Now;
            projectCategory.CreatedBy = _adHelper.Name;

            _repo.Insert(projectCategory);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: ProjectCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectCategory = _repo.GetByKey(id);
            if (projectCategory == null || projectCategory.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(projectCategory);

            var vm = _mapper.Map<EditProjectCategoryViewModel>(projectCategory);

            return View(vm);
        }

        // POST: ProjectCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditProjectCategoryViewModel projectCategoryViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(projectCategoryViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(projectCategoryViewModel);
            }

            var projectCategory = _mapper.Map<ProjectCategory>(projectCategoryViewModel);
            projectCategory.ModifiedBy = _adHelper.Name;
            projectCategory.ModifiedOn = DateTime.Now;

            _repo.Update(projectCategory);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Topic/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectCategory = _repo.GetByKey(id);
            if (projectCategory == null || projectCategory.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(projectCategory);

            var vm = _mapper.Map<DeleteProjectCategoryViewModel>(projectCategory);

            return View(vm);
        }

        // POST: ProjectCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var projectCategory = _repo.GetByKey(id);
            if (projectCategory == null || projectCategory.IsDeleted) return HttpNotFound();

            projectCategory.IsDeleted = true;
            _repo.Update(projectCategory);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
