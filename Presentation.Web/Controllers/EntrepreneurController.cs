﻿#region Head
// <copyright file="EntrepreneurController.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Entrepreneur;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class EntrepreneurController : BaseController
    {
        private readonly IGenericRepository<Entrepreneur> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public EntrepreneurController(IGenericRepository<Entrepreneur> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: Advisor
        [PopulateEntrepreneurCompanies]
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Entrepreneur_Read([DataSourceRequest] DataSourceRequest request)
        {
            var entrepreneurs = _repo.AsQueryable().Where(e => !e.IsDeleted);

            var vms = entrepreneurs.ProjectTo<IndexEntrepreneurViewModel>();

            var entrepreneursVm = vms.ToDataSourceResult(request);
            return Json(entrepreneursVm);
        }

        // GET: Advisor/:companyId
        // Returns all advisors with company id
        public ActionResult EntrepreneursWithCompanyId(int? companyId)
        {
            var entrepreneurs = _repo.AsQueryable().Where(a => !a.IsDeleted && a.IsActive && a.Company.IsActive);
            if (companyId != null)
                entrepreneurs = entrepreneurs.Where(a => a.CompanyId == companyId);

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            // WARN make sure this order matches the order in PopulateAdvisorsFilter and visa versa
            var orderedEntrepreneurs = entrepreneurs.ToList().OrderBy(x => x.ResponsiblePersonName, StringComparer.CurrentCulture);

            var entrepreneurDTOs = _mapper.Map<IEnumerable<EntrepreneurDTO>>(orderedEntrepreneurs);
            return Json(entrepreneurDTOs, JsonRequestBehavior.AllowGet);
        }

        // GET: Advisor/Create
        [PopulateEntrepreneurCompanies]
        public ActionResult Create()
        {
            var vm = new CreateEntrepreneurViewModel();
            return View(vm);
        }

        // POST: Advisor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateEntrepreneurCompanies]
        public ActionResult Create(CreateEntrepreneurViewModel createEntrepreneurViewModel)
        {
            if (!ModelState.IsValid) return View(createEntrepreneurViewModel);

            var entrepreneur = _mapper.Map<Entrepreneur>(createEntrepreneurViewModel);
            entrepreneur.CreatedOn = DateTime.Now;
            entrepreneur.CreatedBy = _adHelper.Name;

            _repo.Insert(entrepreneur);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisor/Edit/{id}
        [PopulateEntrepreneurCompanies]
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var entrepreneur = _repo.GetByKey(id);
            if (entrepreneur == null || entrepreneur.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(entrepreneur);

            var vm = _mapper.Map<EditEntrepreneurViewModel>(entrepreneur);

            return View(vm);
        }

        // POST: Advisors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateEntrepreneurCompanies]
        public ActionResult Edit(EditEntrepreneurViewModel editEntrepreneurViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editEntrepreneurViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editEntrepreneurViewModel);
            }

            var entrepreneur = _mapper.Map<Entrepreneur>(editEntrepreneurViewModel);
            entrepreneur.ModifiedOn = DateTime.Now;
            entrepreneur.ModifiedBy = _adHelper.Name;

            _repo.Update(entrepreneur);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Advisors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var entrepreneur = _repo.GetByKey(id);
            if (entrepreneur == null || entrepreneur.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(entrepreneur);

            var vm = _mapper.Map<DeleteEntrepreneurViewModel>(entrepreneur);

            return View(vm);
        }

        // POST: Advisors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var entrepreneur = _repo.GetByKey(id);
            if (entrepreneur == null || entrepreneur.IsDeleted) return HttpNotFound();

            entrepreneur.IsDeleted = true;

            _repo.Update(entrepreneur);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
