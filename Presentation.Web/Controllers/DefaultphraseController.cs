﻿#region Head
// <copyright file="DefaultPhraseController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.DefaultPhrase;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class DefaultPhraseController : BaseController
    {
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public DefaultPhraseController(IGenericRepository<DefaultPhrase> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: DefaultPhrase
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult DefaultPhrase_Read([DataSourceRequest] DataSourceRequest request)
        {
            var defaultPhrases = _repo.AsQueryable().Where(p => !p.IsDeleted);

            var vms = defaultPhrases.ProjectTo<IndexDefaultPhraseViewModel>();

            var defaultPhrasesVm = vms.ToDataSourceResult(request);
            return Json(defaultPhrasesVm);
        }

        // GET: DefaultPhrase/Create
        public ActionResult Create()
        {
            var vm = new DefaultPhraseViewModel();
            return View(vm);
        }

        // POST: DefaultPhrase/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DefaultPhraseViewModel defaultPhraseViewModel)
        {
            if (!ModelState.IsValid) return View(defaultPhraseViewModel);

            var defaultPhrase = _mapper.Map<DefaultPhrase>(defaultPhraseViewModel);
            defaultPhrase.CreatedOn = DateTime.Now;
            defaultPhrase.CreatedBy = _adHelper.Name;

            _repo.Insert(defaultPhrase);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: DefaultPhrase/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var defaultPhrase = _repo.GetByKey(id);
            if (defaultPhrase == null || defaultPhrase.IsDeleted) return HttpNotFound();

            SetCreatedModifiedInViewBag(defaultPhrase);

            var vm = _mapper.Map<DefaultPhraseViewModel>(defaultPhrase);

            return View(vm);
        }

        // POST: DefaultPhrase/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DefaultPhraseViewModel defaultPhraseViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(defaultPhraseViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(defaultPhraseViewModel);
            }

            var defaultPhrase = _mapper.Map<DefaultPhrase>(defaultPhraseViewModel);
            defaultPhrase.ModifiedBy = _adHelper.Name;
            defaultPhrase.ModifiedOn = DateTime.Now;

            _repo.Update(defaultPhrase);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: DefaultPhrase/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var defaultPhrase = _repo.GetByKey(id);
            if (defaultPhrase == null || defaultPhrase.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(defaultPhrase);

            var vm = _mapper.Map<DefaultPhraseViewModel>(defaultPhrase);

            return View(vm);
        }

        // POST: DefaultPhrase/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var defaultPhrase = _repo.GetByKey(id);
            if (defaultPhrase == null || defaultPhrase.IsDeleted) return HttpNotFound();

            defaultPhrase.IsDeleted = true;
            _repo.Update(defaultPhrase);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

    }
}
