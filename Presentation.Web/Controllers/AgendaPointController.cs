﻿#region Head
// <copyright file="AgendaPointController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.AgendaPoint;
using Presentation.Web.Models.Project;

namespace Presentation.Web.Controllers
{
    public class AgendaPointController : BaseController
    {
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<AgendaPointDocument> _documentRepository;
        private readonly IGenericRepository<PartnerDiscussion> _partnerDiscussionRepository;
        private readonly IDocumentHelper _documentHelper;
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<NextMeeting> _nextMeetingRepo;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDateTime _dateTime;
        private readonly IGeoCoder _geoCoder;
        private readonly IDawaRepository _dawaRepo;
        private readonly IPrivilegeHelper _privilegeHelper;
        private readonly IAdHelper _adHelper;

        public AgendaPointController(
            IGenericRepository<AgendaPoint> agendaPointRepo,
            IGenericRepository<Meeting> meetingRepo,
            IGenericRepository<NextMeeting> nextMeetingRepo,
            IGenericRepository<Topic> topicRepo,
            IGenericRepository<PartnerDiscussion> partnerDiscussionRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IDateTime dateTime,
            IGenericRepository<AgendaPointDocument> documentRepository,
            IDocumentHelper documentHelper,
            IGeoCoder geoCoder,
            IDawaRepository dawaRepo,
            IPrivilegeHelper privilegeHelper,
            IAdHelper adHelper)
        {
            _agendaPointRepo = agendaPointRepo;
            _meetingRepo = meetingRepo;
            _nextMeetingRepo = nextMeetingRepo;
            _topicRepo = topicRepo;
            _partnerDiscussionRepository = partnerDiscussionRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _dateTime = dateTime;
            _documentRepository = documentRepository;
            _documentHelper = documentHelper;
            _geoCoder = geoCoder;
            _dawaRepo = dawaRepo;
            _privilegeHelper = privilegeHelper;
            _adHelper = adHelper;
        }

        // GET: AgendaPoint
        [PopulateTopics]
        [PopulateStreetTypes]
        [PopulateAgendaPointStatuses]
        public ViewResult Index()
        {
            //Update server cookie with new client values.
            Response.Cookies.Set(Request.Cookies.Get("excludePoints") ?? new HttpCookie("excludePoints", "true"));
            Response.Cookies.Set(Request.Cookies.Get("onlyNewPoints") ?? new HttpCookie("onlyNewPoints", "true"));

            ViewBag.nextMeeting = _nextMeetingRepo.AsQueryable()
                .Where( x => x.IsActive && !x.IsDeleted )
                .OrderBy( x => x.Id )
                .FirstOrDefault();

            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult AgendaPoint_Read([DataSourceRequest] DataSourceRequest request)
        {
            var isAdmin = _adHelper.InRole(UserRole.Admin);
            var userName = "userName";
            var agendaPoints = _agendaPointRepo.AsQueryable();
            agendaPoints = agendaPoints.Where(a => a.MeetingAgendaPoint == null || a.MeetingAgendaPoint.Number != 0);

            var excludeCookie = Request.Cookies.Get("excludePoints");
            var onlyNewPointscookie = Request.Cookies.Get("onlyNewPoints");

            if (excludeCookie?.Value == "true")
                agendaPoints = agendaPoints.Where(a => a.MeetingAgendaPoint == null); // Will remove any agendapoints attached to meetings
            if (onlyNewPointscookie?.Value == "true")
            {
                var oneYearBack = _dateTime.Now.AddYears(-1);
                agendaPoints = agendaPoints.Where(a => a.ModifiedOn > oneYearBack);
            }

            var vms = agendaPoints.ProjectTo<IndexAgendaPointViewModel>(new {isAdmin = isAdmin, userName = userName});
            var agendaPointsVm = vms.ToDataSourceResult(request);
            return Json(agendaPointsVm);
        }

        // GET: AgendaPoint/Create
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectLeaders]
        [PopulateLimitedAgendaPointStatuses]
        [PopulateStreetTypes]
        public ActionResult Create()
        {
            var vm = new CreateAgendaPointViewModel
            {
                // Set the street type to be 'public' by default.
                StreetTypeId = 0,
                AgendaPointStatusId = (int)AgendaPointStatusType.Draft,
                MeetingIds = new List<string>()
            };

            return View(vm);
        }

        // POST: AgendaPoint/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectLeaders]
        [PopulateLimitedAgendaPointStatuses]
        [PopulateStreetTypes]
        public ActionResult Create(CreateAgendaPointViewModel agendaPointViewModel)
        {
            if (!ModelState.IsValid)
            {
                if (agendaPointViewModel.MeetingIds == null)
                    agendaPointViewModel.MeetingIds = new List<string>();

                return View(agendaPointViewModel);
            }

            var agendaPoint = _mapper.Map<AgendaPoint>(agendaPointViewModel);

            // foreach guid in vm, get document from db and insert in created project
            var guidList = agendaPoint.Documents.Select(o => o.Guid).ToList();

            agendaPoint.Documents.Clear();
            foreach (var guid in guidList)
            {
                var guid1 = guid;
                var document = _documentRepository.Get(o => o.Guid == guid1).First();
                agendaPoint.Documents.Add(document);
            }

            agendaPoint.CreatedOn = _dateTime.Now;
            agendaPoint.CreatedBy = _adHelper.Name;

            agendaPoint.PreviousMeetings = CreateMeetingCollection(agendaPointViewModel.MeetingIds);

            var topic = _topicRepo.GetByKey(agendaPointViewModel.TopicId);
            if (topic != null)
            {
                agendaPoint.Topic = topic;
            }

            // Add spatial object
            agendaPoint.AddressLocation = _geoCoder.GetSpatialObject(agendaPointViewModel.AddressId);

            // Fetch address details from server and save to local DB
            var addr = _dawaRepo.GetAddress(agendaPointViewModel.AddressId);
            agendaPoint.AddressStreetName = addr.StreetName;
            agendaPoint.AddressNumber = addr.Number;
            agendaPoint.AddressZipCode = addr.ZipCode;
            agendaPoint.AddressCity = addr.City;

            _agendaPointRepo.Insert(agendaPoint);
            _unitOfWork.Save();

            return RedirectToAction("Details", new {id = agendaPoint.Id});
        }

        [HttpPost]
        public ActionResult CopyAgendaPoint(int id)
        {
            var agendaPoint = _agendaPointRepo.GetByKey(id);
            if (agendaPoint == null) { return HttpNotFound(); } // Guard clause

            var createdAgendaPoint = _agendaPointRepo.Create();

            // copying vm data
            createdAgendaPoint.AddressStreetName = agendaPoint.AddressStreetName;
            createdAgendaPoint.AddressNumber = agendaPoint.AddressNumber;
            createdAgendaPoint.AddressZipCode = agendaPoint.AddressZipCode;
            createdAgendaPoint.AddressCity = agendaPoint.AddressCity;
            createdAgendaPoint.AddressLocation = agendaPoint.AddressLocation;
            createdAgendaPoint.ProjectLeaderId = agendaPoint.ProjectLeaderId;
            createdAgendaPoint.TemporaryAddress = agendaPoint.TemporaryAddress;
            createdAgendaPoint.TemporaryAddressName = agendaPoint.TemporaryAddressName;
            createdAgendaPoint.StreetTypeId = agendaPoint.StreetTypeId;
            createdAgendaPoint.TopicId = agendaPoint.TopicId;
            createdAgendaPoint.Description = agendaPoint.Description;
            createdAgendaPoint.Agreement = agendaPoint.Agreement;
            createdAgendaPoint.AgendaPointStatusId = agendaPoint.AgendaPointStatusId;
            createdAgendaPoint.PreviousMeetings = agendaPoint.PreviousMeetings;
            createdAgendaPoint.Documents = new List<AgendaPointDocument>();
            createdAgendaPoint.Area = agendaPoint.Area;

            var createdCbmDiscussion = _partnerDiscussionRepository.Insert(new PartnerDiscussion
            {
                Description = agendaPoint.CbmDiscussion.Description,
                Discussed = agendaPoint.CbmDiscussion.Discussed,
                DiscussedWith = agendaPoint.CbmDiscussion.DiscussedWith
            });

            var createdCbaDiscussion = _partnerDiscussionRepository.Insert(new PartnerDiscussion
            {
                Description = agendaPoint.CbaDiscussion.Description,
                Discussed = agendaPoint.CbaDiscussion.Discussed,
                DiscussedWith = agendaPoint.CbaDiscussion.DiscussedWith
            });

            createdAgendaPoint.CbmDiscussion = createdCbmDiscussion;
            createdAgendaPoint.CbaDiscussion = createdCbaDiscussion;

            createdAgendaPoint.CreatedOn = _dateTime.Now;
            createdAgendaPoint.CreatedBy = _adHelper.Name;

            var topic = _topicRepo.GetByKey(agendaPoint.TopicId);
            if (topic != null)
            {
                createdAgendaPoint.Topic = topic;
            }

            _agendaPointRepo.Insert(createdAgendaPoint);
            _unitOfWork.Save();

            return RedirectToAction("Edit", new {id = createdAgendaPoint.Id, isCopy = true});
        }

        // GET: AgendaPoint/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var agendaPoint = _agendaPointRepo.GetByKey(id);
            if (agendaPoint == null) return HttpNotFound();

            SetCreatedModifiedInViewBag(agendaPoint);
            var agendaPointViewModel = _mapper.Map<DetailsAgendaPointViewModel>(agendaPoint);

            return View(agendaPointViewModel);
        }

        // GET: AgendaPoint/Edit/5
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectLeaders]
        [PopulateLimitedAgendaPointStatuses]
        [PopulateStreetTypes]
        public ActionResult Edit(int? id, bool isCopy = false)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var agendaPoint = _agendaPointRepo.GetByKey(id);
            if (agendaPoint == null) return HttpNotFound();
            if (agendaPoint.MeetingAgendaPoint != null) return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            SetCreatedModifiedInViewBag(agendaPoint);
            ViewBag.IsCopy = isCopy;
            var agendaPointViewModel = _mapper.Map<EditAgendaPointViewModel>(agendaPoint);

            agendaPointViewModel.MeetingIds = new List<string>();

            var meetingIds = new List<string>();
            foreach (var meeting in agendaPoint.PreviousMeetings)
            {
                meetingIds.Add(meeting.Id.ToString(CultureInfo.CurrentCulture));
            }

            agendaPointViewModel.MeetingIds = meetingIds;

            if (agendaPoint.ProjectLeader != null)
                agendaPointViewModel.TeamId = agendaPoint.ProjectLeader.TeamId;

            return View(agendaPointViewModel);
        }

        // POST: AgendaPoint/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PopulateTopics]
        [PopulateTeams]
        [PopulateProjectLeaders]
        [PopulateLimitedAgendaPointStatuses]
        [PopulateStreetTypes]
        public ActionResult Edit(EditAgendaPointViewModel agendaPointViewModel)
        {
            var agendaPoint = _agendaPointRepo.GetByKey(agendaPointViewModel.Id);
            if (agendaPoint == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
            {
                if (agendaPointViewModel.MeetingIds == null)
                    agendaPointViewModel.MeetingIds = new List<string>();

                if (agendaPointViewModel.Documents == null)
                    agendaPointViewModel.Documents = new List<AgendaPointDocumentViewModel>();

                SetCreatedModifiedInViewBag(agendaPoint);
                return View(agendaPointViewModel);
            }

            if (agendaPointViewModel.Documents != null && agendaPointViewModel.Documents.Any())
            {
                // get guid for document(s) to remove
                var originalGuidsList = agendaPoint.Documents.Select(o => o.Guid).ToList();
                var guidsList = agendaPointViewModel.Documents.Where(o => o.Guid != Guid.Empty).Select(o => o.Guid).ToList();
                var exceptGuids = originalGuidsList.Except(guidsList).ToList();

                // Delete removed documents
                foreach (var exceptGuid in exceptGuids)
                {
                    _documentRepository.DeleteByKey(exceptGuid);
                }
                DeleteFilesFromServer(exceptGuids.ToList());
            }

            // Write properties
            var previousLocation = agendaPoint.AddressLocation;
            _mapper.Map(agendaPointViewModel, agendaPoint);

            agendaPoint.ModifiedOn = _dateTime.Now;
            agendaPoint.ModifiedBy = _adHelper.Name;

            var documentList = agendaPoint.Documents.Where(o => o.Guid != Guid.Empty).ToList();

            agendaPoint.Documents.Clear();

            // foreach guid in vm, get document from db and insert in project
            foreach (var doc in documentList)
            {
                var guid1 = doc.Guid;
                var document = _documentRepository.Get(o => o.Guid == guid1).First();
                document.AgendaPointId = agendaPoint.Id;
                document.Comment = doc.Comment;

                // update document through documentRepository, because EF marks navigation properties of parent as unmodified
                _documentRepository.Update(document);

                agendaPoint.Documents.Add(document);
            }

            // Remove meetings from many-to-many relation
            if (agendaPointViewModel.RemovedMeetingIds != null)
            {
                foreach (
                    var removedMeetingId in
                        agendaPointViewModel.RemovedMeetingIds.Split(new[] { ' ' },
                            System.StringSplitOptions.RemoveEmptyEntries))
                {
                    int meetingIdInt;
                    if (!int.TryParse(removedMeetingId, out meetingIdInt)) continue;

                    var existingMeeting = agendaPoint.PreviousMeetings.FirstOrDefault(m => m.Id == meetingIdInt);

                    if (existingMeeting == null) continue;

                    agendaPoint.PreviousMeetings.Remove(existingMeeting);
                }
            }

            // Add new meetings to many-to-many relation
            foreach (var meeting in CreateMeetingCollection(agendaPointViewModel.MeetingIds))
            {
                agendaPoint.PreviousMeetings.Add(meeting);
            }

            // If address changed
            if (agendaPointViewModel.AddressId != Guid.Empty)
            {
                // Fetch address details from server and save to local DB
                var addr = _dawaRepo.GetAddress(agendaPointViewModel.AddressId);
                agendaPoint.AddressStreetName = addr.StreetName;
                agendaPoint.AddressNumber = addr.Number;
                agendaPoint.AddressZipCode = addr.ZipCode;
                agendaPoint.AddressCity = addr.City;

                // Update SP_GEOMETRY if previously null
                if (previousLocation == null)
                    agendaPoint.AddressLocation = _geoCoder.GetSpatialObject(agendaPointViewModel.AddressId);
            } else
            {
                agendaPoint.AddressLocation = previousLocation;
            }

            _agendaPointRepo.Update(agendaPoint);
            _unitOfWork.Save();

            return RedirectToAction("Details", new { id = agendaPoint.Id });
        }

        public ActionResult SaveUploadedFile()
        {
            var fileObj = _documentHelper.UploadFile(Request, Server);
            if (fileObj == null) return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

            // create document
            var document = new AgendaPointDocument
            {
                Comment = Request.Form["comment"],
                Guid = fileObj.Guid,
                FileName = fileObj.File.FileName,
                FileSize = fileObj.File.ContentLength,
                AgendaPointId = fileObj.ForeignKeyId
            };

            _documentRepository.Insert(document);
            _unitOfWork.Save();

            return Json(new { Guid = fileObj.Guid }); ;
        }

        // GET: AgendaPoint/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var agendaPoint = _agendaPointRepo.GetByKey(id);
            SetCreatedModifiedInViewBag(agendaPoint);
            var vm = _mapper.Map<DeleteAgendaPointViewModel>(agendaPoint);

            return View(vm);
        }

        // POST: AgendaPoint/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            // return to index if not allowed to delete
            if (!_privilegeHelper.IsAgendaPointDeleteableForCurrentUser(id))
                return RedirectToAction("Index");

            var documents = _documentRepository.Get(o => o.AgendaPointId == id).ToList();
            // delete all attached documents from server
            DeleteFilesFromServer(documents.Select(o => o.Guid));

            // delete doc from db
            foreach (var doc in documents)
            {
                _documentRepository.DeleteByKey(doc.Guid);
            }

            _agendaPointRepo.DeleteByKey(id);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// For downloading files
        /// </summary>
        /// <param name="guid">The file id.</param>
        /// <returns>The found file.</returns>
        public FileResult Download(Guid guid)
        {
            return _documentHelper.Download(Server, guid);
        }

        [HttpPost]
        public ActionResult DeleteFilesFromServer(IEnumerable<Guid> fileGuidsToDelete)
        {
            if (_documentHelper.DeleteFiles(fileGuidsToDelete, Server))
                return new HttpStatusCodeResult(HttpStatusCode.OK);

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #region Helpers
        /// <summary>
        /// Create collection of attached meetings from list of ids
        /// </summary>
        /// <param name="meetingIds">List of IDs as strings</param>
        /// <returns>Collection with attached meetings if any</returns>
        private ICollection<Meeting> CreateMeetingCollection(IEnumerable<string> meetingIds)
        {
            var meetingCollection = new List<Meeting>();

            if (meetingIds == null) return meetingCollection;

            foreach (var meetingId in meetingIds)
            {
                var meetingIdInt = int.Parse(meetingId, CultureInfo.CurrentCulture);
                var attachedMeeting = _meetingRepo.GetByKey(meetingIdInt);
                if (attachedMeeting == null) continue;

                meetingCollection.Add(attachedMeeting);
            }

            return meetingCollection;
        }
        #endregion
    }
}
