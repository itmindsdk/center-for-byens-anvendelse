﻿#region Head
// <copyright file="HomeController.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Web.Mvc;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Home;

namespace Presentation.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IAppSettings _appSettings;

        public HomeController(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public ActionResult Index()
        {
            var vm = new HomeViewModel
            {
                QuickAddAppendixToAgendaPoint = _appSettings.QuickAddAppendixToAgendaPoint,
                QuickAddAppendixToDrawing = _appSettings.QuickAddAppendixToDrawing,
                QuickAddAppendixToProject = _appSettings.QuickAddAppendixToProject,
                QuickCreateAgendaPoint = _appSettings.QuickCreateAgendaPoint,
                QuickCreateDrawing = _appSettings.QuickCreateDrawing,
                QuickCreateProject = _appSettings.QuickCreateProject
            };
            return View(vm);
        }
    }
}
