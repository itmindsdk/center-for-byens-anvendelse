﻿#region Head
// <copyright file="TeamController.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.DomainModel;
using Core.DomainServices;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Presentation.Web.Attributes;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Team;

namespace Presentation.Web.Controllers
{
    [AuthorizeRoles(UserRole.Admin)]
    public class TeamController : BaseController
    {
        private readonly IGenericRepository<Team> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        public TeamController(IGenericRepository<Team> repo, IUnitOfWork unitOfWork, IMapper mapper, IAdHelper adHelper)
        {
            _repo = repo;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _adHelper = adHelper;
        }

        // GET: Team
        public ActionResult Index()
        {
            return View();
        }

        // Til Kendo Grids Ajax kald.
        [HttpPost]
        public ActionResult Team_Read([DataSourceRequest] DataSourceRequest request)
        {
            var teams = _repo.AsQueryable().Where(t => !t.IsDeleted);

            var vms = teams.ProjectTo<IndexTeamViewModel>();

            var teamsVm = vms.ToDataSourceResult(request);
            return Json(teamsVm);
        }

        // GET: Team/Create
        public ActionResult Create()
        {
            var vm = new CreateTeamViewModel();
            return View(vm);
        }

        // POST: Team/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateTeamViewModel createTeamViewModel)
        {
            if (!ModelState.IsValid) return View(createTeamViewModel);

            var team = _mapper.Map<Team>(createTeamViewModel);

            team.CreatedOn = DateTime.Now;
            team.CreatedBy = _adHelper.Name;
            _repo.Insert(team);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Team/Edit/{id}
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var team = _repo.GetByKey(id);
            if (team == null || team.IsDeleted) return HttpNotFound();
            SetCreatedModifiedInViewBag(team);

            var vm = _mapper.Map<EditTeamViewModel>(team);

            return View(vm);
        }

        // POST: Team/Edit/{id}
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditTeamViewModel editTeamViewModel)
        {
            if (!ModelState.IsValid)
            {
                var logger = _repo.GetByKey(editTeamViewModel.Id) as Logger;
                SetCreatedModifiedInViewBag(logger);
                return View(editTeamViewModel);
            }

            var team = _mapper.Map<Team>(editTeamViewModel);
            team.ModifiedBy = _adHelper.Name;
            team.ModifiedOn = DateTime.Now;

            _repo.Update(team);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }

        // GET: Team/Delete/{id}
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var team = _repo.GetByKey(id);
            if (team == null || team.IsDeleted)
                return HttpNotFound();

            if (team.ProjectLeaders.Any(c => c.IsDeleted == false))
                return View("DeleteError");

            SetCreatedModifiedInViewBag(team);

            var vm = _mapper.Map<DeleteTeamViewModel>(team);

            return View(vm);
        }

        // POST: Team/Delete/{id}
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (id <= 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var team = _repo.GetByKey(id);
            if (team == null || team.IsDeleted)
                return HttpNotFound();

            if (team.ProjectLeaders.Any(c => c.IsDeleted == false))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            team.IsDeleted = true;
            _repo.Update(team);
            _unitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}