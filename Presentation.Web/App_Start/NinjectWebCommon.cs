#region Head
// <copyright file="NinjectWebCommon.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO.Abstractions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainServices;
using Infrastructure.DataAccess;
using Infrastructure.Dawa;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Mvc.FilterBindingSyntax;
using Presentation.Web.App_Start;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Presentation.Web.Models;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Presentation.Web.App_Start
{
    [SuppressMessage("Microsoft.Naming", "CA1704", Justification = "Ninject is the name of the package")]
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Ninject needs dependencies.")]
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<CbaContext>().ToSelf().InRequestScope();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            // Binding trick to avoid having to bind all usages of IGenericRepository. This binds them all!
            kernel.Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>));

            // PrivilegeHelper
            kernel.Bind<IPrivilegeHelper>().To<PrivilegeHelper>();

            // AdHelper
            kernel.Bind<IAdHelper>().To<AdHelper>();

            // AutoMapper
            kernel.Bind<IMapper>().ToMethod(context => Mapper.Instance);

            // System.IO.File
            kernel.Bind<IFileSystem>().To<FileSystem>();

            // DocumentHelper
            kernel.Bind<IDocumentHelper>().To<DocumentHelper>();

            // HttpContextHelper
            kernel.Bind<IHttpContext>().To<HttpContextHelper>();

            // GeoCoder
            kernel.Bind<IGeoCoder>().To<GeoCoder>();

            // System.DateTime wrapper
            kernel.Bind<IDateTime>().To<CustomDateTime>();

            kernel.Bind<IAppSettings>().To<AppSettings>();

            kernel.Bind<ISearchService>().To<SearchService>();
            kernel.Bind<IDawaRepository>().To<DawaRepository>();

            // Populating filters
            kernel.BindFilter<PopulateGroupsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateGroupsAttribute>();

            kernel.BindFilter<PopulateAdvisorsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateAdvisorsAttribute>();

            kernel.BindFilter<PopulateConstructionProgramsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateConstructionProgramsAttribute>();

            kernel.BindFilter<PopulateProjectCategoriesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateProjectCategoriesAttribute>();

            kernel.BindFilter<PopulateRequestorsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateRequestorsAttribute>();

            kernel.BindFilter<PopulateLocalCommunitiesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateLocalCommunitiesAttribute>();

            kernel.BindFilter<PopulateTopicsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateTopicsAttribute>();

            kernel.BindFilter<PopulateProjectLeadersFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateProjectLeadersAttribute>();

            kernel.BindFilter<PopulateInvitedPeopleFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateInvitedPeopleAttribute>();

            kernel.BindFilter<PopulateCouncilsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCouncilsAttribute>();

            kernel.BindFilter<PopulateCouncilsRawFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCouncilsRawAttribute>();

            kernel.BindFilter<PopulateDefaultPhrasesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateDefaultPhrasesAttribute>();

            kernel.BindFilter<PopulateDefaultAgreementPhrasesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateDefaultAgreementPhrasesAttribute>();

            kernel.BindFilter<PopulateDefaultConclusionPhrasesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateDefaultConclusionPhrasesAttribute>();

            kernel.BindFilter<PopulateCompaniesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCompaniesAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulateCompaniesAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulateTeamsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateTeamsAttribute>();

            kernel.BindFilter<PopulateEntrepreneursFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateEntrepreneursAttribute>();

            kernel.BindFilter<PopulateEntrepreneurCompaniesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateEntrepreneurCompaniesAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulateEntrepreneurCompaniesAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulateCouncilPrioritiesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateCouncilPrioritiesAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulateCouncilPrioritiesAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulateMunicipalityPrioritiesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateMunicipalityPrioritiesAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulateMunicipalityPrioritiesAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulateProjectStatusesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateProjectStatusesAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulateProjectStatusesAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulatePlanningsFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulatePlanningsAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulatePlanningsAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulateAgendaPointStatusesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateAgendaPointStatusesAttribute>()
                .WithConstructorArgumentFromActionAttribute<PopulateAgendaPointStatusesAttribute>
                ("showInactives", attribute => attribute.ShowInactives);

            kernel.BindFilter<PopulateLimitedAgendaPointStatusesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateLimitedAgendaPointStatusesAttribute>();

            kernel.BindFilter<PopulateStreetTypesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateStreetTypesAttribute>();

            kernel.BindFilter<PopulateInconveniencesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateInconveniencesAttribute>();

            kernel.BindFilter<PopulateTrafficInconveniencesFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<PopulateTrafficInconveniencesAttribute>();

            kernel.BindFilter<AddStatusExplanationUrlFilter>(FilterScope.Action, 0)
                .WhenActionMethodHas<AddStatusExplanationUrlAttribute>();
        }
    }
}
