﻿using System.Web.Optimization;

namespace Presentation.Web.App_Start
{
    public static class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            // CBA scripts and CSS
            bundles.Add(new ScriptBundle("~/bundles/cba").Include(
                "~/Scripts/select2.full.min.js",
                "~/Scripts/i18n/da.js",
                "~/Scripts/jquery.dirtyforms.js",
                "~/Scripts/autoNumeric/autoNumeric.js",
                "~/Scripts/autoNumeric/options.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/site.js",
                "~/Scripts/jquery.datetimepicker.js",
                "~/Scripts/dropzone/dropzone.js",
                "~/Scripts/partnerdiscussion.js"));

            bundles.Add(new StyleBundle("~/Content/cba").Include(
                "~/Content/css/select2.css",
                "~/Scripts/dropzone/dropzone.css",
                "~/Scripts/dropzone/basic.css",
                "~/Content/themes/base/all.css",
                "~/Content/jquery.datetimepicker.css",
                "~/Content/bootstrap.css",
                "~/Content/printing.css",
                "~/Content/site.css"));

            // Kendo
            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/2016.1.412/jszip.min.js",
                "~/Scripts/kendo/2016.1.412/kendo.all.min.js",
                "~/Scripts/kendo/2016.1.412/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo.modernizr.custom.js",
                "~/Scripts/kendo/2016.1.412/messages/kendo.messages.da-DK.min.js")
                );

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
                    "~/Content/kendo/2016.1.412/kendo.common-bootstrap.min.css",
                    "~/Content/kendo/2016.1.412/kendo.mobile.all.min.css",
                    "~/Content/kendo/2016.1.412/kendo.dataviz.min.css",
                    "~/Content/kendo/2016.1.412/kendo.bootstrap.min.css",
                    "~/Content/kendo/2016.1.412/kendo.dataviz.bootstrap.min.css",
                    "~/Content/kendo/KendoTableFixes.css"
                    )
                );
#if DEBUG
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
#endif
        }
    }
}
