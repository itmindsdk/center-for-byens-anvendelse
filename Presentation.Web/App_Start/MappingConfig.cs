﻿#region Head
// <copyright file="MappingConfig.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Presentation.Web.App_Start;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Advisor;
using Presentation.Web.Models.AgendaPoint;
using Presentation.Web.Models.Company;
using Presentation.Web.Models.Council;
using Presentation.Web.Models.DefaultPhrase;
using Presentation.Web.Models.Entrepreneur;
using Presentation.Web.Models.InvitedPerson;
using Presentation.Web.Models.ConstructionProgram;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.Models.Meeting;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.MeetingAgendaPoint;
using Presentation.Web.Models.Search;
using Presentation.Web.Models.Shared;
using Presentation.Web.Models.ProjectLeader;
using Presentation.Web.Models.Topic;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.NextMeeting;
using Presentation.Web.Models.PartnerDiscussion;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Requestor;
using Presentation.Web.Models.Team;
using CreateAgendaPointViewModel = Presentation.Web.Models.AgendaPoint.CreateAgendaPointViewModel;
using CreateTopicViewModel = Presentation.Web.Models.Topic.CreateTopicViewModel;
using System.Data.Entity.SqlServer;
using Presentation.Web.Models.Inconvenience;
using Presentation.Web.Models.TrafficInconvenience;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MappingConfig), "Start")]

namespace Presentation.Web.App_Start
{
    /// <summary>
    /// Automapper is used to map attributes from your viewmodels to your domain models and vice versa,
    /// depending on the functionality. Using the same names for attributes is highly recommended, but
    /// sometimes more complex configrations are needed, which can also be configured here.
    /// </summary>
    [SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Automapper mapping bootstrapper")]
    public static class MappingConfig
    {
        [SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Automapper mapping bootstrapper")]
        public static void Start()
        {
            // Write your AutoMapper configurations here.
            Mapper.Initialize(mapper =>
            {
                // ViewModel Mappings
                #region ProjectController
                mapper.CreateMap<Project, IndexProjectViewModel>()
                    .ForMember(vm => vm.AddressName, m => m.MapFrom(ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                    ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                    : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ReverseMap();
                mapper.CreateMap<ConstructionProgram, ConstructionProgramViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Project, CreateProjectViewModel>()
                    .ForMember(dest => dest.Budget, opts => opts.MapFrom(src => src.Budget.ToString("N0")))
                    .ReverseMap()
                    .ForMember(dest => dest.Budget, opts => opts.MapFrom(src => int.Parse(src.Budget.Replace(".", ""))));
                mapper.CreateMap<Project, EditProjectViewModel>()
                    .ForMember(vm => vm.AddressName, m => m.MapFrom(ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                    ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                    : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ForMember(dest => dest.Budget, opts => opts.MapFrom(src => src.Budget.ToString("N0")))
                    .ReverseMap()
                    .ForMember(dest => dest.Budget, opts => opts.MapFrom(src => int.Parse(src.Budget.Replace(".", ""))));
                mapper.CreateMap<ProjectDocument, ProjectDocumentViewModel>()
                    .ReverseMap();
                mapper.CreateMap<ProjectStatus, ProjectStatusViewModel>().ReverseMap();
                mapper.CreateMap<Planning, PlanningViewModel>().ReverseMap();
                mapper.CreateMap<CouncilPriority, CouncilPriorityViewModel>().ReverseMap();
                mapper.CreateMap<MunicipalityPriority, MunicipalityPriorityViewModel>().ReverseMap();
                #endregion
                #region TopicController
                mapper.CreateMap<Topic, IndexTopicViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Topic, CreateTopicViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Topic, EditTopicViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Topic, DeleteTopicViewModel>()
                    .ReverseMap();
                #endregion
                #region NextMeetingController
                mapper.CreateMap<NextMeeting, IndexNextMeetingViewModel>().ReverseMap();
                mapper.CreateMap<NextMeeting, CreateNextMeetingViewModel>().ReverseMap();
                mapper.CreateMap<NextMeeting, EditNextMeetingViewModel>().ReverseMap();
                mapper.CreateMap<NextMeeting, DeleteNextMeetingViewModel>().ReverseMap();
                #endregion
                #region AdvisorController
                mapper.CreateMap<Advisor, IndexAdvisorViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Advisor, CreateAdvisorViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Advisor, EditAdvisorViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Advisor, DeleteAdvisorViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Advisor, AdvisorDTO>()
                    .ReverseMap();
                #endregion
                #region GroupController
                mapper.CreateMap<Group, IndexGroupViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Group, CreateGroupViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Group, EditGroupViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Group, DeleteGroupViewModel>()
                    .ReverseMap();
                #endregion
                #region ProjectCategory
                mapper.CreateMap<ProjectCategory, CreateProjectCategoryViewModel>().ReverseMap();
                mapper.CreateMap<ProjectCategory, DeleteProjectCategoryViewModel>().ReverseMap();
                mapper.CreateMap<ProjectCategory, EditProjectCategoryViewModel>().ReverseMap();
                mapper.CreateMap<ProjectCategory, IndexProjectCategoryViewModel>().ReverseMap();
                #endregion
                #region Requestor
                mapper.CreateMap<Requestor, CreateRequestorViewModel>().ReverseMap();
                mapper.CreateMap<Requestor, DeleteRequestorViewModel>().ReverseMap();
                mapper.CreateMap<Requestor, EditRequestorViewModel>().ReverseMap();
                mapper.CreateMap<Requestor, IndexRequestorViewModel>().ReverseMap();
                #endregion
                #region ConstructionProgram
                mapper.CreateMap<ConstructionProgram, IndexConstructionProgramViewModel>()
                    .ReverseMap();
                mapper.CreateMap<ConstructionProgram, CreateConstructionProgramViewModel>()
                    .ReverseMap();
                mapper.CreateMap<ConstructionProgram, EditConstructionProgramViewModel>()
                    .ReverseMap();
                mapper.CreateMap<ConstructionProgram, DeleteConstructionProgramViewModel>()
                    .ReverseMap();
                #endregion
                #region LocalCommunity
                mapper.CreateMap<LocalCommunity, IndexLocalCommunityViewModel>()
                    .ReverseMap();
                mapper.CreateMap<LocalCommunity, CreateLocalCommunityViewModel>()
                    .ReverseMap();
                mapper.CreateMap<LocalCommunity, EditLocalCommunityViewModel>()
                    .ReverseMap();
                #endregion
                #region AgendaPointController
                mapper.CreateMap<Topic, Models.AgendaPoint.CreateTopicViewModel>()
                    .ReverseMap();
                mapper.CreateMap<AgendaPoint, DetailsAgendaPointViewModel>()
                    .ForMember(vm => vm.AddressName, m => m.MapFrom(ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                    ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                    : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ForMember(vm => vm.CanBeEdited, m => m.MapFrom(ap => ap.MeetingAgendaPoint == null))
                    .ReverseMap();
                mapper.CreateMap<AgendaPoint, CreateAgendaPointViewModel>()
                    .ReverseMap();
                mapper.CreateMap<AgendaPoint, EditAgendaPointViewModel>()
                    .ForMember(vm => vm.AddressName, m => m.MapFrom(ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                    ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                    : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ReverseMap();

                bool isAdmin = false; // To parametrize the mapping
                string userName = "";
                mapper.CreateMap<AgendaPoint, IndexAgendaPointViewModel>()
                    .ForMember(vm => vm.AddressName,
                        m =>
                            m.MapFrom(
                                ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0 ?
                                    ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity :
                                    ap.AddressStreetName + " " + ap.AddressNumber
                            )
                    )
                    .ForMember(vm => vm.MeetingIds, m => m.MapFrom(src => src.PreviousMeetings.Select(x => x.Id)))
                    .ForMember(vm => vm.HasDocuments, m => m.MapFrom(ap => ap.Documents.Any()))
                    .ForMember(vm => vm.CanBeEdited, m => m.MapFrom(ap => ap.MeetingAgendaPoint == null))
                    .ForMember(vm => vm.IsDeletableForCurrentUser,
                        m =>
                            m.MapFrom(
                                ap =>
                                    ap.MeetingAgendaPoint == null && (ap.CreatedBy == userName || isAdmin)
                                    ))
                    .ReverseMap();



                mapper.CreateMap<StreetType, StreetTypeViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Meeting, Models.AgendaPoint.DetailsMeetingViewModel>()
                    .ReverseMap();
                mapper.CreateMap<AgendaPointDocument, AgendaPointDocumentViewModel>()
                    .ReverseMap();
                mapper.CreateMap<ProjectDocument, MeetingDocumentViewModel>()
                    .ReverseMap();
                mapper.CreateMap<AgendaPoint, DeleteAgendaPointViewModel>()
                    .ForMember(vm => vm.AddressName, m => m.MapFrom(ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                    ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                    : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ReverseMap();
                mapper.CreateMap<MeetingAgendaPoint, AgendaPointMeetingAgendaPointViewModel>()
                    .ReverseMap();
                mapper.CreateMap<AgendaPointStatus, AgendaPointStatusViewModel>().ReverseMap();
                #endregion
                #region InvitedPerson
                mapper.CreateMap<InvitedPerson, InvitedPersonViewModel>().ReverseMap();
                #endregion
                #region DefaultPhrase

                mapper.CreateMap<DefaultPhraseType, DefaultPhraseTypeViewModel>().ProjectUsing(src =>
                    src == DefaultPhraseType.Agreement ? DefaultPhraseTypeViewModel.Agreement :
                    src == DefaultPhraseType.Conclusion ? DefaultPhraseTypeViewModel.Conclusion :
                    0
                    ); // Use of the ternary operator to get a switch statement like simple lambda expression that can be converted to Linq
                       // Minimally better than just type casting
                       // Equivalent to:
                       // switch(src) {
                       //  case DefaultPhraseType.Agreement: return DefaultPhraseTypeViewModel.Agreement;
                       //  case DefaultPhraseType.Conclusion: return DefaultPhraseTypeViewModel.Conclusion;
                       //  default: return 0;
                       // }

                mapper.CreateMap<DefaultPhrase, DefaultPhraseViewModel>().ReverseMap();
                mapper.CreateMap<DefaultPhrase, IndexDefaultPhraseViewModel>().ReverseMap();
                #endregion
                #region MeetingController

                mapper.CreateMap<AreaType, AreaTypeViewModel>().ProjectUsing(src =>
                    src == AreaType.Midt ? AreaTypeViewModel.Midt :
                    src == AreaType.Nord ? AreaTypeViewModel.Nord :
                    src == AreaType.Syd ? AreaTypeViewModel.Syd :
                    src == AreaType.Vest ? AreaTypeViewModel.Vest :
                    0
                    ); // Use of the ternary operator to get a switch statement like simple lambda expression that can be converted to Linq
                       // Minimally better than just type casting
                       // Equivalent to:
                       // switch(src) {
                       //  case AreaType.Midt: return AreaTypeViewModel.Midt;
                       //  case AreaType.Nord: return AreaTypeViewModel.Nord;
                       //  case AreaType.Syd: return AreaTypeViewModel.Syd;
                       //  case AreaType.Vest: return AreaTypeViewModel.Vest;
                       //  default: return 0;
                       // }

                const char invitedPeopleSeparator = '|';
                mapper.CreateMap<AgendaPoint, Models.Meeting.AgendaPointViewModel>()
                    .ForMember(vm => vm.AddressName,
                        m => m.MapFrom(ap => ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                            ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " +
                              ap.AddressCity
                            : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ForMember(dest => dest.IsAssignedToMeeting,
                        opts => opts.MapFrom(src => src.MeetingAgendaPoint != null))
                    .ForMember(dest => dest.IsAssignedBeforeChange,
                        opts => opts.MapFrom(src => src.MeetingAgendaPoint != null))
                        .ReverseMap();


                mapper.CreateMap<Meeting, CreateMeetingViewModel>()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                    .ReverseMap()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
                mapper.CreateMap<Meeting, IndexMeetingViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Meeting, Models.Meeting.DetailsMeetingViewModel>()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                    .ReverseMap()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
                mapper.CreateMap<Meeting, EditMeetingViewModel>()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                    .ReverseMap()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
                mapper.CreateMap<Meeting, CreateSummariesMeetingViewModel>()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => src.InvitedPeople.Split(new[] { invitedPeopleSeparator }, StringSplitOptions.RemoveEmptyEntries)))
                    .ReverseMap()
                    .ForMember(dest => dest.InvitedPeople, opts => opts.MapFrom(src => String.Join(invitedPeopleSeparator.ToString(CultureInfo.CurrentCulture), src.InvitedPeople)));
                mapper.CreateMap<Person, PersonViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Project, MeetingProjectViewModel>()
                    .ForMember(vm => vm.Topic, opts => opts.MapFrom(src => src.Topic.Name))
                    .ReverseMap();
                #endregion
                #region ProjectLeader
                mapper.CreateMap<IndexProjectLeaderViewModel, ProjectLeader>()
                    .ReverseMap();
                mapper.CreateMap<CreateProjectLeaderViewModel, ProjectLeader>()
                    .ReverseMap();
                mapper.CreateMap<EditProjectLeaderViewModel, ProjectLeader>()
                    .ReverseMap();
                mapper.CreateMap<ProjectLeaderDto, ProjectLeader>()
                    .ReverseMap();
                #endregion
                #region TeamController
                mapper.CreateMap<Team, IndexTeamViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Team, CreateTeamViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Team, EditTeamViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Team, DeleteTeamViewModel>()
                    .ReverseMap();
                #endregion
                #region Council
                mapper.CreateMap<IndexCouncilViewModel, Council>()
                    .ReverseMap();
                mapper.CreateMap<CreateCouncilViewModel, Council>()
                    .ReverseMap();
                mapper.CreateMap<EditCouncilViewModel, Council>()
                    .ReverseMap();
                mapper.CreateMap<CouncilDTO, Council>()
                    .ReverseMap();
                #endregion
                #region PartnerDiscussion
                mapper.CreateMap<CbaPartnerDiscussionViewModel, PartnerDiscussion>()
                    .ReverseMap();
                mapper.CreateMap<CbmPartnerDiscussionViewModel, PartnerDiscussion>()
                    .ReverseMap();
                #endregion
                #region MeetingAgendaPoint
                mapper.CreateMap<MeetingAgendaPoint, MeetingAgendaPointViewModel>()
                    .ReverseMap();
                mapper.CreateMap<MeetingAgendaPoint, MeetingAgendaPointSummariesViewModel>()
                    .ReverseMap();
                #endregion
                #region SearchController

                { // Small hack to get access to project and convert at once. Opened new scope to avoid messing with outside
                    var map = mapper.CreateMap<Int32, String>();
                    map.ProjectUsing(m => SqlFunctions.StringConvert((double)m));
                    map.ConvertUsing(m => m.ToString());
                }

                mapper.CreateMap<Project, ProjectViewModel>()
                    .ForMember(vm => vm.AddressName, m => m.MapFrom(ap =>
                    ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                    ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                    : ap.AddressStreetName + " " + ap.AddressNumber));

                mapper.CreateMap<AgendaPoint, Models.Search.AgendaPointViewModel>()
                    .ForMember(vm => vm.AddressName,
                        m => m.MapFrom(ap =>
                            ap.AddressZipCode != null && ap.AddressZipCode.Length > 0
                            ? ap.AddressStreetName + " " + ap.AddressNumber + ", " + ap.AddressZipCode + " " + ap.AddressCity
                            : ap.AddressStreetName + " " + ap.AddressNumber))
                    .ForMember(vm => vm.ResponsiblePersonName, opts => opts.MapFrom(src => src.Project.ProjectLeader.Name))
                    .ForMember(vm => vm.Status, opts => opts.MapFrom(src => src.AgendaPointStatus))
                    .ForMember(vm => vm.ProjectTopic, opts => opts.MapFrom(src => src.Project.Topic.Name))
                    .ForMember(vm => vm.MeetingIds, opts => opts.MapFrom(src => src.PreviousMeetings.Select(x => x.Id)))
                    .ForMember(vm => vm.HasDocuments, m => m.MapFrom(ap => ap.Documents.Count > 0)); // Null check not there, test if works

                mapper.CreateMap<DateSearchTypeViewModel, DateSearchType>();
                mapper.CreateMap<SearchViewModel, ProjectSearchParameters>();
                mapper.CreateMap<SearchViewModel, AgendaPointSearchParameters>();
                #endregion
                #region Logger
                mapper.CreateMap<Logger, LoggerViewModel>()
                    .ReverseMap();
                #endregion
                #region EntrepreneurController


                mapper.CreateMap<Entrepreneur, IndexEntrepreneurViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Entrepreneur, CreateEntrepreneurViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Entrepreneur, EditEntrepreneurViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Entrepreneur, DeleteEntrepreneurViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Entrepreneur, EntrepreneurDTO>()
                    .ReverseMap();
                #endregion
                #region Company
                mapper.CreateMap<CompanyType, CompanyTypeViewModel>().ProjectUsing(src =>
                    src == CompanyType.Advisor ? CompanyTypeViewModel.Advisor :
                    src == CompanyType.Entrepreneur ? CompanyTypeViewModel.Entrepreneur :
                    0
                    ); // Use of the ternary operator to get a switch statement like simple lambda expression that can be converted to Linq
                       // Minimally better than just type casting
                       // Equivalent to:
                       // switch(src) {
                       //  case CompanyType.Advisor: return CompanyTypeViewModel.Advisor;
                       //  case CompanyType.Entrepreneur: return CompanyTypeViewModel.Entrepreneur;
                       //  default: return 0;
                       // }
                mapper.CreateMap<Company, IndexCompanyViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Company, CreateCompanyViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Company, EditCompanyViewModel>()
                    .ReverseMap();
                mapper.CreateMap<Company, DeleteCompanyViewModel>()
                    .ReverseMap();
                #endregion

                mapper.CreateMap<Inconvenience, InconvenienceViewModel>()
                    .ReverseMap();

                mapper.CreateMap<TrafficInconvenience, TrafficInconvenienceViewModel>()
                    .ReverseMap();
            });
        }
    }
}
