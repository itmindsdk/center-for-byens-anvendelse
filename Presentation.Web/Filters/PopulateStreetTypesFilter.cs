﻿#region Head
// <copyright file="PopulateProjectStatusesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateStreetTypesAttribute : Attribute
    {
    }

    public class PopulateStreetTypesFilter : IActionFilter
    {
        private readonly IGenericRepository<StreetType> _streetTypeRepository;

        public PopulateStreetTypesFilter(IGenericRepository<StreetType> streetTypeRepository)
        {
            _streetTypeRepository = streetTypeRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedStreetType;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<StreetType> streetTypes;
            if (viewModel != null && viewModel.StreetTypeId != null)
            {
                streetTypes = _streetTypeRepository.Get(a => !a.IsDeleted || a.Id == viewModel.StreetTypeId);
            }
            else
            {
                streetTypes = _streetTypeRepository.Get(a => !a.IsDeleted);
            }

            var streetTypeListItems = new List<SelectListItem>();

            var orderedStreetTypeListItems = streetTypes.OrderBy(x => x.Id);
            foreach (var streetType in orderedStreetTypeListItems)
            {
                var streetTypeItem = new SelectListItem
                {
                    Text = streetType.Type,
                    Value = streetType.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (streetType.IsDeleted)
                {
                    streetTypeItem.Text += " (Slettet)";
                    viewBag.IsStreetTypeDeleted = true;
                }

                if (!streetType.IsActive)
                {
                    streetTypeItem.Text += " (Inaktiv)";
                }

                streetTypeListItems.Add(streetTypeItem);
            }

            viewBag.StreetTypeList = streetTypeListItems;
        }
    }
}
