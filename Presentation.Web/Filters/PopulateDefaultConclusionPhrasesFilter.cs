﻿using System;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateDefaultConclusionPhrasesAttribute : Attribute
    {
    }

    public class PopulateDefaultConclusionPhrasesFilter : IActionFilter
    {
        private readonly IGenericRepository<DefaultPhrase> _defaultPhraseRepository;

        public PopulateDefaultConclusionPhrasesFilter(IGenericRepository<DefaultPhrase> defaultPhraseRepository)
        {
            _defaultPhraseRepository = defaultPhraseRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var helper = new PopulateDefaultPhrasesHelper(_defaultPhraseRepository);
            var conclusionPhrases = helper.GetDefaultPhrases(DefaultPhraseType.Conclusion);
            filterContext.Controller.ViewBag.DefaultConclusionPhraseList = conclusionPhrases;
        }
    }
}