﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateInconveniencesAttribute : Attribute
    {
    }

    public class PopulateInconveniencesFilter : IActionFilter
    {
        private readonly IGenericRepository<Inconvenience> _inconvenienceRepository;

        public PopulateInconveniencesFilter(IGenericRepository<Inconvenience> inconvenienceRepository)
        {
            _inconvenienceRepository = inconvenienceRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedInconvenience;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Inconvenience> inconveniences;
            if (viewModel != null && viewModel.InconvenienceId != null)
            {
                inconveniences = _inconvenienceRepository.Get(i => !i.IsDeleted && i.IsActive || i.Id == viewModel.InconvenienceId);
            }
            else
            {
                inconveniences = _inconvenienceRepository.Get(i => !i.IsDeleted && i.IsActive);
            }

            var inconvenienceListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedInconveniences = inconveniences.OrderBy(x => x.Description, StringComparer.CurrentCulture);
            foreach (var inconvenience in orderedInconveniences)
            {
                var inconvenienceItem = new SelectListItem
                {
                    Text = inconvenience.Description,
                    Value = inconvenience.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (inconvenience.IsDeleted)
                {
                    inconvenienceItem.Text += " (Slettet)";
                    viewBag.IsInconvenienceDeleted = true;
                }

                if (!inconvenience.IsActive)
                {
                    inconvenienceItem.Text += " (Inaktiv)";
                }

                inconvenienceListItems.Add(inconvenienceItem);
            }

            viewBag.InconvenienceList = inconvenienceListItems;
        }
    }
}
