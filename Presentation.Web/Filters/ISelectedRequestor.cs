﻿
namespace Presentation.Web.Filters
{
    public interface ISelectedRequestor
    {
        int? RequestorId { get; set; }
    }
}
