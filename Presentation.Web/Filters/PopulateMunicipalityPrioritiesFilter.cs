﻿#region Head
// <copyright file="PopulateMunicipalityPrioritiesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
# endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
namespace Presentation.Web.Filters
{
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class PopulateMunicipalityPrioritiesAttribute : Attribute
	{
		public bool ShowInactives { get; set; } = false;
	}

	public class PopulateMunicipalityPrioritiesFilter : IActionFilter
	{
		private readonly IGenericRepository<MunicipalityPriority> _municipalityPriorityRepository;
		private readonly bool _showInactives;

		public PopulateMunicipalityPrioritiesFilter(IGenericRepository<MunicipalityPriority> municipalityPriorityRepository, bool showInactives)
		{
			_municipalityPriorityRepository = municipalityPriorityRepository;
			_showInactives = showInactives;
		}

		public void OnActionExecuting(ActionExecutingContext filterContext)
		{

		}

		[SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
		 SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
		public void OnActionExecuted(ActionExecutedContext filterContext)
		{
			var viewModel = filterContext.Controller.ViewData.Model as ISelectedMunicipalityPriority;
			var viewBag = filterContext.Controller.ViewBag;

			IEnumerable<MunicipalityPriority> municipalityPriorities;
			if (viewModel != null && viewModel.MunicipalityPriorityId != null)
			{
				municipalityPriorities = _municipalityPriorityRepository.Get(a => (!a.IsDeleted && (a.IsActive || _showInactives)) || a.Id == viewModel.MunicipalityPriorityId);
			}
			else
			{ 
				municipalityPriorities = _municipalityPriorityRepository.Get(a => a.IsDeleted == false && (a.IsActive || _showInactives));
			}

			var municipalityPriorityListItems = new List<SelectListItem>();
			var orderedMunicipalityProprities = municipalityPriorities.OrderBy(x => x.Id);

			foreach (var municipalityPriority in orderedMunicipalityProprities)
			{
			    var municipalityPriorityItem = new SelectListItem
			    {
			        Text = municipalityPriority.Priority,
			        Value = municipalityPriority.Id.ToString(CultureInfo.CurrentCulture)
			    };

				if (municipalityPriority.IsDeleted)
				{
					municipalityPriorityItem.Text += " (Slettet)"; ;
				}

				if (!municipalityPriority.IsActive)
				{
					municipalityPriorityItem.Text += " (Inaktiv)";
				}

				municipalityPriorityListItems.Add(municipalityPriorityItem);
			}

			viewBag.MunicipalityPriorityList = municipalityPriorityListItems;
		}
	}
}
