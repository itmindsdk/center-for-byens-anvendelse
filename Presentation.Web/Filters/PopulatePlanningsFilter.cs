﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulatePlanningsAttribute : Attribute
    {
        public bool ShowInactives { get; set; } = false;
    }
    public class PopulatePlanningsFilter : IActionFilter
    {
        private readonly IGenericRepository<Planning> _planningRepository;
        private readonly bool _showInactives;

        public PopulatePlanningsFilter(IGenericRepository<Planning> planningRepository, bool showInactives)
        {
            _planningRepository = planningRepository;
            _showInactives = showInactives;
        }
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedPlanning;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Planning> plannings;
            if (viewModel != null && viewModel.PlanningId != null)
            {
                plannings = _planningRepository.Get(a => (!a.IsDeleted && (a.IsActive || _showInactives)) || a.Id == viewModel.PlanningId);
            }
            else
            {
                plannings = _planningRepository.Get(a => a.IsDeleted == false && (a.IsActive || _showInactives));
            }

            var planningListItems = new List<SelectListItem>();
            var orderedPlannings = plannings.OrderBy(x => x.Id);

            foreach (var planning in orderedPlannings)
            {
                var planningItem = new SelectListItem
                {
                    Text = planning.Plan,
                    Value = planning.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (planning.IsDeleted)
                {
                    planningItem.Text += " (Slettet)";
                }

                if (!planning.IsActive)
                {
                    planningItem.Text += " (Inaktiv)";
                }

                planningListItems.Add(planningItem);
            }

            viewBag.PlanningList = planningListItems;
        }
    }
}