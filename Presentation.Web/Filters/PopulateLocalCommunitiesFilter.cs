﻿#region Head
// <copyright file="PopulateLocalCommunitiesFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateLocalCommunitiesAttribute : Attribute
    {
    }

    public class PopulateLocalCommunitiesFilter : IActionFilter
    {
        private readonly IGenericRepository<LocalCommunity> _localCommunityRepository;

        public PopulateLocalCommunitiesFilter(IGenericRepository<LocalCommunity> localCommunityRepository)
        {
            _localCommunityRepository = localCommunityRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedLocalCommunity;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<LocalCommunity> localCommunities;
            if (viewModel != null && viewModel.LocalCommunityId != null)
            {
                localCommunities = _localCommunityRepository.Get(a => (!a.IsDeleted && a.IsActive) || a.Id == viewModel.LocalCommunityId);
            }
            else
            {
                localCommunities = _localCommunityRepository.Get(a => a.IsDeleted == false && a.IsActive);
            }

            var localCommunityListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedLocalCommunities = localCommunities.OrderBy(x => x.Name, StringComparer.CurrentCulture);
            foreach (var localCommunity in orderedLocalCommunities)
            {
                var localCommunityItem = new SelectListItem
                {
                    Text = localCommunity.Name,
                    Value = localCommunity.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (localCommunity.IsDeleted)
                {
                    localCommunityItem.Text += " (Slettet)";
                    viewBag.IsLocalCommunityDeleted = true;
                }

                localCommunityListItems.Add(localCommunityItem);
            }

            viewBag.LocalCommunityList = localCommunityListItems;
        }
    }
}
