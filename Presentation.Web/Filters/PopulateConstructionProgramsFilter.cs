﻿#region Head
// <copyright file="PopulateConstructionProgramsFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateConstructionProgramsAttribute : Attribute
    {
    }

    public class PopulateConstructionProgramsFilter : IActionFilter
    {
        private readonly IGenericRepository<ConstructionProgram> _constructionProgramRepository;

        public PopulateConstructionProgramsFilter(IGenericRepository<ConstructionProgram> constructionProgramRepository)
        {
            _constructionProgramRepository = constructionProgramRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedConstructionProgram;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<ConstructionProgram> constructionPrograms;
            if (viewModel != null && viewModel.ConstructionProgramId != null)
            {
                constructionPrograms = _constructionProgramRepository.Get(a => (!a.IsDeleted && a.IsActive) || a.Id == viewModel.ConstructionProgramId);
            }
            else
            {
                constructionPrograms = _constructionProgramRepository.Get(a => a.IsDeleted == false && a.IsActive);
            }

            var constructionProgramListItems = new List<SelectListItem>();

            // this doesn't have to be sorted in memory, but the other poopulate filters do. So for consistency we do the same here.
            var ordererdConstructionPrograms = constructionPrograms.OrderBy(x => x.YearOfProjectInvolvement);
            foreach (var constructionProgram in ordererdConstructionPrograms)
            {
                var constructionProgramItem = new SelectListItem
                {
                    Text = constructionProgram.YearOfProjectInvolvement.ToString(CultureInfo.CurrentCulture),
                    Value = constructionProgram.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (constructionProgram.IsDeleted)
                {
                    constructionProgramItem.Text += " (Slettet)";
                    viewBag.IsConstructionProgramDeleted = true;
                }

                constructionProgramListItems.Add(constructionProgramItem);
            }

            viewBag.ConstructionProgramList = constructionProgramListItems;
        }
    }
}
