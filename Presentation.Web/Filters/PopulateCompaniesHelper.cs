﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    public class PopulateCompaniesHelper
    {
        private readonly IGenericRepository<Company> _companyRepository;

        public PopulateCompaniesHelper(IGenericRepository<Company> companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public IList<SelectListItem> GetCompanies(CompanyType type, bool showInactives)
        {
            var companies = _companyRepository.Get(a => a.IsDeleted == false && (a.IsActive || showInactives) && a.Type == type);

            var companyListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedCompanies = companies.OrderBy(x => x.Name, StringComparer.CurrentCulture);

            foreach (var company in orderedCompanies)
            {
                var companyItem = new SelectListItem
                {
                    Text = company.Name,
                    Value = company.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (!company.IsActive)
                {
                    companyItem.Text += " (Inaktiv)";
                }

                companyListItems.Add(companyItem);
            }

            return companyListItems;
        }
    }
}