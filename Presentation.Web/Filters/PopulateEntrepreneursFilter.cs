﻿#region Head
// <copyright file="PopulateEntrepreneursFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateEntrepreneursAttribute : Attribute
    {
    }

    public class PopulateEntrepreneursFilter : IActionFilter
    {
        private readonly IGenericRepository<Entrepreneur> _entrepreneurRepository;

        public PopulateEntrepreneursFilter(IGenericRepository<Entrepreneur> entrepreneurRepository)
        {
            _entrepreneurRepository = entrepreneurRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedEntrepreneur;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Entrepreneur> entrepreneurs;
            if (viewModel != null && viewModel.EntrepreneurId != null)
            {
                entrepreneurs = _entrepreneurRepository.Get(a => (!a.IsDeleted && a.IsActive && a.Company.IsActive) || a.Id == viewModel.EntrepreneurId);
            }
            else
            {
                entrepreneurs = _entrepreneurRepository.Get(a => !a.IsDeleted && a.IsActive && a.Company.IsActive);
            }

            var entrepreneurListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            // WARN make sure this order matches the order in AdvisorController/AdvisorsWithCompanyId and visa versa
            var orderedEntrepreneurs = entrepreneurs.OrderBy(x => x.ResponsiblePersonName, StringComparer.CurrentCulture);
            foreach (var entrepreneur in orderedEntrepreneurs)
            {
                var entrepreneurItem = new SelectListItem
                {
                    Text = entrepreneur.ResponsiblePersonName,
                    Value = entrepreneur.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (entrepreneur.IsDeleted)
                {
                    entrepreneurItem.Text += " (Slettet)";
                    viewBag.IsEntrepreneurDeleted = true;
                }

                entrepreneurListItems.Add(entrepreneurItem);
            }

            viewBag.EntrepreneurList = entrepreneurListItems;
        }
    }
}
