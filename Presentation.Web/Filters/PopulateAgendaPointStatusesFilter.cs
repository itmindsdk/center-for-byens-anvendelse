﻿#region Head
// <copyright file="PopulateAgendaPointStatusesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateAgendaPointStatusesAttribute : Attribute
    {
        public bool ShowInactives { get; set; } = false;
    }

    public class PopulateAgendaPointStatusesFilter : IActionFilter
    {
        private readonly IGenericRepository<AgendaPointStatus> _agendaPointStatusRepository;
        private readonly bool _showInactives;

        public PopulateAgendaPointStatusesFilter(IGenericRepository<AgendaPointStatus> agendaPointStatusRepository, bool showInactives)
        {
            _agendaPointStatusRepository = agendaPointStatusRepository;
            _showInactives = showInactives;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedAgendaPointStatus;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<AgendaPointStatus> agendaPointStatuses;
            if (viewModel != null && viewModel.AgendaPointStatusId != null)
            {
                agendaPointStatuses = _agendaPointStatusRepository.Get(a => !a.IsDeleted || a.Id == viewModel.AgendaPointStatusId);
            }
            else
            {
                agendaPointStatuses = _agendaPointStatusRepository.Get(a => !a.IsDeleted);
            }

            var agendaPointStatusListItems = new List<SelectListItem>();

            var orderedAgendaPointStatuses = agendaPointStatuses.OrderBy(x => x.Id);
            foreach (var agendaPointStatus in orderedAgendaPointStatuses)
            {
                var agendaPointStatusItem = new SelectListItem
                {
                    Text = agendaPointStatus.Status,
                    Value = agendaPointStatus.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (agendaPointStatus.IsDeleted)
                {
                    agendaPointStatusItem.Text += " (Slettet)";
                }

                if (!agendaPointStatus.IsActive)
                {
                    agendaPointStatusItem.Text += " (Inaktiv)";
                }

                agendaPointStatusListItems.Add(agendaPointStatusItem);
            }

            viewBag.AgendaPointStatusList = agendaPointStatusListItems;
        }
    }
}
