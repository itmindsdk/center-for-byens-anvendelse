﻿#region Head
// <copyright file="PopulateCompaniesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateEntrepreneurCompaniesAttribute : Attribute
    {
        public bool ShowInactives { get; set; } = false;
    }

    public class PopulateEntrepreneurCompaniesFilter : IActionFilter
    {
        private readonly IGenericRepository<Company> _companyRepository;
        private readonly bool _showInactives;

        public PopulateEntrepreneurCompaniesFilter(IGenericRepository<Company> companyRepository, bool showInactives)
        {
            _companyRepository = companyRepository;
            _showInactives = showInactives;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var helper = new PopulateCompaniesHelper(_companyRepository);
            var entrepreneurCompanies = helper.GetCompanies(CompanyType.Entrepreneur, _showInactives);
            filterContext.Controller.ViewBag.EntrepreneurCompanyList = entrepreneurCompanies;
        }
    }
}
