﻿#region Head
// <copyright file="PopulateTeamsFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateTeamsAttribute : Attribute
    {
    }

    public class PopulateTeamsFilter : IActionFilter
    {
        private readonly IGenericRepository<Team> _teamRepository;

        public PopulateTeamsFilter(IGenericRepository<Team> teamRepository)
        {
            _teamRepository = teamRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedTeam;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Team> teams;
            if (viewModel != null && viewModel.TeamId != null)
            {
                teams = _teamRepository.Get(a => (!a.IsDeleted && a.IsActive) || a.Id == viewModel.TeamId);
            }
            else
            {
                teams = _teamRepository.Get(a => a.IsDeleted == false && a.IsActive);
            }

            var teamListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedTeams = teams.OrderBy(x => x.Name, StringComparer.CurrentCulture);
            foreach (var team in orderedTeams)
            {
                var teamItem = new SelectListItem
                {
                    Text = team.Name,
                    Value = team.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (team.IsDeleted)
                {
                    teamItem.Text += " (Slettet)";
                    viewBag.IsTeamDeleted = true;
                }

                teamListItems.Add(teamItem);
            }

            viewBag.TeamList = teamListItems;
        }
    }
}
