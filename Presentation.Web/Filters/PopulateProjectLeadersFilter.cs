﻿#region Head
// <copyright file="PopulateProjectLeadersFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateProjectLeadersAttribute : Attribute
    {
    }

    public class PopulateProjectLeadersFilter : IActionFilter
    {
        private readonly IGenericRepository<ProjectLeader> _projectLeaderRepository;

        public PopulateProjectLeadersFilter(IGenericRepository<ProjectLeader> projectLeaderRepository)
        {
            _projectLeaderRepository = projectLeaderRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedProjectLeader;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<ProjectLeader> projectLeaders;
            if (viewModel != null && viewModel.ProjectLeaderId != null)
            {
                projectLeaders = _projectLeaderRepository.Get(a => (!a.IsDeleted && a.IsActive) || a.Id == viewModel.ProjectLeaderId);
            }
            else
            {
                projectLeaders = _projectLeaderRepository.Get(a => a.IsDeleted == false && a.IsActive);
            }

            var projectLeaderListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedProjectLeaders = projectLeaders.OrderBy(x => x.Name, StringComparer.CurrentCulture);
            foreach (var projectLeader in orderedProjectLeaders)
            {
                var projectLeaderItem = new SelectListItem
                {
                    Text = projectLeader.Name,
                    Value = projectLeader.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (projectLeader.IsDeleted)
                {
                    projectLeaderItem.Text += " (Slettet)";
                    viewBag.IsProjectLeaderDeleted = true;
                }

                projectLeaderListItems.Add(projectLeaderItem);
            }

            viewBag.ProjectLeaderList = projectLeaderListItems;
        }
    }
}
