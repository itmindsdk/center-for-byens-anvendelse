﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateTrafficInconveniencesAttribute : Attribute
    {
    }

    public class PopulateTrafficInconveniencesFilter : IActionFilter
    {
        private readonly IGenericRepository<TrafficInconvenience> _trafficInconvenienceRepository;

        public PopulateTrafficInconveniencesFilter(IGenericRepository<TrafficInconvenience> trafficInconvenienceRepository)
        {
            _trafficInconvenienceRepository = trafficInconvenienceRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedInconvenience;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<TrafficInconvenience> trafficInconveniences;
            if (viewModel != null && viewModel.InconvenienceId != null)
            {
                trafficInconveniences = _trafficInconvenienceRepository.Get(i => !i.IsDeleted && i.IsActive || i.Id == viewModel.InconvenienceId);
            }
            else
            {
                trafficInconveniences = _trafficInconvenienceRepository.Get(i => !i.IsDeleted && i.IsActive);
            }

            var trafficInconvenienceListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedTrafficInconveniences = trafficInconveniences.OrderBy(x => x.Description, StringComparer.CurrentCulture);
            foreach (var inconvenience in orderedTrafficInconveniences)
            {
                var inconvenienceItem = new SelectListItem
                {
                    Text = inconvenience.Description,
                    Value = inconvenience.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (inconvenience.IsDeleted)
                {
                    inconvenienceItem.Text += " (Slettet)";
                    viewBag.IsTrafficInconvenienceDeleted = true;
                }

                if (!inconvenience.IsActive)
                {
                    inconvenienceItem.Text += " (Inaktiv)";
                }

                trafficInconvenienceListItems.Add(inconvenienceItem);
            }

            viewBag.TrafficInconvenienceList = trafficInconvenienceListItems;
        }
    }
}
