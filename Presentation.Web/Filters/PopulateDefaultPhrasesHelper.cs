﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    public class PopulateDefaultPhrasesHelper
    {
        private readonly IGenericRepository<DefaultPhrase> _defaultPhraseRepository;

        public PopulateDefaultPhrasesHelper(IGenericRepository<DefaultPhrase> defaultPhraseRepository)
        {
            _defaultPhraseRepository = defaultPhraseRepository;
        }

        public IList<SelectListItem> GetDefaultPhrases(DefaultPhraseType type)
        {
            var defaultPhrases = _defaultPhraseRepository.Get(a => a.IsDeleted == false && a.IsActive && a.Type == type);

            var defaultPhraseListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedDefaultPhrases = defaultPhrases.OrderBy(x => x.Phrase, StringComparer.CurrentCulture);
            foreach (var defaultPhrase in orderedDefaultPhrases)
            {
                var defaultPhraseItem = new SelectListItem
                {
                    Text = defaultPhrase.Phrase,
                    Value = defaultPhrase.Id.ToString(CultureInfo.CurrentCulture)
                };

                defaultPhraseListItems.Add(defaultPhraseItem);
            }

            return defaultPhraseListItems;
        }
    }
}