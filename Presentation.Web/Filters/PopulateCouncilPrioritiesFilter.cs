﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateCouncilPrioritiesAttribute : Attribute
    {
        public bool ShowInactives { get; set; } = false;
    }
    public class PopulateCouncilPrioritiesFilter : IActionFilter
    {
        private readonly IGenericRepository<CouncilPriority> _councilPriorityRepository;
        private readonly bool _showInactives;

        public PopulateCouncilPrioritiesFilter(IGenericRepository<CouncilPriority> councilPriorityRepository,
            bool showInactives)
        {
            _councilPriorityRepository = councilPriorityRepository;
            _showInactives = showInactives;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedCouncilPriority;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<CouncilPriority> councilPriorities;
            if (viewModel != null && viewModel.CouncilPriorityId != null)
            {
                councilPriorities = _councilPriorityRepository.Get(a => (!a.IsDeleted && (a.IsActive || _showInactives)) || a.Id == viewModel.CouncilPriorityId);
            }
            else
            {
                councilPriorities = _councilPriorityRepository.Get(a => a.IsDeleted == false && (a.IsActive || _showInactives));
            }

            var councilPriorityListItems = new List<SelectListItem>();
            var orderedCouncilPriorities = councilPriorities.OrderBy(x => x.Id);

            foreach (var councilPriority in orderedCouncilPriorities)
            {
                var councilPriorityItem = new SelectListItem
                {
                    Text = councilPriority.Priority,
                    Value = councilPriority.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (councilPriority.IsDeleted)
                {
                    councilPriorityItem.Text += " (Slettet)";
                }

                if (!councilPriority.IsActive)
                {
                    councilPriorityItem.Text += " (Inaktiv)";
                }

                councilPriorityListItems.Add(councilPriorityItem);
            }

            viewBag.CouncilPriorityList = councilPriorityListItems;
        }
    }
}