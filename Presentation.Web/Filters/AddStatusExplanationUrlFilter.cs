﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Presentation.Web.Helpers;

namespace Presentation.Web.Filters
{
    public class AddStatusExplanationUrlAttribute : Attribute
    {
    }
    public class AddStatusExplanationUrlFilter : IActionFilter
    {
        private readonly IAppSettings _appSettings;

        public AddStatusExplanationUrlFilter(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.Controller.ViewBag.StatusExplanationURL = _appSettings.StatusExplanationFileName;
        }
    }
}