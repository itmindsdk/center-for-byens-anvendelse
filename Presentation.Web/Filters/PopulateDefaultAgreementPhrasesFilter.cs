﻿using System;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateDefaultAgreementPhrasesAttribute : Attribute
    {
    }

    public class PopulateDefaultAgreementPhrasesFilter : IActionFilter
    {
        private readonly IGenericRepository<DefaultPhrase> _defaultPhraseRepository;

        public PopulateDefaultAgreementPhrasesFilter(IGenericRepository<DefaultPhrase> defaultPhraseRepository)
        {
            _defaultPhraseRepository = defaultPhraseRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var helper = new PopulateDefaultPhrasesHelper(_defaultPhraseRepository);
            var agreementPhrases = helper.GetDefaultPhrases(DefaultPhraseType.Agreement);
            filterContext.Controller.ViewBag.DefaultAgreementPhraseList = agreementPhrases;
        }
    }
}