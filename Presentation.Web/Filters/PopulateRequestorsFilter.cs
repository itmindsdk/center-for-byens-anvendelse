﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateRequestorsAttribute : Attribute
    {
        public bool ShowInactives { get; set; } = false;
    }
    public class PopulateRequestorsFilter : IActionFilter
    {
        private readonly IGenericRepository<Requestor> _requestorRepository;

        public PopulateRequestorsFilter(IGenericRepository<Requestor> requestoRepository)
        {
            _requestorRepository = requestoRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedRequestor;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Requestor> requestors;
            if (viewModel != null && viewModel.RequestorId != null)
            {
                requestors = _requestorRepository.Get(a => !a.IsDeleted || a.Id == viewModel.RequestorId);
            }
            else
            {
                requestors = _requestorRepository.Get(a => !a.IsDeleted);
            }

            var requestorListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedRequestors = requestors.OrderBy(x => x.Name, StringComparer.CurrentCulture);
            foreach (var requestor in orderedRequestors)
            {
                var requestorItem = new SelectListItem
                {
                    Text = requestor.Name,
                    Value = requestor.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (requestor.IsDeleted)
                {
                    requestorItem.Text += " (Slettet)";
                    viewBag.IsRequestorDeleted = true;
                }

                if (!requestor.IsActive)
                {
                    requestorItem.Text += " (Inaktiv)";
                }

                requestorListItems.Add(requestorItem);
            }

            viewBag.RequestorList = requestorListItems;
        }
    }
}