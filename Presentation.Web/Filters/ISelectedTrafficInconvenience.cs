﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Web.Filters
{
    public interface ISelectedTrafficInconvenience
    {
        int? TrafficInconvenienceId { get; set; }
    }
}
