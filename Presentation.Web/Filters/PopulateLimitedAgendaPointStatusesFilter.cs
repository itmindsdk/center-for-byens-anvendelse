﻿#region Head
// <copyright file="PopulateLimitedAgendaPointStatusesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateLimitedAgendaPointStatusesAttribute : Attribute
    {
    }

    public class PopulateLimitedAgendaPointStatusesFilter : IActionFilter
    {
        private readonly IGenericRepository<AgendaPointStatus> _agendaPointStatusRepository;

        public PopulateLimitedAgendaPointStatusesFilter(IGenericRepository<AgendaPointStatus> agendaPointStatusRepository)
        {
            _agendaPointStatusRepository = agendaPointStatusRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedAgendaPointStatus;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<AgendaPointStatus> agendaPointStatuses;
            if (viewModel != null && viewModel.AgendaPointStatusId != null)
            {
                // In the limited model, do not display options 'approved' and 'disproved' 
                agendaPointStatuses = _agendaPointStatusRepository.Get(a => (a.Id != (int)AgendaPointStatusType.Approved && a.Id != (int)AgendaPointStatusType.Disapproved)
                                                                             && (!a.IsDeleted || a.Id == viewModel.AgendaPointStatusId));
            }
            else
            {
                // In the limited model, do not display options 'approved' and 'disproved' 
                agendaPointStatuses = _agendaPointStatusRepository.Get(a => (a.Id != (int)AgendaPointStatusType.Approved && a.Id != (int)AgendaPointStatusType.Disapproved)
                                                                             && !a.IsDeleted);
            }

            var agendaPointStatusListItems = new List<SelectListItem>();

            var orderedAgendaPointStatuses = agendaPointStatuses.OrderBy(x => x.Id);
            foreach (var agendaPointStatus in orderedAgendaPointStatuses)
            {
                var agendaPointStatusItem = new SelectListItem
                {
                    Text = agendaPointStatus.Status,
                    Value = agendaPointStatus.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (agendaPointStatus.IsDeleted)
                {
                    agendaPointStatusItem.Text += " (Slettet)";
                    viewBag.IsAgendaPointStatusDeleted = true;
                }

                if (!agendaPointStatus.IsActive)
                {
                    agendaPointStatusItem.Text += " (Inaktiv)";
                }

                agendaPointStatusListItems.Add(agendaPointStatusItem);
            }

            viewBag.AgendaPointStatusList = agendaPointStatusListItems;
        }
    }
}
