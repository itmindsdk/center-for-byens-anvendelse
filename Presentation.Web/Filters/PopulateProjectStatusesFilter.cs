﻿#region Head
// <copyright file="PopulateProjectStatusesFilter.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateProjectStatusesAttribute : Attribute
    {
        public bool ShowInactives { get; set; } = false;
    }

    public class PopulateProjectStatusesFilter : IActionFilter
    {
        private readonly IGenericRepository<ProjectStatus> _projectStatusRepository;
        private readonly bool _showInactives;

        public PopulateProjectStatusesFilter(IGenericRepository<ProjectStatus> projectStatusRepository, bool showInactives)
        {
            _projectStatusRepository = projectStatusRepository;
            _showInactives = showInactives;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedProjectStatus;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<ProjectStatus> projectStatuses;
            if (viewModel != null && viewModel.ProjectStatusId != null)
            {
                projectStatuses = _projectStatusRepository.Get(a => !a.IsDeleted || a.Id == viewModel.ProjectStatusId);
            }
            else
            {
                projectStatuses = _projectStatusRepository.Get(a => !a.IsDeleted);
            }

            var projectStatusListItems = new List<SelectListItem>();

            var orderedProjectStatuses = projectStatuses.OrderBy(x => x.Id);
            foreach (var projectStatus in orderedProjectStatuses)
            {
                var projectStatusItem = new SelectListItem
                {
                    Text = projectStatus.Status,
                    Value = projectStatus.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (projectStatus.IsDeleted)
                {
                    projectStatusItem.Text += " (Slettet)";;
                }

                if (!projectStatus.IsActive)
                {
                    projectStatusItem.Text += " (Inaktiv)";
                }

                projectStatusListItems.Add(projectStatusItem);
            }

            viewBag.ProjectStatusList = projectStatusListItems;
        }
    }
}
