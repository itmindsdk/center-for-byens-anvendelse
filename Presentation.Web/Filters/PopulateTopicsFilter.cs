﻿#region Head
// <copyright file="PopulateTopicsFilter.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PopulateTopicsAttribute : Attribute
    {
    }

    public class PopulateTopicsFilter : IActionFilter
    {
        private readonly IGenericRepository<Topic> _topicRepository;

        public PopulateTopicsFilter(IGenericRepository<Topic> topicRepository)
        {
            _topicRepository = topicRepository;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }

        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.Mvc.SelectListItem.set_Text(System.String)", Justification = "Generated view output."),
         SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Slettet", Justification = "Danish word.")]
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model as ISelectedTopic;
            var viewBag = filterContext.Controller.ViewBag;

            IEnumerable<Topic> topics;
            if (viewModel != null && viewModel.TopicId != null)
            {
                topics = _topicRepository.Get(a => (!a.IsDeleted && a.IsActive) || a.Id == viewModel.TopicId);
            }
            else
            {
                topics = _topicRepository.Get(a => a.IsDeleted == false && a.IsActive);
            }

            var topicListItems = new List<SelectListItem>();

            // have to sort it in memory as the database sort æøå wrong and doesn't recognize StringComparer.CurrentCulture
            var orderedTopics = topics.OrderBy(x => x.Name, StringComparer.CurrentCulture);
            foreach (var topic in orderedTopics)
            {
                var topicItem = new SelectListItem
                {
                    Text = topic.Name,
                    Value = topic.Id.ToString(CultureInfo.CurrentCulture)
                };

                if (topic.IsDeleted)
                {
                    topicItem.Text += " (Slettet)";
                    viewBag.IsTopicDeleted = true;
                }

                topicListItems.Add(topicItem);
            }

            viewBag.TopicList = topicListItems;
        }
    }
}
