﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.DefaultPhrase
{
    public enum DefaultPhraseTypeViewModel
    {
        [Display(Name = "Afgørelse")]
        Conclusion,
        [Display(Name = "Aftale")]
        Agreement
    }
}