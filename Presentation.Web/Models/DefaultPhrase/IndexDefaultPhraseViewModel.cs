﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.DefaultPhrase
{
    public class IndexDefaultPhraseViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Standardfrase")]
        public string Phrase { get; set; }

        [DisplayName("Type")]
        public DefaultPhraseTypeViewModel Type { get; set; }
    }
}