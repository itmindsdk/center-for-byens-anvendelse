﻿#region Head
// <copyright file="DeleteAdvisorViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Advisor
{
    using System.ComponentModel;

    public class DeleteAdvisorViewModel : CbaListPropertiesViewModel
    {
        [DisplayName("Virksomhed")]
        public string CompanyName { get; set; }
        [DisplayName("Kontaktperson")]
        public string ResponsiblePersonName { get; set; }
        [DisplayName("Telefonnummer")]
        public string ResponsiblePersonPhoneNumber { get; set; }
    }
}