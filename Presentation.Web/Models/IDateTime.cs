﻿#region Head
// <copyright file="IDateTime.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Presentation.Web.Models
{
    /// <summary>
    /// Interface used to remove dependency on System.DateTime.
    /// </summary>
    public interface IDateTime
    {
        System.DateTime Now { get; }
    }
}
