﻿#region Head
// <copyright file="EditRequestorViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Requestor
{
    public class EditRequestorViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Navn er påkrævet")]
        public string Name { get; set; }

        [DisplayName("E-mail*")]
        [Required(ErrorMessage = "E-mail er påkrævet")]
        [EmailAddress(ErrorMessage = "Ugyldig e-mailadresse")]
        public string Email { get; set; }
    }
}