﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Requestor
{
    public class CreateRequestorViewModel : CbaListPropertiesViewModel
    {
        public CreateRequestorViewModel()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Navn er påkrævet")]
        public string Name { get; set; }

        [DisplayName("E-mail*")]
        [Required(ErrorMessage = "E-mail er påkrævet")]
        [EmailAddress(ErrorMessage = "Ugyldig e-mailadresse")]
        public string Email { get; set; }
    }
}