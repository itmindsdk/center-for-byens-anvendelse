﻿#region Head
// <copyright file="DeleteRequestorViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Requestor
{
    public class DeleteRequestorViewModel : CbaListPropertiesViewModel
    {
        [DisplayName("Navn")]
        public string Name { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }
    }
}