﻿#region Head
// <copyright file="CreateTeamViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Group
{
    public class CreateGroupViewModel : CbaListPropertiesViewModel
    {
        public CreateGroupViewModel()
        {
            IsActive = true;
        }

        [DisplayName("Gruppenavn*")]
        [Required(ErrorMessage = "Gruppenavn er påkrævet.")]
        public string Name { get; set; }
    }
}
