﻿#region Head
// <copyright file="DetailsAgendaPointViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.PartnerDiscussion;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.AgendaPoint
{
    public class DetailsAgendaPointViewModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("Repræsentativ adresse")]
        public string AddressName { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Projektets / områdets navn")]
        public string TemporaryAddressName { get; set; }

        [DisplayName("Projektleder")]
        public string ProjectLeaderName { get; set; }

        public int? ProjectLeaderId { get; set; }

        [DisplayName("Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [DisplayName("Emne")]
        public string TopicName { get; set; }

        public int? TopicId { get; set; }

        [DisplayName("Beskrivelse")]
        public string Description { get; set; }

        [DisplayName("Aftale")]
        public string Agreement { get; set; }

        [DisplayName("Afgørelse")]
        public string Conclusion { get; set; }

        [DisplayName("Status")]
        public AgendaPointStatusViewModel AgendaPointStatus { get; set; }

        [DisplayName("Dokumenter")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        public int? ProjectId { get; set; }

        [DisplayName("Tidligere behandlet på disse møder")]
        public IEnumerable<DetailsMeetingViewModel> PreviousMeetings { get; set; }

        public IEnumerable<string> MeetingIds { get; set; }

        public AgendaPointMeetingAgendaPointViewModel MeetingAgendaPoint { get; set; }

        [DisplayName("Område")]
        public AreaTypeViewModel Area { get; set; }

        public CbaPartnerDiscussionViewModel CbaDiscussion { get; set; }

        public CbmPartnerDiscussionViewModel CbmDiscussion { get; set; }

        public bool CanBeEdited { get; set; }
    }
}
