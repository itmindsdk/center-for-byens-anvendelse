﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.PartnerDiscussion;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.AgendaPoint
{
    public class DeleteAgendaPointViewModel : LoggerViewModel
    {
        [DisplayName("Nr.")]
        public int Id { get; set; }

        [DisplayName("Repræsentativ adresse")]
        public string AddressName { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Projektets / områdets navn")]
        public string TemporaryAddressName { get; set; }

        [DisplayName("Projektleder")]
        public string ProjectLeaderName { get; set; }

        [DisplayName("Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [DisplayName("Emne")]
        public string TopicName { get; set; }

        [DisplayName("Beskrivelse")]
        public string Description { get; set; }

        [DisplayName("Aftale")]
        [DataType(DataType.MultilineText)]
        public string Agreement { get; set; }

        [DisplayName("Afgørelse")]
        [DataType(DataType.MultilineText)]
        public string Conclusion { get; set; }

        [DisplayName("Status")]
        public AgendaPointStatusViewModel AgendaPointStatus { get; set; }

        public int? ProjectId { get; set; }

        [DisplayName("Tidligere møder")]
        public IEnumerable<string> MeetingIds { get; set; }

        [DisplayName("Dokumenter")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        [DisplayName("Tidligere behandlet på disse møder")]
        public IEnumerable<DetailsMeetingViewModel> PreviousMeetings { get; set; }

        public CbaPartnerDiscussionViewModel CbaDiscussion { get; set; }

        public CbmPartnerDiscussionViewModel CbmDiscussion { get; set; }
    }
}
