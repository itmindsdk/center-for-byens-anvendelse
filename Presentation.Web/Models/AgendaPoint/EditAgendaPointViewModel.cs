﻿#region Head
// <copyright file="EditAgendaPointViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Filters;
using Presentation.Web.Models.PartnerDiscussion;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.AgendaPoint
{
    public class EditAgendaPointViewModel : LoggerViewModel, ISelectedTopic
    {
        public int Id { get; set; }

        [DisplayName("Repræsentativ adresse*")]
        public Guid AddressId { get; set; } = Guid.Empty;
        public string AddressName { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Projektets / områdets navn")]
        public string TemporaryAddressName { get; set; }

        [DisplayName("Vejtype*")]
        [Required(ErrorMessage = "Vejtype er påkrævet.")]
        public int? StreetTypeId { get; set; }

        [DisplayName("Emne*")]
        [Required(ErrorMessage = "Emne er påkrævet.")]
        public int? TopicId { get; set; }

        public string TopicName { get; set; }

        [DisplayName("Beskrivelse*")]
        [Required(ErrorMessage = "En beskrivelse er påkrævet.")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Status*")]
        [Required(ErrorMessage = "Status er påkrævet.")]
        public int? AgendaPointStatusId { get; set; }

        [DisplayName("Dokumenter")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        [DisplayName("Tidligere behandlet på disse møder")]
        [MeetingId(ErrorMessage = "Følgende IDer kan ikke genkendes:")]
        public IEnumerable<string> MeetingIds { get; set; }

        public string RemovedMeetingIds { get; set; }

        [DisplayName("Område")]
        public AreaTypeViewModel Area { get; set; }

        [Required(ErrorMessage = "Team er påkrævet.")]
        [DisplayName("Team*")]
        public int? TeamId { get; set; }

        [Required(ErrorMessage = "Projektleder er påkrævet.")]
        [DisplayName("Projektleder*")]
        public int? ProjectLeaderId { get; set; }

        public CbaPartnerDiscussionViewModel CbaDiscussion { get; set; }

        public CbmPartnerDiscussionViewModel CbmDiscussion { get; set; }
    }
}
