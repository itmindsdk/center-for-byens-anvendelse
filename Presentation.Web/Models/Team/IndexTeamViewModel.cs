﻿#region Head
// <copyright file="IndexGroupViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Team
{
    public class IndexTeamViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Navn")]
        public string Name { get; set; }
    }
}
