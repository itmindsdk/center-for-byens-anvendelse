﻿#region Head
// <copyright file="DeleteTeamViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Team
{
    public class DeleteTeamViewModel : CbaListPropertiesViewModel
    {
        [DisplayName("Team navn")]
        public string Name { get; set; }
    }
}
