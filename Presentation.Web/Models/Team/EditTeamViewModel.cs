﻿#region Head
// <copyright file="EditTeamViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Team
{
    public class EditTeamViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }
        [DisplayName("Team navn*")]
        [Required(ErrorMessage = "Team navn er påkrævet.")]
        public string Name { get; set; }
    }
}
