﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public enum CompanyTypeViewModel
    {
        [Display(Name = "Entreprenør")]
        Entrepreneur,

        [Display(Name = "Rådgiver")]
        Advisor
    }
}