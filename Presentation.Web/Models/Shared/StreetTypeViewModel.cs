﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public class StreetTypeViewModel : CbaListPropertiesViewModel
    {
        public StreetTypeViewModel()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Vejtype er påkrævet.")]
        [DisplayName("Vejtype*")]
        public string Type { get; set; }
    }
}