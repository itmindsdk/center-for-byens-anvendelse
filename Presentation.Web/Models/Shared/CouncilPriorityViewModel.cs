﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public class CouncilPriorityViewModel : CbaListPropertiesViewModel
    {
        public CouncilPriorityViewModel()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Prioritet er påkrævet.")]
        [DisplayName("Fællesrådsprioritet")]
        public string Priority { get; set; }
    }
}