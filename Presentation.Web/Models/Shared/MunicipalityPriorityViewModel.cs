﻿using System.ComponentModel;

namespace Presentation.Web.Models.Shared
{
	public class MunicipalityPriorityViewModel : CbaListPropertiesViewModel
	{
		public MunicipalityPriorityViewModel()
		{
			IsActive = true;
		}

		public int Id { get; set; }

		[DisplayName("Kommuneprioritet")]
		public string Priority { get; set; }
	}
}
