﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public class PlanningViewModel : CbaListPropertiesViewModel
    {
        public PlanningViewModel()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "planlægning er påkrævet.")]
        [DisplayName("Planlægning af større anlægsopgaver*")]
        public string Plan { get; set; }
    }
}