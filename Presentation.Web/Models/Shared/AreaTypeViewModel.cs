﻿namespace Presentation.Web.Models.Shared
{
    public enum AreaTypeViewModel
    {
        Midt,
        Nord,
        Vest,
        Syd
    }
}