﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Shared
{
    public class ProjectStatusViewModel : CbaListPropertiesViewModel
    {
        public ProjectStatusViewModel()
        {
            IsActive = true;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Status er påkrævet.")]
        [DisplayName("Status")]
        public string Status { get; set; }
    }
}