﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.PartnerDiscussion
{
    public class CbaPartnerDiscussionViewModel
    {
        [DisplayName("Drøftet med CBA, distriktansvarlig")]
        public bool Discussed { get; set; }

        [DisplayName("Hvem")]
        public string DiscussedWith { get; set; }

        [DisplayName("Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}