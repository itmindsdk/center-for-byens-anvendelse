﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.PartnerDiscussion
{
    public class CbmPartnerDiscussionViewModel
    {
        [DisplayName("Drøftet med CBM")]
        public bool Discussed { get; set; }

        [DisplayName("Hvem")]
        public string DiscussedWith { get; set; }

        [DisplayName("Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}