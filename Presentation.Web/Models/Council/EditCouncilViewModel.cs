﻿#region Head
// <copyright file="EditCouncilViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Filters;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Council
{
    public class EditCouncilViewModel : CbaListPropertiesViewModel, ISelectedGroup
    {
        public int Id { get; set; }

        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Navnet må ikke være tomt.")]
        public string Name { get; set; }

        [DisplayName("Gruppe*")]
        [Required(ErrorMessage = "Der skal vælges en gruppe.")]
        public int? GroupId { get; set; }
    }
}
