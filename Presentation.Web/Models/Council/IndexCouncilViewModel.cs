﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Council
{
    public class IndexCouncilViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [Display (Name = "Navn")]
        public string Name { get; set; }

        [DisplayName ("Gruppe")]
        public IndexGroupViewModel Group { get; set; }
    }
}
