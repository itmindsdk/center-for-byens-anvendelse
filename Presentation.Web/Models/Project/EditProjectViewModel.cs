﻿#region Head
// <copyright file="CreateProjectViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Filters;
using Presentation.Web.Models.Shared;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Project
{
    public class EditProjectViewModel :   LoggerViewModel,
                                            ISelectedAdvisor,
                                            ISelectedConstructionProgram,
                                            ISelectedCouncil,
                                            ISelectedLocalCommunity,
                                            ISelectedProjectCategory,
                                            ISelectedProjectLeader,
                                            ISelectedCompany,
                                            ISelectedTopic,
                                            ISelectedTeam,
                                            ISelectedEntrepreneur,
                                            ISelectedEntrepreneurCompany,
                                            ISelectedRequestor,
                                            ISelectedInconvenience,
                                            ISelectedTrafficInconvenience,
                                            ISelectedPlanning
    {
        public EditProjectViewModel()
        {
            Documents = new List<ProjectDocumentViewModel>();
            SearchInAarhus = true;
            // Set the street type to be 'public' by default.
            StreetTypeId = 1;
            ExpectedStartDate = DateTime.Now;
            ExpectedEndDate = DateTime.Now;

            OneYearReview = DateTime.Now.AddYears(1);
            FiveYearReview = DateTime.Now.AddYears(5);
        }

        public int Id { get; set; }

        [Display(Name = "Emne*")]
        [Required(ErrorMessage = "Vælg et emne fra listen.")]
        public int? TopicId { get; set; }

        [Display(Name = "Adresse*")]
        public Guid AddressId { get; set; } = Guid.Empty;
        public string AddressName { get; set; }

        [Display(Name = "Søg i Aarhus?")]
        public bool SearchInAarhus { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Projektets / områdets navn")]
        public string TemporaryAddressName { get; set; }

        [Display(Name = "eDoc sagsnr.")]
        public string EDoc { get; set; }

        [UIHint ("Date")]
        [Display(Name = "Forventet startdato*")]
        [Required(ErrorMessage = "Angiv venligst projektets startstidspunkt.")]
        public DateTime ExpectedStartDate { get; set; }

        [UIHint("Date")]
        [Display(Name = "Forventet slutdato*")]
        [Required(ErrorMessage = "Angiv venligst projektets slutstidspunkt.")]
        public DateTime ExpectedEndDate { get; set; }

        [Display(Name = "Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Begrundelse for status / prioritering")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [Display(Name = "Vejtype")]
        public int? StreetTypeId { get; set; }

        [Display(Name = "Team*")]
        [Required(ErrorMessage = "Team er påkrævet.")]
        public int? TeamId { get; set; }

        [Display(Name = "Projektleder*")]
        [Required(ErrorMessage = "Projektleder er påkrævet.")]
        public int? ProjectLeaderId { get; set; }

        [Display(Name = "Fællesrådsprioritet")]
        public int? CouncilPriorityId { get; set; }

        [Required(ErrorMessage = "Kommuneprioritet er påkrævet.")]
        [Display(Name = "Kommuneprioritet*")]
        public int? MunicipalityPriorityId { get; set; }

        [Display(Name = "Budget*")]
        [Required(ErrorMessage = "Angiv venligst projektets budget.")]
        [ThousandsNumber(ErrorMessage = "Brug punktum som seperator. Min/max værdi er 2.147.483.647")]
        public string Budget { get; set; }

        [Display(Name = "Status*")]
        [Required(ErrorMessage = "Angiv venligst projektets status.")]
        public int? ProjectStatusId { get; set; }

        [Display(Name = "Planlægning af større anlægsopgaver*")]
        [Required(ErrorMessage = "Angiv venligst projektets Planlægning.")]
        public int? PlanningId { get; set; }

        [UIHint("Date")]
        [Display(Name = "1 års gennemgang*")]
        public DateTime OneYearReview { get; set; }

        [UIHint("Date")]
        [Display(Name = "5 års gennemgang*")]
        public DateTime FiveYearReview { get; set; }

        [Display(Name = "Virksomhed")]
        public int? CompanyId { get; set; }

        [Display(Name = "Rådgiver")]
        public int? AdvisorId { get; set; }

        [Display(Name = "Entreprenørs virksomhed")]
        public int? EntrepreneurCompanyId { get; set; }

        [Display(Name = "Entreprenør")]
        public int? EntrepreneurId { get; set; }

        [Display(Name = "Fællesråd")]
        public int? CouncilId { get; set; }

        [Display(Name = "Projektkategori")]
        public int? ProjectCategoryId { get; set; }

        [Display(Name = "Anlægsprogram")]
        public int? ConstructionProgramId { get; set; }

        [Required(ErrorMessage = "Lokalsamfund er påkrævet.")]
        [Display(Name = "Lokalsamfund*")]
        public int? LocalCommunityId { get; set; }

        public IList<ProjectDocumentViewModel> Documents { get; set; }

        [Display(Name = "Log - Noter, dato, navn og aftale")]
        [DataType(DataType.MultilineText)]
        public string Log { get; set; }

        [Display(Name = "Rekvirent")]
        public int? RequestorId { get; set; }

        [Required(ErrorMessage = "Gener er påkrævet")]
        [Display(Name = "Gener*")]
        public int? InconvenienceId { get; set; }

        [Required(ErrorMessage = "Trafikale gener er påkrævet")]
        [Display(Name = "Trafikale gener*")]
        public int? TrafficInconvenienceId { get; set; }
    }
}
