﻿#region Head
// <copyright file="CreateAgendaPointProjectDto.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.Diagnostics.CodeAnalysis;

namespace Presentation.Web.Models.Project
{
    [SuppressMessage("Microsoft.Naming", "CA1704", Justification = "Dto is a valid abbreviation.")]
    public class ProjectDto
    {
        public int Id { get; set; }
        public string Name{ get; set; }
    }
}
