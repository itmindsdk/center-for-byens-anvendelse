﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Advisor;
using Presentation.Web.Models.Council;
using Presentation.Web.Models.Entrepreneur;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.Models.ProjectLeader;
using Presentation.Web.Models.Requestor;
using Presentation.Web.Models.Shared;
using Presentation.Web.Models.Inconvenience;
using Presentation.Web.Models.TrafficInconvenience;

namespace Presentation.Web.Models.Project
{
    public class IndexProjectViewModel : LoggerViewModel
    {
        [Display(Name = "Projekt nr.")]
        public int Id { get; set; }

        [Display(Name = "Emne")]
        public string TopicName { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Forventede startsdato")]
        public DateTime ExpectedStartDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Forventede slutsdato")]
        public DateTime ExpectedEndDate { get; set; }

        [Display(Name = "eDoc sagsnr.")]
        public string EDoc { get; set; }

        [Display(Name = "Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Begrundelse for status / prioritering")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [Display(Name = "Vejtype")]
        public string StreetTypeType { get; set; }

        [Display(Name = "Projektleder")]
        public string ProjectLeaderName { get; set; }

        [Display(Name = "Fællesrådsprioritet")]
        public string CouncilPriorityPriority { get; set; }

        [Display(Name = "Kommuneprioritet")]
        public string MunicipalityPriorityPriority { get; set; }

        [Display(Name = "Adresse")]
        public string AddressName { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Projektets / områdets navn")]
        public string TemporaryAddressName { get; set; }

        [Display(Name = "Budget")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int Budget { get; set; }

        [Display(Name = "Status")]
        public ProjectStatusViewModel ProjectStatus { get; set; }

        [Display(Name = "Planlægning")]
        public PlanningViewModel Planning { get; set; }

        [Display(Name = "Projektleder")]
        public IndexProjectLeaderViewModel ProjectLeader { get; set; }

        [Display(Name = "Rådgiver")]
        public IndexAdvisorViewModel Advisor { get; set; }

        [Display(Name = "Projektkategori")]
        public IndexProjectCategoryViewModel ProjectCategory { get; set; }

        [Display(Name = "Anlægsprogram")]
        public ConstructionProgramViewModel ConstructionProgram { get; set; }

        [Display(Name = "Lokalsamfund")]
        public IndexLocalCommunityViewModel LocalCommunity { get; set; }

        [Display(Name = "Fællesråd")]
        public IndexCouncilViewModel Council { get; set; }

        [Display(Name = "Dokumenter")]
        public IList<ProjectDocumentViewModel> Documents { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "1 års gennemgang")]
        public DateTime OneYearReview { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "5 års gennemgang")]
        public DateTime FiveYearReview { get; set; }

        public string AdvisorCompanyName { get; set; }

        [Display(Name = "Entreprenør")]
        public IndexEntrepreneurViewModel Entrepreneur { get; set; }

        [Display(Name = "Log")]
        [DataType(DataType.MultilineText)]
        public string Log { get; set; }

        [Display(Name = "Rekvirent")]
        public IndexRequestorViewModel Requestor { get; set; }

        [Display(Name = "Gener")]
        public InconvenienceViewModel Inconvenience { get; set; }

        [Display(Name = "Trafikale gener")]
        public TrafficInconvenienceViewModel TrafficInconvenience { get; set; }
    }
}
