﻿#region Head
// <copyright file="DeleteNextMeetingViewModel.cs" company="IT Minds" year="2017">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.NextMeeting
{
    public class DeleteNextMeetingViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Næste Færdselsmøde")]
        public string Name { get; set; }
    }
}
