﻿#region Head
// <copyright file="CreateConstructionProgramViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.ConstructionProgram
{
    public class CreateConstructionProgramViewModel : CbaListPropertiesViewModel
    {
        public CreateConstructionProgramViewModel()
        {
            IsActive = true;
        }
        [DisplayName("År med projekt involveret*")]
        [Required(ErrorMessage = "Indtast årstal.")]
        public int? YearOfProjectInvolvement { get; set; }
    }
}
