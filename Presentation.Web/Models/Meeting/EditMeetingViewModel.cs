﻿#region Head
// <copyright file="EditMeetingViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Meeting
{
    public class EditMeetingViewModel : LoggerViewModel
    {
        public EditMeetingViewModel()
        {
            MeetingAgendaPoints = new List<MeetingAgendaPointViewModel>();
            InvitedPeople = new List<string>();
        }

        [DisplayName("Nr.")]
        public int Id { get; set; }

        [DisplayName("Dato*")]
        [Required(ErrorMessage = "Vælg en dato for afholdelse af mødet.")]
        public string Date { get; set; }

        [DisplayName("Lokation*")]
        [Required(ErrorMessage = "En lokation er påkrævet.")]
        public string Location { get; set; }

        [DisplayName("Færdselsmødepunkter")]
        public IList<MeetingAgendaPointViewModel> MeetingAgendaPoints { get; set; }

        [DisplayName("Inviterede personer")]
        [UIHint("TagList")]
        public IEnumerable<string> InvitedPeople { get; set; }

        [DisplayName("Link til eDoc")]
        public string EDocsLink { get; set; }

        public bool IsSummaryLocked { get; set; }

        [DisplayName("Godkendelsesdato")]
        public DateTime? AcceptedOn { get; set; }
    }
}
