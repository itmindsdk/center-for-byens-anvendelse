﻿#region Head
// <copyright file="AgendaPointViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.PartnerDiscussion;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Meeting
{
    public class AgendaPointViewModel : LoggerViewModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("Område")]
        public AreaTypeViewModel Area { get; set; }

        [DisplayName("Adresse")]
        public string AddressName { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Projektets / områdets navn")]
        public string TemporaryAddressName { get; set; }

        [DisplayName("Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Afgørelse")]
        [DataType(DataType.MultilineText)]
        public string Conclusion { get; set; }

        [DisplayName("Aftale")]
        [DataType(DataType.MultilineText)]
        public string Agreement { get; set; }

        public bool IsAssignedToMeeting { get; set; }

        public bool IsAssignedBeforeChange { get; set; }

        [DisplayName("Godkendt:")]
        public bool Approved { get; set; }

        [DisplayName("Projektleder")]
        public string ProjectLeaderName { get; set; }

        [DisplayName("Team")]
        public string ProjectLeaderTeamName { get; set; }

        [DisplayName("Projekt")]
        public MeetingProjectViewModel Project { get; set; }

        [DisplayName("Dokumenter tilknyttet Dagsordenspunkt")]
        public IList<AgendaPointDocumentViewModel> Documents { get; set; }

        [DisplayName("Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        public CbaPartnerDiscussionViewModel CbaDiscussion { get; set; }

        public CbmPartnerDiscussionViewModel CbmDiscussion { get; set; }

        [DisplayName("Status")]
        public int? AgendaPointStatusId { get; set; }
    }
}
