﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Meeting;

namespace Presentation.Web.Models.MeetingAgendaPoint
{
    public class MeetingAgendaPointSummariesViewModel
    {
        public int Id { get; set; }
        public int MeetingId { get; set; }

        [DisplayName("Mødepunkts ID")]
        public int AgendaPointId { get; set; }
        public AgendaPointViewModel AgendaPoint { get; set; }

        [Display (Name = "Dagsordenspunkt")]
        public int Number { get; set; }
    }
}
