﻿using System;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Validators;

namespace Presentation.Web.Models.Meeting.MeetingAgendaPoint
{
    public class MeetingAgendaPointViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Færdselsmøde nr.")]
        public int MeetingId { get; set; }
        public int AgendaPointId { get; set; }
        public AgendaPointViewModel AgendaPoint { get; set; }

        [Display(Name = "Mødepunktsnummer")]
        [MinValue(1, ErrorMessage = "Mødepunktsnummeret skal være større end 0")]
        public int Number { get; set; }

        public bool IsAgendaPointSummaryStringsEmpty()
        {
            if(!AgendaPoint.IsAssignedBeforeChange)
                return true;

            return (String.IsNullOrEmpty(this.AgendaPoint.Conclusion) &&
                    String.IsNullOrEmpty(this.AgendaPoint.Agreement));
        }
    }
}
