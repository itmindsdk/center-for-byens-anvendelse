﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Models.Home
{
    public class HomeViewModel
    {
        public string QuickCreateProject { get; set; }
        public string QuickAddAppendixToProject { get; set; }
        public string QuickCreateAgendaPoint { get; set; }
        public string QuickAddAppendixToAgendaPoint { get; set; }
        public string QuickCreateDrawing { get; set; }
        public string QuickAddAppendixToDrawing { get; set; }
    }
}