﻿#region Head
// <copyright file="CreateLocalCommunityViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.LocalCommunity
{
    public class CreateLocalCommunityViewModel : CbaListPropertiesViewModel
    {
        public CreateLocalCommunityViewModel()
        {
            IsActive = true;
        }
        [DisplayName("Nummer*")]
        [Required(ErrorMessage = "Skriv nummeret på lokalsamfundet.")]
        public int? Number { get; set; }

        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Skriv navnet på lokalsamfundet.")]
        public string Name { get; set; }
    }
}
