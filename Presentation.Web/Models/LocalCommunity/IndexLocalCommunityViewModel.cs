﻿#region Head
// <copyright file="IndexLocalCommunityViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.LocalCommunity
{
    public class IndexLocalCommunityViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Nummer")]
        public int Number { get; set; }

        [DisplayName("Navn")]
        public string Name { get; set; }
    }
}
