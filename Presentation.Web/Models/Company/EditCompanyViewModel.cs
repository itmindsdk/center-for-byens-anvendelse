﻿#region Head
// <copyright file="EditCompanyViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Company
{
    public class EditCompanyViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }
        [DisplayName("Navn*")]
        [Required(ErrorMessage = "Navn er påkrævet.")]
        public string Name { get; set; }
        [DisplayName("Type*")]
        [Required(ErrorMessage = "Type er påkrævet.")]
        public CompanyTypeViewModel Type { get; set; }
        public bool IsDeleted { get; set; }
    }
}