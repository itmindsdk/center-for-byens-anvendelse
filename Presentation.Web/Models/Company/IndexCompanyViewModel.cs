﻿#region Head
// <copyright file="IndexCompanyViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Company
{
    public class IndexCompanyViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }
        [DisplayName("Virksomhed")]
        public string Name { get; set; }
        [DisplayName("Type")]
        public CompanyTypeViewModel Type { get; set; }
        public bool IsDeleted { get; set; }
    }
}