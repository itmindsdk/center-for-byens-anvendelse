﻿#region Head
// <copyright file="IndexEntrepreneurViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Company;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Entrepreneur
{
    public class IndexEntrepreneurViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Virksomhed")]
        public IndexCompanyViewModel Company { get; set; }

        [DisplayName("Kontaktperson")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Telefonnummer")]
        public string ResponsiblePersonPhoneNumber { get; set; }
    }
}
