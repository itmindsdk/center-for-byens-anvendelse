﻿#region Head
// <copyright file="CreateEntrepreneurViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Filters;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Entrepreneur
{
    public class CreateEntrepreneurViewModel : CbaListPropertiesViewModel, ISelectedCompany
    {
        public CreateEntrepreneurViewModel()
        {
            IsActive = true;
        }

        [DisplayName("Virksomhed*")]
        [Required(ErrorMessage="Virksomhedsnavn er påkrævet.")]
        public int? CompanyId { get; set; }

        [DisplayName("Kontaktperson*")]
        [Required(ErrorMessage = "Kontaktperson er påkrævet.")]
        public string ResponsiblePersonName { get; set; }

        [DisplayName("Telefonnummer*")]
        [Required(ErrorMessage = "Telefonnummer er påkrævet.")]
        public string ResponsiblePersonPhoneNumber { get; set; }
    }
}
