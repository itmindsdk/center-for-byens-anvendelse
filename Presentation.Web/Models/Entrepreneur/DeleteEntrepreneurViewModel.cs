﻿#region Head
// <copyright file="DeleteEntrepreneurViewModel.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Entrepreneur
{
    using System.ComponentModel;

    public class DeleteEntrepreneurViewModel : CbaListPropertiesViewModel
    {
        [DisplayName("Virksomhed")]
        public string CompanyName { get; set; }
        [DisplayName("Kontaktperson")]
        public string ResponsiblePersonName { get; set; }
        [DisplayName("Telefonnummer")]
        public string ResponsiblePersonPhoneNumber { get; set; }
    }
}