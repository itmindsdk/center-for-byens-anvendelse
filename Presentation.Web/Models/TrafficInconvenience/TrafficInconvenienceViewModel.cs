﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Models.TrafficInconvenience
{
    public class TrafficInconvenienceViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}