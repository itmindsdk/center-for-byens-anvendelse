﻿#region Head
// <copyright file="CustomDateTime.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;

namespace Presentation.Web.Models
{
    /// <summary>
    /// Implementation of a DateTime class using System.DateTime. By using this in controllers, they remain testable.
    /// </summary>
    public class CustomDateTime : IDateTime
    {

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
