﻿#region Head
// <copyright file="IndexProjectLeaderViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.ProjectLeader
{
    public class IndexProjectLeaderViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Navn")]
        public string Name { get; set; }

        [DisplayName("Initialer")]
        public string Initials { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Team")]
        public string TeamName { get; set; }
    }
}
