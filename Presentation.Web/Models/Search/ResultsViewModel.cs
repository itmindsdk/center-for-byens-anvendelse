﻿#region Head
// <copyright file="ResultsSearchViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using Core.ApplicationServices;

namespace Presentation.Web.Models.Search
{
    public class ResultsViewModel
    {
        public bool ShowAgendaPoints;
        public Action<Kendo.Mvc.UI.Fluent.DataSourceFilterDescriptorFactory<AgendaPointViewModel>> AgendaPointFilter;
        public AgendaPointSearchParameters AgendaPointSearchParameters;

        public bool ShowProjects;
        public Action<Kendo.Mvc.UI.Fluent.DataSourceFilterDescriptorFactory<ProjectViewModel>> ProjectFilter;
        public ProjectSearchParameters ProjectSearchParameters;
    }
}
