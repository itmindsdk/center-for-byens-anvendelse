﻿using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Search
{
    public enum DateSearchTypeViewModel
    {
        [Display(Name = "1 års gennemgang")]
        OneYearReview,

        [Display(Name = "5 års gennemgang")]
        FiveYearReview
    }
}