﻿#region Head
// <copyright file="ProjectViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Advisor;
using Presentation.Web.Models.Council;
using Presentation.Web.Models.Group;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.Models.Project;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.Models.ProjectLeader;
using Presentation.Web.Models.Requestor;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Search
{
    public class ProjectViewModel : LoggerViewModel
    {
        [DisplayName("Projekt nr.")]
        public int Id { get; set; }

        [DisplayName("Emne")]
        public string TopicName { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Forventet startdato")]
        public DateTime ExpectedStartDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Forventet slutdato")]
        public DateTime ExpectedEndDate { get; set; }

        [Display(Name = "eDoc sagsnr.")]
        public string EDoc { get; set; }

        [Display(Name = "Beskrivelse")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Begrundelse for status / prioritering")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [Display(Name = "Vejtype")]
        public StreetTypeViewModel StreetType { get; set; }

        [Display(Name = "Projektleder")]
        public string ProjectLeaderName { get; set; }

        [Display(Name = "Fællesrådsprioritet")]
        public CouncilPriorityViewModel CouncilPriority { get; set; }

        [Display(Name = "Kommuneprioritet")]
        public MunicipalityPriorityViewModel MunicipalityPriority { get; set; }

        [Display(Name = "Adresse")]
        public string AddressName { get; set; }

        [Display(Name = "Foreløbig adresse?")]
        public bool TemporaryAddress { get; set; }

        [Display(Name = "Foreløbig adresse / projektnavn")]
        public string TemporaryAddressName { get; set; }

        [Display(Name = "Budget")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int Budget { get; set; }

        [Display(Name = "Status")]
        public ProjectStatusViewModel ProjectStatus { get; set; }

        [Display(Name = "Planlægning")]
        public PlanningViewModel Planning { get; set; }

        [Display(Name = "Projektleder")]
        public IndexProjectLeaderViewModel ProjectLeader { get; set; }

        [Display(Name = "Rådgiver")]
        public IndexAdvisorViewModel Advisor { get; set; }

        [Display(Name = "Projektkategori")]
        public IndexProjectCategoryViewModel ProjectCategory { get; set; }

        [Display(Name = "Gruppe")]
        public IndexGroupViewModel Group { get; set; }

        [Display(Name = "Anlægsprogram")]
        public ConstructionProgramViewModel ConstructionProgram { get; set; }

        [Display(Name = "Lokalsamfund")]
        public IndexLocalCommunityViewModel LocalCommunity { get; set; }

        [Display(Name = "Fællesråd")]
        public IndexCouncilViewModel Council { get; set; }

        [Display(Name = "Dokumenter")]
        public IList<ProjectDocumentViewModel> Documents { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "1 års gennemgang")]
        public DateTime OneYearReview { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "5 års gennemgang")]
        public DateTime FiveYearReview { get; set; }

        [Display(Name = "Log")]
        [DataType(DataType.MultilineText)]
        public string Log { get; set; }

        [Display(Name = "Rekvirent")]
        public IndexRequestorViewModel Requestor { get; set; }

        public int? EntrepreneurCompanyId { get; set; }
        public int? TopicId { get; set; }

        [Display(Name = "Gener")]
        public string InconvenienceDescription { get; set; }

        [Display(Name = "Trafikale gener")]
        public string TrafficInconvenienceDescription { get; set; }
    }
}
