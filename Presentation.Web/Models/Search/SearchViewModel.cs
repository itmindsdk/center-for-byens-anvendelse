﻿#region Head
// <copyright file="SearchViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Web.Models.Search
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            SearchInAgendaPoints = true;
            SearchInProjects = true;
        }

        [DisplayName("Vejnavn")]
        public string Address { get; set; }

        [DisplayName("Foreløbig adresse / projektnavn")]
        public string TemporaryAddressName { get; set; }

        [DisplayName("Projekt nr.")]
        public int? ProjectId { get; set; }

        [DisplayName("Færdselsmøde nr.")]
        public int? MeetingId { get; set; }

        [DisplayName("Projekt status")]
        public int? ProjectStatusId { get; set; }

        [DisplayName("Færdselsmødepunkts status")]
        public int? AgendaPointStatusId { get; set; }

        [DisplayName("Emner")]
        public IEnumerable<int> TopicIds { get; set; }

        [DisplayName("Fællesråd")]
        public int? CouncilId { get; set; }

        [DisplayName("Gruppe")]
        public int? GroupId { get; set; }

        [DisplayName("Projektkategori")]
        public int? ProjectCategoryId { get; set; }

        [DisplayName("Virksomhed (Rådgiver)")]
        public int? CompanyId { get; set; }

        [DisplayName("Virksomhed (Entreprenør)")]
        public int? EntrepreneurCompanyId { get; set; }

        [DisplayName("Rekvirent")]
        public int? RequestorId { get; set; }

        [DisplayName("Dato")]
        [UIHint("Date")]
        public DateTime? StartDate { get; set; }

        [UIHint("Date")]
        public DateTime? EndDate { get; set; }

        public DateSearchTypeViewModel DateSearchType { get; set; }

        [DisplayName("Færdselsmødepunkter")]
        public bool SearchInAgendaPoints { get; set; }

        [DisplayName("Projekter")]
        public bool SearchInProjects { get; set; }

        [DisplayName("Lokalsamfund")]
        public int? LocalCommunityId { get; set; }

        public bool AnyParametersSet()
        {
            if (String.IsNullOrWhiteSpace(Address) &&
                String.IsNullOrWhiteSpace(TemporaryAddressName) &&
                ProjectId == null &&
                MeetingId == null &&
                TopicIds == null &&
                ProjectStatusId == null &&
                ProjectCategoryId == null &&
                RequestorId == null &&
                AgendaPointStatusId == null &&
                CouncilId == null &&
                GroupId == null &&
                CompanyId == null &&
                EntrepreneurCompanyId == null &&
                LocalCommunityId == null &&
                StartDate == null &&
                EndDate == null ||
                !SearchInAgendaPoints &&
                !SearchInProjects)
            {
                return false;
            }

            return true;
        }
    }
}
