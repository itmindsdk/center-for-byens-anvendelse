﻿#region Head
// <copyright file="DeleteTopicViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Topic
{
    public class DeleteTopicViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }

        [DisplayName("Emnenavn")]
        public string Name { get; set; }
    }
}
