﻿#region Head
// <copyright file="EditTopicViewModel.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Presentation.Web.Models.Shared;

namespace Presentation.Web.Models.Topic
{
    public class EditTopicViewModel : CbaListPropertiesViewModel
    {
        public int Id { get; set; }
        [DisplayName("Emnenavn*")]
        [Required(ErrorMessage = "Emnenavn er påkrævet.")]
        public string Name { get; set; }
    }
}
