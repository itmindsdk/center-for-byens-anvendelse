﻿#region Head
// <copyright file="AuthorizeRolesAttribute.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Web.Mvc;
using Presentation.Web.Helpers;

namespace Presentation.Web.Attributes
{
    /// <summary>
    /// Set authorization roles from AD groups.
    /// </summary>
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params UserRole[] roles)
        {
            var adHelper = DependencyResolver.Current.GetService<IAdHelper>();

            var roleNames = String.Empty;

            if (roles.Length == 1)
            {
                roleNames = adHelper.GetRoles(roles[0]);
            }

            if (roles.Length > 1)
            {
                roleNames = adHelper.GetRoles(roles[0]);

                for (var i = 1; i < roles.Length; i++)
                {
                    roleNames += "," + adHelper.GetRoles(roles[i]);
                }
            }

            Roles = roleNames;
        }
    }
}
