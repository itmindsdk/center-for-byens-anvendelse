﻿#region Head
// <copyright file="AppSettings.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Core.DomainModel;
using Core.DomainServices;
using Presentation.Web.Exceptions;

namespace Presentation.Web.Helpers
{
    /// <summary>
    /// Access AppSettings configuration.
    /// </summary>
    public class AppSettings : IAppSettings
    {
        private readonly Dictionary<string, string> _appSettings;

        public AppSettings(IGenericRepository<AppSetting> repo)
        {
            _appSettings = repo.Get().ToDictionary(x => x.Key, x => x.Value);
        }

        private string GetSetting(string key)
        {
            try
            {
                return _appSettings[key];
            }
            catch (KeyNotFoundException)
            {
                throw new ErrorMessageException($"AppSettings missing {key}");
            }
        }

        public string AppName => GetSetting("appName");
        public IEnumerable<string> AdminRoles => GetSetting("adAdminRoles").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        public IEnumerable<string> UserRoles => GetSetting("adUserRoles").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        public string AgendaPointZeroDescription => GetSetting("AgendaPointZeroDescription");
        public string UploadPath => GetSetting("UploadPath");
        public string StatusExplanationFileName => GetSetting("StatusExplanationFileName");
        public string QuickCreateProject => GetSetting("QuickCreateProject");
        public string QuickAddAppendixToProject => GetSetting("QuickAddAppendixToProject");
        public string QuickCreateAgendaPoint => GetSetting("QuickCreateAgendaPoint");
        public string QuickAddAppendixToAgendaPoint => GetSetting("QuickAddAppendixToAgendaPoint");
        public string QuickCreateDrawing => GetSetting("QuickCreateDrawing");
        public string QuickAddAppendixToDrawing => GetSetting("QuickAddAppendixToDrawing");
    }
}
