﻿namespace Presentation.Web.Helpers
{
    public enum UserRole
    {
        Admin,
        User
    }
}