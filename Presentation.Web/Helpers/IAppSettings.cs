﻿#region Head
// <copyright file="IAppSettings.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;

namespace Presentation.Web.Helpers
{
    public interface IAppSettings
    {
        IEnumerable<string> AdminRoles { get; }
        IEnumerable<string> UserRoles { get; }
        string AppName { get; }
        string AgendaPointZeroDescription { get; }
        string UploadPath { get; }
        string StatusExplanationFileName { get; }
        string QuickCreateProject { get; }
        string QuickAddAppendixToProject { get; }
        string QuickCreateAgendaPoint { get; }
        string QuickAddAppendixToAgendaPoint { get; }
        string QuickCreateDrawing { get; }
        string QuickAddAppendixToDrawing { get; }
    }
}
