﻿#region Head
// <copyright file="IDocumentHelper.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Web.Helpers
{
    public interface IDocumentHelper
    {
        FileObject UploadFile(HttpRequestBase request, HttpServerUtilityBase server);
        FileResult Download(HttpServerUtilityBase server, Guid id);
        bool DeleteFiles(IEnumerable<Guid> fileGuidsToDelete, HttpServerUtilityBase server);
    }
}
