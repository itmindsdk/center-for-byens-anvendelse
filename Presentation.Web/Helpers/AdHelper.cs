﻿#region Head
// <copyright file="AdHelper.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infrastructure.DataAccess;

namespace Presentation.Web.Helpers
{
    /// <summary>
    /// Active Directory helper to access the Identity framework.
    /// </summary>
    public class AdHelper : IAdHelper
    {
        private readonly IAppSettings _appSettings;
        public AdHelper(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        /// <summary>
        /// Determines whether the current user belongs in the specified role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>True if user belongs in role otherwise false.</returns>
        public bool InRole(UserRole role)
        {
            var roles = GetRolesFromAppSettings(role);
            return roles.Any(r => HttpContext.Current.User.IsInRole(r));
        }

        /// <summary>
        /// Gets a value that indicates whether a user has been authenticated.
        /// </summary>
        public bool IsAuthenticated {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        public string Name
        {
            get { return HttpContext.Current.User.Identity.Name; }
        }

        /// <summary>
        /// Get AD roles as a comma separated list.
        /// </summary>
        /// <param name="role">The role group.</param>
        /// <returns>Comma separated list of role names.</returns>
        public string GetRoles(UserRole role)
        {
            return String.Join(",", GetRolesFromAppSettings(role));
        }

        /// <summary>
        /// Get roles from <see cref="AppSettings"/>.
        /// </summary>
        /// <param name="role">The role group.</param>
        /// <returns>IEnumerable of strings with each role.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when the argument is out of range.</exception>
        private IEnumerable<string> GetRolesFromAppSettings(UserRole role)
        {
            IEnumerable<string> roles;
            switch (role)
            {
                case UserRole.Admin:
                    roles = _appSettings.AdminRoles;
                    break;
                case UserRole.User:
                    roles = _appSettings.UserRoles;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("role");
            }
            return roles;
        }
    }
}
