﻿namespace Presentation.Web.Helpers
{
    public interface IHttpContext
    {
        bool IsLocal();
    }
}
