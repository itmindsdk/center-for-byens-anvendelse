﻿namespace Presentation.Web.Helpers
{
    public interface IPrivilegeHelper
    {
        bool IsAgendaPointDeleteableForCurrentUser(int agendaPointId);
    }
}
