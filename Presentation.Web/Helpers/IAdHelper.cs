﻿namespace Presentation.Web.Helpers
{
    public interface IAdHelper
    {
        bool InRole(UserRole userRole);

        bool IsAuthenticated { get; }

        string Name {get; }
        string GetRoles(UserRole userRole);
    }
}