﻿using System.Linq;

namespace Presentation.Web.Helpers
{
    public static class KendoGridHelpers
    {
        public static Kendo.Mvc.UI.Fluent.GridTemplateColumnBuilder<T> AddCustomButton<T>(this Kendo.Mvc.UI.Fluent.GridTemplateColumnBuilder<T> columnBuilder, string content, string actionName) where T : class
        {
            string action = actionName.Split('/').Last();

            columnBuilder.Column.ClientTemplate += "<a class=\"k-grid-" + action + "\" href=\"" + actionName + "/#=Id#\">" + content +"</a>";
            return columnBuilder;
        }
    }
}