﻿using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Helpers
{
    /// <summary>
    /// Helper for determing specific privileges for roles
    /// </summary>
    public class PrivilegeHelper : IPrivilegeHelper
    {
        private readonly IGenericRepository<AgendaPoint> _agendapointRepository;
        private readonly IAdHelper _adHelper;

        public PrivilegeHelper(IGenericRepository<AgendaPoint> agendapointRepository, IAdHelper adHelper)
        {
            _agendapointRepository = agendapointRepository;
            _adHelper = adHelper;
        }

        /// <summary>
        /// For checking if an Agendapoint can be deleted by the current user.
        /// An agendapoint can be deleted if:
        /// 1: It is not attached to a meeting (MeetingAgendapoint should be zero)
        /// 2: The current user is the creater of the agendapoint (CreatedBy)
        /// 3: The current user is administrator
        /// </summary>
        /// <returns></returns>
        public bool IsAgendaPointDeleteableForCurrentUser(int agendaPointId)
        {
            var agendaPoint = _agendapointRepository.GetByKey(agendaPointId);
            var isAdmin = _adHelper.InRole(UserRole.Admin);

            // if agendapoint not attached to a meeting return true &&
            // if current user is creator || if current user admin, return true
            if (agendaPoint.MeetingAgendaPoint == null && (agendaPoint.CreatedBy == _adHelper.Name || isAdmin))
                return true;

            return false;
        }
    }
}