﻿using System;
using System.Linq;
using Core.ApplicationServices;
using Presentation.Web.Models.Search;
using FilterFuncProjects = System.Action<Kendo.Mvc.UI.Fluent.DataSourceFilterDescriptorFactory<Presentation.Web.Models.Search.ProjectViewModel>>;
using FilterFuncAgendaPoints = System.Action<Kendo.Mvc.UI.Fluent.DataSourceFilterDescriptorFactory<Presentation.Web.Models.Search.AgendaPointViewModel>>;

namespace Presentation.Web.Helpers
{
    public static class SearchHelper
    {
        public static bool ProjectFilterActuallyFilters(ProjectSearchParameters searchParameters)
        {
            return !String.IsNullOrWhiteSpace(searchParameters.Address) ||
                   !String.IsNullOrWhiteSpace(searchParameters.TemporaryAddressName) ||
                   searchParameters.ProjectId.HasValue ||
                   searchParameters.ProjectStatusId.HasValue ||
                   searchParameters.CouncilId.HasValue ||
                   searchParameters.ProjectCategoryId.HasValue ||
                   searchParameters.RequestorId.HasValue ||
                   searchParameters.GroupId.HasValue ||
                   searchParameters.CompanyId.HasValue ||
                   searchParameters.EntrepreneurCompanyId.HasValue ||
                   (searchParameters.TopicIds != null && searchParameters.TopicIds.Count() != 0) ||
                   searchParameters.LocalCommunityId.HasValue ||
                   searchParameters.StartDate.HasValue && searchParameters.EndDate.HasValue;
        }

        public static bool AgendaPointFilterActuallyFilters(AgendaPointSearchParameters searchParameters)
        {
            return !String.IsNullOrWhiteSpace(searchParameters.Address) ||
                   !String.IsNullOrWhiteSpace(searchParameters.TemporaryAddressName) ||
                   searchParameters.ProjectId.HasValue ||
                   searchParameters.MeetingId.HasValue ||
                   searchParameters.AgendaPointStatusId.HasValue ||
                   (searchParameters.TopicIds != null && searchParameters.TopicIds.Count() != 0);
        }

        public static FilterFuncProjects ConstructFilterFuncProjectFromSearchParams(ProjectSearchParameters searchParameters)
        {
            FilterFuncProjects projectsFilter = f =>
                {
                    if (!String.IsNullOrWhiteSpace(searchParameters.Address))
                    {
                        f.Add(a => a.AddressName).Contains(searchParameters.Address);
                    }

                    if (!String.IsNullOrWhiteSpace(searchParameters.TemporaryAddressName))
                    {
                        f.Add(a => a.TemporaryAddressName).Contains(searchParameters.TemporaryAddressName);
                    }

                    if (searchParameters.ProjectId.HasValue)
                    {
                        f.Add(a => a.Id).IsEqualTo(searchParameters.ProjectId.Value);
                    }
                    if (searchParameters.ProjectStatusId.HasValue)
                    {
                        f.Add(a => a.ProjectStatus.Id).IsEqualTo(searchParameters.ProjectStatusId.Value);
                    }

                    if (searchParameters.CouncilId.HasValue)
                    {
                        f.Add(a => a.Council.Id).IsEqualTo(searchParameters.CouncilId.Value);
                    }

                    if (searchParameters.ProjectCategoryId.HasValue)
                    {
                        f.Add(a => a.ProjectCategory.Id).IsEqualTo(searchParameters.ProjectCategoryId.Value);
                    }

                    if (searchParameters.RequestorId.HasValue)
                    {
                        f.Add(a => a.Requestor.Id).IsEqualTo(searchParameters.RequestorId.Value);
                    }

                    if (searchParameters.GroupId.HasValue)
                    {
                        f.Add(a => a.Council.Group.Id).IsEqualTo(searchParameters.GroupId.Value);
                    }

                    if (searchParameters.CompanyId.HasValue)
                    {
                        f.Add(a => a.Advisor.Company.Id).IsEqualTo(searchParameters.CompanyId.Value);
                    }

                    if (searchParameters.EntrepreneurCompanyId.HasValue)
                    {
                        f.Add(a => a.EntrepreneurCompanyId).IsEqualTo(searchParameters.EntrepreneurCompanyId.Value);
                    }

                    if (searchParameters.TopicIds != null && searchParameters.TopicIds.Count() != 0)
                    {
                        var firstTopicId = searchParameters.TopicIds.First();
                        var compositeFilter = f.Add(a => a.TopicId).IsEqualTo(firstTopicId);
                        var remainingTopicIds = searchParameters.TopicIds.Skip(1);

                        foreach (var topicId in remainingTopicIds)
                        {
                            compositeFilter.Or().IsEqualTo(topicId);
                        }
                    }

                    if (searchParameters.LocalCommunityId.HasValue)
                    {
                        f.Add(a => a.LocalCommunity.Id).IsEqualTo(searchParameters.LocalCommunityId.Value);
                    }

                    if (searchParameters.StartDate.HasValue && searchParameters.EndDate.HasValue)
                    {
                        // Sanitizing dates to secure results on dates with time set
                        // Start: 26/10/2015 14:00:22 -> 26/10/2015 00:00:00
                        // End: 26/10/2015 14:00:22 -> 26/10/2015 23:59:59
                        var sanitizedStartDate = searchParameters.StartDate.Value.Date;
                        var sanitizedEndDate = searchParameters.EndDate.Value.Date.Add(new TimeSpan(23, 59, 59));

                        switch (searchParameters.DateSearchType)
                        {
                            case DateSearchType.OneYearReview:
                                f.Add(a => a.OneYearReview).IsGreaterThanOrEqualTo(sanitizedStartDate).And().IsLessThanOrEqualTo(sanitizedEndDate);
                                break;
                            case DateSearchType.FiveYearReview:
                                f.Add(a => a.FiveYearReview).IsGreaterThanOrEqualTo(sanitizedStartDate).And().IsLessThanOrEqualTo(sanitizedEndDate);
                                break;
                        }
                    }
                };
            return projectsFilter;
        }

        public static FilterFuncAgendaPoints ConstructFilterFuncAgendaPointFromSearchParams(AgendaPointSearchParameters searchParameters)
        {
            FilterFuncAgendaPoints agendaPointFilter = f =>
                {
                    // FIX: This is to filter out agenda points that are auto-created when creating a meeting
                    f.Add(a => a.MeetingAgendaPoint.Number).IsNotEqualTo(0);

                    if (!String.IsNullOrWhiteSpace(searchParameters.Address))
                    {
                        f.Add(a => a.AddressName).Contains(searchParameters.Address);
                    }

                    if (!String.IsNullOrWhiteSpace(searchParameters.TemporaryAddressName))
                    {
                        f.Add(a => a.TemporaryAddressName).Contains(searchParameters.TemporaryAddressName);
                    }

                    if (searchParameters.ProjectId.HasValue)
                    {
                        f.Add(a => a.Id).IsEqualTo(searchParameters.ProjectId.Value);
                    }

                    if (searchParameters.MeetingId.HasValue)
                    {
                        f.Add(a => a.MeetingAgendaPoint.MeetingId).IsEqualTo(searchParameters.MeetingId.Value);
                    }

                    if (searchParameters.AgendaPointStatusId.HasValue)
                    {
                        f.Add(a => a.Status.Id).IsEqualTo(searchParameters.AgendaPointStatusId.Value);
                    }

                    if (searchParameters.TopicIds != null && searchParameters.TopicIds.Count() != 0)
                    {
                        var firstTopicId = searchParameters.TopicIds.First();
                        var compositeFilter = f.Add(a => a.TopicId).IsEqualTo(firstTopicId);
                        var remainingTopicIds = searchParameters.TopicIds.Skip(1);

                        foreach (var topicId in remainingTopicIds)
                        {
                            compositeFilter.Or().IsEqualTo(topicId);
                        }
                    }
                };
            return agendaPointFilter;
        }
    }
}
