﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.Helpers
{
    public class StringComparerNullLast : IComparer<string>
    {
        public int Compare(string left, string right)
        {
            if (!string.IsNullOrEmpty(left) && string.IsNullOrEmpty(right))
            {
                return -1;
            }
            else if (string.IsNullOrEmpty(left) && !string.IsNullOrEmpty(right))
            {
                return 1;
            }
            else
            {
                return string.CompareOrdinal(left, right);
            }
        }
    }
}