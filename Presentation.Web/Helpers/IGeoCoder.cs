﻿#region Head
// <copyright file="IGeoCoder.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Data.Entity.Spatial;

namespace Presentation.Web.Helpers
{
    public interface IGeoCoder
    {
        DbGeometry GetSpatialObject(Guid addressId);
    }
}
