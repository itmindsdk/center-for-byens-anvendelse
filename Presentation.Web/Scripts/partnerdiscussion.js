﻿// Function for setting up partner discussion checkboxes and textfields
// E.g. used in the AgendaPoint create and edit views
function setupPartnerDiscussionElements(checkboxId, textfieldId) {

    // jQuery the partner discussion elements
    var checkbox = $("#" + checkboxId);
    var textfield = $("#" + textfieldId);

    // Function for enabling or disable the textfield
    // based on checked value
    function updateTextfield(enabled) {
        if (enabled) {
            textfield.prop("disabled", false);
        } else {
            textfield.val("");
            textfield.prop("disabled", true);
        }
    }

    // Attach event handlers to partner discussion checkboxes
    checkbox.change(function () {
        updateTextfield($(this).is(":checked"));
    });

    // Run update method for initial configuration
    updateTextfield(checkbox.is(":checked"));
}