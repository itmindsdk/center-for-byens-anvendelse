﻿function cbaSetCookie(entry, value) {
    var d = new Date();
    d.setTime(d.getTime() + (24 * 60 * 60 * 1000)); //Expire in 24h.
    var expires = "expires=" + d.toUTCString();
    document.cookie = entry + "=" + value + ";" + expires + ";path=/";
    location.reload();
}

function cbaGetCookie(entry, defaultValue) {
    var name = entry + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return defaultValue;
}
