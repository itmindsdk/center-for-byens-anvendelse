﻿function initStatePersistance(gridName) {
    var grid = $("#" + gridName).data("kendoGrid");
    loadState(gridName, grid);

    grid.bind("dataBound", function () {
        persistState(gridName, grid);
    });
}

function persistState(gridName, gridRef) {
    sessionStorage[gridName + "-options"] = kendo.stringify(gridRef.getOptions());
}

function loadState(gridName, gridRef) {
    var options = sessionStorage[gridName + "-options"];
    if (options) {
        var opt = JSON.parse(options);
        for (var i = 0; i < opt.columns.length; ++i) {
            if (opt.columns[i].filterable) {
                opt.columns[i].filterable.ui = gridRef.columns[i].filterable.ui;
            }
        }
        gridRef.setOptions(opt);
    }
}

function kendoTableTruncateString(s, maxLen) {
    if (s.length > maxLen) {
        return s.substring(0, maxLen - 3) + "...";
    } else return s;
}

function removeOperatorDropdown(element) {
    element.parent().find("select").remove();
}
