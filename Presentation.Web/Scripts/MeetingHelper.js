﻿$(function () {
    $("#CheckAllAgendaPoints").on("click", function (event) {
        if (event.target.checked) {
            $("input.checkbox-inline").prop("checked", true);
        }
        else {
            $("input.checkbox-inline").prop("checked", false);
        }
    });

    // Unchecks the "check all" checkbox if any checkbox gets turned from true to false
    $("#AgendaPointList input.checkbox-inline").slice(1).on("click", function (event) {
        if (!event.target.checked) {
            $("#CheckAllAgendaPoints").prop("checked", false);
        } else {
            var allChecked = true;
            $("#AgendaPointList input.checkbox-inline").slice(1).each(function () {
                if (!$(this).prop("checked")) {
                    allChecked = false;
                    return false;
                }
                return true;
            }).promise().then(function () {
                if (allChecked) {
                    $("#CheckAllAgendaPoints").prop("checked", true);
                }
            });
        }
    });
});