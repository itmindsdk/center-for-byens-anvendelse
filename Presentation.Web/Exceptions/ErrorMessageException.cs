﻿using System;

namespace Presentation.Web.Exceptions
{
    public class ErrorMessageException : Exception
    {
        public ErrorMessageException(string message) : base(message) {}
    }
}
