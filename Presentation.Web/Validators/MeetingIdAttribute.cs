﻿#region Head
// <copyright file="MeetingIdAttribute.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;

namespace Presentation.Web.Validators
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class MeetingIdAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var repo = DependencyResolver.Current.GetService<IGenericRepository<Meeting>>();
            var ids = value as IEnumerable<string>;
            if (ids == null) return ValidationResult.Success;

            var idsWithError = new Collection<string>();
            foreach (var id in ids)
            {
                int idInt;
                if (Int32.TryParse(id, out idInt))
                {
                    var dbId = repo.GetByKey(idInt);
                    if (dbId == null)
                        idsWithError.Add(id);
                }
                else
                {
                    idsWithError.Add(id);
                }
            }
            if (idsWithError.Count != 0)
            {
                var error = idsWithError.Aggregate(ErrorMessageString, (current, e) => current + " " + e);
                return new ValidationResult(error);
            }

            return ValidationResult.Success;
        }
    }
}
