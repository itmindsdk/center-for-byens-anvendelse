﻿#region Head
// <copyright file="IUnitOfWork.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainServices
{
    using System.Threading.Tasks;

    /// <summary>
    /// Responsible for saving changes in the data context
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Save changes to context
        /// </summary>
        /// <returns>Number of entities written to the context</returns>
        int Save();

        /// <summary>
        /// Save changes to context async
        /// </summary>
        /// <returns>Number of entities written to the context</returns>
        Task<int> SaveAsync();
    }
}
