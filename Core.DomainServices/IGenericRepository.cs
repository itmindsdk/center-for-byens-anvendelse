﻿#region Head
// <copyright file="IGenericRepository.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
namespace Core.DomainServices
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for a generic repository
    /// </summary>
    /// <typeparam name="T">Type for repository</typeparam>
    public interface IGenericRepository<T>
    {
        /// <summary>
        /// Get entities from repository
        /// </summary>
        /// <param name="filter">Filter expression</param>
        /// <param name="orderBy">Result ordering</param>
        /// <param name="includeProperties">Properties to include</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Enumerable of T</returns>
        [SuppressMessage("Microsoft.Naming", "CA1716", Justification = "C# only")]
        IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "",
            int? page = null,
            int? pageSize = null);

        /// <summary>
        /// Get entities from repository async
        /// </summary>
        /// <param name="filter">Filter expression</param>
        /// <param name="orderBy">Result ordering</param>
        /// <param name="includeProperties">Properties to include</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Enumerable of T</returns>
        Task<IEnumerable<T>> GetAsync(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "",
            int? page = null,
            int? pageSize = null);

        /// <summary>
        /// Get data set as <c>queryable</c>
        /// </summary>
        /// <returns>Data set as <c>queryable</c></returns>
        IQueryable<T> AsQueryable();

        /// <summary>
        /// Creates new instance of T.
        /// Note that it is not attached.
        /// </summary>
        /// <returns>New instance of T</returns>
        T Create();

        /// <summary>
        /// Get entity by key
        /// </summary>
        /// <param name="key">Primary key</param>
        /// <returns>Found entity or null.</returns>
        T GetByKey(params object[] key);

        /// <summary>
        /// Get entity by key async
        /// </summary>
        /// <param name="key">Primary key</param>
        /// <returns>Found entity or null.</returns>
        Task<T> GetByKeyAsync(params object[] key);

        /// <summary>
        /// Insert entity
        /// </summary>
        /// <param name="entity">Entity to insert</param>
        /// <returns>Object with populated key</returns>
        T Insert(T entity);

        /// <summary>
        /// Delete entity from repository by key
        /// </summary>
        /// <param name="key">Primary key of entity to delete</param>
        void DeleteByKey(params object[] key);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity with updated properties</param>
        void Update(T entity);

        /// <summary>
        /// Return count of entities in repository
        /// </summary>
        /// <param name="filter">Filter expression</param>
        /// <returns>Count of entities</returns>
        int Count(Expression<Func<T, bool>> filter = null);

        /// <summary>
        /// Return count of entities in repository async
        /// </summary>
        /// <param name="filter">Filter expression</param>
        /// <returns>Count of entities</returns>
        Task<int> CountAsync(Expression<Func<T, bool>> filter = null);
    }
}
