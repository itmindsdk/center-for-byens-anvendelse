﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Topic;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class TopicControllerUnitTests
    {
        private readonly TopicController _topicController;
        private readonly IGenericRepository<Topic> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        private readonly CreateTopicViewModel _createTopicViewModel;
        private readonly DeleteTopicViewModel _deleteTopicViewModel;
        private readonly EditTopicViewModel _editTopicViewModel;
        private readonly IndexTopicViewModel _indexTopicViewModel;

        public TopicControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Topic>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();

            _createTopicViewModel = new CreateTopicViewModel();
            _deleteTopicViewModel = new DeleteTopicViewModel();
            _editTopicViewModel = new EditTopicViewModel();
            _indexTopicViewModel = new IndexTopicViewModel();

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<Topic>(Arg.Any<CreateTopicViewModel>())
                .Returns(new Topic());
            _mapper.Map<Topic>(Arg.Any<EditTopicViewModel>())
                .Returns(new Topic());

            _mapper.Map<CreateTopicViewModel>(Arg.Any<Topic>())
                .Returns(_createTopicViewModel);
            _mapper.Map<DeleteTopicViewModel>(Arg.Any<Topic>())
                .Returns(_deleteTopicViewModel);
            _mapper.Map<EditTopicViewModel>(Arg.Any<Topic>())
                .Returns(_editTopicViewModel);
            _mapper.Map<IndexTopicViewModel>(Arg.Any<Topic>())
                .Returns(_indexTopicViewModel);

            _topicController = new TopicController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _topicController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _topicController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateTopicViewModel();

            // Act
            var ret = _topicController.Create(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateTopicViewModel();
            _topicController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _topicController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateTopicViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateTopicViewModel();

            // Act
            _topicController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<Topic>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var topic = new Topic();
            int? id = 1;
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var topic = new Topic();
            int? id = 1;
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditTopicViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _topicController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _topicController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Topic)null);

            // Act
            var ret = _topicController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidTopicDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var topic = new Topic {IsDeleted = true};
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditTopicViewModel();

            // Act
            var ret = _topicController.Edit(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditTopicViewModel();
            _topicController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _topicController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditTopicViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditTopicViewModel();

            // Act
            _topicController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<Topic>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var Topic = new Topic();
            int? id = 1;
            _repo.GetByKey(id).Returns(Topic);

            // Act
            var ret = _topicController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var topic = new Topic();
            int? id = 1;
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DeleteTopicViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var topic = new Topic();
            int? id = 1;
            _repo.GetByKey(id).Returns(topic);

            // Act
            _topicController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;

            // Act
            var ret = _topicController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidTopicDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var topic = new Topic {IsDeleted = true};
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _topicController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_UpdateDeletedGroupInRepo()
        {
            // Arrange
            const int id = 1;
            var topic = new Topic();
            _repo.GetByKey(id).Returns(topic);

            // Act
            _topicController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(topic);
            _unitOfWork.Received(1).Save();
            Assert.True(topic.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var topic = new Topic();
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;

            // Act
            var ret = _topicController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().DeleteByKey(Arg.Any<int>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdValidTopicDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var topic = new Topic { IsDeleted = true };
            _repo.GetByKey(id).Returns(topic);

            // Act
            var ret = _topicController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
