﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace UnitTests
{
    public class PopulateCouncilsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Council> _repo;
        private readonly PopulateCouncilsFilter _populateCouncilsFilter;

        public PopulateCouncilsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _executingContext.Controller = _controller;

            _repo = Substitute.For<IGenericRepository<Council>>();

            _populateCouncilsFilter = new PopulateCouncilsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuting_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var Council = new Council {Id = 0, Name = "Name"};
            var Councils = new List<Council> { Council };

            _repo.Get().Returns(Councils);

            var expectedListItem = new SelectListItem {Text = Council.Name, Value = Council.Id.ToString()};
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateCouncilsFilter.OnActionExecuting(_executingContext);

            // Assert
            var viewBagCouncilList = _controller.ViewBag.CouncilList as List<SelectListItem>;
            Assert.True(CheckIfListsAreEqual(expectedList, viewBagCouncilList));
        }

        [Fact]
        public void OnActionExecuting_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<Council>();
            _repo.Get().Returns(emptyList);

            // Act
            _populateCouncilsFilter.OnActionExecuting(_executingContext);
            var viewBagCouncilList = _controller.ViewBag.CouncilList as List<SelectListItem>;

            // Assert
            Assert.Equal(0, viewBagCouncilList.Count);
        }

        [Fact]
        public void OnActionExecuted_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateCouncilsFilter.OnActionExecuted(_executedContext);

            // Assert
        }

        static bool CheckIfListsAreEqual(IEnumerable<SelectListItem> expectedListItems, IEnumerable<SelectListItem> actualListItems)
        {
            foreach (var actualListItem in actualListItems)
            {
                if (!expectedListItems.Any(expectedItem => expectedItem.Text == actualListItem.Text && expectedItem.Value == actualListItem.Value))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
