﻿using System.Web.Mvc;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class HomeControllerUnitTests
    {
        private readonly HomeController _homeController;
        private readonly IAppSettings _appSettings;

        public HomeControllerUnitTests()
        {
            _appSettings = Substitute.For<IAppSettings>();
            _homeController = new HomeController(_appSettings);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _homeController.Index();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }
        #endregion
    }
}
