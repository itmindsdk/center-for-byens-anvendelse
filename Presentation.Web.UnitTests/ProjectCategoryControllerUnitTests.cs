﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ProjectCategory;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class ProjectCategoryControllerUnitTests
    {
        private readonly ProjectCategoryController _projectCategoryController;
        private readonly IGenericRepository<ProjectCategory> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        private readonly CreateProjectCategoryViewModel _createProjectCategoryViewModel;
        private readonly DeleteProjectCategoryViewModel _deleteProjectCategoryViewModel;
        private readonly EditProjectCategoryViewModel _editProjectCategoryViewModel;
        private readonly IndexProjectCategoryViewModel _indexProjectCategoryViewModel;

        public ProjectCategoryControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<ProjectCategory>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<ProjectCategory>(Arg.Any<CreateProjectCategoryViewModel>())
                .Returns(new ProjectCategory());
            _mapper.Map<ProjectCategory>(Arg.Any<EditProjectCategoryViewModel>())
                .Returns(new ProjectCategory());

            _createProjectCategoryViewModel = new CreateProjectCategoryViewModel();
            _deleteProjectCategoryViewModel = new DeleteProjectCategoryViewModel();
            _editProjectCategoryViewModel = new EditProjectCategoryViewModel();
            _indexProjectCategoryViewModel = new IndexProjectCategoryViewModel();

            _mapper.Map<CreateProjectCategoryViewModel>(Arg.Any<ProjectCategory>())
                .Returns(_createProjectCategoryViewModel);
            _mapper.Map<DeleteProjectCategoryViewModel>(Arg.Any<ProjectCategory>())
                .Returns(_deleteProjectCategoryViewModel);
            _mapper.Map<EditProjectCategoryViewModel>(Arg.Any<ProjectCategory>())
                .Returns(_editProjectCategoryViewModel);
            _mapper.Map<IndexProjectCategoryViewModel>(Arg.Any<ProjectCategory>())
                .Returns(_indexProjectCategoryViewModel);

            _projectCategoryController = new ProjectCategoryController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _projectCategoryController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _projectCategoryController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateProjectCategoryViewModel();

            // Act
            var ret = _projectCategoryController.Create(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateProjectCategoryViewModel();
            _projectCategoryController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _projectCategoryController.Create(vm) as ViewResult;

            // Assert
            Assert.IsType<CreateProjectCategoryViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateProjectCategoryViewModel();

            // Act
            _projectCategoryController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<ProjectCategory>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var projectCategory = new ProjectCategory();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var projectCategory = new ProjectCategory();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditProjectCategoryViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _projectCategoryController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _projectCategoryController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((ProjectCategory)null);

            // Act
            var ret = _projectCategoryController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidProjectCategoryDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var projectCategory = new ProjectCategory {IsDeleted = true};
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditProjectCategoryViewModel();

            // Act
            var ret = _projectCategoryController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditProjectCategoryViewModel();
            _projectCategoryController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _projectCategoryController.Edit(vm) as ViewResult;

            // Assert
            Assert.IsType<EditProjectCategoryViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditProjectCategoryViewModel();

            // Act
            _projectCategoryController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<ProjectCategory>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete Get
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var projectCategory = new ProjectCategory();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var projectCategory = new ProjectCategory();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DeleteProjectCategoryViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var projectCategory = new ProjectCategory();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            _projectCategoryController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((ProjectCategory)null);

            // Act
            var ret = _projectCategoryController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidProjectCategoryDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var projectCategory = new ProjectCategory {IsDeleted = true};
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _projectCategoryController.Delete(id) as HttpStatusCodeResult;

            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_CallUpdateInRepo()
        {
            // Arrange
            const int id = 1;
            var projectCategory = new ProjectCategory();
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            _projectCategoryController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(projectCategory);
            _unitOfWork.Received(1).Save();
            Assert.True(projectCategory.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var projectCategory = new ProjectCategory();
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;

            // Act
            var ret = _projectCategoryController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().DeleteByKey(Arg.Any<int>());
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdValidProjectCategoryNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _repo.GetByKey(id).Returns((ProjectCategory)null);

            // Act
            var ret = _projectCategoryController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdValidProjectCategoryDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var projectCategory = new ProjectCategory {IsDeleted = true};
            _repo.GetByKey(id).Returns(projectCategory);

            // Act
            var ret = _projectCategoryController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
