﻿#region Head
// <copyright file="TeamControllerUnitTests.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Team;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class TeamControllerUnitTests
    {
        private readonly TeamController _teamController;
        private readonly IGenericRepository<Team> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        private readonly CreateTeamViewModel _createTeamViewModel;
        private readonly DeleteTeamViewModel _deleteTeamViewModel;
        private readonly EditTeamViewModel _editTeamViewModel;
        private readonly IndexTeamViewModel _indexTeamViewModel;

        public TeamControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Team>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();

            _createTeamViewModel = new CreateTeamViewModel();
            _deleteTeamViewModel = new DeleteTeamViewModel();
            _editTeamViewModel = new EditTeamViewModel();
            _indexTeamViewModel = new IndexTeamViewModel();

            new UserMock().LogOn();

            _mapper.Map<Team>(Arg.Any<CreateTeamViewModel>())
                .Returns(new Team());
            _mapper.Map<Team>(Arg.Any<EditTeamViewModel>())
                .Returns(new Team());

            _mapper.Map<CreateTeamViewModel>(Arg.Any<Team>())
                .Returns(_createTeamViewModel);
            _mapper.Map<DeleteTeamViewModel>(Arg.Any<Team>())
                .Returns(_deleteTeamViewModel);
            _mapper.Map<EditTeamViewModel>(Arg.Any<Team>())
                .Returns(_editTeamViewModel);
            _mapper.Map<IndexTeamViewModel>(Arg.Any<Team>())
                .Returns(_indexTeamViewModel);

            _teamController = new TeamController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_GetCalled_ReturnsView()
        {
            // Act
            var ret = _teamController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create
        [Fact]
        public void Create_GetCalled_ReturnsView()
        {
            // Act
            var ret = _teamController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Create_GetCalled_ReturnsCreateTeamViewModel()
        {
            // Act
            var ret = _teamController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<CreateTeamViewModel>(ret.Model);
        }

        [Fact]
        public void Create_PostCalledWithValidViewModel_TeamCreated()
        {
            // Arrange
            var vm = new CreateTeamViewModel();
            var team = new Team();

            _mapper.Map<Team>(Arg.Any<CreateTeamViewModel>())
                .Returns(team);

            // Act
            _teamController.Create(vm);

            // Assert
            _repo.ReceivedWithAnyArgs(1).Insert(team);
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit
        [Fact]
        public void Edit_GetCalledWithoutId_ReturnsHttpBadRequest()
        {
            // Act
            var ret = _teamController.Edit((int?)null);

            // Assert
            Assert.IsType(typeof(HttpStatusCodeResult), ret);
            Assert.Equal(((HttpStatusCodeResult)ret).StatusCode, (int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void Edit_GetCalledWithValidId_ReturnsEditTeamViewModel()
        {
            // Arrange
            var validId = 1;
            var team = new Team();
            _repo.GetByKey(validId).Returns(team);

            // Act
            var ret = _teamController.Edit(validId) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<EditTeamViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_GetCalledWithInvalidId_ReturnsHttpNotFound()
        {
            // Arrange
            var invalidId = 1;
            _repo.GetByKey(invalidId).Returns((Team)null);

            // Act
            var ret = _teamController.Edit(invalidId);

            // Assert
            Assert.IsType(typeof(HttpNotFoundResult), ret);
        }

        [Fact]
        public void Edit_PostCalledWithValidViewModel_TeamUpdated()
        {
            // Arrange
            var vm = new EditTeamViewModel();
            var team = new Team();

            _mapper.Map<Team>(Arg.Any<EditTeamViewModel>())
                .Returns(team);

            // Act
            _teamController.Edit(vm);

            // Assert
            _repo.Received(1).Update(team);
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_GetCalledWithoutId_ReturnsHttpBadRequest()
        {
            // Act
            var ret = _teamController.Delete(null);

            // Assert
            Assert.IsType(typeof(HttpStatusCodeResult), ret);
            Assert.Equal(((HttpStatusCodeResult)ret).StatusCode, (int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void Delete_GetCalledWithValidId_ReturnsDeleteTeamViewModel()
        {
            // Arrange
            var validId = 1;
            var team = new Team();
            _repo.GetByKey(validId).Returns(team);

            // Act
            var ret = _teamController.Delete(validId) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<DeleteTeamViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_GetCalledWithValidIdWhileProjectLeaderIsAssociatedWithTeam_DoesNotReturnDeleteTeamViewModel()
        {
            // Arrange
            var validId = 1;
            var team = new Team();
            team.ProjectLeaders.Add(new ProjectLeader());
            _repo.GetByKey(validId).Returns(team);

            // Act
            var ret = _teamController.Delete(validId) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsNotType(typeof(DeleteTeamViewModel), ret.Model);
        }

        [Fact]
        public void Delete_GetCalledWithInvalidId_ReturnsHttpNotFound()
        {
            // Arrange
            var invalidId = 1;
            _repo.GetByKey(invalidId).Returns((Team)null);

            // Act
            var ret = _teamController.Delete(invalidId);

            // Assert
            Assert.IsType(typeof(HttpNotFoundResult), ret);
        }

        [Fact]
        public void Delete_PostGetCalledWithValidId_TeamIsDeletedUpdated()
        {
            // Arrange
            var validId = 1;
            var team = new Team();
            _repo.GetByKey(validId).Returns(team);

            // Act
            _teamController.DeleteConfirmed(validId);

            // Assert
            Assert.True(team.IsDeleted);
            _repo.Received(1).Update(team);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void Delete_PostGetCalledWithValidIdWhileProjectLeaderIsAssociatedWithTeam_DoesNotDeleteTeam()
        {
            // Arrange
            var validId = 1;
            var team = new Team();
            team.ProjectLeaders.Add(new ProjectLeader());
            _repo.GetByKey(validId).Returns(team);

            // Act
            _teamController.Delete(validId);

            // Assert
            _repo.DidNotReceive().DeleteByKey(validId);
        }
        #endregion
    }
}
