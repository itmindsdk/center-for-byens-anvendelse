﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using NSubstitute.Core.Arguments;
using Presentation.Web.Exceptions;
using Presentation.Web.Helpers;
using Xunit;

namespace Presentation.Web.UnitTests.Helpers
{
    public class AppSettingsUnitTests
    {
        private readonly IGenericRepository<AppSetting> _appSettingsRepo;

        public AppSettingsUnitTests()
        {
            _appSettingsRepo = Substitute.For<IGenericRepository<AppSetting>>();
        }

        [Fact]
        public void GetAdminRoles_ValidAppSettingSupplied_CorrectEnumerableReturned()
        {
            // Arrange
            var input = "rolle1;roleTwo;string";
            _appSettingsRepo
                .Get()
                .Returns(new List<AppSetting>{new AppSetting {Key = "adAdminRoles", Value = input} });

            var expectedOutput = new List<string>
            {
                "rolle1",
                "roleTwo",
                "string"
            }.OrderBy(x => x);  // Make sure order is irrelevant

            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var roles = appSettings.AdminRoles;

            // Assert
            roles = roles.OrderBy(x => x);  // Make sure order is irrelevant
            Assert.Equal(expectedOutput, roles);
        }

        [Fact]
        public void GetAdminRoles_NoAdAdminRoles_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.AdminRoles);
        }

        [Fact]
        public void GetUserRoles_ValidAppSettingSupplied_CorrectEnumerableReturned()
        {
            // Arrange
            var input = "rolle1;roleTwo;string";
            _appSettingsRepo
                .Get()
                .Returns(new List<AppSetting> { new AppSetting { Key = "adUserRoles", Value = input } });

            var expectedOutput = new List<string>
            {
                "rolle1",
                "roleTwo",
                "string"
            }.OrderBy(x => x);  // Make sure order is irrelevant

            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var roles = appSettings.UserRoles;

            // Assert
            roles = roles.OrderBy(x => x);  // Make sure order is irrelevant
            Assert.Equal(expectedOutput, roles);
        }

        [Fact]
        public void GetUserRoles_NoAdUserRoles_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.UserRoles);
        }

        [Fact]
        public void GetStatusExplanationFileName_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> {new AppSetting {Key = "StatusExplanationFileName", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.StatusExplanationFileName;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetStatusExplanationFileName_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.StatusExplanationFileName);
        }

        [Fact]
        public void GetAgendaPointZeroDescription_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "AgendaPointZeroDescription", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.AgendaPointZeroDescription;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetAgendaPointZeroDescription_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.AgendaPointZeroDescription);
        }

        [Fact]
        public void GetAppName_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "appName", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.AppName;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetAppName_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.AppName);
        }

        [Fact]
        public void GetQuickAddAppendixToAgendaPoint_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "QuickAddAppendixToAgendaPoint", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.QuickAddAppendixToAgendaPoint;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetQuickAddAppendixToAgendaPoint_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.AppName);
        }

        [Fact]
        public void GetQuickAddAppendixToDrawing_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "QuickAddAppendixToDrawing", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.QuickAddAppendixToDrawing;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetQuickAddAppendixToDrawing_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.QuickAddAppendixToDrawing);
        }

        [Fact]
        public void GetQuickAddAppendixToProject_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "QuickAddAppendixToProject", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.QuickAddAppendixToProject;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetQuickAddAppendixToProject_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.QuickAddAppendixToProject);
        }

        [Fact]
        public void GetQuickCreateAgendaPoint_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "QuickCreateAgendaPoint", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.QuickCreateAgendaPoint;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetQuickCreateAgendaPoint_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.QuickCreateAgendaPoint);
        }

        [Fact]
        public void GetQuickCreateDrawing_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "QuickCreateDrawing", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.QuickCreateDrawing;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetQuickCreateDrawing_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.QuickCreateDrawing);
        }

        [Fact]
        public void GetQuickCreateProject_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "QuickCreateProject", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.QuickCreateProject;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetQuickCreateProject_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.QuickCreateProject);
        }

        [Fact]
        public void GetUploadPath_ValidAppSettingSupplied_CorrectValueReturned()
        {
            // Arrange
            const string expectedValue = "a value";
            _appSettingsRepo.Get().Returns(new List<AppSetting> { new AppSetting { Key = "UploadPath", Value = expectedValue } });
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            var result = appSettings.UploadPath;

            // Assert
            Assert.Equal(expectedValue, result);
        }

        [Fact]
        public void GetUploadPath_NoStatusExplanationFileName_ErrorMessageExceptionThrown()
        {
            // Arrange
            _appSettingsRepo.Get().Returns(new List<AppSetting>());
            var appSettings = new AppSettings(_appSettingsRepo);

            // Act
            // Assert
            Assert.Throws<ErrorMessageException>(() => appSettings.UploadPath);
        }
    }
}
