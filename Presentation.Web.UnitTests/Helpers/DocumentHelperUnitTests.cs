﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO.Abstractions;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Helpers;
using Xunit;

namespace Presentation.Web.UnitTests.Helpers
{
    public class DocumentHelperUnitTests
    {
        private readonly HttpRequestBase _request;
        private readonly HttpServerUtilityBase _server;
        private readonly DocumentHelper _documentHelper;
        private readonly HttpPostedFileBase _uploadedFile;
        private readonly IFileSystem _fileSystem;
        private readonly IGenericRepository<Document> _documentRepo;
        private int? _foreignKeyId;
        private readonly IAppSettings _appSettings;

        public DocumentHelperUnitTests()
        {
            _request = Substitute.For<HttpRequestBase>();
            _server = Substitute.For<HttpServerUtilityBase>();
            var fakedFileCollection = Substitute.For<HttpFileCollectionBase>();

            // setting uploaded file
            _uploadedFile = Substitute.For<HttpPostedFileBase>();
            _uploadedFile.ContentLength.Returns(1);
            _uploadedFile.FileName.Returns("firstFile.txt");
            _uploadedFile.ContentType.Returns("text");

            var fileKeys = new[] { "1" };

            // Fake file collection
            fakedFileCollection.GetEnumerator().Returns(fileKeys.GetEnumerator());
            fakedFileCollection[fileKeys[0]].Returns(_uploadedFile);

            // Fake form data
            _foreignKeyId = 1;
            var requestForm = Substitute.For<NameValueCollection>();
            requestForm.GetEnumerator().Returns(fileKeys.GetEnumerator());
            requestForm["id"].Returns(_foreignKeyId.ToString());

            // request returns
            _request.Files.Returns(fakedFileCollection);
            _request.Form.Returns(requestForm);

            _documentRepo = Substitute.For<IGenericRepository<Document>>();
            _appSettings = Substitute.For<IAppSettings>();
            _appSettings.UploadPath.Returns("/Documents/CBA_Upload_folder");

            var httpContext = Substitute.For<IHttpContext>();
            // Test as local
            httpContext.IsLocal().Returns(true);

            // the documenthelper
            _fileSystem = Substitute.For<IFileSystem>();
            _documentHelper = new DocumentHelper(_documentRepo, _fileSystem, _appSettings, httpContext);
        }

        #region UploadFile

        [Fact]
        public void UploadFile_SavedSuccesfullyAndHasForeignKeyInForm_ReturnsObjectWithGuidFileForeignkey()
        {
            // Arrange
            // See constructor

            // Act
            var res = _documentHelper.UploadFile(_request, _server);

            // Assert
            Assert.IsAssignableFrom<Guid>(res.Guid);
            Assert.Equal(_uploadedFile, res.File);
            Assert.Equal(_foreignKeyId, res.ForeignKeyId);
        }

        [Fact]
        public void UploadFile_NoForeignKey_ReturnsObjectWithGuidFileNoForeignkey()
        {
            // Arrange
            _foreignKeyId = null;

            // Act
            var res = _documentHelper.UploadFile(_request, _server);

            // Assert
            Assert.IsAssignableFrom<Guid>(res.Guid);
            Assert.Equal(_uploadedFile, res.File);
            Assert.Equal(_foreignKeyId, null);
        }

        #endregion

        #region Download

        [Fact]
        public void Download_FileFound_ReturnsOfTypeFileResult()
        {
            // Arrange
            _foreignKeyId = null;
            _fileSystem.File.ReadAllBytes(Arg.Any<string>()).Returns(new byte[5]);
            var guid = new Guid();
            var doc = new ProjectDocument
            {
                Guid = guid,
                FileName = _uploadedFile.FileName
            };
            var docList = new List<Document>();
            docList.Add(doc);
            _documentRepo.Get(Arg.Any<Expression<Func<Document,bool>>>()).Returns(docList);

            // Act
            var res = _documentHelper.Download(_server, guid);

            // Assert
            Assert.IsAssignableFrom<FileResult>(res);
        }

        [Fact]
        public void Download_FileNotFound_ThrowsArgumentNullException()
        {
            // Arrange
            _foreignKeyId = null;
            _fileSystem.File.ReadAllBytes(Arg.Any<string>()).Returns(null as byte[]);
            var guid = new Guid();
            var doc = new ProjectDocument
            {
                Guid = guid,
                FileName = _uploadedFile.FileName
            };
            var docList = new List<Document>();
            docList.Add(doc);
            _documentRepo.Get(Arg.Any<Expression<Func<Document, bool>>>()).Returns(docList);

            // Act
            var dele = new Func<FileResult>(() => _documentHelper.Download(_server, guid));

            // Assert
            Assert.Throws<ArgumentNullException>(dele);
        }

        #endregion

        #region DeleteFiles

        [Fact]
        public void DeleteFiles_FileExists_ReturnsStatusCodeOk()
        {
            // Arrange
            var guid = new Guid();
            var guidList = new List<Guid>
            {
                guid
            };
            _fileSystem.File.Exists(Arg.Any<string>()).Returns(true);

            // Act
            var res = _documentHelper.DeleteFiles(guidList, _server);

            // Assert
            Assert.Equal(true, res);
        }

        [Fact]
        public void DeleteFiles_FileDoesNotExists_ReturnsStatusCodeBadRequest()
        {
            // Arrange
            var guid = new Guid();
            var guidList = new List<Guid>
            {
                guid
            };
            _fileSystem.File.Exists(Arg.Any<string>()).Returns(false);

            // Act
            var res = _documentHelper.DeleteFiles(guidList, _server);

            // Assert
            Assert.Equal(false, res);
        }

        #endregion
    }
}
