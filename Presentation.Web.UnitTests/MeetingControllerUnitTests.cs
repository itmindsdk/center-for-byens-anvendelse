﻿#region Head
// <copyright file="MeetingControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Meeting;
using Presentation.Web.Models.Meeting.MeetingAgendaPoint;
using Presentation.Web.Models.MeetingAgendaPoint;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class MeetingControllerUnitTests
    {
        private readonly MeetingController _meetingController;

        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<MeetingAgendaPoint> _meetingAgendaPointRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDateTime _dateTime;
        private readonly UserMock _userMock;
        private readonly IAppSettings _appSettings;
        private readonly IAdHelper _adHelper;

        public MeetingControllerUnitTests()
        {
            _meetingRepo = Substitute.For<IGenericRepository<Meeting>>();
            _agendaPointRepo = Substitute.For<IGenericRepository<AgendaPoint>>();
            _meetingAgendaPointRepo = Substitute.For<IGenericRepository<MeetingAgendaPoint>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMapper>();
            _dateTime = Substitute.For<IDateTime>();
            _adHelper = Substitute.For<IAdHelper>();
            _appSettings = Substitute.For<IAppSettings>();
            _appSettings.AgendaPointZeroDescription
                .Returns("Some description");


            _userMock = new UserMock();
            _userMock.LogOn();

            _meetingController = new MeetingController(
                _meetingRepo,
                _meetingAgendaPointRepo,
                _agendaPointRepo,
                _unitOfWork,
                _mapper,
                _dateTime,
                _appSettings,
                _adHelper);
        }

        #region Index

        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnView()
        {
            // Arrange
            // Act
            var ret = _meetingController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnViewModel()
        {
            // Arrange
            // Act
            var model = _meetingController.Create() as ViewResult;

            // Assert
            Assert.IsType<CreateMeetingViewModel>(model.Model);
        }

        [Fact]
        public void Create_Call_AgendaPointGetCalled()
        {
            // Arrange
            // Act
            _meetingController.Create();

            // Assert
            _agendaPointRepo.Received(1).Get(Arg.Any<Expression<Func<AgendaPoint, bool>>>());
        }

        [Fact]
        public void Create_MeetingAgendaPointsFromRepo_MeetingAgendaPointViewModelsInModel()
        {
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                new AgendaPoint(),
                new AgendaPoint()
            };

            var agendaPointViewModelList = new List<AgendaPointViewModel>
            {
                new AgendaPointViewModel(),
                new AgendaPointViewModel()
            };

            _agendaPointRepo.Get(Arg.Any<Expression<Func<AgendaPoint, bool>>>())
                .Returns(agendaPointList);

            _mapper.Map<IList<AgendaPointViewModel>>(Arg.Any<IEnumerable<AgendaPoint>>())
                .Returns(agendaPointViewModelList);

            // Act
            var ret = _meetingController.Create() as ViewResult;
            var model = ret.Model as CreateMeetingViewModel;

            // Assert
            Assert.NotNull(model.MeetingAgendaPoints);
            Assert.IsAssignableFrom<IList<MeetingAgendaPointViewModel>>(model.MeetingAgendaPoints);
            Assert.Equal(2, model.MeetingAgendaPoints.Count);
        }

        [Fact]
        public void Create_EmptyAgendaPointListFromRepo_EmptyListInModel()
        {
            // Arrange
            var emptyAgendaPointList = new List<AgendaPoint>();
            var emptyAgendaPointViewModelList = new List<AgendaPointViewModel>();
            _agendaPointRepo.Get(Arg.Any<Expression<Func<AgendaPoint, bool>>>())
                .Returns(emptyAgendaPointList);

            _mapper.Map<IList<AgendaPointViewModel>>(Arg.Any<IEnumerable<AgendaPoint>>())
                .Returns(emptyAgendaPointViewModelList);

            // Act
            var ret = _meetingController.Create() as ViewResult;
            var model = ret.Model as CreateMeetingViewModel;

            // Assert
            Assert.NotNull(model.MeetingAgendaPoints);
            Assert.IsAssignableFrom<IList<MeetingAgendaPointViewModel>>(model.MeetingAgendaPoints);
            Assert.Equal(0, model.MeetingAgendaPoints.Count);
        }

        [Fact]
        public void Create_NullFromRepo_EmptyListInModel()
        {
            // Arrange
            _agendaPointRepo.Get(Arg.Any<Expression<Func<AgendaPoint, bool>>>())
                .Returns((IEnumerable<AgendaPoint>)null);

            // Act
            var ret = _meetingController.Create() as ViewResult;
            var model = ret.Model as CreateMeetingViewModel;

            // Assert
            Assert.NotNull(model.MeetingAgendaPoints);
            Assert.IsAssignableFrom<IList<MeetingAgendaPointViewModel>>(model.MeetingAgendaPoints);
            Assert.Equal(0, model.MeetingAgendaPoints.Count);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelNull_ReturnBadRequest()
        {
            // Arrange
            var model = (CreateMeetingViewModel)null;

            // Act
            var ret = _meetingController.Create(model) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void CreatePost_ModelNotValidNoAgendaPoints_ReturnSameViewModel()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            _meetingController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _meetingController.Create(createMeetingViewModel) as ViewResult;

            // Assert
            Assert.Equal(createMeetingViewModel, ret.Model);
        }

        [Fact]
        public void CreatePost_ModelNotValidNoAgendaPoints_ReturnView()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            _meetingController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _meetingController.Create(createMeetingViewModel);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelValidAgendaPointsNull_RedirectAction()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _meetingRepo.Create().Returns(meeting);
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);

            // Act
            var ret = _meetingController.Create(createMeetingViewModel);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelValidAgendaPointsNull_InsertAndUpdateCalled()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _meetingRepo.Create().Returns(meeting);
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);

            // Act
            _meetingController.Create(createMeetingViewModel);

            // Assert
            _meetingRepo.Received(1).Insert(meeting);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void CreatePost_ModelValidAgendaPointsNull_CreatedOnSet()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);
            _meetingRepo.Create().Returns(meeting);

            var dateTime = new DateTime(2015, 10, 1);
            _dateTime.Now.Returns(dateTime);

            // Act
            _meetingController.Create(createMeetingViewModel);

            // Assert
            Assert.Equal(dateTime, meeting.CreatedOn);
        }

        [Fact]
        public void CreatePost_ModelValid1CheckedMeetingAgendaPoint_MeetingAgendaPointRepoInsertCalledOnce()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _meetingRepo.Create().Returns(meeting);
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);

            var agendaPoint1 = new AgendaPointViewModel();
            var agendaPoint2 = new AgendaPointViewModel { IsAssignedToMeeting = true };

            var meetingAgendaPointViewModelList = new List<MeetingAgendaPointViewModel>
            {
                new MeetingAgendaPointViewModel {Id = 1, AgendaPoint = agendaPoint1},
                new MeetingAgendaPointViewModel {Id = 1, AgendaPoint = agendaPoint2}
            };
            createMeetingViewModel.MeetingAgendaPoints = meetingAgendaPointViewModelList;

            _agendaPointRepo.GetByKey(Arg.Any<object>()).Returns(new AgendaPoint());

            // Act
            _meetingController.Create(createMeetingViewModel);

            // Assert
            _meetingAgendaPointRepo.Received(1).Insert(Arg.Any<MeetingAgendaPoint>());
        }

        [Fact]
        public void CreatePost_ModelValidNoCheckedAgendaPoints_MeetingAgendaPointRepoInsertNotCalled()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _meetingRepo.Create().Returns(meeting);
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);

            var agendaPoint1 = new AgendaPointViewModel { IsAssignedToMeeting = false };
            var agendaPoint2 = new AgendaPointViewModel { IsAssignedToMeeting = false };

            var meetingAgendaPointViewModelList = new List<MeetingAgendaPointViewModel>
            {
                new MeetingAgendaPointViewModel {Id = 1, AgendaPoint = agendaPoint1},
                new MeetingAgendaPointViewModel {Id = 1, AgendaPoint = agendaPoint2}
            };
            createMeetingViewModel.MeetingAgendaPoints = meetingAgendaPointViewModelList;

            // Act
            _meetingController.Create(createMeetingViewModel);

            // Assert
            _meetingAgendaPointRepo.Received(0).Insert(new MeetingAgendaPoint());
        }

        [Fact]
        public void CreatePost_ModelValidNoCheckedAgendaPoints_MeetingAgendaPointZeroCreated()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _meetingRepo.Create().Returns(meeting);
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);

            var agendaPoint1 = new AgendaPointViewModel { IsAssignedToMeeting = false };
            var agendaPoint2 = new AgendaPointViewModel { IsAssignedToMeeting = false };

            var meetingAgendaPointViewModelList = new List<MeetingAgendaPointViewModel>
            {
                new MeetingAgendaPointViewModel {Id = 1, AgendaPoint = agendaPoint1},
                new MeetingAgendaPointViewModel {Id = 1, AgendaPoint = agendaPoint2}
            };
            createMeetingViewModel.MeetingAgendaPoints = meetingAgendaPointViewModelList;

            // Act
            _meetingController.Create(createMeetingViewModel);

            // Assert
            Assert.Equal(1, meeting.MeetingAgendaPoints.Count);
        }


        [Fact]
        public void CreatePost_ModelValidNoInvitedPersons_InvitedPeopleStringNull()
        {
            // Arrange
            var createMeetingViewModel = new CreateMeetingViewModel();
            var meeting = new Meeting();
            _meetingRepo.Create().Returns(meeting);
            _mapper.Map<Meeting>(createMeetingViewModel)
                .Returns(meeting);

            // Act
            _meetingController.Create(createMeetingViewModel);

            // Assert
            Assert.Null(meeting.InvitedPeople);
        }
        #endregion

        #region Details
        [Fact]
        public void Details_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _meetingController.Details(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Details_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _meetingRepo.GetByKey(id).Returns((Meeting)null);

            // Act
            var ret = _meetingController.Details(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Details_Call_ReturnView()
        {
            // Arrange
            const int id = 1;
            var meeting = new Meeting();
            _meetingRepo.GetByKey(id)
                .Returns(meeting);
            var meetingViewModel = new DetailsMeetingViewModel();
            _mapper.Map<DetailsMeetingViewModel>(meeting)
                .Returns(meetingViewModel);

            // Act
            var ret = _meetingController.Details(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Details_Call_ReturnViewModel()
        {
            // Arrange
            const int id = 1;
            var meeting = new Meeting();
            _meetingRepo.GetByKey(id)
                .Returns(meeting);
            var meetingViewModel = new DetailsMeetingViewModel();
            _mapper.Map<DetailsMeetingViewModel>(meeting)
                .Returns(meetingViewModel);

            // Act
            var ret = _meetingController.Details(id) as ViewResult;

            // Assert
            Assert.IsType<DetailsMeetingViewModel>(ret.Model);
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _meetingController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _meetingRepo.GetByKey(id)
                .Returns((Meeting)null);

            // Act
            var ret = _meetingController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_Call_ReturnView()
        {
            // Arrange
            const int id = 1;
            var meeting = new Meeting();
            _meetingRepo.GetByKey(id)
                .Returns(meeting);
            var meetingViewModel = new EditMeetingViewModel();
            _mapper.Map<EditMeetingViewModel>(meeting)
                .Returns(meetingViewModel);

            // Act
            var ret = _meetingController.Edit(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Edit_Call_ReturnViewModel()
        {
            // Arrange
            const int id = 1;
            var meeting = new Meeting();
            _meetingRepo.GetByKey(id)
                .Returns(meeting);
            var meetingViewModel = new EditMeetingViewModel();
            _mapper.Map<EditMeetingViewModel>(meeting)
                .Returns(meetingViewModel);

            // Act
            var ret = _meetingController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditMeetingViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_2AgendaPointsFromRepoSecondSelectedInMeeting_SecondAgendaPointSelectedInViewModel()
        {
            // Arrange
            const int meetingId = 1;
            var selectedAgendaPoint = new AgendaPoint
            {
                Id = 2,
                MeetingAgendaPoint = new MeetingAgendaPoint
                {
                    Number = 1
                }
            };
            var selectedMeetingAgendaPoint = new MeetingAgendaPoint() { Id = 2, AgendaPoint = selectedAgendaPoint };
            var meeting = new Meeting
            {
                MeetingAgendaPoints = new List<MeetingAgendaPoint>
                {
                    selectedMeetingAgendaPoint
                }
            };
            _meetingRepo.GetByKey(meetingId)
                .Returns(meeting);

            var repoAgendaPoints = new List<AgendaPoint>
            {
                new AgendaPoint {Id = 1},
                new AgendaPoint {Id = 3}
            };

            _agendaPointRepo.Get(Arg.Any<Expression<Func<AgendaPoint, bool>>>())
                .Returns(repoAgendaPoints);

            var repoAgendaPointViewModels = new List<AgendaPointViewModel>
            {
                new AgendaPointViewModel {Id = 1},
                new AgendaPointViewModel {Id = 2, IsAssignedToMeeting = true},
                new AgendaPointViewModel {Id = 3}
            };
            var meetingViewModel = new EditMeetingViewModel
            {
                MeetingAgendaPoints = new List<MeetingAgendaPointViewModel>
                {
                    new MeetingAgendaPointViewModel
                    {
                        AgendaPoint = new AgendaPointViewModel{ IsAssignedToMeeting = true, Id = 2}
                    }
                }
            };

            _mapper.Map<EditMeetingViewModel>(meeting)
                .Returns(meetingViewModel);

            _mapper.Map<IEnumerable<AgendaPointViewModel>>(repoAgendaPoints)
                .Returns(repoAgendaPointViewModels);

            // Act
            var ret = _meetingController.Edit(meetingId) as ViewResult;
            var model = ret.Model as EditMeetingViewModel;

            // Assert
            Assert.Equal(3, model.MeetingAgendaPoints.Count);
            Assert.True(model.MeetingAgendaPoints[1].AgendaPoint.IsAssignedToMeeting);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelNull_ReturnBadRequest()
        {
            // Arrange
            EditMeetingViewModel model = null;

            // Act
            var ret = _meetingController.Edit(model) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnView()
        {
            // Arrange
            var model = new EditMeetingViewModel();
            _meetingController.ModelState.AddModelError("error", "error");
            var meeting = new Meeting();
            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);
            // Act
            var ret = _meetingController.Edit(model);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnViewModel()
        {
            // Arrange
            var model = new EditMeetingViewModel { Id = 1 };
            _meetingController.ModelState.AddModelError("error", "error");
            var meeting = new Meeting();
            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            // Act
            var ret = _meetingController.Edit(model) as ViewResult;

            // Assert
            Assert.IsType<EditMeetingViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelNotValid_CreatedModifedViewBagSet()
        {
            // Arrange
            var model = new EditMeetingViewModel();
            _meetingController.ModelState.AddModelError("error", "error");
            var dateTime = new DateTime(2015, 10, 5);
            const string creator = "TesterCreated";
            const string modifier = "TesterModified";
            var meeting = new Meeting
            {
                CreatedBy = creator,
                CreatedOn = dateTime,
                ModifiedBy = modifier,
                ModifiedOn = dateTime
            };
            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            // Act
            _meetingController.Edit(model);

            // Assert
            Assert.Equal(creator, _meetingController.ViewBag.CreatedBy);
            Assert.Equal(dateTime.ToShortDateString(), _meetingController.ViewBag.CreatedOn);
            Assert.Equal(modifier, _meetingController.ViewBag.ModifiedBy);
            Assert.Equal(dateTime, _meetingController.ViewBag.ModifiedOn);
        }

        [Fact]
        public void EditPost_IdNotFound_ReturnNotFound()
        {
            // Arrange
            var model = new EditMeetingViewModel { Id = 1 };
            _meetingRepo.GetByKey(model.Id)
                .Returns((Meeting)null);

            // Act
            var ret = _meetingController.Edit(model) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, ret.StatusCode);
        }

        [Fact]
        public void EditPost_ModelValid_UpdateAndSaveCalled()
        {
            // Arrange
            var model = new EditMeetingViewModel { Id = 1 };
            var meeting = new Meeting();
            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            // Act
            _meetingController.Edit(model);

            // Assert
            _meetingRepo.Received(1).Update(meeting);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void EditPost_ModelValid_RedirectResult()
        {
            // Arrange
            var model = new EditMeetingViewModel { Id = 1 };
            var meeting = new Meeting();
            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            // Act
            var ret = _meetingController.Edit(model);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelValidWith1SelectedMeetingAgendaPoint_AttachedMeetingAgendaPointsAddedToMeeting()
        {
            // Arrange
            int selectedId = 2;

            var model = new EditMeetingViewModel
            {
                Id = 1,
                MeetingAgendaPoints = new List<MeetingAgendaPointViewModel>
                {
                    new MeetingAgendaPointViewModel
                    {
                        Id = 1,
                        Number = 0,
                        AgendaPoint = new AgendaPointViewModel { Id = 1, IsAssignedToMeeting = false }

                    },
                    new MeetingAgendaPointViewModel
                    {
                        Id = selectedId,
                        Number = 1,
                        AgendaPoint = new AgendaPointViewModel { Id = selectedId, IsAssignedToMeeting = true }

                    }
                }
            };

            var agendaPoint1 = new AgendaPoint { Id = 1 };
            var agendaPoint2 = new AgendaPoint { Id = selectedId };
            var meetingAgendaPoint2 = new MeetingAgendaPoint
            {
                Number = 1,
                Id = selectedId,
                AgendaPoint = agendaPoint2
            };

            var meeting = new Meeting
            {
                Id = 1,
                MeetingAgendaPoints = new List<MeetingAgendaPoint>
                {
                    new MeetingAgendaPoint
                    {
                        Number = 0,
                        Id = 1,
                        AgendaPoint = agendaPoint1

                    },
                    meetingAgendaPoint2
                }
            };

            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            _agendaPointRepo.GetByKey(agendaPoint1.Id)
                .Returns((AgendaPoint)null);
            _agendaPointRepo.GetByKey(agendaPoint2.Id)
                .Returns(agendaPoint2);

            _meetingAgendaPointRepo.GetByKey(selectedId).Returns(meetingAgendaPoint2);
            // Meeting point zero
            _mapper.Map<MeetingAgendaPointViewModel>(Arg.Any<MeetingAgendaPoint>())
                .Returns(new MeetingAgendaPointViewModel
                {
                    Number = 0,
                    AgendaPoint = new AgendaPointViewModel()
                });

            // Act
            _meetingController.Edit(model);

            // Assert
            Assert.Equal(1, meeting.MeetingAgendaPoints.Count);
            Assert.Contains(meetingAgendaPoint2, meeting.MeetingAgendaPoints);
        }

        [Fact]
        public void EditPost_ModelValidWith1NewUncheckedAgendaPoint_DeleteByKeyCalledForTheUncheckedAgendaPoint()
        {
            // Arrange
            int UncheckedId = 2;

            var model = new EditMeetingViewModel
            {
                Id = 1,
                MeetingAgendaPoints = new List<MeetingAgendaPointViewModel>
                {
                    new MeetingAgendaPointViewModel
                    {
                        Id = 1,
                        Number = 0,
                        AgendaPoint = new AgendaPointViewModel { Id = 1, IsAssignedToMeeting = false }
                    }
                }
            };

            var agendaPoint1 = new AgendaPoint { Id = 1 };
            var agendaPoint2 = new AgendaPoint { Id = UncheckedId };
            var meetingAgendaPoint2 = new MeetingAgendaPoint
            {
                Number = 1,
                Id = UncheckedId,
                AgendaPoint = agendaPoint2
            };

            var meeting = new Meeting
            {
                Id = 1,
                MeetingAgendaPoints = new List<MeetingAgendaPoint>
                {
                    new MeetingAgendaPoint
                    {
                        Number = 0,
                        Id = 1,
                        AgendaPoint = agendaPoint1
                    },
                    meetingAgendaPoint2
                }
            };

            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            _agendaPointRepo.GetByKey(agendaPoint1.Id)
                .Returns((AgendaPoint)null);
            _agendaPointRepo.GetByKey(agendaPoint2.Id)
                .Returns(agendaPoint2);

            _meetingAgendaPointRepo.GetByKey(UncheckedId).Returns(meetingAgendaPoint2);
            // Meeting point zero
            _mapper.Map<MeetingAgendaPointViewModel>(Arg.Any<MeetingAgendaPoint>())
                .Returns(new MeetingAgendaPointViewModel
                {
                    Number = 0,
                    AgendaPoint = new AgendaPointViewModel()
                });

            // Act
            _meetingController.Edit(model);

            // Assert
            _meetingAgendaPointRepo.Received(1).DeleteByKey(UncheckedId);
        }

        [Fact]
        public void EditPost_ModelValidWithChecked1NewAgendaPoint_CreateUpdateAndSaveCalledInRepo()
        {
            // Arrange
            int selectedId = 2;

            var model = new EditMeetingViewModel
            {
                Id = 1,
                MeetingAgendaPoints = new List<MeetingAgendaPointViewModel>
                {
                    new MeetingAgendaPointViewModel
                    {
                        Id = selectedId,
                        Number = 1,
                        AgendaPoint = new AgendaPointViewModel { Id = selectedId, IsAssignedToMeeting = true }
                    }
                }
            };

            var agendaPoint1 = new AgendaPoint { Id = 1 };
            var agendaPoint2 = new AgendaPoint { Id = selectedId };
            var meetingAgendaPoint2 = new MeetingAgendaPoint
            {
                Number = 1,
                Id = selectedId,
                AgendaPoint = agendaPoint2
            };

            var meeting = new Meeting
            {
                Id = 1,
                MeetingAgendaPoints = new List<MeetingAgendaPoint>
                {
                    new MeetingAgendaPoint
                    {
                        Number = 0,
                        Id = 1,
                        AgendaPoint = agendaPoint1

                    },
                    meetingAgendaPoint2
                }
            };

            _meetingRepo.GetByKey(model.Id)
                .Returns(meeting);

            _agendaPointRepo.GetByKey(agendaPoint1.Id)
                .Returns((AgendaPoint)null);
            _agendaPointRepo.GetByKey(agendaPoint2.Id)
                .Returns(agendaPoint2);

            _meetingAgendaPointRepo.GetByKey(selectedId).Returns(null as MeetingAgendaPoint);
            _meetingAgendaPointRepo.Create().Returns(new MeetingAgendaPoint());
            // Meeting point zero
            _mapper.Map<MeetingAgendaPointViewModel>(Arg.Any<MeetingAgendaPoint>())
                .Returns(new MeetingAgendaPointViewModel
                {
                    Number = 0,
                    AgendaPoint = new AgendaPointViewModel()
                });

            // Act
            _meetingController.Edit(model);

            // Assert
            _meetingAgendaPointRepo.Received(1).Create();
            _meetingRepo.Received(1).Update(Arg.Any<Meeting>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region CreateSummaries Get
        [Fact]
        public void CreateSummariesGet_ValidState_ReturnsMeetingVM()
        {
            // Arrange
            var id = 1;
            var meeting = new Meeting();
            _meetingRepo.GetByKey(id).Returns(meeting);
            _mapper.Map<CreateSummariesMeetingViewModel>(meeting)
                .Returns(new CreateSummariesMeetingViewModel());

            // Act
            var res = _meetingController.CreateSummaries(id) as ViewResult;

            // Assert
            Assert.IsType<CreateSummariesMeetingViewModel>(res.Model);
        }

        [Fact]
        public void CreateSummariesGet_IdNullAsParam_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const HttpStatusCode expectedHttpStatusCode = HttpStatusCode.BadRequest;

            // Act
            var res = _meetingController.CreateSummaries(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)expectedHttpStatusCode, res.StatusCode);
        }

        [Fact]
        public void CreateSummariesGet_IdNotFoundAsParam_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _meetingRepo.GetByKey(id)
                .Returns((Meeting)null);

            // Act
            var res = _meetingController.CreateSummaries(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(res);
        }
        #endregion

        #region CreateSummaries Post
        [Fact]
        public void CreateSummariesPost_ValidModelState_UpdateReceivedOnAgendaPointRepoMock()
        {
            // Arrange
            var vm = new CreateSummariesMeetingViewModel();
            vm.MeetingAgendaPoints = new List<MeetingAgendaPointSummariesViewModel>
            {
                new MeetingAgendaPointSummariesViewModel
                {
                    AgendaPoint = new AgendaPointViewModel()
                }
            };
            var meeting = new Meeting();
            meeting.MeetingAgendaPoints = new List<MeetingAgendaPoint>
            {
                new MeetingAgendaPoint
                {
                    AgendaPoint = new AgendaPoint()
                }
            };
            var count = meeting.MeetingAgendaPoints.Count;

            _mapper.Map<Meeting>(vm)
                .Returns(meeting);

            _meetingRepo.GetByKey().ReturnsForAnyArgs(meeting);
            _agendaPointRepo.GetByKey().ReturnsForAnyArgs(new AgendaPoint());

            // Act
            _meetingController.CreateSummaries(vm);

            // Assert
            _agendaPointRepo.Received(count).Update(Arg.Any<AgendaPoint>());
            _unitOfWork.Received().Save();
        }

        [Fact]
        public void CreateSummariesPost_InvalidModelState_ReturnsViewModel()
        {
            // Assert
            var vm = new CreateSummariesMeetingViewModel();
            _meetingController.ModelState.AddModelError("Error", "Error");

            // Act
            var res = _meetingController.CreateSummaries(vm) as ViewResult;

            // Assert
            Assert.Equal(vm, res.Model);
        }

        [Fact]
        public void CreateSummariesPost_NullArg_ReturnsBadRequest()
        {
            // Assert
            var vm = null as CreateSummariesMeetingViewModel;
            _mapper.Map<Meeting>(vm)
                .Returns(new Meeting());

            // Act
            var res = _meetingController.CreateSummaries(vm) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, res.StatusCode);
        }
        #endregion

        #region LockSummary
        [Fact]
        public void LockSummary_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const HttpStatusCode expectedHttpStatusCode = HttpStatusCode.BadRequest;

            // Act
            var ret = _meetingController.LockSummary(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)expectedHttpStatusCode, ret.StatusCode);
        }

        [Fact]
        public void LockSummary_IdNotFoundInRepo_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns((Meeting) null);

            // Act
            var ret = _meetingController.LockSummary(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void LockSummary_IdFound_RepoUpdateCalledWithLockedMeeting()
        {
            // Arrange
            var meeting = new Meeting();
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns(meeting);

            // Act
            _meetingController.LockSummary(id);

            // Assert
            _meetingRepo.Received(1).Update(meeting);
            _meetingRepo.Received(1).Update(Arg.Is<Meeting>(m => m.IsSummaryLocked));
        }

        [Fact]
        public void LockSummary_IdFound_UnitOfWorkSaveCalled()
        {
            // Arrange
            var meeting = new Meeting();
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns(meeting);

            // Act
            _meetingController.LockSummary(id);

            // Assert
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void LockSummary_IdFound_ReturnRedirectResult()
        {
            // Arrange
            var meeting = new Meeting();
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns(meeting);

            // Act
            var ret = _meetingController.LockSummary(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }
        #endregion

        #region UnlockSummary
        [Fact]
        public void UnlockSummary_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const HttpStatusCode expectedHttpStatusCode = HttpStatusCode.BadRequest;

            // Act
            var ret = _meetingController.UnlockSummary(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)expectedHttpStatusCode, ret.StatusCode);
        }

        [Fact]
        public void UnlockSummary_IdNotFoundInRepo_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns((Meeting)null);

            // Act
            var ret = _meetingController.UnlockSummary(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void UnlockSummary_IdFound_RepoUpdateCalledWithUnlockedMeeting()
        {
            // Arrange
            var meeting = new Meeting();
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns(meeting);

            // Act
            _meetingController.UnlockSummary(id);

            // Assert
            _meetingRepo.Received(1).Update(meeting);
            _meetingRepo.Received(1).Update(Arg.Is<Meeting>(m => !m.IsSummaryLocked));
        }

        [Fact]
        public void UnlockSummary_IdFound_UnitOfWorkSaveCalled()
        {
            // Arrange
            var meeting = new Meeting();
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns(meeting);

            // Act
            _meetingController.UnlockSummary(id);

            // Assert
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void UnlockSummary_IdFound_ReturnRedirectResult()
        {
            // Arrange
            var meeting = new Meeting();
            int? id = 1;
            _meetingRepo.GetByKey(id)
                .Returns(meeting);

            // Act
            var ret = _meetingController.UnlockSummary(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }
        #endregion
    }
}
