﻿#region Head
// <copyright file="ProjectLeaderControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ProjectLeader;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class ProjectLeaderControllerUnitTests
    {
        private readonly ProjectLeaderController _projectLeaderController;
        private readonly IGenericRepository<ProjectLeader> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        public ProjectLeaderControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<ProjectLeader>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<ProjectLeader>(Arg.Any<CreateProjectLeaderViewModel>())
                .Returns(new ProjectLeader());
            _mapper.Map<ProjectLeader>(Arg.Any<EditProjectLeaderViewModel>())
                .Returns(new ProjectLeader());

            _projectLeaderController = new ProjectLeaderController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _projectLeaderController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _projectLeaderController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _projectLeaderController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateProjectLeaderViewModel();

            // Act
            var ret = _projectLeaderController.Create(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateProjectLeaderViewModel();
            _projectLeaderController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _projectLeaderController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateProjectLeaderViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateProjectLeaderViewModel();

            // Act
            _projectLeaderController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<ProjectLeader>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var projectLeader = new ProjectLeader();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            var ret = _projectLeaderController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var projectLeader = new ProjectLeader();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectLeader);
            _mapper.Map<EditProjectLeaderViewModel>(projectLeader)
                .Returns(new EditProjectLeaderViewModel());

            // Act
            var ret = _projectLeaderController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditProjectLeaderViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const int expectedStatusCode = 400;

            // Act
            var ret = _projectLeaderController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal(expectedStatusCode, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _projectLeaderController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((ProjectLeader)null);

            // Act
            var ret = _projectLeaderController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidProjectLeaderDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var projectLeader = new ProjectLeader { IsDeleted = true };
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            var ret = _projectLeaderController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditProjectLeaderViewModel();

            // Act
            var ret = _projectLeaderController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditProjectLeaderViewModel();
            _projectLeaderController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _projectLeaderController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditProjectLeaderViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditProjectLeaderViewModel();

            // Act
            _projectLeaderController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<ProjectLeader>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var projectLeader = new ProjectLeader();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            var ret = _projectLeaderController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var projectLeader = new ProjectLeader();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectLeader);
            _mapper.Map<IndexProjectLeaderViewModel>(projectLeader)
                .Returns(new IndexProjectLeaderViewModel());

            // Act
            var ret = _projectLeaderController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<IndexProjectLeaderViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var projectLeader = new ProjectLeader();
            int? id = 1;
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            _projectLeaderController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoGetByKeyReturnsNull_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((ProjectLeader)null);

            // Act
            var ret = _projectLeaderController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidProjectLeaderDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var projectLeader = new ProjectLeader { IsDeleted = true };
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            var ret = _projectLeaderController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _projectLeaderController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_RepoUpdateCalled()
        {
            // Arrange
            const int id = 1;
            var projectLeader = new ProjectLeader();
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            _projectLeaderController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(projectLeader);
            _unitOfWork.Received(1).Save();
            Assert.True(projectLeader.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var projectLeader = new ProjectLeader();
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            var ret = _projectLeaderController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;
            _repo.GetByKey(id).Returns((ProjectLeader)null);

            // Act
            var ret = _projectLeaderController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _repo.GetByKey(id).Returns((ProjectLeader)null);

            // Act
            var ret = _projectLeaderController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdFoundProjectLeaderDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var projectLeader = new ProjectLeader { IsDeleted = true };
            _repo.GetByKey(id).Returns(projectLeader);

            // Act
            var ret = _projectLeaderController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
