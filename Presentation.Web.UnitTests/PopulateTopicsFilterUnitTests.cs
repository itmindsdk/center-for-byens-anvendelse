﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace UnitTests
{
    public class PopulateTopicsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Topic> _repo;
        private readonly PopulateTopicsFilter _populateTopicsFilter;

        public PopulateTopicsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _executingContext.Controller = _controller;

            _repo = Substitute.For<IGenericRepository<Topic>>();

            _populateTopicsFilter = new PopulateTopicsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuting_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var topic = new Topic {Id = 0, Name = "Name"};
            var topics = new List<Topic> { topic };

            _repo.Get().Returns(topics);

            var expectedListItem = new SelectListItem {Text = topic.Name, Value = topic.Name};
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateTopicsFilter.OnActionExecuting(_executingContext);

            // Assert
            var viewBagTopicList = _controller.ViewBag.TopicList as List<SelectListItem>;
            Assert.True(CheckIfListsAreEqual(expectedList, viewBagTopicList));
        }

        [Fact]
        public void OnActionExecuting_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<Topic>();
            _repo.Get().Returns(emptyList);

            // Act
            _populateTopicsFilter.OnActionExecuting(_executingContext);
            var viewBagTopicList = _controller.ViewBag.TopicList as List<SelectListItem>;

            // Assert
            Assert.Equal(0, viewBagTopicList.Count);
        }

        [Fact]
        public void OnActionExecuted_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateTopicsFilter.OnActionExecuted(_executedContext);

            // Assert
        }

        static bool CheckIfListsAreEqual(IEnumerable<SelectListItem> expectedListItems, IEnumerable<SelectListItem> actualListItems)
        {
            foreach (var actualListItem in actualListItems)
            {
                if (!expectedListItems.Any(expectedItem => expectedItem.Text == actualListItem.Text && expectedItem.Value == actualListItem.Value))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
