﻿#region Head
// <copyright file="SearchControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Presentation.Web.Controllers;
using Presentation.Web.Models.Search;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class SearchControllerUnitTests
    {
        private readonly SearchController _searchController;
        private readonly ISearchService _searchService;
        private readonly IMapper _mapper;

        public SearchControllerUnitTests()
        {
            _searchService = Substitute.For<ISearchService>();
            _mapper = Substitute.For<IMapper>();

            var projectReposity = Substitute.For<IGenericRepository<Project>>();
            var agendaPointReposity = Substitute.For<IGenericRepository<AgendaPoint>>();
            _searchController = new SearchController(_mapper, _searchService, projectReposity, agendaPointReposity);
        }

        #region Index
        [Fact]
        public void Index_ViewModelNull_ViewWithEmptyViewModel()
        {
            // Arrange
            SearchViewModel vm = null;

            // Act
            var ret = _searchController.Index(vm) as ViewResult;
            var model = ret.Model as SearchViewModel;

            // Assert
            Assert.False(model.AnyParametersSet());
        }

        [Fact]
        public void Index_ViewModelValid_ViewWithViewModel()
        {
            // Arrange
            var vm = new SearchViewModel();

            // Act
            var ret = _searchController.Index(vm) as ViewResult;
            var model = ret.Model as SearchViewModel;

            // Assert
            Assert.Equal(vm, model);
        }
        #endregion

        #region Index Post
        [Fact]
        public void IndexPost_ViewModelNull_ReturnBadRequest()
        {
            // Arrange
            SearchViewModel vm = null;

            // Act
            var ret = _searchController.IndexPost(vm).Result as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void IndexPost_EmptyViewModel_ViewBagNoSearchParametersSet()
        {
            // Arrange
            var vm = new SearchViewModel();

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;

            // Assert
            Assert.True(ret.ViewBag.NoSearchParameters);
        }

        [Fact]
        public void IndexPost_EmptyViewModel_ReturnViewModel()
        {
            // Arrange
            var vm = new SearchViewModel();

            // Act
            var ret = _searchController.IndexPost(vm).Result as ViewResult;

            // Assert
            Assert.Equal(vm, ret.Model);
        }
        #endregion
    }
}
