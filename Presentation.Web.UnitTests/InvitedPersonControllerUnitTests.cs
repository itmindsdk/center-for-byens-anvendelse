﻿#region Head
// <copyright file="InvitedPersonControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.InvitedPerson;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class InvitedPersonControllerUnitTests
    {
        private readonly InvitedPersonController _invitedPersonController;
        private readonly IGenericRepository<InvitedPerson> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        public InvitedPersonControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<InvitedPerson>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();
            new UserMock().LogOn(); // setting user mock
            _mapper.Map<InvitedPerson>(Arg.Any<InvitedPersonViewModel>())
                .Returns(new InvitedPerson());
            _mapper.Map<InvitedPerson>(Arg.Any<InvitedPersonViewModel>())
                .Returns(new InvitedPerson());

            _mapper.Map<InvitedPersonViewModel>(Arg.Any<InvitedPerson>())
                .Returns(new InvitedPersonViewModel());

            _invitedPersonController = new InvitedPersonController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _invitedPersonController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _invitedPersonController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _invitedPersonController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new InvitedPersonViewModel();

            // Act
            var ret = _invitedPersonController.Create(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelNotvalid_ReturnView()
        {
            // Arrange
            var vm = new InvitedPersonViewModel();
            _invitedPersonController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _invitedPersonController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<InvitedPersonViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new InvitedPersonViewModel();

            // Act
            _invitedPersonController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<InvitedPerson>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var invitedPerson = new InvitedPerson();
            int? id = 1;
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var invitedPerson = new InvitedPerson();
            int? id = 1;
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<InvitedPersonViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _invitedPersonController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _invitedPersonController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidInvitedPersonNotFound_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((InvitedPerson)null);

            // Act
            var ret = _invitedPersonController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidInvitedPersonDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var invitedPerson = new InvitedPerson { IsDeleted = true };
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new InvitedPersonViewModel();

            // Act
            var ret = _invitedPersonController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnView()
        {
            // Arrange
            var vm = new InvitedPersonViewModel();
            _invitedPersonController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _invitedPersonController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<InvitedPersonViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new InvitedPersonViewModel();

            // Act
            _invitedPersonController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<InvitedPerson>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var invitedPerson = new InvitedPerson();
            int? id = 1;
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var invitedPerson = new InvitedPerson();
            int? id = 1;
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<InvitedPersonViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var invitedPerson = new InvitedPerson();
            int? id = 1;
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            _invitedPersonController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidInvitedPersonNotFound_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((InvitedPerson)null);

            // Act
            var ret = _invitedPersonController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidInvitedPersonDeleted_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            var invitedPerson = new InvitedPerson { IsDeleted = true };
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _invitedPersonController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_UpdateDeletedInvitedPersonInRepo()
        {
            // Arrange
            const int id = 1;
            var invitedPerson = new InvitedPerson();
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            _invitedPersonController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(invitedPerson);
            _unitOfWork.Received(1).Save();
            Assert.True(invitedPerson.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var invitedPerson = new InvitedPerson();
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdValidInvitedPersonDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var invitedPerson = new InvitedPerson { IsDeleted = true };
            _repo.GetByKey(id).Returns(invitedPerson);

            // Act
            var ret = _invitedPersonController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }


        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;

            // Act
            var ret = _invitedPersonController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().DeleteByKey(Arg.Any<int>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion
    }
}
