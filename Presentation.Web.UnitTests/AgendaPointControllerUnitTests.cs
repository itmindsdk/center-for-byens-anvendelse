﻿#region Head
// <copyright file="AgendaPointControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using NSubstitute.Extensions;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.AgendaPoint;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class AgendaPointControllerUnitTests
    {
        private readonly AgendaPointController _agendaPointController;

        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<ProjectLeader> _projectLeaderRepo;
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<NextMeeting> _nextMeetingRepo;
        private readonly IGenericRepository<AgendaPointDocument> _agendaPointDocumentRepository;
        private readonly IGenericRepository<PartnerDiscussion> _partnerDiscussionRepo;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDateTime _dateTime;
        private readonly IDawaRepository _dawaRepo;
        private readonly IDocumentHelper _documentHelper;
        private readonly IPrivilegeHelper _privilegeHelper;
        private readonly IAdHelper _adHelper;
        private readonly UserMock _userMock;

        private readonly CreateAgendaPointViewModel _createAgendaPointViewModel;
        private readonly EditAgendaPointViewModel _editAgendaPointViewModel;
        private readonly DetailsAgendaPointViewModel _detailsAgendaPointViewModel;
        private readonly IndexAgendaPointViewModel _indexAgendaPointViewModel;
        private readonly CreateTopicViewModel _createTopicViewModel;
        private readonly AgendaPoint _agendaPoint;

        public AgendaPointControllerUnitTests()
        {
            _agendaPointRepo = Substitute.For<IGenericRepository<AgendaPoint>>();
            _agendaPointDocumentRepository = Substitute.For<IGenericRepository<AgendaPointDocument>>();
            _meetingRepo = Substitute.For<IGenericRepository<Meeting>>();
            _nextMeetingRepo = Substitute.For<IGenericRepository<NextMeeting>>();
            _partnerDiscussionRepo = Substitute.For<IGenericRepository<PartnerDiscussion>>();
            _topicRepo = Substitute.For<IGenericRepository<Topic>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _mapper = Substitute.For<IMapper>();
            _dateTime = Substitute.For<IDateTime>();
            _dawaRepo = Substitute.For<IDawaRepository>();
            _documentHelper = Substitute.For<IDocumentHelper>();
            _privilegeHelper = Substitute.For<IPrivilegeHelper>();
            _adHelper = Substitute.For<IAdHelper>();

            var documentRepository = Substitute.For<IGenericRepository<Document>>();

            // fill with dummy data
            var documentList = new List<AgendaPointDocument>
            {
                new AgendaPointDocument
                {
                    FileName = "lala"
                }
            };

            _createAgendaPointViewModel = new CreateAgendaPointViewModel();
            _editAgendaPointViewModel = new EditAgendaPointViewModel();
            _detailsAgendaPointViewModel = new DetailsAgendaPointViewModel();
            _indexAgendaPointViewModel = new IndexAgendaPointViewModel();
            _createTopicViewModel = new CreateTopicViewModel();
            _agendaPoint = new AgendaPoint();

            _mapper.Map<CreateAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_createAgendaPointViewModel);
            _mapper.Map(Arg.Any<EditAgendaPointViewModel>(), Arg.Any<AgendaPoint>())
                .Returns(_agendaPoint);
            _mapper.Map<EditAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_editAgendaPointViewModel);
            _mapper.Map<DetailsAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_detailsAgendaPointViewModel);
            _mapper.Map<IndexAgendaPointViewModel>(Arg.Any<AgendaPoint>())
                .Returns(_indexAgendaPointViewModel);
            _mapper.Map<IEnumerable<CreateTopicViewModel>>(Arg.Any<IEnumerable<Topic>>())
                .Returns(new List<CreateTopicViewModel> { _createTopicViewModel });
            _mapper.Map<AgendaPoint>(_createAgendaPointViewModel)
                .Returns(_agendaPoint);
            documentRepository.Get(Arg.Any<Expression<Func<Document, bool>>>())
                .Returns(documentList);

            var geoCoder = Substitute.For<IGeoCoder>();

            _userMock = new UserMock();
            _userMock.LogOn();
            _agendaPointController = new AgendaPointController(
                _agendaPointRepo,
                _meetingRepo,
                _nextMeetingRepo,
                _topicRepo,
                _partnerDiscussionRepo,
                _unitOfWork,
                _mapper,
                _dateTime,
                _agendaPointDocumentRepository,
                _documentHelper,
                geoCoder,
                _dawaRepo,
                _privilegeHelper,
                _adHelper
                );
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            var httpContext = Substitute.For<HttpContextBase>();
            var cookies = new HttpCookieCollection();
            httpContext.Response.Cookies.Returns(cookies);
            httpContext.Request.Cookies.Returns(cookies);
            var controllerContext = new ControllerContext(httpContext, new RouteData(), _agendaPointController);

            _agendaPointController.ControllerContext = controllerContext;

            // Act
            var ret = _agendaPointController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _agendaPointController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var model = _agendaPointController.Create() as ViewResult;

            // Assert
            Assert.IsType<CreateAgendaPointViewModel>(model.Model);
        }

        [Fact]
        public void Create_Call_StreetTypeIsPublicStreet()
        {
            // Arrange
            // Act
            var view = _agendaPointController.Create() as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            var publicStreetId = 0;
            Assert.Equal(publicStreetId, model.StreetTypeId);
        }

        [Fact]
        public void Create_Call_StatusIsAwaiting()
        {
            // Arrange
            // Act
            var view = _agendaPointController.Create() as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.Equal((int)AgendaPointStatusType.Draft, model.AgendaPointStatusId);
        }

        [Fact]
        public void Create_Call_MeetingsidListInitialised()
        {
            // Arrange
            // Act
            var view = _agendaPointController.Create() as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(0, model.MeetingIds.Count());
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            var ret = _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelValid_PropertyCreatedOnSet()
        {
            // Arrange
            var dateTime = new DateTime(2015, 1, 1);
            _dateTime.Now.Returns(dateTime);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.CreatedOn.Equals(dateTime)));
        }

        [Fact]
        public void CreatePost_ModelValid1MeetingId_PreviousMeetingHoldsId()
        {
            // Arrange
            const string id = "1234";
            int idInt = int.Parse(id);
            var meeting = new Meeting { Id = idInt };
            _createAgendaPointViewModel.MeetingIds = new List<string> { id };
            _meetingRepo.GetByKey(idInt).Returns(meeting);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.Count.Equals(1)));
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.First().Id.Equals(idInt)));
        }

        [Fact]
        public void CreatePost_ModelValid2MeetingId_PreviousMeetingHoldsLastId()
        {
            // Arrange
            const string id1 = "1234";
            const string id2 = "4321";
            _createAgendaPointViewModel.MeetingIds = new List<string> { id1, id2 };
            var id1Int = int.Parse(id1);
            var id2Int = int.Parse(id2);
            var meeting2 = new Meeting { Id = id2Int };
            _meetingRepo.GetByKey(id1Int).Returns((Meeting)null);
            _meetingRepo.GetByKey(id2Int).Returns(meeting2);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.Count.Equals(1)));
            _agendaPointRepo.Received(1).Insert(Arg.Is<AgendaPoint>(a => a.PreviousMeetings.Last().Id.Equals(id2Int)));
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Insert(_agendaPoint);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void CreatePost_ModelValidTopicAttached_AttachedTopicOnModel()
        {
            // Arrange
            var topic = new Topic { Id = 1 };
            _createAgendaPointViewModel.TopicId = topic.Id;
            _topicRepo.GetByKey(topic.Id)
                .Returns(topic);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Create(_createAgendaPointViewModel);

            // Assert
            Assert.Equal(topic, _agendaPoint.Topic);
        }

        [Fact]
        public void CreatePost_ModelNotvalid_ReturnViewModel()
        {
            // Arrange
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _agendaPointController.Create(_createAgendaPointViewModel) as ViewResult;

            // Assert
            Assert.IsType<CreateAgendaPointViewModel>(ret.Model);
            Assert.Equal(_createAgendaPointViewModel, ret.Model);
        }

        [Fact]
        public void CreatePost_ModelNotValid_MeetingsidListInitialised()
        {
            // Arrange
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var view = _agendaPointController.Create(_createAgendaPointViewModel) as ViewResult;
            var model = view.Model as CreateAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(0, model.MeetingIds.Count());
        }

        #endregion

        #region Details
        [Fact]
        public void Details_IdNull_ReturnsBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _agendaPointController.Details(id);
            var httpStatusCode = ret as HttpStatusCodeResult;

            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, httpStatusCode.StatusCode);
        }

        [Fact]
        public void Details_IdNonExistingInRepo_ReturnsNotFound()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns((AgendaPoint)null);

            // Act
            var ret = _agendaPointController.Details(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Details_IdValidRepoReturnsAgendaPoint_ReturnsViewModel()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var model = _agendaPointController.Details(id) as ViewResult;

            // Assert
            Assert.IsType<DetailsAgendaPointViewModel>(model.Model);
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdNull_ReturnsBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _agendaPointController.Edit(id);
            var httpStatusCode = ret as HttpStatusCodeResult;

            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, httpStatusCode.StatusCode);
        }

        [Fact]
        public void Edit_IdNonExistingInRepo_ReturnsNotFound()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns((AgendaPoint)null);

            // Act
            var ret = _agendaPointController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidRepoReturnsAgendaPoint_ReturnsViewModel()
        {
            // Arrange
            int? id = 1;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var model = _agendaPointController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditAgendaPointViewModel>(model.Model);
        }

        [Fact]
        public void Edit_IdValidRepoReturnsAgendaPointWithPriviousMeetings_MeetingsidListInitialised()
        {
            // Arrange
            int? id = 1;
            var meetings = new List<Meeting>()
            {
                new Meeting {Id = 1},
                new Meeting {Id = 2}
            };
            _agendaPoint.PreviousMeetings = meetings;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);

            // Act
            var view = _agendaPointController.Edit(id) as ViewResult;
            var model = view.Model as EditAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(meetings.Count, model.MeetingIds.Count());
        }

        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValidRepoReturnsNoAgendaPoint_ReturnHttpBadRequest()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns((AgendaPoint)null);

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel);
            var httpStatusCode = ret as HttpStatusCodeResult;

            // Assert
            Assert.IsType<HttpStatusCodeResult>(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, httpStatusCode.StatusCode);
        }

        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Update(_agendaPoint);
            _unitOfWork.Received(1).Save();
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnViewModel()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _agendaPointController.Edit(_editAgendaPointViewModel) as ViewResult;

            // Assert
            Assert.IsType<EditAgendaPointViewModel>(ret.Model);
            Assert.Equal(_editAgendaPointViewModel, ret.Model);
        }

        [Fact]
        public void EditPost_ModelNotValidMeetingIdsListNull_MeetingsidListInitialised()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _agendaPointController.ModelState.AddModelError("error", "error");

            // Act
            var view = _agendaPointController.Edit(_editAgendaPointViewModel) as ViewResult;
            var model = view.Model as EditAgendaPointViewModel;

            // Assert
            Assert.IsAssignableFrom<IEnumerable<string>>(model.MeetingIds);
            Assert.Equal(0, model.MeetingIds.Count());
        }

        [Fact]
        public void EditPost_ModelValid_PropertyModifiedOn()
        {
            // Arrange
            const int id = 1;
            _editAgendaPointViewModel.Id = id;
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            var dateTime = new DateTime(2015, 1, 1);
            _dateTime.Now.Returns(dateTime);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            _agendaPointRepo.Received(1).Update(Arg.Is<AgendaPoint>(a => a.ModifiedOn.Equals(dateTime)));
        }

        [Fact]
        public void EditPost_ModelValidRemovedMeetingId_MeetingRemovedFromAgendaPoint()
        {
            // Arrange
            const int id = 1;
            const int meetingId1 = 1;
            const int meetingId2 = 2;
            _editAgendaPointViewModel.Id = id;
            _editAgendaPointViewModel.MeetingIds = new List<string> { meetingId1.ToString(), meetingId2.ToString() };
            _agendaPoint.PreviousMeetings = new List<Meeting>
            {
                new Meeting {Id = meetingId1},
                new Meeting {Id = meetingId2}
            };
            _editAgendaPointViewModel.RemovedMeetingIds = meetingId2 + " " + meetingId2 + " ";
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.Equal(1, _agendaPoint.PreviousMeetings.Count);
        }

        [Fact]
        public void EditPost_ModelValidRemovedMeetingIdNotValid_MeetingNotRemovedFromAgendaPoint()
        {
            // Arrange
            const int id = 1;
            const int meetingId1 = 1;
            const int meetingId2 = 2;
            const string nonValidId = "abc";
            _editAgendaPointViewModel.Id = id;
            _editAgendaPointViewModel.MeetingIds = new List<string> { meetingId1.ToString(), meetingId2.ToString() };
            _agendaPoint.PreviousMeetings = new List<Meeting>
            {
                new Meeting {Id = meetingId1},
                new Meeting {Id = meetingId2}
            };
            _editAgendaPointViewModel.RemovedMeetingIds = nonValidId + " ";
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.Equal(2, _agendaPoint.PreviousMeetings.Count);
        }

        [Fact]
        public void EditPost_ModelValidAddedMeetingId_MeetingAddedToAgendaPoint()
        {
            // Arrange
            const int id = 1;
            const int meetingId1 = 1;
            const int meetingId2 = 2;
            const int newMeetingId = 3;
            _editAgendaPointViewModel.Id = id;
            _editAgendaPointViewModel.MeetingIds = new List<string> { meetingId1.ToString(), meetingId2.ToString(), newMeetingId.ToString() };
            _meetingRepo.GetByKey(newMeetingId).Returns(new Meeting { Id = newMeetingId });
            _agendaPoint.PreviousMeetings = new List<Meeting>
            {
                new Meeting {Id = meetingId1},
                new Meeting {Id = meetingId2}
            };
            _agendaPointRepo.GetByKey(id).Returns(_agendaPoint);
            _dawaRepo.GetAddress(new Guid()).ReturnsForAnyArgs(new Address());

            // Act
            _agendaPointController.Edit(_editAgendaPointViewModel);

            // Assert
            Assert.Equal(3, _agendaPoint.PreviousMeetings.Count);
        }
        #endregion

        #region Copy Post
        [Fact]
        public void CopyPost_WrongId_Return404()
        {
            // Arrange
            _agendaPointRepo.GetByKey(1).Returns((AgendaPoint) null);

            // Act
            var ret = _agendaPointController.CopyAgendaPoint(1);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
            Assert.Equal((int)HttpStatusCode.NotFound, ((HttpNotFoundResult)ret).StatusCode);
        }

        [Fact]
        public void CopyPost_ValidID_SuccessfullyCallsRepo()
        {
            // Arrange
            const string cbaDesc = "cbatestdesc";
            const bool cbaDisc = false;
            const string cbaDiscW = "disccbatest";
            const string cbmDesc = "cbmtestdesc";
            const bool cbmDisc = true;
            const string cbmDiscW = "disccbmtest";
            AgendaPointDocument doc = new AgendaPointDocument {Guid = new Guid("00000000-0000-0000-0000-000000000000") };

            var repoAp = new AgendaPoint()
            {
                MeetingAgendaPoint = new MeetingAgendaPoint(),
                ProjectLeaderId = 2,
                StreetTypeId = 3,
                TopicId = 4,
                AgendaPointStatusId = 5,
                CreatedOn = new DateTime(2000,1,1),
                TemporaryAddressName = "tempAddressName",
                AddressStreetName = "addressStreetName",
                Documents = new List<AgendaPointDocument> { doc },
                AddressZipCode = "addressZipCode",
                AddressNumber = "addressNumber",
                TemporaryAddress = true,
                AddressCity = "addressCity",
                PreviousMeetings = new List<Meeting> {},
                AddressLocation = null,
                ModifiedBy = "modifiedBy",
                CreatedBy = "createdBy",
                Agreement = "agreement",
                Description = "description",
                CbaDiscussion = new PartnerDiscussion { Description = cbaDesc, Discussed = cbaDisc, DiscussedWith = cbaDiscW },
                Conclusion = "conclusion",
                CbmDiscussion = new PartnerDiscussion { Description = cbmDesc, Discussed = cbmDisc, DiscussedWith = cbmDiscW },
                Area = AreaType.Midt
            };
            _agendaPointRepo.GetByKey(1).Returns(repoAp);
            _agendaPointRepo.Create().Returns(new AgendaPoint());

            // Couldn't figure out how to return different values, so I came up with this
            var partnerDiscussionRepoEnumerator = new[]
            {
                new PartnerDiscussion { Description = cbmDesc, Discussed = cbmDisc, DiscussedWith = cbmDiscW },
                new PartnerDiscussion { Description = cbaDesc, Discussed = cbaDisc, DiscussedWith = cbaDiscW }
            }.GetEnumerator();

            _partnerDiscussionRepo.Insert(Arg.Any<PartnerDiscussion>()).Returns(x =>
            {
                partnerDiscussionRepoEnumerator.MoveNext();
                return partnerDiscussionRepoEnumerator.Current;
            });

            AgendaPoint insertAp = null;
            _agendaPointRepo.Insert(Arg.Do<AgendaPoint>(x => insertAp = x));

            _dateTime.Now.Returns(repoAp.CreatedOn.AddDays(1));

            var retDocs = new List<AgendaPointDocument> {doc};
            retDocs.Add(new AgendaPointDocument());
            _agendaPointDocumentRepository.ReturnsForAll<IEnumerable<AgendaPointDocument>> (retDocs);

            // Act
            var ret = _agendaPointController.CopyAgendaPoint(1);

            // Assert
            Assert.NotEqual(repoAp.MeetingAgendaPoint, insertAp.MeetingAgendaPoint);    // Associated active meeting can't be equal
            Assert.Equal(repoAp.ProjectLeaderId, insertAp.ProjectLeaderId);
            Assert.Equal(repoAp.StreetTypeId, insertAp.StreetTypeId);
            Assert.Equal(repoAp.TopicId, insertAp.TopicId);
            Assert.Equal(repoAp.AgendaPointStatusId, insertAp.AgendaPointStatusId);
            Assert.NotEqual(repoAp.CreatedOn, insertAp.CreatedOn);                      // The copied agendapoint should have a new creation datetime
            Assert.Equal(repoAp.TemporaryAddressName, insertAp.TemporaryAddressName);
            Assert.Equal(repoAp.TemporaryAddress, insertAp.TemporaryAddress);

            Assert.Equal(repoAp.AddressLocation,   insertAp.AddressLocation);
            Assert.Equal(repoAp.AddressZipCode,    insertAp.AddressZipCode);
            Assert.Equal(repoAp.AddressNumber,     insertAp.AddressNumber);
            Assert.Equal(repoAp.AddressCity,       insertAp.AddressCity);
            Assert.Equal(repoAp.AddressStreetName, insertAp.AddressStreetName);

            Assert.Equal(repoAp.PreviousMeetings, insertAp.PreviousMeetings);
            Assert.Equal(repoAp.Agreement, insertAp.Agreement);
            Assert.Equal(repoAp.Description, insertAp.Description);

            Assert.False(ReferenceEquals(repoAp.CbaDiscussion, insertAp.CbaDiscussion));              // Should be a copy and not a reference to the same discussion
            Assert.Equal(repoAp.CbaDiscussion.Description, insertAp.CbaDiscussion.Description);
            Assert.Equal(repoAp.CbaDiscussion.DiscussedWith, insertAp.CbaDiscussion.DiscussedWith);
            Assert.Equal(repoAp.CbaDiscussion.Discussed, insertAp.CbaDiscussion.Discussed);


            Assert.False(ReferenceEquals(repoAp.CbmDiscussion, insertAp.CbmDiscussion));              // Should be a copy and not a reference to the same discussion
            Assert.Equal(repoAp.CbmDiscussion.Description, insertAp.CbmDiscussion.Description);
            Assert.Equal(repoAp.CbmDiscussion.DiscussedWith, insertAp.CbmDiscussion.DiscussedWith);
            Assert.Equal(repoAp.CbmDiscussion.Discussed, insertAp.CbmDiscussion.Discussed);

            Assert.Equal(repoAp.Area, insertAp.Area);
        }

        #endregion

        #region UploadFile

        [Fact]
        public void UploadFile_ValidState_UploadFileCalledInDocumentHelper()
        {
            // Arrange

            // Act
            _agendaPointController.SaveUploadedFile();

            // Assert
            _documentHelper.Received(1).UploadFile(Arg.Any<HttpRequestBase>(), Arg.Any<HttpServerUtilityBase>());
        }

        #endregion

        #region DownloadFile

        [Fact]
        public void DownloadFile_ValidState_DownloadCalledInDocumentHelper()
        {
            // Arrange
            var guid = new Guid();

            // Act
            _agendaPointController.Download(guid);

            // Assert
            _documentHelper.Received(1).Download(_agendaPointController.Server, guid);
        }

        #endregion

        #region RemoveFile

        [Fact]
        public void DeleteFile_ValidState_DeleteCalledInDocumentHelper()
        {
            // Arrange
            var guid = new Guid();
            var guidList = new List<Guid>
            {
                guid
            };

            // Act
            _agendaPointController.DeleteFilesFromServer(guidList);

            // Assert
            _documentHelper.Received(1).DeleteFiles(guidList, _agendaPointController.Server);
        }

        #endregion
    }
}
