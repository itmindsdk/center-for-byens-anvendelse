﻿#region Head
// <copyright file="CouncilControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Council;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class CouncilControllerUnitTests
    {
        private readonly CouncilController _councilController;
        private readonly IGenericRepository<Council> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        private readonly CreateCouncilViewModel _createCouncilViewModel;

        public CouncilControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Council>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();

            _createCouncilViewModel = new CreateCouncilViewModel();

            _mapper.Map<CreateCouncilViewModel>(Arg.Any<Council>())
                .Returns(_createCouncilViewModel);

            new UserMock().LogOn(); // setting user mock
            _mapper.Map<Council>(Arg.Any<CreateCouncilViewModel>())
                .Returns(new Council());
            _mapper.Map<Council>(Arg.Any<EditCouncilViewModel>())
                .Returns(new Council());

            _councilController = new CouncilController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _councilController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _councilController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _councilController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateCouncilViewModel();

            // Act
            var ret = _councilController.Create(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateCouncilViewModel();
            _councilController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _councilController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateCouncilViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateCouncilViewModel();

            // Act
            _councilController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<Council>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var council = new Council();
            int? id = 1;
            _repo.GetByKey(id).Returns(council);

            // Act
            var ret = _councilController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var council = new Council();
            int? id = 1;
            _repo.GetByKey(id).Returns(council);
            _mapper.Map<EditCouncilViewModel>(council)
                .Returns(new EditCouncilViewModel());

            // Act
            var ret = _councilController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditCouncilViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const int expectedStatusCode = 400;

            // Act
            var ret = _councilController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal(expectedStatusCode, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _councilController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Council)null);

            // Act
            var ret = _councilController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidCouncilDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var council = new Council { IsDeleted = true };
            _repo.GetByKey(id).Returns(council);

            // Act
            var ret = _councilController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditCouncilViewModel();

            // Act
            var ret = _councilController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditCouncilViewModel();
            _councilController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _councilController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditCouncilViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditCouncilViewModel();

            // Act
            _councilController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<Council>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var council = new Council();
            int? id = 1;
            _repo.GetByKey(id).Returns(council);

            // Act
            var ret = _councilController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var council = new Council();
            int? id = 1;
            _repo.GetByKey(id).Returns(council);
            _mapper.Map<IndexCouncilViewModel>(council)
                .Returns(new IndexCouncilViewModel());

            // Act
            var ret = _councilController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<IndexCouncilViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var council = new Council();
            int? id = 1;
            _repo.GetByKey(id).Returns(council);

            // Act
            _councilController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoGetByKeyReturnsNull_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Council)null);

            // Act
            var ret = _councilController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidCouncilDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var council = new Council { IsDeleted = true };
            _repo.GetByKey(id).Returns(council);

            // Act
            var ret = _councilController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _councilController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_RepoUpdateCalled()
        {
            // Arrange
            const int id = 1;
            var council = new Council();
            _repo.GetByKey(id).Returns(council);

            // Act
            _councilController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(council);
            _unitOfWork.Received(1).Save();
            Assert.True(council.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var council = new Council();
            _repo.GetByKey(id).Returns(council);

            // Act
            var ret = _councilController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;
            _repo.GetByKey(id).Returns((Council)null);

            // Act
            var ret = _councilController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _repo.GetByKey(id).Returns((Council)null);

            // Act
            var ret = _councilController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdFoundCouncilDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var council = new Council { IsDeleted = true };
            _repo.GetByKey(id).Returns(council);

            // Act
            var ret = _councilController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
