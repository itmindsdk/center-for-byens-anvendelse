﻿#region Head
// <copyright file="PopulateDefaultPhrasesFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateDefaultPhrasesFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly PopulateDefaultPhrasesFilter _populateDefaultPhrasesFilter;

        public PopulateDefaultPhrasesFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<DefaultPhrase>>();

            _executedContext.Controller = _controller;

            _populateDefaultPhrasesFilter = new PopulateDefaultPhrasesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase { Id = 1, Phrase = "Phrase" };
            var defaultPhrases = new List<DefaultPhrase> { defaultPhrase };

            _repo.Get(Arg.Any<Expression<Func<DefaultPhrase, bool>>>())
                .Returns(defaultPhrases);

            var expectedListItem = new SelectListItem { Text = defaultPhrase.Phrase, Value = defaultPhrase.Id.ToString(CultureInfo.CurrentCulture) };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateDefaultPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultPhraseList = _controller.ViewBag.DefaultPhraseList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagDefaultPhraseList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items_ListContaining2Elements()
        {
            // Arrange
            var defaultPhrase1 = new DefaultPhrase { Id = 1, Phrase = "Phrase" };
            var defaultPhrase2 = new DefaultPhrase { Id = 2, Phrase = "Phrase" };
            var defaultPhrases = new List<DefaultPhrase> { defaultPhrase1, defaultPhrase2 };

            _repo.Get(Arg.Any<Expression<Func<DefaultPhrase, bool>>>())
                .Returns(defaultPhrases);

            var expectedListItem1 = new SelectListItem { Text = defaultPhrase1.Phrase, Value = defaultPhrase1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = defaultPhrase2.Phrase, Value = defaultPhrase2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            // Act
            _populateDefaultPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultPhraseList = _controller.ViewBag.DefaultPhraseList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagDefaultPhraseList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ViewBagListDefined()
        {
            // Arrange
            // Act
            _populateDefaultPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.NotNull(_controller.ViewBag.DefaultPhraseList as List<SelectListItem>);
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<DefaultPhrase>();
            _repo.Get(Arg.Any<Expression<Func<DefaultPhrase, bool>>>())
                .Returns(emptyList);

            // Act
            _populateDefaultPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultPhraseList = _controller.ViewBag.DefaultPhraseList as List<SelectListItem>;
            Assert.Equal(0, viewBagDefaultPhraseList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateDefaultPhrasesFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
