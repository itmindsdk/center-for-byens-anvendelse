﻿#region Head
// <copyright file="PopulateAdvisorsFilterUnitTest.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateAdvisorsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Advisor> _repo;
        private readonly ISelectedAdvisor _selectedAdvisor;
        private readonly PopulateAdvisorsFilter _populateAdvisorsFilter;

        public PopulateAdvisorsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<Advisor>>();
            _selectedAdvisor = Substitute.For<ISelectedAdvisor>();

            _controller.ViewData.Model = _selectedAdvisor;
            _executedContext.Controller = _controller;

            _populateAdvisorsFilter = new PopulateAdvisorsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var company = new Company
            {
                Name = "CompanyName"
            };
            var advisor = new Advisor {Id = 1, ResponsiblePersonName = "Name", Company = company};
            var advisors = new List<Advisor> { advisor };

            _repo.Get(Arg.Any<Expression<Func<Advisor, bool>>>()).Returns(advisors);

            var expectedListItem = new SelectListItem {Text = advisor.ResponsiblePersonName, Value = advisor.Id.ToString()};
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateAdvisorsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagAdvisorList = _controller.ViewBag.AdvisorList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagAdvisorList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsAdvisorDeletedSet()
        {
            // Arrange
            var company = new Company
            {
                Name = "CompanyName"
            };
            var advisor1 = new Advisor { Id = 1, Company = company };
            var advisor2 = new Advisor { Id = 1, Company = company, IsDeleted = true };
            var advisors = new List<Advisor> { advisor1, advisor2 };

            _repo.Get(Arg.Any<Expression<Func<Advisor, bool>>>()).Returns(advisors);

            // Act
            _populateAdvisorsFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsAdvisorDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var company = new Company
            {
                Name = "CompanyName"
            };
            var advisor1 = new Advisor { Id = 1, ResponsiblePersonName = "Name", Company = company };
            var advisor2 = new Advisor { Id = 1, ResponsiblePersonName = "Name", Company = company };
            var advisors = new List<Advisor> { advisor1, advisor2 };

            _repo.Get(Arg.Any<Expression<Func<Advisor, bool>>>()).Returns(advisors);

            var expectedListItem1 = new SelectListItem { Text = advisor1.ResponsiblePersonName, Value = advisor1.Id.ToString(CultureInfo.CurrentCulture) };
            var expectedListItem2 = new SelectListItem { Text = advisor2.ResponsiblePersonName, Value = advisor2.Id.ToString(CultureInfo.CurrentCulture) };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedAdvisor.AdvisorId = advisor1.Id;

            // Act
            _populateAdvisorsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagAdvisorList = _controller.ViewBag.AdvisorList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagAdvisorList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<Advisor>();
            _repo.Get(Arg.Any<Expression<Func<Advisor, bool>>>()).Returns(emptyList);

            // Act
            _populateAdvisorsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagAdvisorList = _controller.ViewBag.AdvisorList as List<SelectListItem>;
            Assert.Equal(0, viewBagAdvisorList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateAdvisorsFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
