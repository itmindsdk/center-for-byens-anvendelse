﻿#region Head
// <copyright file="PopulateProjectLeadersFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateProjectLeadersFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<ProjectLeader> _repo;
        private readonly ISelectedProjectLeader _selectedProjectLeader;
        private readonly PopulateProjectLeadersFilter _populateProjectLeadersFilter;

        public PopulateProjectLeadersFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<ProjectLeader>>();
            _selectedProjectLeader = Substitute.For<ISelectedProjectLeader>();

            _controller.ViewData.Model = _selectedProjectLeader;
            _executedContext.Controller = _controller;

            _populateProjectLeadersFilter = new PopulateProjectLeadersFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var projectLeader = new ProjectLeader { Id = 1, Name = "Name" };
            var projectLeaders = new List<ProjectLeader> { projectLeader };

            _repo.Get(Arg.Any<Expression<Func<ProjectLeader, bool>>>()).Returns(projectLeaders);

            var expectedListItem = new SelectListItem { Text = projectLeader.Name, Value = projectLeader.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateProjectLeadersFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagProjectLeaderList = _controller.ViewBag.ProjectLeaderList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagProjectLeaderList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsProjectLeaderDeletedSet()
        {
            // Arrange
            var projectLeader1 = new ProjectLeader { Id = 1, Name = "Name" };
            var projectLeader2 = new ProjectLeader { Id = 1, Name = "Name", IsDeleted = true };
            var projectLeaders = new List<ProjectLeader> { projectLeader1, projectLeader2 };

            _repo.Get(Arg.Any<Expression<Func<ProjectLeader, bool>>>()).Returns(projectLeaders);

            // Act
            _populateProjectLeadersFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsProjectLeaderDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var projectLeader1 = new ProjectLeader { Id = 1, Name = "Name" };
            var projectLeader2 = new ProjectLeader { Id = 1, Name = "Name" };
            var projectLeaders = new List<ProjectLeader> { projectLeader1, projectLeader2 };

            _repo.Get(Arg.Any<Expression<Func<ProjectLeader, bool>>>()).Returns(projectLeaders);

            var expectedListItem1 = new SelectListItem { Text = projectLeader1.Name, Value = projectLeader1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = projectLeader2.Name, Value = projectLeader2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedProjectLeader.ProjectLeaderId = projectLeader1.Id;

            // Act
            _populateProjectLeadersFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagProjectLeaderList = _controller.ViewBag.ProjectLeaderList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagProjectLeaderList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<ProjectLeader>();
            _repo.Get(Arg.Any<Expression<Func<ProjectLeader, bool>>>()).Returns(emptyList);

            // Act
            _populateProjectLeadersFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagProjectLeaderList = _controller.ViewBag.ProjectLeaderList as List<SelectListItem>;
            Assert.Equal(0, viewBagProjectLeaderList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateProjectLeadersFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
