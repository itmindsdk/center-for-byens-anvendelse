﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Presentation.Web.Helpers;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class AddStatusExplanationUrlUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly AddStatusExplanationUrlFilter _addStatusExplanationUrlFilter;
        private readonly IAppSettings _appSettings;

        public AddStatusExplanationUrlUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _appSettings = Substitute.For<IAppSettings>();

            _executedContext.Controller = _controller;

            _addStatusExplanationUrlFilter = new AddStatusExplanationUrlFilter(_appSettings);
        }

        [Fact]
        public void OnActionExecuted_StringGiven_ViewBagContainsGivenString()
        {
            // Arrange
            const string testString = "this is a test string";

            _appSettings.StatusExplanationFileName.Returns(testString);

            // Act
            _addStatusExplanationUrlFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagStatusExplanationUrl = _controller.ViewBag.StatusExplanationURL as string;
            Assert.Equal(testString, viewBagStatusExplanationUrl);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _addStatusExplanationUrlFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
