﻿#region Head
// <copyright file="PopulateGroupsFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateGroupsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Group> _repo;
        private readonly ISelectedGroup _selectedGroup;
        private readonly PopulateGroupsFilter _populateGroupsFilter;

        public PopulateGroupsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<Group>>();
            _selectedGroup = Substitute.For<ISelectedGroup>();

            _controller.ViewData.Model = _selectedGroup;
            _executedContext.Controller = _controller;

            _populateGroupsFilter = new PopulateGroupsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var group = new Group { Id = 1, Name = "Name" };
            var groups = new List<Group> { group };

            _repo.Get(Arg.Any<Expression<Func<Group, bool>>>()).Returns(groups);

            var expectedListItem = new SelectListItem { Text = group.Name, Value = group.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateGroupsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagGroupList = _controller.ViewBag.GroupList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagGroupList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsGroupDeletedSet()
        {
            // Arrange
            var group1 = new Group { Id = 1, Name = "Name" };
            var group2 = new Group { Id = 1, Name = "Name", IsDeleted = true };
            var groups = new List<Group> { group1, group2 };

            _repo.Get(Arg.Any<Expression<Func<Group, bool>>>()).Returns(groups);

            // Act
            _populateGroupsFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsGroupDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var group1 = new Group { Id = 1, Name = "Name" };
            var group2 = new Group { Id = 1, Name = "Name" };
            var groups = new List<Group> { group1, group2 };

            _repo.Get(Arg.Any<Expression<Func<Group, bool>>>()).Returns(groups);

            var expectedListItem1 = new SelectListItem { Text = group1.Name, Value = group1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = group2.Name, Value = group2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedGroup.GroupId = group1.Id;

            // Act
            _populateGroupsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagGroupList = _controller.ViewBag.GroupList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagGroupList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<Group>();
            _repo.Get(Arg.Any<Expression<Func<Group, bool>>>()).Returns(emptyList);

            // Act
            _populateGroupsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagGroupList = _controller.ViewBag.GroupList as List<SelectListItem>;
            Assert.Equal(0, viewBagGroupList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateGroupsFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
