﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateInconveniencesUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Inconvenience> _repo;
        private readonly ISelectedInconvenience _selectedInconvenience;
        private readonly PopulateInconveniencesFilter _populateInconveniencesFilter;

        public PopulateInconveniencesUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<Inconvenience>>();
            _selectedInconvenience = Substitute.For<ISelectedInconvenience>();

            _controller.ViewData.Model = _selectedInconvenience;
            _executedContext.Controller = _controller;

            _populateInconveniencesFilter = new PopulateInconveniencesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var inconvenience = new Inconvenience
            {
                Description = "Inconvenience description",
                Id = 1,
                IsActive = true
            };
            var inconveniences = new List<Inconvenience> { inconvenience };

            _repo.Get(Arg.Any<Expression<Func<Inconvenience, bool>>>()).Returns(inconveniences);

            // Act
            _populateInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInconvenienceList = _controller.ViewBag.InconvenienceList as List<SelectListItem>;
            Assert.Equal(viewBagInconvenienceList.Count, 1);
        }

        [Fact]
        public void OnActionExecuted_ListWith2ItemsSelectedIsDeleted_IsInconvenienceDeletedSet()
        {
            // Arrange
            var inconvenience1 = new Inconvenience
            {
                Description = "Inconvenience description",
                Id = 1,
                IsActive = true
            };
            var inconvenience2 = new Inconvenience
            {
                Description = "Description iconvenience",
                Id = 2,
                IsActive = true,
                IsDeleted = true
            };
            var inconveniences = new List<Inconvenience> { inconvenience1, inconvenience2 };

            _repo.Get(Arg.Any<Expression<Func<Inconvenience, bool>>>()).Returns(inconveniences);

            // Act
            _populateInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsInconvenienceDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var inconvenience1 = new Inconvenience { Id = 1, Description = "Name", IsActive = true };
            var inconvenience2 = new Inconvenience { Id = 1, Description = "Name", IsActive = true };
            var inconveniences = new List<Inconvenience> { inconvenience1, inconvenience2 };

            _repo.Get(Arg.Any<Expression<Func<Inconvenience, bool>>>()).Returns(inconveniences);

            _selectedInconvenience.InconvenienceId = inconvenience1.Id;

            // Act
            _populateInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInconvenienceList = _controller.ViewBag.InconvenienceList as List<SelectListItem>;
            Assert.Equal(viewBagInconvenienceList.Count, 2);
        }

        [Fact]
        public void OnActionExecuted_ListWith8Items_ListContaining8ElementsSortedByDescription()
        {
            // Arrange
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("da-dk");

            var inconvenience1 = new Inconvenience { Id = 1, Description = "a", IsActive = true };
            var inconvenience2 = new Inconvenience { Id = 2, Description = "ba", IsActive = true };
            var inconvenience3 = new Inconvenience { Id = 3, Description = "bb", IsActive = true };
            var inconvenience4 = new Inconvenience { Id = 4, Description = "bc", IsActive = true };
            var inconvenience5 = new Inconvenience { Id = 5, Description = "bcb", IsActive = true };
            var inconvenience6 = new Inconvenience { Id = 6, Description = "æ", IsActive = true };
            var inconvenience7 = new Inconvenience { Id = 7, Description = "ø", IsActive = true };
            var inconvenience8 = new Inconvenience { Id = 8, Description = "å", IsActive = true };
            var inconveniences = new List<Inconvenience>
            {
                inconvenience6,
                inconvenience4,
                inconvenience1,
                inconvenience8,
                inconvenience3,
                inconvenience5,
                inconvenience2,
                inconvenience7,
            };

            _repo.Get(Arg.Any<Expression<Func<Inconvenience, bool>>>()).Returns(inconveniences);

            var expectedListItem1 = new SelectListItem { Text = inconvenience1.Description, Value = inconvenience1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = inconvenience2.Description, Value = inconvenience2.Id.ToString() };
            var expectedListItem3 = new SelectListItem { Text = inconvenience3.Description, Value = inconvenience3.Id.ToString() };
            var expectedListItem4 = new SelectListItem { Text = inconvenience4.Description, Value = inconvenience4.Id.ToString() };
            var expectedListItem5 = new SelectListItem { Text = inconvenience5.Description, Value = inconvenience5.Id.ToString() };
            var expectedListItem6 = new SelectListItem { Text = inconvenience6.Description, Value = inconvenience6.Id.ToString() };
            var expectedListItem7 = new SelectListItem { Text = inconvenience7.Description, Value = inconvenience7.Id.ToString() };
            var expectedListItem8 = new SelectListItem { Text = inconvenience8.Description, Value = inconvenience8.Id.ToString() };
            var expectedList = new List<SelectListItem>
            {
                expectedListItem1,
                expectedListItem2,
                expectedListItem3,
                expectedListItem4,
                expectedListItem5,
                expectedListItem6,
                expectedListItem7,
                expectedListItem8
            };

            // Act
            _populateInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInconvenienceList = _controller.ViewBag.InconvenienceList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagInconvenienceList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<Inconvenience>();
            _repo.Get(Arg.Any<Expression<Func<Inconvenience, bool>>>()).Returns(emptyList);

            // Act
            _populateInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInconvenienceList = _controller.ViewBag.InconvenienceList as List<SelectListItem>;
            Assert.Equal(0, viewBagInconvenienceList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateInconveniencesFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
