﻿#region Head
// <copyright file="PopulateProjectCategoriesFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateProjectCategoriesFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<ProjectCategory> _repo;
        private readonly ISelectedProjectCategory _selectedProjectCategory;
        private readonly PopulateProjectCategoriesFilter _populateProjectCategoriesFilter;

        public PopulateProjectCategoriesFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<ProjectCategory>>();
            _selectedProjectCategory = Substitute.For<ISelectedProjectCategory>();

            _controller.ViewData.Model = _selectedProjectCategory;
            _executedContext.Controller = _controller;

            _populateProjectCategoriesFilter = new PopulateProjectCategoriesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var projectCategory = new ProjectCategory { Id = 1, Category = "Name", IsActive = true };
            var projectCategories = new List<ProjectCategory> { projectCategory };

            _repo.Get(Arg.Any<Expression<Func<ProjectCategory, bool>>>()).Returns(projectCategories);

            var expectedListItem = new SelectListItem { Text = projectCategory.Category, Value = projectCategory.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateProjectCategoriesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagProjectCategoryList = _controller.ViewBag.ProjectCategoryList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagProjectCategoryList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsProjectCategoryDeletedSet()
        {
            // Arrange
            var projectCategory1 = new ProjectCategory { Id = 1, Category = "Name" };
            var projectCategory2 = new ProjectCategory { Id = 1, Category = "Name", IsDeleted = true };
            var projectCategories = new List<ProjectCategory> { projectCategory1, projectCategory2 };

            _repo.Get(Arg.Any<Expression<Func<ProjectCategory, bool>>>()).Returns(projectCategories);

            // Act
            _populateProjectCategoriesFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsProjectCategoryDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var projectCategory1 = new ProjectCategory { Id = 1, Category = "Name", IsActive = true };
            var projectCategory2 = new ProjectCategory { Id = 1, Category = "Name", IsActive = true };
            var projectCategories = new List<ProjectCategory> { projectCategory1, projectCategory2 };

            _repo.Get(Arg.Any<Expression<Func<ProjectCategory, bool>>>()).Returns(projectCategories);

            var expectedListItem1 = new SelectListItem { Text = projectCategory1.Category, Value = projectCategory1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = projectCategory2.Category, Value = projectCategory2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedProjectCategory.ProjectCategoryId = projectCategory1.Id;

            // Act
            _populateProjectCategoriesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagProjectCategoryList = _controller.ViewBag.ProjectCategoryList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagProjectCategoryList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<ProjectCategory>();
            _repo.Get(Arg.Any<Expression<Func<ProjectCategory, bool>>>()).Returns(emptyList);

            // Act
            _populateProjectCategoriesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagProjectCategoryList = _controller.ViewBag.ProjectCategoryList as List<SelectListItem>;
            Assert.Equal(0, viewBagProjectCategoryList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateProjectCategoriesFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
