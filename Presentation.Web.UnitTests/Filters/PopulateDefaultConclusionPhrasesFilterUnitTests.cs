﻿#region Head
// <copyright file="PopulateDefaultConclusionPhrasesFilterUnitTests.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateDefaultConclusionPhrasesFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly PopulateDefaultConclusionPhrasesFilter _populateDefaultConclusionPhrasesFilter;

        public PopulateDefaultConclusionPhrasesFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<DefaultPhrase>>();

            _executedContext.Controller = _controller;
            _populateDefaultConclusionPhrasesFilter = new PopulateDefaultConclusionPhrasesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith0ConclusionItems_ListContaining0Element()
        {
            // Arrange
            var defaultPhrases = new List<DefaultPhrase>();

            _repo.Get().ReturnsForAnyArgs(defaultPhrases);

            // Act
            _populateDefaultConclusionPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultConclusionPhraseList = _controller.ViewBag.DefaultConclusionPhraseList as List<SelectListItem>;
            Assert.Equal(0, viewBagDefaultConclusionPhraseList.Count);
        }

        [Fact]
        public void OnActionExecuted_ListWith1AConclusionItem_ListContaining1Element()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase { Id = 1, Phrase = "Phrase", Type = DefaultPhraseType.Conclusion };
            var defaultPhrases = new List<DefaultPhrase> { defaultPhrase };

            _repo.Get().ReturnsForAnyArgs(defaultPhrases);

            // Act
            _populateDefaultConclusionPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultConclusionPhraseList = _controller.ViewBag.DefaultConclusionPhraseList as List<SelectListItem>;
            Assert.Equal(1, viewBagDefaultConclusionPhraseList.Count);
        }
    }
}
