﻿#region Head
// <copyright file="PopulateConstructionProgramsFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateConstructionProgramsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<ConstructionProgram> _repo;
        private readonly ISelectedConstructionProgram _selectedConstructionProgram;
        private readonly PopulateConstructionProgramsFilter _populateConstructionProgramsFilter;

        public PopulateConstructionProgramsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<ConstructionProgram>>();
            _selectedConstructionProgram = Substitute.For<ISelectedConstructionProgram>();

            _controller.ViewData.Model = _selectedConstructionProgram;
            _executedContext.Controller = _controller;

            _populateConstructionProgramsFilter = new PopulateConstructionProgramsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var constructionProgram = new ConstructionProgram { Id = 1, YearOfProjectInvolvement = 2015 };
            var constructionPrograms = new List<ConstructionProgram> { constructionProgram };

            _repo.Get(Arg.Any<Expression<Func<ConstructionProgram, bool>>>()).Returns(constructionPrograms);

            var expectedListItem = new SelectListItem { Text = constructionProgram.YearOfProjectInvolvement.ToString(), Value = constructionProgram.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateConstructionProgramsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagConstructionProgramList = _controller.ViewBag.ConstructionProgramList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagConstructionProgramList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsConstructionProgramDeletedSet()
        {
            // Arrange
            var constructionProgram1 = new ConstructionProgram { Id = 1, YearOfProjectInvolvement = 2015 };
            var constructionProgram2 = new ConstructionProgram { Id = 1, YearOfProjectInvolvement = 2015, IsDeleted = true };
            var constructionPrograms = new List<ConstructionProgram> { constructionProgram1, constructionProgram2 };

            _repo.Get(Arg.Any<Expression<Func<ConstructionProgram, bool>>>()).Returns(constructionPrograms);

            // Act
            _populateConstructionProgramsFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsConstructionProgramDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var constructionProgram1 = new ConstructionProgram { Id = 1, YearOfProjectInvolvement = 2015 };
            var constructionProgram2 = new ConstructionProgram { Id = 1, YearOfProjectInvolvement = 2015 };
            var constructionPrograms = new List<ConstructionProgram> { constructionProgram1, constructionProgram2 };

            _repo.Get(Arg.Any<Expression<Func<ConstructionProgram, bool>>>()).Returns(constructionPrograms);

            var expectedListItem1 = new SelectListItem { Text = constructionProgram1.YearOfProjectInvolvement.ToString(), Value = constructionProgram1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = constructionProgram2.YearOfProjectInvolvement.ToString(), Value = constructionProgram2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedConstructionProgram.ConstructionProgramId = constructionProgram1.Id;

            // Act
            _populateConstructionProgramsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagConstructionProgramList = _controller.ViewBag.ConstructionProgramList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagConstructionProgramList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<ConstructionProgram>();
            _repo.Get(Arg.Any<Expression<Func<ConstructionProgram, bool>>>()).Returns(emptyList);

            // Act
            _populateConstructionProgramsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagConstructionProgramList = _controller.ViewBag.ConstructionProgramList as List<SelectListItem>;
            Assert.Equal(0, viewBagConstructionProgramList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateConstructionProgramsFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
