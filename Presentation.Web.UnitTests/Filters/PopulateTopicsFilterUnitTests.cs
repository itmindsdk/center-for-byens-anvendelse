﻿#region Head
// <copyright file="PopulateTopicsFilterUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateTopicsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Topic> _repo;
        private readonly ISelectedTopic _selectedTopic;
        private readonly PopulateTopicsFilter _populateTopicsFilter;

        public PopulateTopicsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<Topic>>();
            _selectedTopic = Substitute.For<ISelectedTopic>();

            _controller.ViewData.Model = _selectedTopic;
            _executedContext.Controller = _controller;

            _populateTopicsFilter = new PopulateTopicsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var topic = new Topic { Id = 1, Name = "Name" };
            var topics = new List<Topic> { topic };

            _repo.Get(Arg.Any<Expression<Func<Topic, bool>>>()).Returns(topics);

            var expectedList = new List<SelectListItem>();

            var expectedListItem = new SelectListItem { Text = topic.Name, Value = topic.Id.ToString() };
            expectedList.Add(expectedListItem);

            // Act
            _populateTopicsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTopicList = _controller.ViewBag.TopicList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagTopicList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1SelectedIsDeleted_IsTopicDeletedSet()
        {
            // Arrange
            var topic1 = new Topic { Id = 1, Name = "Name" };
            var topic2 = new Topic { Id = 1, Name = "Name", IsDeleted = true };
            var topics = new List<Topic> { topic1, topic2 };

            _repo.Get(Arg.Any<Expression<Func<Topic, bool>>>()).Returns(topics);

            // Act
            _populateTopicsFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsTopicDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var topic1 = new Topic { Id = 1, Name = "Name" };
            var topic2 = new Topic { Id = 1, Name = "Name" };
            var topics = new List<Topic> { topic1, topic2 };

            _repo.Get(Arg.Any<Expression<Func<Topic, bool>>>()).Returns(topics);

            var expectedListItem1 = new SelectListItem { Text = topic1.Name, Value = topic1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = topic2.Name, Value = topic2.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedTopic.TopicId = topic1.Id;

            // Act
            _populateTopicsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTopicList = _controller.ViewBag.TopicList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagTopicList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Elements()
        {
            // Arrange
            var emptyList = new List<Topic>();
            _repo.Get(Arg.Any<Expression<Func<Topic, bool>>>()).Returns(emptyList);

            // Act
            _populateTopicsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTopicList = _controller.ViewBag.TopicList as List<SelectListItem>;
            Assert.Equal(0, viewBagTopicList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateTopicsFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
