#region Head
// <copyright file="SelectListItemComparer.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System.Collections.Generic;
using System.Web.Mvc;

namespace Presentation.Web.UnitTests.Filters
{
    /// <summary>
    /// SelectListItemComparer used to compare values of <see cref="SelectListItem"/>s.
    /// </summary>
    public class SelectListItemComparer : IEqualityComparer<SelectListItem>
    {
        public bool Equals(SelectListItem x, SelectListItem y)
        {
            if (ReferenceEquals(x, y)) return true;

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            bool @group;

            if (x.Group == null && y.Group != null || x.Group != null && y.Group == null)
                @group = false;
            else if (x.Group == null && y.Group == null)
                @group = true;
            else
                @group = x.Group.Equals(y.Group);

            return  ((x.Text == null && y.Text == null) || x.Text.Equals(y.Text)) &&
                    ((x.Value == null && y.Value == null) || x.Value.Equals(y.Value)) &&
                    x.Selected.Equals(y.Selected) &&
                    x.Disabled.Equals(y.Disabled) &&
                    @group;
        }

        public int GetHashCode(SelectListItem obj)
        {
            if (ReferenceEquals(obj, null)) return 0;

            var hashText = obj.Text == null ? 0 : obj.Text.GetHashCode();
            var hashValue = obj.Value.GetHashCode();
            var hashSelect = obj.Selected.GetHashCode();
            var hashDisabled = obj.Disabled.GetHashCode();
            var hashGroup = obj.Group == null ? 0 : obj.Group.GetHashCode();

            return hashText ^ hashValue ^ hashSelect ^ hashDisabled ^ hashGroup;
        }
    }
}
