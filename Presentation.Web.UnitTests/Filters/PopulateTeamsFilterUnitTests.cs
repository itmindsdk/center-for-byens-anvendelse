﻿#region Head
// <copyright file="PopulateTeamsFilterUnitTests.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateTeamsFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<Team> _repo;
        private readonly ISelectedTeam _selectedTeam;
        private readonly PopulateTeamsFilter _populateTeamsFilter;

        public PopulateTeamsFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<Team>>();
            _selectedTeam = Substitute.For<ISelectedTeam>();

            _controller.ViewData.Model = _selectedTeam;
            _executedContext.Controller = _controller;

            _populateTeamsFilter = new PopulateTeamsFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith0Items_ListContaining0Elements()
        {
            // Arrange
            var teams = new List<Team>();

            _repo.Get().ReturnsForAnyArgs(teams);

            // Act
            _populateTeamsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTeamList = _controller.ViewBag.TeamList as List<SelectListItem>;
            var expectedCount = 0;
            Assert.Equal(expectedCount, viewBagTeamList.Count);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var team = new Team { Id = 1, Name = "Name" };
            var teams = new List<Team> { team };

            _repo.Get().ReturnsForAnyArgs(teams);

            // Act
            _populateTeamsFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTeamList = _controller.ViewBag.TeamList as List<SelectListItem>;
            var expectedCount = 1;
            Assert.Equal(expectedCount, viewBagTeamList.Count);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1IsDeleted_IsGroupDeletedSet()
        {
            // Arrange
            var team1 = new Team { Id = 1, Name = "Name" };
            var team2 = new Team { Id = 1, Name = "Name", IsDeleted = true };
            var teams = new List<Team> { team1, team2 };

            _repo.Get().ReturnsForAnyArgs(teams);

            // Act
            _populateTeamsFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsTeamDeleted);
        }
    }
}
