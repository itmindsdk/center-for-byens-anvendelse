﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateTrafficInconveniencesUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<TrafficInconvenience> _repo;
        private readonly ISelectedTrafficInconvenience _selectedTrafficInconvenience;
        private readonly PopulateTrafficInconveniencesFilter _populateTrafficInconveniencesFilter;

        public PopulateTrafficInconveniencesUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<TrafficInconvenience>>();
            _selectedTrafficInconvenience = Substitute.For<ISelectedTrafficInconvenience>();

            _controller.ViewData.Model = _selectedTrafficInconvenience;
            _executedContext.Controller = _controller;

            _populateTrafficInconveniencesFilter = new PopulateTrafficInconveniencesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith1Item_ListContaining1Element()
        {
            // Arrange
            var trafficInconvenience = new TrafficInconvenience
            {
                Description = "TrafficInconvenience description",
                Id = 1,
                IsActive = true
            };
            var trafficInconveniences = new List<TrafficInconvenience> { trafficInconvenience };

            _repo.Get(Arg.Any<Expression<Func<TrafficInconvenience, bool>>>()).Returns(trafficInconveniences);

            var expectedListItem = new SelectListItem { Text = trafficInconvenience.Description, Value = trafficInconvenience.Id.ToString() };
            var expectedList = new List<SelectListItem> { expectedListItem };

            // Act
            _populateTrafficInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTrafficInconvenienceList = _controller.ViewBag.TrafficInconvenienceList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagTrafficInconvenienceList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith2ItemsSelectedIsDeleted_IsTrafficInconvenienceDeletedSet()
        {
            // Arrange
            var trafficInconvenience1 = new TrafficInconvenience
            {
                Description = "TrafficInconvenience description",
                Id = 1,
                IsActive = true
            };
            var trafficInconvenience2 = new TrafficInconvenience
            {
                Description = "Description iconvenience",
                Id = 2,
                IsActive = true,
                IsDeleted = true
            };
            var trafficInconveniences = new List<TrafficInconvenience> { trafficInconvenience1, trafficInconvenience2 };

            _repo.Get(Arg.Any<Expression<Func<TrafficInconvenience, bool>>>()).Returns(trafficInconveniences);

            // Act
            _populateTrafficInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            Assert.True(_controller.ViewBag.IsTrafficInconvenienceDeleted);
        }

        [Fact]
        public void OnActionExecuted_ListWith2Items1Selected_ListContaining2Elements()
        {
            // Arrange
            var trafficInconvenience1 = new TrafficInconvenience { Id = 1, Description = "Name", IsActive = true };
            var trafficInconvenience2 = new TrafficInconvenience { Id = 1, Description = "Name", IsActive = true };
            var trafficInconveniences = new List<TrafficInconvenience> { trafficInconvenience1, trafficInconvenience2 };

            _repo.Get(Arg.Any<Expression<Func<TrafficInconvenience, bool>>>()).Returns(trafficInconveniences);

            var expectedListItem1 = new SelectListItem { Text = trafficInconvenience1.Description, Value = trafficInconvenience1.Id.ToString()};
            var expectedListItem2 = new SelectListItem { Text = trafficInconvenience2.Description, Value = trafficInconvenience2.Id.ToString()};
            var expectedList = new List<SelectListItem> { expectedListItem1, expectedListItem2 };

            _selectedTrafficInconvenience.TrafficInconvenienceId = trafficInconvenience1.Id;

            // Act
            _populateTrafficInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTrafficInconvenienceList = _controller.ViewBag.TrafficInconvenienceList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagTrafficInconvenienceList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_ListWith8Items_ListContaining8ElementsSortedByDescription()
        {
            // Arrange
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("da-dk");

            var trafficInconvenience1 = new TrafficInconvenience { Id = 1, Description = "a", IsActive = true };
            var trafficInconvenience2 = new TrafficInconvenience { Id = 2, Description = "ba", IsActive = true };
            var trafficInconvenience3 = new TrafficInconvenience { Id = 3, Description = "bb", IsActive = true };
            var trafficInconvenience4 = new TrafficInconvenience { Id = 4, Description = "bc", IsActive = true };
            var trafficInconvenience5 = new TrafficInconvenience { Id = 5, Description = "bcb", IsActive = true };
            var trafficInconvenience6 = new TrafficInconvenience { Id = 6, Description = "æ", IsActive = true };
            var trafficInconvenience7 = new TrafficInconvenience { Id = 7, Description = "ø", IsActive = true };
            var trafficInconvenience8 = new TrafficInconvenience { Id = 8, Description = "å", IsActive = true };
            var trafficInconveniences = new List<TrafficInconvenience>
            {
                trafficInconvenience6,
                trafficInconvenience4,
                trafficInconvenience1,
                trafficInconvenience8,
                trafficInconvenience3,
                trafficInconvenience5,
                trafficInconvenience2,
                trafficInconvenience7,
            };

            _repo.Get(Arg.Any<Expression<Func<TrafficInconvenience, bool>>>()).Returns(trafficInconveniences);

            var expectedListItem1 = new SelectListItem { Text = trafficInconvenience1.Description, Value = trafficInconvenience1.Id.ToString() };
            var expectedListItem2 = new SelectListItem { Text = trafficInconvenience2.Description, Value = trafficInconvenience2.Id.ToString() };
            var expectedListItem3 = new SelectListItem { Text = trafficInconvenience3.Description, Value = trafficInconvenience3.Id.ToString() };
            var expectedListItem4 = new SelectListItem { Text = trafficInconvenience4.Description, Value = trafficInconvenience4.Id.ToString() };
            var expectedListItem5 = new SelectListItem { Text = trafficInconvenience5.Description, Value = trafficInconvenience5.Id.ToString() };
            var expectedListItem6 = new SelectListItem { Text = trafficInconvenience6.Description, Value = trafficInconvenience6.Id.ToString() };
            var expectedListItem7 = new SelectListItem { Text = trafficInconvenience7.Description, Value = trafficInconvenience7.Id.ToString() };
            var expectedListItem8 = new SelectListItem { Text = trafficInconvenience8.Description, Value = trafficInconvenience8.Id.ToString() };
            var expectedList = new List<SelectListItem>
            {
                expectedListItem1,
                expectedListItem2,
                expectedListItem3,
                expectedListItem4,
                expectedListItem5,
                expectedListItem6,
                expectedListItem7,
                expectedListItem8
            };

            // Act
            _populateTrafficInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagInconvenienceList = _controller.ViewBag.TrafficInconvenienceList as List<SelectListItem>;
            Assert.Equal(expectedList, viewBagInconvenienceList, new SelectListItemComparer());
        }

        [Fact]
        public void OnActionExecuted_RepoReturnsEmptyList_ListContaining0Element()
        {
            // Arrange
            var emptyList = new List<TrafficInconvenience>();
            _repo.Get(Arg.Any<Expression<Func<TrafficInconvenience, bool>>>()).Returns(emptyList);

            // Act
            _populateTrafficInconveniencesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagTrafficInconvenienceList = _controller.ViewBag.TrafficInconvenienceList as List<SelectListItem>;
            Assert.Equal(0, viewBagTrafficInconvenienceList.Count);
        }

        [Fact]
        public void OnActionExecuting_Call_ForCoverage()
        {
            // Arrange
            // Act
            _populateTrafficInconveniencesFilter.OnActionExecuting(_executingContext);

            // Assert
        }
    }
}
