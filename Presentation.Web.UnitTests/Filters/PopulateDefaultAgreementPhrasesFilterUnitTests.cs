﻿#region Head
// <copyright file="PopulateDefaultAgreementPhrasesFilterUnitTests.cs" company="IT Minds" year="2016">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Filters;
using Xunit;

namespace Presentation.Web.UnitTests.Filters
{
    public class PopulateDefaultAgreementPhrasesFilterUnitTests
    {
        private readonly Controller _controller;
        private readonly ActionExecutingContext _executingContext;
        private readonly ActionExecutedContext _executedContext;
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly PopulateDefaultAgreementPhrasesFilter _populateDefaultAgreementPhrasesFilter;

        public PopulateDefaultAgreementPhrasesFilterUnitTests()
        {
            _controller = Substitute.For<Controller>();
            _executingContext = Substitute.For<ActionExecutingContext>();
            _executedContext = Substitute.For<ActionExecutedContext>();
            _repo = Substitute.For<IGenericRepository<DefaultPhrase>>();

            _executedContext.Controller = _controller;
            _populateDefaultAgreementPhrasesFilter = new PopulateDefaultAgreementPhrasesFilter(_repo);
        }

        [Fact]
        public void OnActionExecuted_ListWith0AgreementItems_ListContaining0Element()
        {
            // Arrange
            var defaultPhrases = new List<DefaultPhrase>();

            _repo.Get().ReturnsForAnyArgs(defaultPhrases);

            // Act
            _populateDefaultAgreementPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultAgreementPhraseList = _controller.ViewBag.DefaultAgreementPhraseList as List<SelectListItem>;
            Assert.Equal(0, viewBagDefaultAgreementPhraseList.Count);
        }

        [Fact]
        public void OnActionExecuted_ListWith1AgreementItem_ListContaining1Element()
        {
            // Arrange
            var defaultPhrase = new DefaultPhrase { Id = 1, Phrase = "Phrase", Type = DefaultPhraseType.Agreement };
            var defaultPhrases = new List<DefaultPhrase> { defaultPhrase };

            _repo.Get().ReturnsForAnyArgs(defaultPhrases);

            // Act
            _populateDefaultAgreementPhrasesFilter.OnActionExecuted(_executedContext);

            // Assert
            var viewBagDefaultAgreementPhraseList = _controller.ViewBag.DefaultAgreementPhraseList as List<SelectListItem>;
            Assert.Equal(1, viewBagDefaultAgreementPhraseList.Count);
        }
    }
}
