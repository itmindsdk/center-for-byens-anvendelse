﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Advisor;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class AdvisorControllerUnitTests
    {
        private readonly AdvisorController _advisorController;
        private readonly IGenericRepository<Advisor> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        private readonly CreateAdvisorViewModel _createAdvisorViewModel;
        private readonly DeleteAdvisorViewModel _deleteAdvisorViewModel;
        private readonly EditAdvisorViewModel _editAdvisorViewModel;

        public AdvisorControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<Advisor>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();
            new UserMock().LogOn(); // setting user mock

            _createAdvisorViewModel = new CreateAdvisorViewModel();
            _deleteAdvisorViewModel = new DeleteAdvisorViewModel();
            _editAdvisorViewModel = new EditAdvisorViewModel();

            _mapper.Map<CreateAdvisorViewModel>(Arg.Any<Advisor>())
                .Returns(_createAdvisorViewModel);
            _mapper.Map<DeleteAdvisorViewModel>(Arg.Any<Advisor>())
                .Returns(_deleteAdvisorViewModel);
            _mapper.Map<EditAdvisorViewModel>(Arg.Any<Advisor>())
                .Returns(_editAdvisorViewModel);
            _mapper.Map<Advisor>(Arg.Any<EditAdvisorViewModel>())
                .Returns(new Advisor());
            _mapper.Map<Advisor>(Arg.Any<CreateAdvisorViewModel>())
                .Returns(new Advisor());

            _advisorController = new AdvisorController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _advisorController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            // Act
            var ret = _advisorController.Create();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            // Act
            var ret = _advisorController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateAdvisorViewModel();

            // Act
            var ret = _advisorController.Create(vm);

            // Assert
            Assert.IsType(typeof(RedirectToRouteResult), ret);
        }

        [Fact]
        public void CreatePost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateAdvisorViewModel();
            _advisorController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _advisorController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateAdvisorViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateAdvisorViewModel();

            // Act
            _advisorController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<Advisor>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var advisor = new Advisor();
            int? id = 1;
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var advisor = new Advisor();
            int? id = 1;
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditAdvisorViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;
            const int expectedStatusCode = 400;

            // Act
            var ret = _advisorController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal(expectedStatusCode, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _advisorController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidCallGetByKeyInRepoReturnsNull_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Advisor)null);

            // Act
            var ret = _advisorController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidAdvisorDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var advisor = new Advisor {IsDeleted = true};
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditAdvisorViewModel();

            // Act
            var ret = _advisorController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelInvalid_ReturnView()
        {
            // Arrange
            var vm = new EditAdvisorViewModel();
            _advisorController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _advisorController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditAdvisorViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditAdvisorViewModel();

            // Act
            _advisorController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<Advisor>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var advisor = new Advisor();
            int? id = 1;
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var advisor = new Advisor();
            int? id = 1;
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DeleteAdvisorViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var advisor = new Advisor();
            int? id = 1;
            _repo.GetByKey(id).Returns(advisor);

            // Act
            _advisorController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidRepoGetByKeyReturnsNull_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((Advisor)null);

            // Act
            var ret = _advisorController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidAdvisorDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var advisor = new Advisor { IsDeleted = true };
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _advisorController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_RepoUpdateCalled()
        {
            // Arrange
            const int id = 1;
            var advisor = new Advisor();
            _repo.GetByKey(id).Returns(advisor);

            // Act
            _advisorController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(advisor);
            _unitOfWork.Received(1).Save();
            Assert.True(advisor.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var advisor = new Advisor();
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;
            _repo.GetByKey(id).Returns((Advisor) null);

            // Act
            var ret = _advisorController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void DeleteConfirmed_IdNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            _repo.GetByKey(id).Returns((Advisor)null);

            // Act
            var ret = _advisorController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdFoundAdvisorDeleted_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var advisor = new Advisor { IsDeleted = true };
            _repo.GetByKey(id).Returns(advisor);

            // Act
            var ret = _advisorController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion
    }
}
