﻿#region Head
// <copyright file="ConstructionProgramControllerUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ConstructionProgram;
using Xunit;

namespace Presentation.Web.UnitTests
{
    public class ConstructionProgramControllerUnitTests
    {
        private readonly ConstructionProgramController _constructionProgramController;
        private readonly IGenericRepository<ConstructionProgram> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        public ConstructionProgramControllerUnitTests()
        {
            _repo = Substitute.For<IGenericRepository<ConstructionProgram>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            _mapper = Substitute.For<IMapper>();
            new UserMock().LogOn(); // setting user mock
            _mapper.Map<ConstructionProgram>(Arg.Any<CreateConstructionProgramViewModel>())
                .Returns(new ConstructionProgram());
            _mapper.Map<ConstructionProgram>(Arg.Any<EditConstructionProgramViewModel>())
                .Returns(new ConstructionProgram());

            _constructionProgramController = new ConstructionProgramController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Index
        [Fact]
        public void Index_Call_ReturnsView()
        {
            // Arrange
            _mapper.Map<IndexConstructionProgramViewModel>(Arg.Any<ConstructionProgram>())
                .Returns(new IndexConstructionProgramViewModel());

            // Act
            var ret = _constructionProgramController.Index();

            // Assert
            Assert.IsType(typeof(ViewResult), ret);
        }
        #endregion

        #region Create Get
        [Fact]
        public void Create_Call_ReturnsView()
        {
            // Arrange
            _mapper.Map<CreateConstructionProgramViewModel>(Arg.Any<ConstructionProgram>())
                .Returns(new CreateConstructionProgramViewModel());

            // Act
            var ret = _constructionProgramController.Create();

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Create_Call_ReturnsViewModel()
        {
            // Arrange
            _mapper.Map<CreateConstructionProgramViewModel>(Arg.Any<ConstructionProgram>())
                .Returns(new CreateConstructionProgramViewModel());

            // Act
            var ret = _constructionProgramController.Create() as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.NotNull(ret.Model);
        }
        #endregion

        #region Create Post
        [Fact]
        public void CreatePost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new CreateConstructionProgramViewModel();

            // Act
            var ret = _constructionProgramController.Create(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void CreatePost_ModelNotvalid_ReturnView()
        {
            // Arrange
            var vm = new CreateConstructionProgramViewModel();
            _constructionProgramController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _constructionProgramController.Create(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<CreateConstructionProgramViewModel>(ret.Model);
        }

        [Fact]
        public void CreatePost_ModelValid_CallCreateInRepo()
        {
            // Arrange
            var vm = new CreateConstructionProgramViewModel();

            // Act
            _constructionProgramController.Create(vm);

            // Assert
            _repo.Received(1).Insert(Arg.Any<ConstructionProgram>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Edit Get
        [Fact]
        public void Edit_IdValid_ReturnView()
        {
            // Arrange
            var constructionProgram = new ConstructionProgram();
            int? id = 1;
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            var ret = _constructionProgramController.Edit(id);

            // Assert
            Assert.IsAssignableFrom<ViewResult>(ret);
        }

        [Fact]
        public void Edit_IdValid_ReturnViewModel()
        {
            // Arrange
            var constructionProgram = new ConstructionProgram();
            int? id = 1;
            _repo.GetByKey(id).Returns(constructionProgram);
            _mapper.Map<EditConstructionProgramViewModel>(Arg.Any<ConstructionProgram>())
                .Returns(new EditConstructionProgramViewModel());

            // Act
            var ret = _constructionProgramController.Edit(id) as ViewResult;

            // Assert
            Assert.IsType<EditConstructionProgramViewModel>(ret.Model);
        }

        [Fact]
        public void Edit_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _constructionProgramController.Edit(id) as HttpStatusCodeResult;

            // Assert
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }

        [Fact]
        public void Edit_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            int? id = 1;

            // Act
            _constructionProgramController.Edit(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Edit_IdValidConstructionProgramNotFound_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((ConstructionProgram)null);

            // Act
            var ret = _constructionProgramController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Edit_IdValidConstructionProgramDeleted_ReturnNotFound()
        {
            // Arrange
            int? id = 1;
            var constructionProgram = new ConstructionProgram { IsDeleted = true };
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            var ret = _constructionProgramController.Edit(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }
        #endregion

        #region Edit Post
        [Fact]
        public void EditPost_ModelValid_RedirectAction()
        {
            // Arrange
            var vm = new EditConstructionProgramViewModel();

            // Act
            var ret = _constructionProgramController.Edit(vm);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void EditPost_ModelNotValid_ReturnView()
        {
            // Arrange
            var vm = new EditConstructionProgramViewModel();
            _constructionProgramController.ModelState.AddModelError("error", "error");

            // Act
            var ret = _constructionProgramController.Edit(vm) as ViewResult;

            // Assert
            Assert.NotNull(ret);
            Assert.IsType<EditConstructionProgramViewModel>(ret.Model);
        }

        [Fact]
        public void EditPost_ModelValid_CallUpdateInRepo()
        {
            // Arrange
            var vm = new EditConstructionProgramViewModel();

            // Act
            _constructionProgramController.Edit(vm);

            // Assert
            _repo.Received(1).Update(Arg.Any<ConstructionProgram>());
            _unitOfWork.Received(1).Save();
        }
        #endregion

        #region Delete
        [Fact]
        public void Delete_IdValid_ReturnView()
        {
            // Arrange
            var constructionProgram = new ConstructionProgram();
            int? id = 1;
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            var ret = _constructionProgramController.Delete(id);

            // Assert
            Assert.IsType<ViewResult>(ret);
        }

        [Fact]
        public void Delete_IdValid_ReturnViewModel()
        {
            // Arrange
            var constructionProgram = new ConstructionProgram();
            int? id = 1;
            _repo.GetByKey(id).Returns(constructionProgram);
            _mapper.Map<DeleteConstructionProgramViewModel>(Arg.Any<ConstructionProgram>())
                .Returns(new DeleteConstructionProgramViewModel());

            // Act
            var ret = _constructionProgramController.Delete(id) as ViewResult;

            // Assert
            Assert.IsType<DeleteConstructionProgramViewModel>(ret.Model);
        }

        [Fact]
        public void Delete_IdValid_CallGetByKeyInRepo()
        {
            // Arrange
            var constructionProgram = new ConstructionProgram();
            int? id = 1;
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            _constructionProgramController.Delete(id);

            // Assert
            _repo.Received(1).GetByKey(id);
        }

        [Fact]
        public void Delete_IdValidConstructionProgramNotFound_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            _repo.GetByKey(id).Returns((ConstructionProgram)null);

            // Act
            var ret = _constructionProgramController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdValidConstructionProgramDeleted_ReturnHttpNotFound()
        {
            // Arrange
            int? id = 1;
            var constructionProgram = new ConstructionProgram { IsDeleted = true };
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            var ret = _constructionProgramController.Delete(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void Delete_IdNull_ReturnBadRequest()
        {
            // Arrange
            int? id = null;

            // Act
            var ret = _constructionProgramController.Delete(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().GetByKey(Arg.Any<int?>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion

        #region DeleteConfirmed
        [Fact]
        public void DeleteConfirmed_IdValid_UpdateDeletedConstructionProgramInRepo()
        {
            // Arrange
            const int id = 1;
            var constructionProgram = new ConstructionProgram();
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            _constructionProgramController.DeleteConfirmed(id);

            // Assert
            _repo.Received(1).Update(constructionProgram);
            _unitOfWork.Received(1).Save();
            Assert.True(constructionProgram.IsDeleted);
        }

        [Fact]
        public void DeleteConfirmed_IdValid_RedirectAction()
        {
            // Arrange
            const int id = 1;
            var constructionProgram = new ConstructionProgram();
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            var ret = _constructionProgramController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<RedirectToRouteResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdValidConstructionProgramDeleted_ReturnHttpNotFound()
        {
            // Arrange
            const int id = 1;
            var constructionProgram = new ConstructionProgram {IsDeleted = true};
            _repo.GetByKey(id).Returns(constructionProgram);

            // Act
            var ret = _constructionProgramController.DeleteConfirmed(id);

            // Assert
            Assert.IsType<HttpNotFoundResult>(ret);
        }

        [Fact]
        public void DeleteConfirmed_IdNotValid_ReturnBadRequest()
        {
            // Arrange
            const int id = 0;

            // Act
            var ret = _constructionProgramController.DeleteConfirmed(id) as HttpStatusCodeResult;

            // Assert
            _repo.DidNotReceive().DeleteByKey(Arg.Any<int>());
            Assert.NotNull(ret);
            Assert.Equal((int)HttpStatusCode.BadRequest, ret.StatusCode);
        }
        #endregion
    }
}
