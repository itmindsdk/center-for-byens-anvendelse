﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Presentation.Web.IntegrationTests
{
    class ControllerContextHelpers
    {
        public static HttpRequest ConstructHttpRequestWithCookies(params HttpCookie[] cookies)
        {
            var httpRequest = ConstructHttpRequest();
            foreach (var cookie in cookies)
            {
                httpRequest.Cookies.Set(cookie);
            }
            return httpRequest;
        }

        public static HttpRequest ConstructHttpRequest()
        {
            return new HttpRequest("", "http://testing", "");
        }

        public static ControllerContext ConstructControllerContext(ControllerBase controller, HttpRequest request = null)
        {
            HttpRequest httpRequest = request ?? ConstructHttpRequest();
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            // ControllerContext
            return new ControllerContext(new HttpContextWrapper(httpContext), new RouteData(), controller);
        }
    }
}
