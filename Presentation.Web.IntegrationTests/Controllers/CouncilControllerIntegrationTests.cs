﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Council;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class CouncilControllerIntegrationTests
    {
        private readonly IGenericRepository<Council> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly CouncilController _councilController;

        public CouncilControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Council>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _councilController = new CouncilController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Council_read
        [Fact]
        public void Council_Read_GetPage1PageSize10_GetsPageOfCouncils()
        {
            // Arrange
            var councilList = new List<Council>();
            for (int i = 0; i < 25; ++i)
            {
                councilList.Add(new Council());
            }
            _repo.AsQueryable().Returns(councilList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _councilController.WithCallTo(c => c.Council_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexCouncilViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Council_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfCouncils()
        {
            // Arrange
            var councilList = new List<Council>();
            for (int i = 0; i < 25; ++i)
            {
                councilList.Add(new Council { Id = i });
            }
            _repo.AsQueryable().Returns(councilList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _councilController.WithCallTo(c => c.Council_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCouncilViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void Council_Read_GetCouncilsIsActiveTrue_GetsCouncilsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var councilList = new List<Council>{
                new Council {Id = 1, IsActive = !valueWeWant},
                new Council {Id = 2, IsActive = valueWeWant},
                new Council {Id = 3, IsActive = valueWeWant},
                new Council {Id = 4, IsActive = !valueWeWant},
                new Council {Id = 5, IsActive = !valueWeWant},
                new Council {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(councilList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _councilController.WithCallTo(c => c.Council_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCouncilViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Council_Read_GetCouncilsIsActiveFalse_GetsCouncilsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var councilList = new List<Council>{
                new Council {Id = 1, IsActive = valueWeWant},
                new Council {Id = 2, IsActive = !valueWeWant},
                new Council {Id = 3, IsActive = !valueWeWant},
                new Council {Id = 4, IsActive = valueWeWant},
                new Council {Id = 5, IsActive = valueWeWant},
                new Council {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(councilList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _councilController.WithCallTo(c => c.Council_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCouncilViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Council_Read_GetCouncilsEqualToGroupName_GetsOnlyCouncilsWithExactGroupName()
        {
            // Arrange
            var nameWeWant = "GroupName";
            var companyList = new List<Council>{
                new Council {Id = 1, Group = new Group {Name = "No"} },
                new Council {Id = 2, Group = new Group {Name = nameWeWant + " with a suffix"} },
                new Council {Id = 3, Group = new Group {Name = "With a prefix " + nameWeWant} },
                new Council {Id = 4, Group = new Group {Name = "Not the correct name at all"} },
                new Council {Id = 5, Group = new Group {Name = nameWeWant} },
                new Council {Id = 6, Group = new Group {Name = "Another"} }
            };
            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Group.Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _councilController.WithCallTo(c => c.Council_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCouncilViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Group.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void Council_Read_GetCouncilsWithGroupNameContainingPartOfFilter_GetsCouncilsWithGroupNamesContainingFilterString()
        {
            // Arrange
            var nameWeWant = "GroupName";
            var companyList = new List<Council>{
                new Council {Id = 1, Group = new Group {Name = "No"} },
                new Council {Id = 2, Group = new Group {Name = nameWeWant + " with a suffix"} },
                new Council {Id = 3, Group = new Group {Name = "With a prefix " + nameWeWant} },
                new Council {Id = 4, Group = new Group {Name = "Not the correct name at all"} },
                new Council {Id = 5, Group = new Group {Name = nameWeWant} },
                new Council {Id = 6, Group = new Group {Name = "Another"} }
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Group.Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _councilController.WithCallTo(c => c.Council_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCouncilViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        #endregion
    }
}
