﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class LocalCommunityControllerIntegrationTests
    {
        private readonly IGenericRepository<LocalCommunity> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly LocalCommunityController _localCommunityController;

        public LocalCommunityControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<LocalCommunity>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _localCommunityController = new LocalCommunityController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        [Fact]
        public void LocalCommunity_Read_GetPage1PageSize10_GetsPageOfLocalCommunities()
        {
            // Arrange
            var localCommunityList = new List<LocalCommunity>();
            for (int i = 0; i < 25; ++i)
            {
                localCommunityList.Add(new LocalCommunity());
            }
            _repo.AsQueryable().Returns(localCommunityList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfLocalCommunities()
        {
            // Arrange
            var localCommunityList = new List<LocalCommunity>();
            for (int i = 0; i < 25; ++i)
            {
                localCommunityList.Add(new LocalCommunity() { Id = i });
            }
            _repo.AsQueryable().Returns(localCommunityList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesWithNumberLessThan_GetsLocalCommunityiesWithNumberLessThanFilteredNumber()
        {
            // Arrange
            var valuesLessThanThis = 10;
            var companyList = new List<LocalCommunity>
            {
                new LocalCommunity {Id = 1, Number = 1},
                new LocalCommunity {Id = 2, Number = 2},
                new LocalCommunity {Id = 3, Number = 3},
                new LocalCommunity {Id = 4, Number = 5},
                new LocalCommunity {Id = 5, Number = 10},
                new LocalCommunity {Id = 6, Number = 100},
                new LocalCommunity {Id = 7, Number = 1000},
                new LocalCommunity {Id = 8, Number = 9999}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Number", Operator = FilterOperator.IsLessThan, Value = valuesLessThanThis });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 1, 2, 3, 4 },
                        (c1, c2) => c1.Id == c2 && c1.Number < valuesLessThanThis
                    );
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesWithNumberGreaterThan_GetsLocalCommunityWithNumberGreaterThanFilteredNumber()
        {
            // Arrange
            var valuesGreaterThanThis = 10;
            var companyList = new List<LocalCommunity>
            {
                new LocalCommunity {Id = 1, Number = 1},
                new LocalCommunity {Id = 2, Number = 2},
                new LocalCommunity {Id = 3, Number = 3},
                new LocalCommunity {Id = 4, Number = 5},
                new LocalCommunity {Id = 5, Number = 10},
                new LocalCommunity {Id = 6, Number = 100},
                new LocalCommunity {Id = 7, Number = 1000},
                new LocalCommunity {Id = 8, Number = 9999}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Number", Operator = FilterOperator.IsGreaterThan, Value = valuesGreaterThanThis });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 6, 7, 8 },
                        (c1, c2) => c1.Id == c2 && c1.Number > valuesGreaterThanThis
                    );
            });
        }
        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesEqualToNumber_GetsLocalCommunitiesEqualToFilteredNumber()
        {
            // Arrange
            var valueWeWant = 10;
            var companyList = new List<LocalCommunity>
            {
                new LocalCommunity {Id = 1, Number = 1},
                new LocalCommunity {Id = 2, Number = 2},
                new LocalCommunity {Id = 3, Number = 3},
                new LocalCommunity {Id = 4, Number = 5},
                new LocalCommunity {Id = 5, Number = 10},
                new LocalCommunity {Id = 6, Number = 100},
                new LocalCommunity {Id = 7, Number = 1000},
                new LocalCommunity {Id = 8, Number = 9999}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Number", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Number == valueWeWant
                    );
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesEqualToName_GetsOnlyLocalCommunitiesWithExactName()
        {
            // Arrange
            var nameWeWant = "Aarhus Vest";
            var localCommunityList = new List<LocalCommunity>{
                new LocalCommunity {Id = 1, Name = "No"},
                new LocalCommunity {Id = 2, Name = nameWeWant + " with a suffix"},
                new LocalCommunity {Id = 3, Name = "With a prefix " + nameWeWant},
                new LocalCommunity {Id = 4, Name = "Not the correct name at all"},
                new LocalCommunity {Id = 5, Name = nameWeWant},
                new LocalCommunity {Id = 6, Name = "Another"}
            };

            _repo.AsQueryable().Returns(localCommunityList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesContainingPartOfName_GetsLocalCommunitiesWithNameContainingFilterString()
        {
            // Arrange
            var nameWeWant = "Aarhus Vest";
            var localCommunityList = new List<LocalCommunity>{
                new LocalCommunity {Id = 1, Name = "No"},
                new LocalCommunity {Id = 2, Name = nameWeWant + " with a suffix"},
                new LocalCommunity {Id = 3, Name = "With a prefix " + nameWeWant},
                new LocalCommunity {Id = 4, Name = "Not the correct name at all"},
                new LocalCommunity {Id = 5, Name = nameWeWant},
                new LocalCommunity {Id = 6, Name = "Another"}
            };

            _repo.AsQueryable().Returns(localCommunityList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesIsActiveTrue_GetsLocalCommunitiesIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var localCommunityList = new List<LocalCommunity>
            {
                new LocalCommunity {Id = 1, IsActive = !valueWeWant},
                new LocalCommunity {Id = 2, IsActive = valueWeWant},
                new LocalCommunity {Id = 3, IsActive = valueWeWant},
                new LocalCommunity {Id = 4, IsActive = !valueWeWant},
                new LocalCommunity {Id = 5, IsActive = !valueWeWant},
                new LocalCommunity {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(localCommunityList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void LocalCommunity_Read_GetLocalCommunitiesIsActiveFalse_GetsLocalCommunitiesIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var localCommunityList = new List<LocalCommunity>
            {
                new LocalCommunity {Id = 1, IsActive = valueWeWant},
                new LocalCommunity {Id = 2, IsActive = !valueWeWant},
                new LocalCommunity {Id = 3, IsActive = !valueWeWant},
                new LocalCommunity {Id = 4, IsActive = valueWeWant},
                new LocalCommunity {Id = 5, IsActive = valueWeWant},
                new LocalCommunity {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(localCommunityList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _localCommunityController.WithCallTo(c => c.LocalCommunity_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexLocalCommunityViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
