﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Group;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class GroupControllerIntegrationTests
    {
        private readonly IGenericRepository<Group> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly GroupController _groupController;

        public GroupControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Group>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _groupController = new GroupController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        [Fact]
        public void Group_Read_GetPage1PageSize10_GetsPageOfGroups()
        {
            // Arrange
            var groupPhraseList = new List<Group>();
            for (int i = 0; i < 25; ++i)
            {
                groupPhraseList.Add(new Group());
            }
            _repo.AsQueryable().Returns(groupPhraseList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _groupController.WithCallTo(c => c.Group_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexGroupViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Group_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfGroups()
        {
            // Arrange
            var groupPhraseList = new List<Group>();
            for (int i = 0; i < 25; ++i)
            {
                groupPhraseList.Add(new Group() { Id = i });
            }
            _repo.AsQueryable().Returns(groupPhraseList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _groupController.WithCallTo(c => c.Group_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexGroupViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void Group_Read_GetGroupsEqualToName_GetsOnlyGroupsWithExactName()
        {
            // Arrange
            var nameWeWant = "Group#101";
            var entrepreneurList = new List<Group>{
                new Group {Id = 1, Name = "Not the correct group"},
                new Group {Id = 2, Name = nameWeWant + " a suffix"},
                new Group {Id = 3, Name ="a prefix" + nameWeWant},
                new Group {Id = 4, Name ="Not the right one either"},
                new Group {Id = 5, Name = nameWeWant},
                new Group {Id = 6, Name = "Nopepe"}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _groupController.WithCallTo(c => c.Group_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexGroupViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void Group_Read_GetGroupsContainingPartOfName_GetsGroupsWithNameContainingFilterString()
        {
            // Arrange
            var nameWeWant = "Group#101";
            var entrepreneurList = new List<Group>{
                new Group {Id = 1, Name = "Not the correct group"},
                new Group {Id = 2, Name = nameWeWant + " a suffix"},
                new Group {Id = 3, Name ="a prefix" + nameWeWant},
                new Group {Id = 4, Name ="Not the right one either"},
                new Group {Id = 5, Name = nameWeWant},
                new Group {Id = 6, Name = "Nopepe"}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _groupController.WithCallTo(c => c.Group_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexGroupViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Group_Read_GetGroupsIsActiveTrue_GetsGroupsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var groupList = new List<Group>
            {
                new Group {Id = 1, IsActive = !valueWeWant},
                new Group {Id = 2, IsActive = valueWeWant},
                new Group {Id = 3, IsActive = valueWeWant},
                new Group {Id = 4, IsActive = !valueWeWant},
                new Group {Id = 5, IsActive = !valueWeWant},
                new Group {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(groupList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _groupController.WithCallTo(c => c.Group_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexGroupViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Group_Read_GetGroupsIsActiveFalse_GetsGroupsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var groupList = new List<Group>
            {
                new Group {Id = 1, IsActive = valueWeWant},
                new Group {Id = 2, IsActive = !valueWeWant},
                new Group {Id = 3, IsActive = !valueWeWant},
                new Group {Id = 4, IsActive = valueWeWant},
                new Group {Id = 5, IsActive = valueWeWant},
                new Group {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(groupList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _groupController.WithCallTo(c => c.Group_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexGroupViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
