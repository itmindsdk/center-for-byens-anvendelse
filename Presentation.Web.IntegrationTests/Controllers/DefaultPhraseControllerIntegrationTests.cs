﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.DefaultPhrase;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class DefaultPhraseControllerIntegrationTests
    {
        private readonly IGenericRepository<DefaultPhrase> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly DefaultPhraseController _defaultPhraseController;

        public DefaultPhraseControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<DefaultPhrase>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _defaultPhraseController = new DefaultPhraseController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region ConstructionProgram_read
        [Fact]
        public void DefaultPhrase_Read_GetPage1PageSize10_GetsPageOfDefaultPhrase()
        {
            // Arrange
            var defaultPhraseList = new List<DefaultPhrase>();
            for (int i = 0; i < 25; ++i)
            {
                defaultPhraseList.Add(new DefaultPhrase());
            }
            _repo.AsQueryable().Returns(defaultPhraseList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void DefaultPhrase_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfDefaultPhrase()
        {
            // Arrange
            var companyList = new List<DefaultPhrase>();
            for (int i = 0; i < 25; ++i)
            {
                companyList.Add(new DefaultPhrase { Id = i });
            }
            _repo.AsQueryable().Returns(companyList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void DefaultPhrase_Read_GetDefaultPhrasesEqualToPhrase_GetsOnlyDefaultPhrasesWithExactPhrase()
        {
            // Arrange
            var phraseWeWant = "A default phrase, it has to be exactly like this one";
            var defaultPhraseList = new List<DefaultPhrase>{
                new DefaultPhrase {Id = 1, Phrase = "No"},
                new DefaultPhrase {Id = 2, Phrase = phraseWeWant + " with a suffix"},
                new DefaultPhrase {Id = 3, Phrase = "With a prefix " + phraseWeWant},
                new DefaultPhrase {Id = 4, Phrase = "Not the correct name at all"},
                new DefaultPhrase {Id = 5, Phrase = phraseWeWant},
                new DefaultPhrase {Id = 6, Phrase = "Another"}
            };
            _repo.AsQueryable().Returns(defaultPhraseList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Phrase", Operator = FilterOperator.IsEqualTo, Value = phraseWeWant });

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Phrase == phraseWeWant
                    );
            });
        }

        [Fact]
        public void DefaultPhrase_Read_GetDefaultPhrasesContainingPartOfPhrase_GetsDefaultPhrasesWithPhraseContainingFilterString()
        {
            // Arrange
            var phraseWeWant = "A default phrase, can differ from this one, but it has to contain this";
            var defaultPhraseList = new List<DefaultPhrase>{
                new DefaultPhrase {Id = 1, Phrase = "No"},
                new DefaultPhrase {Id = 2, Phrase = phraseWeWant + " with a suffix"},
                new DefaultPhrase {Id = 3, Phrase = "With a prefix " + phraseWeWant},
                new DefaultPhrase {Id = 4, Phrase = "Not the correct name at all"},
                new DefaultPhrase {Id = 5, Phrase = phraseWeWant},
                new DefaultPhrase {Id = 6, Phrase = "Another"}
            };

            _repo.AsQueryable().Returns(defaultPhraseList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Phrase", Operator = FilterOperator.Contains, Value = phraseWeWant });

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void DefaultPhrase_Read_GetDefaultPhrasesEqualToType_GetsDefaultPhrasesOfTypeInFilter()
        {
            // Arrange
            var typeWeWant = DefaultPhraseType.Agreement;
            var typeWeDontWant = DefaultPhraseType.Conclusion;
            var typeWeWantVM = DefaultPhraseTypeViewModel.Agreement;
            var companyList = new List<DefaultPhrase>{
                new DefaultPhrase {Id = 1, Type = typeWeDontWant},
                new DefaultPhrase {Id = 2, Type = typeWeDontWant},
                new DefaultPhrase {Id = 3, Type = typeWeWant},
                new DefaultPhrase {Id = 4, Type = typeWeDontWant},
                new DefaultPhrase {Id = 5, Type = typeWeWant},
                new DefaultPhrase {Id = 6, Type = typeWeDontWant}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Type", Operator = FilterOperator.IsEqualTo, Value = typeWeWantVM });

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().Equal(
                        new[] { 3, 5 },
                        (c1, c2) => c1.Id == c2 && c1.Type == typeWeWantVM
                    );
            });
        }

        [Fact]
        public void DefaultPhrase_Read_GetDefaultPhrasesIsActiveTrue_GetsDefaultPhrasesIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var defaultPhraseList = new List<DefaultPhrase>
            {
                new DefaultPhrase {Id = 1, IsActive = !valueWeWant},
                new DefaultPhrase {Id = 2, IsActive = valueWeWant},
                new DefaultPhrase {Id = 3, IsActive = valueWeWant},
                new DefaultPhrase {Id = 4, IsActive = !valueWeWant},
                new DefaultPhrase {Id = 5, IsActive = !valueWeWant},
                new DefaultPhrase {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(defaultPhraseList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void DefaultPhrase_Read_GetDefaultPhrasesIsActiveFalse_GetsDefaultPhrasesIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var defaultPhraseList = new List<DefaultPhrase>{
                new DefaultPhrase {Id = 1, IsActive = valueWeWant},
                new DefaultPhrase {Id = 2, IsActive = !valueWeWant},
                new DefaultPhrase {Id = 3, IsActive = !valueWeWant},
                new DefaultPhrase {Id = 4, IsActive = valueWeWant},
                new DefaultPhrase {Id = 5, IsActive = valueWeWant},
                new DefaultPhrase {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(defaultPhraseList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _defaultPhraseController.WithCallTo(c => c.DefaultPhrase_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexDefaultPhraseViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        #endregion
    }
}
