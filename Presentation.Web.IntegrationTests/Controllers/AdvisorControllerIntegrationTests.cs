﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Advisor;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class AdvisorControllerIntegrationTests
    {
        private readonly AdvisorController _advisorController;
        private readonly IGenericRepository<Advisor> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;

        public AdvisorControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Advisor>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _advisorController = new AdvisorController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region Advisor_Read
        [Fact]
        public void Advisor_Read_GetPage1PageSize10_GetsPageOfAdvisors()
        {
            // Arrange
            var advisorList = new List<Advisor>();
            for(int i = 0; i < 25; ++i) { advisorList.Add(new Advisor()); }
            _repo.AsQueryable().Returns(advisorList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest {Page = 1, PageSize = 10};

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexAdvisorViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Advisor_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfAdvisors()
        {
            // Arrange
            var advisorList = new List<Advisor>();
            for (var i = 0; i < 25; ++i)
            {
                advisorList.Add(new Advisor{ Id = i });
            }
            _repo.AsQueryable().Returns(advisorList.AsQueryable());
            var request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAdvisorViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1,c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void Advisor_Read_GetAdvisorsEqualToCompany_GetsOnlyAdvisorsWithExactCompany()
        {
            // Arrange
            var companyNameWeWant = "Acme";
            var companyNameWeDontWant = "Emca";
            var advisorList = new List<Advisor>{
                new Advisor {Id = 1, Company = new Company {Name = companyNameWeWant}},
                new Advisor {Id = 2, Company = new Company {Name = companyNameWeWant + "Almost equal"}},
                new Advisor {Id = 3, Company = new Company {Name = companyNameWeWant}},
                new Advisor {Id = 4, Company = new Company {Name = companyNameWeDontWant}},
                new Advisor {Id = 5, Company = new Company {Name = companyNameWeDontWant}},
                new Advisor {Id = 6, Company = new Company {Name = companyNameWeWant}}
            };
            _repo.AsQueryable().Returns(advisorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest{Filters = new List<IFilterDescriptor>()};
            request.Filters.Add(new FilterDescriptor { Member = "Company.Name", Operator = FilterOperator.IsEqualTo, Value = companyNameWeWant });

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAdvisorViewModel>).Should().Equal(
                    new []{ 1, 3, 6 },
                    (c1,c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Advisor_Read_GetAdvisorsContainingPartOfCompany_GetsAdvisorsWithCompanyContainingFilterString()
        {
            // Arrange
            var companyNameWeWant = "Acme";
            var companyNameWeDontWant = "Emca";
            var advisorList = new List<Advisor>{
                new Advisor {Id = 1, Company = new Company {Name = companyNameWeWant + " LLC"}},
                new Advisor {Id = 2, Company = new Company {Name = companyNameWeDontWant}},
                new Advisor {Id = 3, Company = new Company {Name = "Super " + companyNameWeWant}},
                new Advisor {Id = 4, Company = new Company {Name = companyNameWeDontWant}},
                new Advisor {Id = 5, Company = new Company {Name = companyNameWeDontWant}},
                new Advisor {Id = 6, Company = new Company {Name = "An arbitrary prefix" + companyNameWeWant + " Not quite the same"}}
            };
            _repo.AsQueryable().Returns(advisorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Company.Name", Operator = FilterOperator.Contains, Value = companyNameWeWant });

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAdvisorViewModel>).Should().Equal(
                    new[] { 1, 3, 6 },
                    (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Advisor_Read_GetOnlyOneAdvisorEqualToName_GetsOnlyOneAdvisorsWithExactName()
        {
            // Arrange
            var nameWeWant = "navn5";
            var advisorList = new List<Advisor>{
                new Advisor {Id = 1, ResponsiblePersonName = "n@vn1"},
                new Advisor {Id = 2, ResponsiblePersonName = "navn2"},
                new Advisor {Id = 3, ResponsiblePersonName = "n4vn3"},
                new Advisor {Id = 4, ResponsiblePersonName = "na\\/n4"},
                new Advisor {Id = 5, ResponsiblePersonName = nameWeWant},
                new Advisor {Id = 6, ResponsiblePersonName = "nawn6"}
            };
            _repo.AsQueryable().Returns(advisorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonName", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAdvisorViewModel>).Should().Equal(
                    new[] { 5 },
                    (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Advisor_Read_GetOnlyOneAdvisorEqualToPhoneNumber_GetsOnlyOneAdvisorsWithExactPhoneNumber()
        {
            // Arrange
            var phoneNumberWeWant = "86889092";
            var advisorList = new List<Advisor>{
                new Advisor {Id = 1, ResponsiblePersonPhoneNumber = "10101010"},
                new Advisor {Id = 2, ResponsiblePersonPhoneNumber = "08385671"},
                new Advisor {Id = 3, ResponsiblePersonPhoneNumber = phoneNumberWeWant},
                new Advisor {Id = 4, ResponsiblePersonPhoneNumber = "60720971"},
                new Advisor {Id = 5, ResponsiblePersonPhoneNumber = "+45" + phoneNumberWeWant}, // shouldn't get this one
                new Advisor {Id = 6}
            };
            _repo.AsQueryable().Returns(advisorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonPhoneNumber", Operator = FilterOperator.IsEqualTo, Value = phoneNumberWeWant });

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAdvisorViewModel>).Should().Equal(
                    new[] { 3 },
                    (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Advisor_Read_GetAdvisorsContainingPartOfPhonenumber_GetsOnlyAdvisorsWithPhonenumberContainingFilterString()
        {
            // Arrange
            var phoneNumberWeWant = "678493";
            var advisorList = new List<Advisor>{
                new Advisor {Id = 1, ResponsiblePersonPhoneNumber = "10101010"},
                new Advisor {Id = 2, ResponsiblePersonPhoneNumber = "08385671"},
                new Advisor {Id = 3, ResponsiblePersonPhoneNumber = "99" + phoneNumberWeWant},
                new Advisor {Id = 4, ResponsiblePersonPhoneNumber = "60720971"},
                new Advisor {Id = 5, ResponsiblePersonPhoneNumber = "+458" + phoneNumberWeWant + "1"},
                new Advisor {Id = 6, ResponsiblePersonPhoneNumber = phoneNumberWeWant.Substring(1) + "120"} // Shouldn't get this one even if it is very similar
            };
            _repo.AsQueryable().Returns(advisorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonPhoneNumber", Operator = FilterOperator.Contains, Value = phoneNumberWeWant });

            // Act
            _advisorController.WithCallTo(c => c.Advisor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAdvisorViewModel>).Should().Equal(
                    new[] { 3, 5 },
                    (c1, c2) => c1.Id == c2
                    );
            });
        }
        #endregion
    }
}
