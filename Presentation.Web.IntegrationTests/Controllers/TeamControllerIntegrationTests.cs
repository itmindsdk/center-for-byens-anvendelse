﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.NextMeeting;
using Presentation.Web.Models.Team;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class TeamControllerIntegrationTests
    {
        private readonly IGenericRepository<Team> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;

        private readonly TeamController _teamController;

        public TeamControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Team>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _teamController = new TeamController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        [Fact]
        public void Team_Read_GetPage1PageSize10_GetsPageOfTeams()
        {
            // Arrange
            var teamList = new List<Team>();
            for (int i = 0; i < 25; ++i)
            {
                teamList.Add(new Team());
            }
            _repo.AsQueryable().Returns(teamList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _teamController.WithCallTo(c => c.Team_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexTeamViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Team_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfTeams()
        {
            // Arrange
            var teamList = new List<Team>();
            for (int i = 0; i < 25; ++i)
            {
                teamList.Add(new Team { Id = i });
            }
            _repo.AsQueryable().Returns(teamList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _teamController.WithCallTo(c => c.Team_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTeamViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void Team_Read_GetTeamsEqualToName_GetsOnlyTeamsWithExactName()
        {
            // Arrange
            var NameWeWant = "Name";
            var teamList = new List<Team>{
                new Team {Id = 1, Name = "!Name"},
                new Team {Id = 2, Name = NameWeWant + " with a suffix"},
                new Team {Id = 3, Name = "with a prefix" + NameWeWant},
                new Team {Id = 4, Name = "Nam"},
                new Team {Id = 5, Name = NameWeWant},
                new Team {Id = 6, Name = "ame"}
            };

            _repo.AsQueryable().Returns(teamList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = NameWeWant });

            // Act
            _teamController.WithCallTo(c => c.Team_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTeamViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == NameWeWant
                    );
            });
        }

        [Fact]
        public void Team_Read_GetTeamsContainingPartOfName_GetsTeamsWithNameContainingFilterString()
        {
            // Arrange
            var NameWeWant = "Name";
            var teamList = new List<Team>{
                new Team {Id = 1, Name = "wat"},
                new Team {Id = 2, Name = NameWeWant + " with a suffix"},
                new Team {Id = 3, Name = "with a prefix" + NameWeWant},
                new Team {Id = 4, Name = "Nam"},
                new Team {Id = 5, Name = NameWeWant},
                new Team {Id = 6, Name = "ame"}
            };

            _repo.AsQueryable().Returns(teamList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = NameWeWant });

            // Act
            _teamController.WithCallTo(c => c.Team_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTeamViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }


        [Fact]
        public void Team_Read_GetTeamsIsActiveTrue_GetsTeamsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var teamList = new List<Team>
            {
                new Team {Id = 1, IsActive = !valueWeWant},
                new Team {Id = 2, IsActive = valueWeWant},
                new Team {Id = 3, IsActive = valueWeWant},
                new Team {Id = 4, IsActive = !valueWeWant},
                new Team {Id = 5, IsActive = !valueWeWant},
                new Team {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(teamList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _teamController.WithCallTo(c => c.Team_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTeamViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Team_Read_GetTeamsIsActiveFalse_GetsTeamsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var teamList = new List<Team>{
                new Team {Id = 1, IsActive = valueWeWant},
                new Team {Id = 2, IsActive = !valueWeWant},
                new Team {Id = 3, IsActive = !valueWeWant},
                new Team {Id = 4, IsActive = valueWeWant},
                new Team {Id = 5, IsActive = valueWeWant},
                new Team {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(teamList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _teamController.WithCallTo(c => c.Team_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTeamViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
