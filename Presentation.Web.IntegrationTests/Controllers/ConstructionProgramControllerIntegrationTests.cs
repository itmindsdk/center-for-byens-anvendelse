﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ConstructionProgram;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class ConstructionProgramControllerIntegrationTests
    {
        private readonly IGenericRepository<ConstructionProgram> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAdHelper _adHelper;
        private readonly IMapper _mapper;
        private readonly ConstructionProgramController _constructionProgramController;

        public ConstructionProgramControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<ConstructionProgram>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _constructionProgramController = new ConstructionProgramController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        #region ConstructionProgram_read
        [Fact]
        public void ConstructionProgram_Read_GetPage1PageSize10_GetsPageOfConstructionPrograms()
        {
            // Arrange
            var companyList = new List<ConstructionProgram>();
            for (int i = 0; i < 25; ++i)
            {
                companyList.Add(new ConstructionProgram());
            }
            _repo.AsQueryable().Returns(companyList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void ConstructionProgram_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfConstructionPrograms()
        {
            // Arrange
            var companyList = new List<ConstructionProgram>();
            for (int i = 0; i < 25; ++i)
            {
                companyList.Add(new ConstructionProgram { Id = i });
            }
            _repo.AsQueryable().Returns(companyList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void ConstructionProgram_Read_GetConstructionProgramsBeforePoint_GetsConstructionProgramsFromBeforeFilteredNumber()
        {
            // Arrange
            var valuesBeforeThisYear = 2017;
            var companyList = new List<ConstructionProgram>
            {
                new ConstructionProgram {Id = 1, YearOfProjectInvolvement = -1},
                new ConstructionProgram {Id = 2, YearOfProjectInvolvement = 0},
                new ConstructionProgram {Id = 3, YearOfProjectInvolvement = 1},
                new ConstructionProgram {Id = 4, YearOfProjectInvolvement = 2016},
                new ConstructionProgram {Id = 5, YearOfProjectInvolvement = 2017},
                new ConstructionProgram {Id = 6, YearOfProjectInvolvement = 2050},
                new ConstructionProgram {Id = 7, YearOfProjectInvolvement = 1900},
                new ConstructionProgram {Id = 8, YearOfProjectInvolvement = 9999}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "YearOfProjectInvolvement", Operator = FilterOperator.IsLessThan, Value = valuesBeforeThisYear });

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().Equal(
                        new[] { 1, 2, 3, 4, 7 },
                        (c1, c2) => c1.Id == c2 && c1.YearOfProjectInvolvement < valuesBeforeThisYear
                    );
            });
        }

        [Fact]
        public void ConstructionProgram_Read_GetConstructionProgramsAfterPoint_GetsConstructionProgramsAfterFilteredNumber()
        {
            // Arrange
            var valuesAfterThisYear = 2017;
            var companyList = new List<ConstructionProgram>
            {
                new ConstructionProgram {Id = 1, YearOfProjectInvolvement = -1},
                new ConstructionProgram {Id = 2, YearOfProjectInvolvement = 0},
                new ConstructionProgram {Id = 3, YearOfProjectInvolvement = 1},
                new ConstructionProgram {Id = 4, YearOfProjectInvolvement = 2016},
                new ConstructionProgram {Id = 5, YearOfProjectInvolvement = 2017},
                new ConstructionProgram {Id = 6, YearOfProjectInvolvement = 2050},
                new ConstructionProgram {Id = 7, YearOfProjectInvolvement = 1900},
                new ConstructionProgram {Id = 8, YearOfProjectInvolvement = 9999}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "YearOfProjectInvolvement", Operator = FilterOperator.IsGreaterThan, Value = valuesAfterThisYear });

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().Equal(
                        new[] { 6, 8 },
                        (c1, c2) => c1.Id == c2 && c1.YearOfProjectInvolvement > valuesAfterThisYear
                    );
            });
        }
        [Fact]
        public void ConstructionProgram_Read_GetConstructionProgramsEqualToNumber_GetsConstructionProgramsEqualToFilteredNumber()
        {
            // Arrange
            var valueWeWant = 2017;
            var companyList = new List<ConstructionProgram>
            {
                new ConstructionProgram {Id = 1, YearOfProjectInvolvement = -1},
                new ConstructionProgram {Id = 2, YearOfProjectInvolvement = 0},
                new ConstructionProgram {Id = 3, YearOfProjectInvolvement = 1},
                new ConstructionProgram {Id = 4, YearOfProjectInvolvement = 2016},
                new ConstructionProgram {Id = 5, YearOfProjectInvolvement = 2017},
                new ConstructionProgram {Id = 6, YearOfProjectInvolvement = 2050},
                new ConstructionProgram {Id = 7, YearOfProjectInvolvement = 1900},
                new ConstructionProgram {Id = 8, YearOfProjectInvolvement = 9999}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "YearOfProjectInvolvement", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.YearOfProjectInvolvement == valueWeWant
                    );
            });
        }

        [Fact]
        public void ConstructionProgram_Read_GetConstructionProgramsIsActiveTrue_GetsConstructionProgramsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var constructionProgramList = new List<ConstructionProgram>{
                new ConstructionProgram {Id = 1, IsActive = !valueWeWant},
                new ConstructionProgram {Id = 2, IsActive = valueWeWant},
                new ConstructionProgram {Id = 3, IsActive = valueWeWant},
                new ConstructionProgram {Id = 4, IsActive = !valueWeWant},
                new ConstructionProgram {Id = 5, IsActive = !valueWeWant},
                new ConstructionProgram {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(constructionProgramList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void ConstructionProgram_Read_GetConstructionProgramsIsActiveFalse_GetsConstructionProgramsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var constructionProgramList = new List<ConstructionProgram>{
                new ConstructionProgram {Id = 1, IsActive = valueWeWant},
                new ConstructionProgram {Id = 2, IsActive = !valueWeWant},
                new ConstructionProgram {Id = 3, IsActive = !valueWeWant},
                new ConstructionProgram {Id = 4, IsActive = valueWeWant},
                new ConstructionProgram {Id = 5, IsActive = valueWeWant},
                new ConstructionProgram {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(constructionProgramList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _constructionProgramController.WithCallTo(c => c.ConstructionProgram_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexConstructionProgramViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
        #endregion

    }
}
