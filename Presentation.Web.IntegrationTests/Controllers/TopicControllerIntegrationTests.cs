﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.NextMeeting;
using Presentation.Web.Models.Topic;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class TopicControllerIntegrationTests
    {
        private readonly IGenericRepository<Topic> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly TopicController _topicController;

        public TopicControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Topic>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _topicController = new TopicController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        [Fact]
        public void Topic_Read_GetPage1PageSize10_GetsPageOfTopics()
        {
            // Arrange
            var topicList = new List<Topic>();
            for (int i = 0; i < 25; ++i)
            {
                topicList.Add(new Topic());
            }
            _repo.AsQueryable().Returns(topicList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _topicController.WithCallTo(c => c.Topic_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexTopicViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Topic_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfTopics()
        {
            // Arrange
            var topicList = new List<Topic>();
            for (int i = 0; i < 25; ++i)
            {
                topicList.Add(new Topic { Id = i });
            }
            _repo.AsQueryable().Returns(topicList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _topicController.WithCallTo(c => c.Topic_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTopicViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void Topic_Read_GetTopicsEqualToName_GetsOnlyTopicsWithExactName()
        {
            // Arrange
            var NameWeWant = "Topic";
            var topicList = new List<Topic>{
                new Topic {Id = 1, Name = "!Topic"},
                new Topic {Id = 2, Name = NameWeWant + " with a suffix"},
                new Topic {Id = 3, Name = "with a prefix" + NameWeWant},
                new Topic {Id = 4, Name = "Topi"},
                new Topic {Id = 5, Name = NameWeWant},
                new Topic {Id = 6, Name = "opic"}
            };

            _repo.AsQueryable().Returns(topicList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = NameWeWant });

            // Act
            _topicController.WithCallTo(c => c.Topic_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTopicViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == NameWeWant
                    );
            });
        }

        [Fact]
        public void Topic_Read_GetTopicsContainingPartOfName_GetsTopicsWithNameContainingFilterString()
        {
            // Arrange
            var NameWeWant = "Topic";
            var topicList = new List<Topic>{
                new Topic {Id = 1, Name = "opi"},
                new Topic {Id = 2, Name = NameWeWant + " with a suffix"},
                new Topic {Id = 3, Name = "with a prefix" + NameWeWant},
                new Topic {Id = 4, Name = "Topi"},
                new Topic {Id = 5, Name = NameWeWant},
                new Topic {Id = 6, Name = "opic"}
            };

            _repo.AsQueryable().Returns(topicList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = NameWeWant });

            // Act
            _topicController.WithCallTo(c => c.Topic_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTopicViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }


        [Fact]
        public void Topic_Read_GetTopicsIsActiveTrue_GetsTopicsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var topicList = new List<Topic>
            {
                new Topic {Id = 1, IsActive = !valueWeWant},
                new Topic {Id = 2, IsActive = valueWeWant},
                new Topic {Id = 3, IsActive = valueWeWant},
                new Topic {Id = 4, IsActive = !valueWeWant},
                new Topic {Id = 5, IsActive = !valueWeWant},
                new Topic {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(topicList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _topicController.WithCallTo(c => c.Topic_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTopicViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Topic_Read_GetTopicsIsActiveFalse_GetsTopicsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var topicList = new List<Topic>{
                new Topic {Id = 1, IsActive = valueWeWant},
                new Topic {Id = 2, IsActive = !valueWeWant},
                new Topic {Id = 3, IsActive = !valueWeWant},
                new Topic {Id = 4, IsActive = valueWeWant},
                new Topic {Id = 5, IsActive = valueWeWant},
                new Topic {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(topicList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _topicController.WithCallTo(c => c.Topic_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexTopicViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
