﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Project;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class ProjectControllerIntegrationTests
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IMapper _mapper;
        private readonly IGenericRepository<ProjectDocument> _documentRepository;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IDocumentHelper _documentHelper;
        private readonly IDateTime _dateTime;
        private readonly IGeoCoder _geoCoder;
        private readonly IDawaRepository _dawaRepo;
        private readonly IAdHelper _adHelper;
        private readonly ProjectController _projectController;

        public ProjectControllerIntegrationTests()
        {
            _projectRepository = Substitute.For<IGenericRepository<Project>>();
            _documentRepository = Substitute.For<IGenericRepository<ProjectDocument>>();
            _topicRepo = Substitute.For<IGenericRepository<Topic>>();
            _dawaRepo = Substitute.For<IDawaRepository>();
            _documentHelper = Substitute.For<IDocumentHelper>();
            _dateTime = Substitute.For<IDateTime>();
            _geoCoder = Substitute.For<IGeoCoder>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _projectController = new ProjectController(
                _unitOfWork,
                _projectRepository,
                _mapper,
                _documentRepository,
                _topicRepo,
                _documentHelper,
                _dateTime,
                _geoCoder,
                _dawaRepo,
                _adHelper
                );
        }

        #region TestHelpers
        private class ProjectableProjectParams
        {
            public int? Id { get; set; }
            public int? TopicId { get; set; }
            public Topic Topic { get; set; }
            public DateTime? ExpectedStartDate { get; set; }
            public DateTime? ExpectedEndDate { get; set; }
            public DateTime? OneYearReview { get; set; }
            public DateTime? FiveYearReview { get; set; }
            public string EDoc { get; set; }
            public string Description { get; set; }
            public string Note { get; set; }
            public int? AdvisorId { get; set; }
            public Advisor Advisor { get; set; }
            public int? StreetTypeId { get; set; }
            public StreetType StreetType { get; set; }
            public ProjectLeader ProjectLeader { get; set; }
            public int? ProjectLeaderId { get; set; }
            public string CouncilName { get; set; }
            public int? ProjectCategoryId { get; set; }
            public ProjectCategory ProjectCategory { get; set; }
            public DbGeometry AddressLocation { get; set; }
            public int? AddressLocationSRID { get; set; }
            public string AddressStreetName { get; set; }
            public string AddressNumber { get; set; }
            public string AddressZipCode { get; set; }
            public string AddressCity { get; set; }
            public bool? TemporaryAddress { get; set; }
            public string TemporaryAddressName { get; set; }
            public int? LocalCommunityId { get; set; }
            public LocalCommunity LocalCommunity { get; set; }
            public int? Budget { get; set; }
            public int? ConstructionProgramId { get; set; }
            public ConstructionProgram ConstructionProgram { get; set; }
            public int? CouncilId { get; set; }
            public Council Council { get; set; }
            public ICollection<ProjectDocument> Documents { get; set; }
            public int? CouncilPriorityId { get; set; }
            public CouncilPriority CouncilPriority { get; set; }
            public int? MunicipalityPriorityId { get; set; }
            public MunicipalityPriority MunicipalityPriority { get; set; }
            public int? ProjectStatusId { get; set; }
            public ProjectStatus ProjectStatus { get; set; }
            public int? PlanningId { get; set; }
            public Planning Planning { get; set; }
            public int? EntrepreneurId { get; set; }
            public Entrepreneur Entrepreneur { get; set; }
            public string Log { get; set; }
            public int? RequestorId { get; set; }
            public Requestor Requestor { get; set; }
            public DateTime? CreatedOn { get; set; }
            public string CreatedBy { get; set; }
            public DateTime? ModifiedOn { get; set; }
            public string ModifiedBy { get; set; }
        }

        private Project ConstructProjectableProject(ProjectableProjectParams p = null)
        {
            p = p ?? new ProjectableProjectParams();
            return new Project
            {
                Id = p.Id ?? default(int),
                LocalCommunity = p.LocalCommunity ?? new LocalCommunity(),
                ProjectLeader = p.ProjectLeader ?? new ProjectLeader {Team = new Team() },
                ConstructionProgram = p.ConstructionProgram,
                Topic = p.Topic ?? new Topic(),
                Entrepreneur = p.Entrepreneur,
                ModifiedOn = p.ModifiedOn,
                StreetType = p.StreetType ?? new StreetType(),
                TemporaryAddress = p.TemporaryAddress ?? default(bool),
                Documents = p.Documents ?? new List<ProjectDocument>(),
                CreatedOn = p.CreatedOn ?? default(DateTime),
                AddressStreetName = p.AddressStreetName,
                AddressCity = p.AddressCity,
                AddressZipCode = p.AddressZipCode,
                AddressLocation = p.AddressLocation,
                AddressNumber = p.AddressNumber,
                StreetTypeId = p.StreetTypeId,
                Council = p.Council,
                Requestor = p.Requestor,
                ProjectCategory = p.ProjectCategory,
                ProjectLeaderId = p.ProjectLeaderId,
                ProjectStatus = p.ProjectStatus,
                TopicId = p.TopicId,
                TemporaryAddressName = p.TemporaryAddressName,
                Description = p.Description,
                ModifiedBy = p.ModifiedBy,
                CreatedBy = p.CreatedBy,
                OneYearReview = p.OneYearReview ?? new DateTime(),
                Advisor = p.Advisor ?? new Advisor { Company = new Company () },
                ExpectedEndDate = p.ExpectedEndDate ?? new DateTime(),
                Planning = p.Planning,
                CouncilPriority = p.CouncilPriority ?? new CouncilPriority { },
                ExpectedStartDate = p.ExpectedStartDate ?? new DateTime(),
                Budget = p.Budget ?? default(int),
                MunicipalityPriority = p.MunicipalityPriority ?? new MunicipalityPriority { },
                AddressLocationSRID = p.AddressLocationSRID ?? default(int),
                AdvisorId = p.AdvisorId,
                ConstructionProgramId = p.ConstructionProgramId,
                CouncilId = p.CouncilId,
                CouncilName = p.CouncilName,
                CouncilPriorityId = p.CouncilPriorityId,
                EDoc = p.EDoc,
                EntrepreneurId = p.EntrepreneurId,
                FiveYearReview = p.FiveYearReview ?? new DateTime(),
                LocalCommunityId = p.LocalCommunityId,
                Log = p.Log,
                MunicipalityPriorityId = p.MunicipalityPriorityId,
                Note = p.Note,
                PlanningId = p.PlanningId ?? default(int),
                ProjectCategoryId = p.ProjectCategoryId,
                ProjectStatusId = p.ProjectStatusId ?? default(int),
                RequestorId = p.RequestorId
            };
        }
        #endregion
        #region Project_Read

        [Fact]
        public void Project_Read_GetPage1PageSize10_GetsPageOfProjects()
        {
            // Arrange
            var projectList = new List<Project>();
            for (int i = 0; i < 25; ++i)
            {
                projectList.Add(ConstructProjectableProject());
            }
            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };
            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexProjectViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Project_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfProjects()
        {
            // Arrange
            var projectList = new List<Project>();
            for (int i = 0; i < 25; ++i)
            {
                var prj = ConstructProjectableProject();
                prj.Id = i;
                projectList.Add(prj);
            }
            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };
            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void Project_Read_GetProjectLessThanId_GetsProjectsWithIdLessThanFilteredNumber()
        {
            // Arrange
            var LessThanThisValue = 3;
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 2}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = LessThanThisValue}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 5}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 10}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 100}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 10000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 99999})
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Id", Operator = FilterOperator.IsLessThan, Value = LessThanThisValue });
            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2 },
                        (c1, c2) => c1.Id == c2 && c1.Id < LessThanThisValue
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsGreaterThanId_GetsProjectsWithIdGreaterThanFilteredNumber()
        {
            // Arrange
            var GreaterThanThisValue = 3;
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 2}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = GreaterThanThisValue}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 5}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 10}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 100}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 10000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 99999})
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Id", Operator = FilterOperator.IsGreaterThan, Value = GreaterThanThisValue });
            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 5, 10, 100, 1000, 10000, 99999 },
                        (c1, c2) => c1.Id == c2 && c1.Id > GreaterThanThisValue
                    );
            });
        }
        [Fact]
        public void Project_Read_GetProjectsEqualToId_GetsProjectsWithIdEqualToFilteredNumber()
        {
            // Arrange
            var valueWeWant = 3;
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 2}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = valueWeWant}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 5}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 10}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 100}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 10000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 99999})
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Id", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { valueWeWant },
                        (c1, c2) => c1.Id == c2 && c1.Id == valueWeWant
                    );
            });
        }


        [Fact]
        public void Project_Read_GetProjectsFilterAddressNameEqualTo_GetsProjectsWithExactAddressName()
        {
            var addressToFind = "Vej 1, 0000 By";
            // Arrange
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    AddressStreetName = "Vej",
                    AddressNumber = "2",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "1000",
                    AddressCity = "By"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "yB"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    AddressStreetName = "jeV",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                })
            };
            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "AddressName", Operator = FilterOperator.IsEqualTo, Value = addressToFind });


            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                    new[] { 1 },
                    (c1, c2) => c1.Id == c2 && c1.AddressName == addressToFind);
            });
        }

        [Fact]
        public void Project_Read_GetProjectsFilterAddressNameContains_GetsProjectsWithAddressNameContainingFilterString()
        {
            var addressToFind = "Vej";
            // Arrange
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    AddressStreetName = "Vej",
                    AddressNumber = "2",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "1000",
                    AddressCity = "By"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "yB"
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    AddressStreetName = "jeV",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                })
            };
            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "AddressName", Operator = FilterOperator.Contains, Value = addressToFind });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                    new[] { 1, 2, 3, 4 },
                    (c1, c2) => c1.Id == c2 && c1.AddressName.Contains(addressToFind, StringComparison.OrdinalIgnoreCase));
            });
        }

        [Fact]
        public void Project_Read_GetProjectsContainingPartOfTopicName_GetsProjectsWithTopicNameContainingFilterString()
        {
            // Arrange
            var topicNameWeWant = "Topic";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    Topic = new Topic {Name = topicNameWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    Topic = new Topic {Name = "A prefix" +topicNameWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    Topic = new Topic {Name = topicNameWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    Topic = new Topic {Name = "opic"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    Topic = new Topic {Name = "Topi"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    Topic = new Topic {Name = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TopicName", Operator = FilterOperator.Contains, Value = topicNameWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToTopicName_GetsProjectsOfTopicNameInFilter()
        {
            // Arrange
            var topicNameWeWant = "Topic";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    Topic = new Topic {Name = topicNameWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    Topic = new Topic {Name = "A prefix" +topicNameWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    Topic = new Topic {Name = topicNameWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    Topic = new Topic {Name = "opic"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    Topic = new Topic {Name = "Topi"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    Topic = new Topic {Name = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TopicName", Operator = FilterOperator.IsEqualTo, Value = topicNameWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.TopicName == topicNameWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEarlierThanExpectedStartDate_GetsProjectsWithExpectedStartDateEarlierThanFilter()
        {
            // Arrange
            var earlierThanThisPoint = new DateTime(2000,1,1);
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1, ExpectedStartDate = DateTime.MinValue
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2, ExpectedStartDate = earlierThanThisPoint.AddDays(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3, ExpectedStartDate = earlierThanThisPoint.AddMonths(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4, ExpectedStartDate = earlierThanThisPoint.AddYears(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5, ExpectedStartDate = earlierThanThisPoint
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6, ExpectedStartDate = earlierThanThisPoint.AddDays(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 7, ExpectedStartDate = earlierThanThisPoint.AddMonths(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 8, ExpectedStartDate = earlierThanThisPoint.AddYears(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 9, ExpectedStartDate = DateTime.MaxValue
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ExpectedStartDate", Operator = FilterOperator.IsLessThan, Value = earlierThanThisPoint });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3, 4 },
                        (c1, c2) => c1.Id == c2 && c1.ExpectedStartDate < earlierThanThisPoint
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsLaterThanExpectedStartDate_GetsProjectsWithExpectedStartDateLaterThanFilter()
        {
            // Arrange
            var laterThanThisPoint = new DateTime(2000, 1, 1);
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1, ExpectedStartDate = DateTime.MinValue
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2, ExpectedStartDate = laterThanThisPoint.AddDays(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3, ExpectedStartDate = laterThanThisPoint.AddMonths(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4, ExpectedStartDate = laterThanThisPoint.AddYears(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5, ExpectedStartDate = laterThanThisPoint
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6, ExpectedStartDate = laterThanThisPoint.AddDays(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 7, ExpectedStartDate = laterThanThisPoint.AddMonths(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 8, ExpectedStartDate = laterThanThisPoint.AddYears(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 9, ExpectedStartDate = DateTime.MaxValue
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ExpectedStartDate", Operator = FilterOperator.IsGreaterThan, Value = laterThanThisPoint });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 6, 7, 8, 9 },
                        (c1, c2) => c1.Id == c2 && c1.ExpectedStartDate > laterThanThisPoint
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToExpectedStartDate_GetsProjectsWithExpectedStartDateEqualToFilter()
        {
            // Arrange
            var exactlyThisPoint = new DateTime(2000, 1, 1);
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1, ExpectedStartDate = DateTime.MinValue
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2, ExpectedStartDate = exactlyThisPoint.AddDays(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3, ExpectedStartDate = exactlyThisPoint.AddMonths(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4, ExpectedStartDate = exactlyThisPoint.AddYears(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5, ExpectedStartDate = exactlyThisPoint
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6, ExpectedStartDate = exactlyThisPoint.AddDays(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 7, ExpectedStartDate = exactlyThisPoint.AddMonths(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 8, ExpectedStartDate = exactlyThisPoint.AddYears(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 9, ExpectedStartDate = DateTime.MaxValue
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ExpectedStartDate", Operator = FilterOperator.IsEqualTo, Value = exactlyThisPoint });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.ExpectedStartDate == exactlyThisPoint
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEarlierThanExpectedEndDate_GetsProjectsWithExpectedEndDateEarlierThanFilter()
        {
            // Arrange
            var earlierThanThisPoint = new DateTime(2000, 1, 1);
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1, ExpectedEndDate = DateTime.MinValue
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2, ExpectedEndDate = earlierThanThisPoint.AddDays(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3, ExpectedEndDate = earlierThanThisPoint.AddMonths(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4, ExpectedEndDate = earlierThanThisPoint.AddYears(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5, ExpectedEndDate = earlierThanThisPoint
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6, ExpectedEndDate = earlierThanThisPoint.AddDays(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 7, ExpectedEndDate = earlierThanThisPoint.AddMonths(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 8, ExpectedEndDate = earlierThanThisPoint.AddYears(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 9, ExpectedEndDate = DateTime.MaxValue
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ExpectedEndDate", Operator = FilterOperator.IsLessThan, Value = earlierThanThisPoint });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3, 4 },
                        (c1, c2) => c1.Id == c2 && c1.ExpectedEndDate < earlierThanThisPoint
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsLaterThanExpectedEndDate_GetsProjectsWithExpectedEndDateLaterThanFilter()
        {
            // Arrange
            var laterThanThisPoint = new DateTime(2000, 1, 1);
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1, ExpectedEndDate = DateTime.MinValue
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2, ExpectedEndDate = laterThanThisPoint.AddDays(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3, ExpectedEndDate = laterThanThisPoint.AddMonths(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4, ExpectedEndDate = laterThanThisPoint.AddYears(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5, ExpectedEndDate = laterThanThisPoint
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6, ExpectedEndDate = laterThanThisPoint.AddDays(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 7, ExpectedEndDate = laterThanThisPoint.AddMonths(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 8, ExpectedEndDate = laterThanThisPoint.AddYears(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 9, ExpectedEndDate = DateTime.MaxValue
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ExpectedEndDate", Operator = FilterOperator.IsGreaterThan, Value = laterThanThisPoint });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 6, 7, 8, 9 },
                        (c1, c2) => c1.Id == c2 && c1.ExpectedEndDate > laterThanThisPoint
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToExpectedEndDate_GetsProjectsWithExpectedEndDateEqualToFilter()
        {
            // Arrange
            var exactlyThisPoint = new DateTime(2000, 1, 1);
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1, ExpectedEndDate = DateTime.MinValue
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2, ExpectedEndDate = exactlyThisPoint.AddDays(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3, ExpectedEndDate = exactlyThisPoint.AddMonths(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4, ExpectedEndDate = exactlyThisPoint.AddYears(-1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5, ExpectedEndDate = exactlyThisPoint
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6, ExpectedEndDate = exactlyThisPoint.AddDays(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 7, ExpectedEndDate = exactlyThisPoint.AddMonths(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 8, ExpectedEndDate = exactlyThisPoint.AddYears(1)
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 9, ExpectedEndDate = DateTime.MaxValue
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ExpectedEndDate", Operator = FilterOperator.IsEqualTo, Value = exactlyThisPoint });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.ExpectedEndDate == exactlyThisPoint
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsContainingPartOfStreetTypeType_GetsProjectsWithStreetTypeTypeContainingFilterString()
        {
            // Arrange
            var StreetTypeTypeWeWant = "StreetType";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    StreetType = new StreetType{Type = StreetTypeTypeWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    StreetType = new StreetType{Type = "A prefix" + StreetTypeTypeWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    StreetType = new StreetType{Type = StreetTypeTypeWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    StreetType = new StreetType{Type = "treetType"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    StreetType = new StreetType{Type = "StreetTyp"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    StreetType = new StreetType{Type = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "StreetTypeType", Operator = FilterOperator.Contains, Value = StreetTypeTypeWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToStreetTypeType_GetsProjectsOfStreetTypeTypeInFilter()
        {
            // Arrange
            var StreetTypeTypeWeWant = "StreetType";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    StreetType = new StreetType{Type = StreetTypeTypeWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    StreetType = new StreetType{Type = "A prefix" +StreetTypeTypeWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    StreetType = new StreetType{Type = StreetTypeTypeWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    StreetType = new StreetType{Type = "treetType"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    StreetType = new StreetType{Type = "StreetTyp"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    StreetType = new StreetType{Type = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "StreetTypeType", Operator = FilterOperator.IsEqualTo, Value = StreetTypeTypeWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.StreetTypeType == StreetTypeTypeWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsContainingPartOfProjectLeaderName_GetsProjectsWithProjectLeaderNameContainingFilterString()
        {
            // Arrange
            var projectLeaderNameWeWant = "ProjectLeaderName";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectLeader = new ProjectLeader { Name = projectLeaderNameWeWant + " a suffix", Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectLeader = new ProjectLeader { Name = "A prefix" + projectLeaderNameWeWant, Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectLeader = new ProjectLeader { Name = projectLeaderNameWeWant, Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectLeader = new ProjectLeader { Name = "rojectLeaderName", Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectLeader = new ProjectLeader { Name = "ProjectLeaderNam", Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectLeader = new ProjectLeader { Name = "Something completely different", Team = new Team()}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ProjectLeaderName", Operator = FilterOperator.Contains, Value = projectLeaderNameWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToProjectLeaderName_GetsProjectsOfProjectLeaderNameInFilter()
        {
            // Arrange
            var projectLeaderNameWeWant = "ProjectLeaderName";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectLeader = new ProjectLeader { Name = projectLeaderNameWeWant + " a suffix", Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectLeader = new ProjectLeader { Name = "A prefix" + projectLeaderNameWeWant, Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectLeader = new ProjectLeader { Name = projectLeaderNameWeWant, Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectLeader = new ProjectLeader { Name = "rojectLeaderName", Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectLeader = new ProjectLeader { Name = "ProjectLeaderNam", Team = new Team()}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectLeader = new ProjectLeader { Name = "Something completely different", Team = new Team()}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ProjectLeaderName", Operator = FilterOperator.IsEqualTo, Value = projectLeaderNameWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.ProjectLeaderName == projectLeaderNameWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsContainingPartOfCouncilPriority_GetsProjectsWithCouncilPriorityContainingFilterString()
        {
            // Arrange
            var CouncilPriorityWeWant = "CouncilPriority";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    CouncilPriority = new CouncilPriority { Priority = CouncilPriorityWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    CouncilPriority = new CouncilPriority { Priority = "A prefix" + CouncilPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    CouncilPriority = new CouncilPriority { Priority = CouncilPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    CouncilPriority = new CouncilPriority { Priority = "ouncilPriority"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    CouncilPriority = new CouncilPriority { Priority = "CouncilPriorit"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    CouncilPriority = new CouncilPriority { Priority = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "CouncilPriorityPriority", Operator = FilterOperator.Contains, Value = CouncilPriorityWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToCouncilPriority_GetsProjectsOfCouncilPriorityInFilter()
        {
            // Arrange
            var CouncilPriorityWeWant = "CouncilPriority";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    CouncilPriority = new CouncilPriority { Priority = CouncilPriorityWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    CouncilPriority = new CouncilPriority { Priority = "A prefix" + CouncilPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    CouncilPriority = new CouncilPriority { Priority = CouncilPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    CouncilPriority = new CouncilPriority { Priority = "ouncilPriority"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    CouncilPriority = new CouncilPriority { Priority = "CouncilPriorit"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    CouncilPriority = new CouncilPriority { Priority = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "CouncilPriorityPriority", Operator = FilterOperator.IsEqualTo, Value = CouncilPriorityWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.CouncilPriorityPriority == CouncilPriorityWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsContainingPartOfMunicipalityPriority_GetsProjectsWithMunicipalityPriorityContainingFilterString()
        {
            // Arrange
            var municipalityPriorityWeWant = "MunicipalityPriority";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    MunicipalityPriority = new MunicipalityPriority { Priority = municipalityPriorityWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "A prefix" + municipalityPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    MunicipalityPriority = new MunicipalityPriority { Priority = municipalityPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "unicipalityPriority"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "MunicipalityPriorit"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "MunicipalityPriorityPriority", Operator = FilterOperator.Contains, Value = municipalityPriorityWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToMunicipalityPriority_GetsProjectsOfMunicipalityPriorityInFilter()
        {
            // Arrange
            var municipalityPriorityWeWant = "MunicipalityPriority";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    MunicipalityPriority = new MunicipalityPriority { Priority = municipalityPriorityWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "A prefix" + municipalityPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    MunicipalityPriority = new MunicipalityPriority { Priority = municipalityPriorityWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "unicipalityPriority"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "MunicipalityPriorit"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    MunicipalityPriority = new MunicipalityPriority { Priority = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "MunicipalityPriorityPriority", Operator = FilterOperator.IsEqualTo, Value = municipalityPriorityWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.MunicipalityPriorityPriority == municipalityPriorityWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsTemporaryAddressEqualToTrue_GetsProjectsWithTemporaryAddressTrue()
        {
            // Arrange
            var valueWeWant = true;
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    TemporaryAddress = valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    TemporaryAddress = !valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    TemporaryAddress = !valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    TemporaryAddress = !valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    TemporaryAddress = valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    TemporaryAddress = !valueWeWant
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TemporaryAddress", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 5 },
                        (c1, c2) => c1.Id == c2 && c1.TemporaryAddress == valueWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsTemporaryAddressEqualToFalse_GetsProjectsWithTemporaryAddressFalse()
        {
            // Arrange
            var valueWeWant = false;
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    TemporaryAddress = valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    TemporaryAddress = !valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    TemporaryAddress = !valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    TemporaryAddress = !valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    TemporaryAddress = valueWeWant
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    TemporaryAddress = !valueWeWant
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TemporaryAddress", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 5 },
                        (c1, c2) => c1.Id == c2 && c1.TemporaryAddress == valueWeWant
                    );
            });
        }


        [Fact]
        public void Project_Read_GetProjectLessThanBudget_GetsProjectsWithBudgetLessThanFilteredNumber()
        {
            // Arrange
            var LessThanThisValue = 3;
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1, Budget = 1}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 2, Budget = 2}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 3, Budget = LessThanThisValue}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 4, Budget = 5}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 5, Budget = 10}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 6, Budget = 100}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 7, Budget = 1000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 8, Budget = 10000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 9, Budget = 99999})
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Budget", Operator = FilterOperator.IsLessThan, Value = LessThanThisValue });
            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2 },
                        (c1, c2) => c1.Id == c2 && c1.Budget < LessThanThisValue
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsGreaterThanBudget_GetsProjectsWithBudgetGreaterThanFilteredNumber()
        {
            // Arrange
            var GreaterThanThisValue = 3;
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1, Budget = 1}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 2, Budget = 2}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 3, Budget = GreaterThanThisValue}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 4, Budget = 5}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 5, Budget = 10}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 6, Budget = 100}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 7, Budget = 1000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 8, Budget = 10000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 9, Budget = 99999})
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Budget", Operator = FilterOperator.IsGreaterThan, Value = GreaterThanThisValue });
            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 4, 5, 6, 7, 8, 9},
                        (c1, c2) => c1.Id == c2 && c1.Budget > GreaterThanThisValue
                    );
            });
        }
        [Fact]
        public void Project_Read_GetProjectsEqualToBudget_GetsProjectsWithBudgetEqualToFilteredNumber()
        {
            // Arrange
            var valueWeWant = 3;
            var projectList = new List<Project>
            {
                ConstructProjectableProject(new ProjectableProjectParams{Id = 1, Budget = 1}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 2, Budget = 2}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 3, Budget = valueWeWant}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 4, Budget = 5}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 5, Budget = 10}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 6, Budget = 100}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 7, Budget = 1000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 8, Budget = 10000}),
                ConstructProjectableProject(new ProjectableProjectParams{Id = 9, Budget = 99999})
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Budget", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.Budget == valueWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsContainingPartOfProjectStatus_GetsProjectsWithProjectStatusContainingFilterString()
        {
            // Arrange
            var projectStatusWeWant = "Status";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectStatus = new ProjectStatus {Status = projectStatusWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectStatus = new ProjectStatus {Status = "A prefix" + projectStatusWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectStatus = new ProjectStatus {Status = projectStatusWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectStatus = new ProjectStatus {Status = "tatus"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectStatus = new ProjectStatus {Status = "stat"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectStatus = new ProjectStatus {Status = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ProjectStatus.Status", Operator = FilterOperator.Contains, Value = projectStatusWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectsEqualToProjectStatus_GetsProjectsOfProjectStatusInFilter()
        {
            // Arrange
            var projectStatusWeWant = "Status";
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectStatus = new ProjectStatus {Status = projectStatusWeWant + " a suffix"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectStatus = new ProjectStatus {Status = "A prefix" + projectStatusWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectStatus = new ProjectStatus {Status = projectStatusWeWant}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectStatus = new ProjectStatus {Status = "tatus"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectStatus = new ProjectStatus {Status = "stat"}
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectStatus = new ProjectStatus {Status = "Something completely different"}
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ProjectStatus.Status", Operator = FilterOperator.IsEqualTo, Value = projectStatusWeWant });

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 3 },
                        (c1, c2) => c1.Id == c2 && c1.ProjectStatus.Status == projectStatusWeWant
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectSelectAwaitingCookieTrue_GetOnlyProjectsWithStatusAwaitingOrOngoing()
        {
            // Arrange
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectStatusId = (int)ProjectStatusType.Awaiting,
                    ProjectStatus = new ProjectStatus {Id = (int)ProjectStatusType.Awaiting }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectStatusId = (int)ProjectStatusType.Ongoing,
                    ProjectStatus = new ProjectStatus {Id = (int)ProjectStatusType.Ongoing }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectStatusId = 3,
                    ProjectStatus = new ProjectStatus {Id = 3 }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectStatusId = int.MaxValue,
                    ProjectStatus = new ProjectStatus {Id = int.MaxValue }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectStatusId = int.MinValue,
                    ProjectStatus = new ProjectStatus {Id = int.MinValue }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectStatusId = -1,
                    ProjectStatus = new ProjectStatus {Id = -1 }
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { };


            var httpRequest = ControllerContextHelpers.ConstructHttpRequestWithCookies(
                new HttpCookie("select-awaiting", "true")
                );

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController, httpRequest);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1,2 },
                        (c1, c2) => c1.Id == c2 && (c1.ProjectStatus.Id == (int)ProjectStatusType.Awaiting || c1.ProjectStatus.Id == (int)ProjectStatusType.Ongoing)
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectSelectAwaitingCookieFalse_GetAllProjects()
        {
            // Arrange
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectStatusId = (int)ProjectStatusType.Awaiting,
                    ProjectStatus = new ProjectStatus {Id = (int)ProjectStatusType.Awaiting }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectStatusId = (int)ProjectStatusType.Ongoing,
                    ProjectStatus = new ProjectStatus {Id = (int)ProjectStatusType.Ongoing }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectStatusId = 3,
                    ProjectStatus = new ProjectStatus {Id = 3 }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectStatusId = int.MaxValue,
                    ProjectStatus = new ProjectStatus {Id = int.MaxValue }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectStatusId = int.MinValue,
                    ProjectStatus = new ProjectStatus {Id = int.MinValue }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectStatusId = -1,
                    ProjectStatus = new ProjectStatus {Id = -1 }
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { };


            var httpRequest = ControllerContextHelpers.ConstructHttpRequestWithCookies(
                new HttpCookie("select-awaiting", "false")
                );

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController, httpRequest);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3, 4, 5, 6 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Project_Read_GetProjectSelectAwaitingCookieNull_GetAllProjects()
        {
            // Arrange
            var projectList = new List<Project>{
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 1,
                    ProjectStatusId = (int)ProjectStatusType.Awaiting,
                    ProjectStatus = new ProjectStatus {Id = (int)ProjectStatusType.Awaiting }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 2,
                    ProjectStatusId = (int)ProjectStatusType.Ongoing,
                    ProjectStatus = new ProjectStatus {Id = (int)ProjectStatusType.Ongoing }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 3,
                    ProjectStatusId = 3,
                    ProjectStatus = new ProjectStatus {Id = 3 }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 4,
                    ProjectStatusId = int.MaxValue,
                    ProjectStatus = new ProjectStatus {Id = int.MaxValue }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 5,
                    ProjectStatusId = int.MinValue,
                    ProjectStatus = new ProjectStatus {Id = int.MinValue }
                }),
                ConstructProjectableProject(new ProjectableProjectParams
                {
                    Id = 6,
                    ProjectStatusId = -1,
                    ProjectStatus = new ProjectStatus {Id = -1 }
                })
            };

            _projectRepository.AsQueryable().Returns(projectList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { };

            _projectController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_projectController);

            // Act
            _projectController.WithCallTo(c => c.Project_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectViewModel>).Should().Equal(
                        new[] { 1, 2, 3, 4, 5, 6 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }
        #endregion

    }
}
