﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.Meeting;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class MeetingControllerIntegrationTests
    {
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<MeetingAgendaPoint> _meetingAgendaPointRepo;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDateTime _dateTime;
        private readonly IAppSettings _appSettings;
        private readonly IAdHelper _adHelper;
        private readonly MeetingController _meetingController;

        public MeetingControllerIntegrationTests()
        {
            _meetingRepo = Substitute.For<IGenericRepository<Meeting>>();
            _meetingAgendaPointRepo = Substitute.For<IGenericRepository<MeetingAgendaPoint>>();
            _agendaPointRepository = Substitute.For<IGenericRepository<AgendaPoint>>();
            _dateTime = Substitute.For<IDateTime>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _appSettings = Substitute.For<IAppSettings>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _meetingController = new MeetingController(
                _meetingRepo,
                _meetingAgendaPointRepo,
                _agendaPointRepository,
                _unitOfWork,
                _mapper,
                _dateTime,
                _appSettings,
                _adHelper
                );
        }
        [Fact]
        public void Meeting_Read_GetPage1PageSize10_GetsPageOfMeetings()
        {
            // Arrange
            var meetingList = new List<Meeting>();
            for (int i = 0; i < 25; ++i)
            {
                meetingList.Add(new Meeting());
            }
            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexMeetingViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Meeting_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfMeetings()
        {
            // Arrange
            var meetingList = new List<Meeting>();
            for (int i = 0; i < 25; ++i)
            {
                meetingList.Add(new Meeting { Id = i, MeetingAgendaPoints = new List<MeetingAgendaPoint>(), Reviewer = new Person()});
            }
            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingsLessThanId_GetsMeetingsWithIdLessThanFilteredNumber()
        {
            // Arrange
            var valuesLessthanThis = 1000;
            var meetingList = new List<Meeting>
            {
                new Meeting {Id = 1},
                new Meeting {Id = 2},
                new Meeting {Id = 5},
                new Meeting {Id = 10},
                new Meeting {Id = 100},
                new Meeting {Id = 1000},
                new Meeting {Id = 10000},
                new Meeting {Id = 99999}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Id", Operator = FilterOperator.IsLessThan, Value = valuesLessthanThis });

            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 1, 2, 5, 10, 100},
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingsGreaterThanId_GetsMeetingsWithIdLessThanFilteredNumber()
        {
            // Arrange
            var valuesGreaterThan = 1000;
            var meetingList = new List<Meeting>
            {
                new Meeting {Id = 1},
                new Meeting {Id = 2},
                new Meeting {Id = 5},
                new Meeting {Id = 10},
                new Meeting {Id = 100},
                new Meeting {Id = 1000},
                new Meeting {Id = 10000},
                new Meeting {Id = 99999}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Id", Operator = FilterOperator.IsGreaterThan, Value = valuesGreaterThan });

            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 10000, 99999 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }
        [Fact]
        public void Meeting_Read_GetMeetingsEqualToId_GetsMeetingsWithIdEqualToFilteredNumber()
        {
            // Arrange
            var valueWeWant = 1000;
            var meetingList = new List<Meeting>
            {
                new Meeting {Id = 1},
                new Meeting {Id = 2},
                new Meeting {Id = 5},
                new Meeting {Id = 10},
                new Meeting {Id = 100},
                new Meeting {Id = 1000},
                new Meeting {Id = 10000},
                new Meeting {Id = 99999}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Id", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { valueWeWant },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingsEqualToDate_GetsOnlyMeetingsWithExactDate()
        {
            // {0:dd/MM/yyyy HH:MM}
            // Arrange
            var dateWeWant = "01/01/2000";
            var meetingList = new List<Meeting>{
                new Meeting {Id = 1, Date = "31/12/1999 00:00"},
                new Meeting {Id = 2, Date = "02/01/2000 00:00"},
                new Meeting {Id = 3, Date = "01/01/2001 00:00"},
                new Meeting {Id = 4, Date = "01/01/1970 00:00"},
                new Meeting {Id = 5, Date = dateWeWant},
                new Meeting {Id = 6, Date = "19/01/2038 03:14"}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Date", Operator = FilterOperator.IsEqualTo, Value = dateWeWant });
            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Date == dateWeWant
                    );
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingsContainingPartOfDate_GetsMeetingsWithDateContainingFilterString()
        {
            // Arrange
            var dateWeWant = "2000";
            var meetingList = new List<Meeting>{
                new Meeting {Id = 1, Date = "31/12/1999 00:00"},
                new Meeting {Id = 2, Date = "02/01/2000 00:00"},
                new Meeting {Id = 3, Date = "01/01/2001 00:00"},
                new Meeting {Id = 4, Date = "01/01/1970 00:00"},
                new Meeting {Id = 5, Date = "30/06/2000 00:00"},
                new Meeting {Id = 6, Date = "19/01/2038 03:14"}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Date", Operator = FilterOperator.Contains, Value = dateWeWant });
            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 2, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingsEqualToLocation_GetsOnlyMeetingsWithExactLocation()
        {
            // Arrange
            var roadWeWant = "Lilje Vej 16";
            var meetingList = new List<Meeting>{
                new Meeting {Id = 1, Location = "Lilje Vej 17"},
                new Meeting {Id = 2, Location = "Paludan Müllers Vej 100"},
                new Meeting {Id = 3, Location = "Andre Vej Navne 83"},
                new Meeting {Id = 4, Location = "Genvej 75"},
                new Meeting {Id = 5, Location = roadWeWant},
                new Meeting {Id = 6, Location = "Lilje Vej 15"}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Location", Operator = FilterOperator.IsEqualTo, Value = roadWeWant });
            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Location == roadWeWant
                    );
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingsContainingPartOfLocation_GetsMeetingsWithLocationContainingFilterString()
        {
            // Arrange
            var roadWeWant = "Lilje Vej";
            var meetingList = new List<Meeting>{
                new Meeting {Id = 1, Location = "Lilje Vej 17"},
                new Meeting {Id = 2, Location = "Paludan Müllers Vej 100"},
                new Meeting {Id = 3, Location = "Andre Vej Navne 83"},
                new Meeting {Id = 4, Location = "Genvej 75"},
                new Meeting {Id = 5, Location = roadWeWant + "16"},
                new Meeting {Id = 6, Location = "Lilje Vej 15"}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Location", Operator = FilterOperator.Contains, Value = roadWeWant });
            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 1, 5, 6 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Meeting_Read_GetMeetingCreatedOnLessThanAYearOld_GetsMeetingsWithCreatedOnLessThanAYearOld()
        {
            // Arrange
            var dateTimeNow = new DateTime(2000, 1, 1);
            var meetingList = new List<Meeting>{
                new Meeting {Id = 1, CreatedOn = dateTimeNow.AddYears(-1)},
                new Meeting {Id = 2, CreatedOn = dateTimeNow.AddYears(-1).AddDays(1)},
                new Meeting {Id = 3, CreatedOn = dateTimeNow.AddYears(-1).AddMonths(1)},
                new Meeting {Id = 4, CreatedOn = dateTimeNow.AddYears(-1).AddDays(-1)},
                new Meeting {Id = 5, CreatedOn = dateTimeNow.AddYears(1)},
                new Meeting {Id = 6, CreatedOn = dateTimeNow}
            };

            _meetingRepo.AsQueryable().Returns(meetingList.AsQueryable());
            _dateTime.Now.Returns(dateTimeNow);
            DataSourceRequest request = new DataSourceRequest { };

            var httpRequest = ControllerContextHelpers.ConstructHttpRequestWithCookies(new HttpCookie("onlyNewPointsMeeting", "true"));
            _meetingController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_meetingController, httpRequest);

            // Act
            _meetingController.WithCallTo(c => c.Meeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexMeetingViewModel>).Should().Equal(
                        new[] { 2, 3, 5, 6 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }
    }
}
