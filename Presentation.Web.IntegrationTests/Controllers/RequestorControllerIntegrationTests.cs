﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.NextMeeting;
using Presentation.Web.Models.Requestor;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class RequestorControllerIntegrationTests
    {
        private readonly IGenericRepository<Requestor> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly RequestorController _requestorController;

        public RequestorControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Requestor>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _requestorController = new RequestorController(_repo, _unitOfWork, _mapper, _adHelper);
        }
        [Fact]
        public void Requestor_Read_GetPage1PageSize10_GetsPageOfRequestors()
        {
            // Arrange
            var requestorList = new List<Requestor>();
            for (int i = 0; i < 25; ++i)
            {
                requestorList.Add(new Requestor());
            }
            _repo.AsQueryable().Returns(requestorList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _requestorController.WithCallTo(c => c.Requestor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexRequestorViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Requestor_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfRequestors()
        {
            // Arrange
            var requestorList = new List<Requestor>();
            for (int i = 0; i < 25; ++i)
            {
                requestorList.Add(new Requestor { Id = i });
            }
            _repo.AsQueryable().Returns(requestorList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _requestorController.WithCallTo(c => c.Requestor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexRequestorViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void Requestor_Read_GetRequestorsEqualToName_GetsOnlyRequestorsWithExactName()
        {
            // Arrange
            var NameWeWant = "Name";
            var requestorList = new List<Requestor>{
                new Requestor {Id = 1, Name = "!Name"},
                new Requestor {Id = 2, Name = NameWeWant + " with a suffix"},
                new Requestor {Id = 3, Name = "with a prefix" + NameWeWant},
                new Requestor {Id = 4, Name = "Nam"},
                new Requestor {Id = 5, Name = NameWeWant},
                new Requestor {Id = 6, Name = "ame"}
            };

            _repo.AsQueryable().Returns(requestorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = NameWeWant });

            // Act
            _requestorController.WithCallTo(c => c.Requestor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexRequestorViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == NameWeWant
                    );
            });
        }

        [Fact]
        public void Requestor_Read_GetRequestorsContainingPartOfName_GetsRequestorsWithNameContainingFilterString()
        {
            // Arrange
            var NameWeWant = "Name";
            var requestorList = new List<Requestor>{
                new Requestor {Id = 1, Name = "wat"},
                new Requestor {Id = 2, Name = NameWeWant + " with a suffix"},
                new Requestor {Id = 3, Name = "with a prefix" + NameWeWant},
                new Requestor {Id = 4, Name = "Nam"},
                new Requestor {Id = 5, Name = NameWeWant},
                new Requestor {Id = 6, Name = "ame"}
            };

            _repo.AsQueryable().Returns(requestorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = NameWeWant });

            // Act
            _requestorController.WithCallTo(c => c.Requestor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexRequestorViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }


        [Fact]
        public void Requestor_Read_GetRequestorsIsActiveTrue_GetsRequestorsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var requestorList = new List<Requestor>
            {
                new Requestor {Id = 1, IsActive = !valueWeWant},
                new Requestor {Id = 2, IsActive = valueWeWant},
                new Requestor {Id = 3, IsActive = valueWeWant},
                new Requestor {Id = 4, IsActive = !valueWeWant},
                new Requestor {Id = 5, IsActive = !valueWeWant},
                new Requestor {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(requestorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _requestorController.WithCallTo(c => c.Requestor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexRequestorViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Requestor_Read_GetRequestorsIsActiveFalse_GetsRequestorsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var requestorList = new List<Requestor>{
                new Requestor {Id = 1, IsActive = valueWeWant},
                new Requestor {Id = 2, IsActive = !valueWeWant},
                new Requestor {Id = 3, IsActive = !valueWeWant},
                new Requestor {Id = 4, IsActive = valueWeWant},
                new Requestor {Id = 5, IsActive = valueWeWant},
                new Requestor {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(requestorList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _requestorController.WithCallTo(c => c.Requestor_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexRequestorViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
