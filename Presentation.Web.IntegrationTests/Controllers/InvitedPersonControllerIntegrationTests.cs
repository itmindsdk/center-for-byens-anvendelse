﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.InvitedPerson;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class InvitedPersonControllerIntegrationTests
    {
        private readonly IGenericRepository<InvitedPerson> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly InvitedPersonController _invitedPersonController;

        public InvitedPersonControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<InvitedPerson>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _invitedPersonController = new InvitedPersonController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        [Fact]
        public void Group_Read_GetPage1PageSize10_GetsPageOfGroups()
        {
            // Arrange
            var invitedPersonList = new List<InvitedPerson>();
            for (int i = 0; i < 25; ++i)
            {
                invitedPersonList.Add(new InvitedPerson());
            }
            _repo.AsQueryable().Returns(invitedPersonList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _invitedPersonController.WithCallTo(c => c.InvitedPerson_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<InvitedPersonViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Group_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfGroups()
        {
            // Arrange
            var invitedPersonList = new List<InvitedPerson>();
            for (int i = 0; i < 25; ++i)
            {
                invitedPersonList.Add(new InvitedPerson() { Id = i });
            }
            _repo.AsQueryable().Returns(invitedPersonList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _invitedPersonController.WithCallTo(c => c.InvitedPerson_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<InvitedPersonViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void InvitedPerson_Read_GetInvitedPersonsEqualToName_GetsOnlyInvitedPersonsWithExactName()
        {
            // Arrange
            var nameWeWant = "John Doe";
            var invitedPersonList = new List<InvitedPerson>{
                new InvitedPerson {Id = 1, Name = "Mads Langer"},
                new InvitedPerson {Id = 2, Name = nameWeWant + " Surname"},
                new InvitedPerson {Id = 3, Name ="Prefix " + nameWeWant},
                new InvitedPerson {Id = 4, Name ="Not the right one either"},
                new InvitedPerson {Id = 5, Name = nameWeWant},
                new InvitedPerson {Id = 6, Name = "Heinrich Hauge"}
            };

            _repo.AsQueryable().Returns(invitedPersonList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _invitedPersonController.WithCallTo(c => c.InvitedPerson_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<InvitedPersonViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void InvitedPerson_Read_GetInvitedPersonsContainingPartOfName_GetsInvitedPersonsWithNameContainingFilterString()
        {
            // Arrange
            var nameWeWant = "John Doe";
            var invitedPersonList = new List<InvitedPerson>{
                new InvitedPerson {Id = 1, Name = "Mads Langer"},
                new InvitedPerson {Id = 2, Name = nameWeWant + " Surname"},
                new InvitedPerson {Id = 3, Name ="Prefix " + nameWeWant},
                new InvitedPerson {Id = 4, Name ="Not the right one either"},
                new InvitedPerson {Id = 5, Name = nameWeWant},
                new InvitedPerson {Id = 6, Name = "Heinrich Hauge"}
            };

            _repo.AsQueryable().Returns(invitedPersonList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _invitedPersonController.WithCallTo(c => c.InvitedPerson_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<InvitedPersonViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void InvitedPerson_Read_GetInvitedPersonsIsActiveTrue_GetsInvitedPersonsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var invitedPersonList = new List<InvitedPerson>
            {
                new InvitedPerson {Id = 1, IsActive = !valueWeWant},
                new InvitedPerson {Id = 2, IsActive = valueWeWant},
                new InvitedPerson {Id = 3, IsActive = valueWeWant},
                new InvitedPerson {Id = 4, IsActive = !valueWeWant},
                new InvitedPerson {Id = 5, IsActive = !valueWeWant},
                new InvitedPerson {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(invitedPersonList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _invitedPersonController.WithCallTo(c => c.InvitedPerson_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<InvitedPersonViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void InvitedPerson_Read_GetInvitedPersonsIsActiveFalse_GetsInvitedPersonsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var invitedPersonList = new List<InvitedPerson>
            {
                new InvitedPerson {Id = 1, IsActive = valueWeWant},
                new InvitedPerson {Id = 2, IsActive = !valueWeWant},
                new InvitedPerson {Id = 3, IsActive = !valueWeWant},
                new InvitedPerson {Id = 4, IsActive = valueWeWant},
                new InvitedPerson {Id = 5, IsActive = valueWeWant},
                new InvitedPerson {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(invitedPersonList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _invitedPersonController.WithCallTo(c => c.InvitedPerson_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<InvitedPersonViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
