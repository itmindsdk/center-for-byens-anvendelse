﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Entrepreneur;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class EntrepreneurControllerIntegrationTests
    {
        private readonly IGenericRepository<Entrepreneur> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly EntrepreneurController _entrepreneurController;

        public EntrepreneurControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Entrepreneur>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _entrepreneurController = new EntrepreneurController(_repo, _unitOfWork, _mapper, _adHelper);
        }


        #region ConstructionProgram_read
        [Fact]
        public void Entrepreneur_Read_GetPage1PageSize10_GetsPageOfEntrepreneurs()
        {
            // Arrange
            var entrepreneurList = new List<Entrepreneur>();
            for (int i = 0; i < 25; ++i)
            {
                entrepreneurList.Add(new Entrepreneur());
            }
            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfEntrepreneurs()
        {
            // Arrange
            var entrepreneurList = new List<Entrepreneur>();
            for (int i = 0; i < 25; ++i)
            {
                entrepreneurList.Add(new Entrepreneur { Id = i });
            }
            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursEqualToCompanyName_GetsOnlyEntrepreneursWithExactCompanyName()
        {
            // Arrange
            var companyNameWeWant = "A company name";
            var entrepreneurList = new List<Entrepreneur>{
                new Entrepreneur {Id = 1, Company = new Company { Name = "No"}},
                new Entrepreneur {Id = 2, Company = new Company { Name = companyNameWeWant + " with a suffix"}},
                new Entrepreneur {Id = 3, Company = new Company { Name = "With a prefix " + companyNameWeWant}},
                new Entrepreneur {Id = 4, Company = new Company { Name = "Not the correct name at all"}},
                new Entrepreneur {Id = 5, Company = new Company { Name = companyNameWeWant}},
                new Entrepreneur {Id = 6, Company = new Company { Name = "Another"}}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Company.Name", Operator = FilterOperator.IsEqualTo, Value = companyNameWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Company.Name == companyNameWeWant
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursContainingPartOfCompanyName_GetsEntrepreneursWithCompanyNameContainingFilterString()
        {
            // Arrange
            var companyNameWeWant = "A company name";
            var entrepreneurList = new List<Entrepreneur> {
                new Entrepreneur {Id = 1, Company = new Company { Name = "No"}},
                new Entrepreneur {Id = 2, Company = new Company { Name = companyNameWeWant + " with a suffix"}},
                new Entrepreneur {Id = 3, Company = new Company { Name = "With a prefix " + companyNameWeWant}},
                new Entrepreneur {Id = 4, Company = new Company { Name = "Not the correct name at all"}},
                new Entrepreneur {Id = 5, Company = new Company { Name = companyNameWeWant}},
                new Entrepreneur {Id = 6, Company = new Company { Name = "Another"}}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Company.Name", Operator = FilterOperator.Contains, Value = companyNameWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursEqualToResponsiblePersonName_GetsOnlyEntrepreneursWithExactResponsiblePersonName()
        {
            // Arrange
            var responsiblePersonNameWeWant = "A completely normal name";
            var entrepreneurList = new List<Entrepreneur>{
                new Entrepreneur {Id = 1, ResponsiblePersonName = "No"},
                new Entrepreneur {Id = 2, ResponsiblePersonName = responsiblePersonNameWeWant + " with a suffix"},
                new Entrepreneur {Id = 3, ResponsiblePersonName ="With a prefix " + responsiblePersonNameWeWant},
                new Entrepreneur {Id = 4, ResponsiblePersonName ="Not the correct name at all"},
                new Entrepreneur {Id = 5, ResponsiblePersonName = responsiblePersonNameWeWant},
                new Entrepreneur {Id = 6, ResponsiblePersonName = "Another"}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonName", Operator = FilterOperator.IsEqualTo, Value = responsiblePersonNameWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.ResponsiblePersonName == responsiblePersonNameWeWant
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursContainingPartOfResponsiblePersonName_GetsEntrepreneursWithResponsiblePersonNameContainingFilterString()
        {
            // Arrange
            var responsiblePersonNameWeWant = "A completely normal name";
            var entrepreneurList = new List<Entrepreneur>{
                new Entrepreneur {Id = 1, ResponsiblePersonName = "No"},
                new Entrepreneur {Id = 2, ResponsiblePersonName = responsiblePersonNameWeWant + " with a suffix"},
                new Entrepreneur {Id = 3, ResponsiblePersonName ="With a prefix " + responsiblePersonNameWeWant},
                new Entrepreneur {Id = 4, ResponsiblePersonName ="Not the correct name at all"},
                new Entrepreneur {Id = 5, ResponsiblePersonName = responsiblePersonNameWeWant},
                new Entrepreneur {Id = 6, ResponsiblePersonName = "Another"}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonName", Operator = FilterOperator.Contains, Value = responsiblePersonNameWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursEqualToResponsiblePersonPhoneNumber_GetsOnlyEntrepreneursWithExactResponsiblePersonPhoneNumber()
        {
            // Arrange
            var responsiblePersonPhoneNumberWeWant = "10901901";
            var entrepreneurList = new List<Entrepreneur>{
                new Entrepreneur {Id = 1, ResponsiblePersonPhoneNumber = "1123566"},
                new Entrepreneur {Id = 2, ResponsiblePersonPhoneNumber = responsiblePersonPhoneNumberWeWant + "34535345"},
                new Entrepreneur {Id = 3, ResponsiblePersonPhoneNumber ="234234234" + responsiblePersonPhoneNumberWeWant},
                new Entrepreneur {Id = 4, ResponsiblePersonPhoneNumber ="44563451"},
                new Entrepreneur {Id = 5, ResponsiblePersonPhoneNumber = responsiblePersonPhoneNumberWeWant},
                new Entrepreneur {Id = 6, ResponsiblePersonPhoneNumber = "7891427914"}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonPhoneNumber", Operator = FilterOperator.IsEqualTo, Value = responsiblePersonPhoneNumberWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.ResponsiblePersonPhoneNumber == responsiblePersonPhoneNumberWeWant
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursContainingPartOfResponsiblePersonPhoneNumber_GetsEntrepreneursWithResponsiblePersonPhoneNumberContainingFilterString()
        {
            // Arrange
            var responsiblePersonPhoneNumberWeWant = "10901901";
            var entrepreneurList = new List<Entrepreneur>{
                new Entrepreneur {Id = 1, ResponsiblePersonPhoneNumber = "1123566"},
                new Entrepreneur {Id = 2, ResponsiblePersonPhoneNumber = responsiblePersonPhoneNumberWeWant + "34535345"},
                new Entrepreneur {Id = 3, ResponsiblePersonPhoneNumber ="234234234" + responsiblePersonPhoneNumberWeWant},
                new Entrepreneur {Id = 4, ResponsiblePersonPhoneNumber ="44563451"},
                new Entrepreneur {Id = 5, ResponsiblePersonPhoneNumber = responsiblePersonPhoneNumberWeWant},
                new Entrepreneur {Id = 6, ResponsiblePersonPhoneNumber = "7891427914"}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ResponsiblePersonPhoneNumber", Operator = FilterOperator.Contains, Value = responsiblePersonPhoneNumberWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursIsActiveTrue_GetsEntrepreneursIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var entrepreneurList = new List<Entrepreneur>
            {
                new Entrepreneur {Id = 1, IsActive = !valueWeWant},
                new Entrepreneur {Id = 2, IsActive = valueWeWant},
                new Entrepreneur {Id = 3, IsActive = valueWeWant},
                new Entrepreneur {Id = 4, IsActive = !valueWeWant},
                new Entrepreneur {Id = 5, IsActive = !valueWeWant},
                new Entrepreneur {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Entrepreneur_Read_GetEntrepreneursIsActiveFalse_GetsEntrepreneursIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var entrepreneurList = new List<Entrepreneur>{
                new Entrepreneur {Id = 1, IsActive = valueWeWant},
                new Entrepreneur {Id = 2, IsActive = !valueWeWant},
                new Entrepreneur {Id = 3, IsActive = !valueWeWant},
                new Entrepreneur {Id = 4, IsActive = valueWeWant},
                new Entrepreneur {Id = 5, IsActive = valueWeWant},
                new Entrepreneur {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(entrepreneurList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _entrepreneurController.WithCallTo(c => c.Entrepreneur_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexEntrepreneurViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
        #endregion
    }
}
