﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models;
using Presentation.Web.Models.AgendaPoint;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class AgendaPointControllerIntegrationTests
    {
        private readonly AgendaPointController _agendaPointController;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<AgendaPointDocument> _documentRepository;
        private readonly IGenericRepository<PartnerDiscussion> _partnerDiscussionRepository;
        private readonly IDocumentHelper _documentHelper;
        private readonly IGenericRepository<Meeting> _meetingRepo;
        private readonly IGenericRepository<NextMeeting> _nextMeetingRepo;
        private readonly IGenericRepository<Topic> _topicRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDateTime _dateTime;
        private readonly IGeoCoder _geoCoder;
        private readonly IDawaRepository _dawaRepo;
        private readonly IPrivilegeHelper _privilegeHelper;
        private readonly IAdHelper _adHelper;

        public AgendaPointControllerIntegrationTests()
        {
            _agendaPointRepo = Substitute.For<IGenericRepository<AgendaPoint>>();
            _documentRepository = Substitute.For<IGenericRepository<AgendaPointDocument>>();
            _partnerDiscussionRepository = Substitute.For<IGenericRepository<PartnerDiscussion>>();
            _documentHelper = Substitute.For<IDocumentHelper>();
            _meetingRepo = Substitute.For<IGenericRepository<Meeting>>();
            _nextMeetingRepo = Substitute.For<IGenericRepository<NextMeeting>>();
            _topicRepo = Substitute.For<IGenericRepository<Topic>>();
            _dateTime = Substitute.For<IDateTime>();
            _geoCoder = Substitute.For<IGeoCoder>();
            _dawaRepo = Substitute.For<IDawaRepository>();
            _privilegeHelper = Substitute.For<IPrivilegeHelper>();
            _adHelper = Substitute.For<IAdHelper>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _agendaPointController = new AgendaPointController(
                _agendaPointRepo,
                _meetingRepo,
                _nextMeetingRepo,
                _topicRepo,
                _partnerDiscussionRepository,
                _unitOfWork,
                _mapper,
                _dateTime,
                _documentRepository,
                _documentHelper,
                _geoCoder,
                _dawaRepo,
                _privilegeHelper,
                _adHelper);
        }

        #region TestHelpers
        private class ProjectableAgendaPointParams
        {
            public int? Id { get; set; }
            public DbGeometry AddressLocation { get; set; }
            public string AddressStreetName { get; set; }
            public string AddressNumber { get; set; }
            public string AddressZipCode { get; set; }
            public string AddressCity { get; set; }
            public ProjectLeader ProjectLeader { get; set; }
            public int? ProjectLeaderId { get; set; }
            public bool? TemporaryAddress { get; set; }
            public string TemporaryAddressName { get; set; }
            public int? StreetTypeId { get; set; }
            public StreetType StreetType { get; set; }
            public int? ProjectId { get; set; }
            public Project Project { get; set; }
            public MeetingAgendaPoint MeetingAgendaPoint { get; set; }
            public string Description { get; set; }
            public string Conclusion { get; set; }
            public string Agreement { get; set; }
            public ICollection<Meeting> PreviousMeetings { get; set; }
            public ICollection<AgendaPointDocument> Documents { get; set; }
            public int? TopicId { get; set; }
            public Topic Topic { get; set; }
            public int? AgendaPointStatusId { get; set; }
            public AgendaPointStatus AgendaPointStatus { get; set; }
            public AreaType? Area { get; set; }
            public PartnerDiscussion CbmDiscussion { get; set; }
            public PartnerDiscussion CbaDiscussion { get; set; }
            public DateTime? CreatedOn { get; set; }
            public DateTime? ModifiedOn { get; set; }
            public string ModifiedBy { get; set; }
            public string CreatedBy { get; set; }
        }
        private AgendaPoint ConstructProjectableAgendaPoint(ProjectableAgendaPointParams p = null)
        {
            p = p ?? new ProjectableAgendaPointParams();
            return new AgendaPoint
            {
                Project = p.Project ?? null,
                Id = p.Id ?? default(int),
                CreatedOn = p.CreatedOn ?? default(DateTime),
                ModifiedOn = p.ModifiedOn ?? null,
                AddressStreetName = p.AddressStreetName ?? default(string),
                AddressLocation = p.AddressLocation ?? default(DbGeometry),
                AddressZipCode = p.AddressZipCode ?? default(string),
                AddressNumber = p.AddressNumber ?? default(string),
                AddressCity = p.AddressCity ?? default(string),
                Agreement = p.Agreement ?? default(string),
                Description = p.Description ?? default(string),
                StreetTypeId = p.StreetTypeId ?? default(int?),
                Conclusion = p.Conclusion ?? default(string),
                StreetType = p.StreetType ?? default(StreetType),
                TemporaryAddress = p.TemporaryAddress ?? default(bool),
                CbaDiscussion = p.CbaDiscussion ?? default(PartnerDiscussion),
                Area = p.Area ?? default(AreaType),
                TopicId = p.TopicId ?? default(int?),
                TemporaryAddressName = p.TemporaryAddressName ?? default(string),
                ProjectId = p.ProjectId ?? default(int?),
                AgendaPointStatusId = p.AgendaPointStatusId ?? default(int?),
                CbmDiscussion = p.CbmDiscussion ?? default(PartnerDiscussion),
                ProjectLeaderId = p.ProjectLeaderId ?? default(int?),
                AgendaPointStatus = p.AgendaPointStatus ?? default(AgendaPointStatus),
                ModifiedBy = p.ModifiedBy ?? default(string),
                CreatedBy = p.CreatedBy ?? default(string),
                ProjectLeader = p.ProjectLeader ?? new ProjectLeader(),
                MeetingAgendaPoint = p.MeetingAgendaPoint ?? null,
                Topic = p.Topic ?? new Topic(),
                Documents = p.Documents ?? new List<AgendaPointDocument>(),
                PreviousMeetings = p.PreviousMeetings ?? new List<Meeting>(),
            };
        }
        #endregion


        #region AgendaPoint_Read
        [Fact]
        public void AgendaPoint_Read_GetOnePageOfSize10_GetsPageOfAgendaPoints()
        {
            // Arrange
            var agendaPointList = new List<AgendaPoint>();
            for (int i = 0; i < 25; ++i)
            {
                var agendaPoint = ConstructProjectableAgendaPoint();
                agendaPointList.Add(agendaPoint);
            }
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexAgendaPointViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetLastPageResults_GetsPageHalfFullOfAgendaPoints()
        {
            // Arrange
            var agendaPointList = new List<AgendaPoint>();
            for (int i = 0; i < 25; ++i)
            {
                var agendaPoint = ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams{ Id = i });
                agendaPointList.Add(agendaPoint);
            }
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Page = 3, PageSize = 10 };

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterAddressNameEqualTo_GetsAgendaPointsWithExactAddressName()
        {
            var addressToFind = "Vej 1, 0000 By";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    AddressStreetName = "Vej",
                    AddressNumber = "2",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "1000",
                    AddressCity = "By"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "yB"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    AddressStreetName = "jeV",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "AddressName", Operator = FilterOperator.IsEqualTo, Value = addressToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1 },
                    (c1, c2) => c1.Id == c2 && c1.AddressName == addressToFind);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterAddressNameContains_GetsAgendaPointsWithAddressNameContainingFilterString()
        {
            var addressToFind = "Vej";
               // Arrange
               var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    AddressStreetName = "Vej",
                    AddressNumber = "2",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "1000",
                    AddressCity = "By"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    AddressStreetName = "Vej",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "yB"
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    AddressStreetName = "jeV",
                    AddressNumber = "1",
                    AddressZipCode = "0000",
                    AddressCity = "By"
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "AddressName", Operator = FilterOperator.Contains, Value = addressToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1,2,3,4 },
                    (c1, c2) => c1.Id == c2 && c1.AddressName.Contains(addressToFind, StringComparison.OrdinalIgnoreCase));
            });
        }


        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterProjectLeaderNameEqualTo_GetsAgendaPointsWithExactProjectLeaderName()
        {
            var nameToFind = "Navn4";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    ProjectLeader = new ProjectLeader {Name = "Navn1"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    ProjectLeader = new ProjectLeader {Name = "Navn2"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    ProjectLeader = new ProjectLeader {Name = "Navn3"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    ProjectLeader = new ProjectLeader {Name = "Navn4"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    ProjectLeader = new ProjectLeader {Name = "Navn5"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ProjectLeaderName", Operator = FilterOperator.IsEqualTo, Value = nameToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 4 },
                    (c1, c2) => c1.Id == c2 && c1.ProjectLeaderName == nameToFind);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterProjectLeaderNameContains_GetsAgendaPointsWithProjectLeaderNameContainingFilterString()
        {
            var nameToFind = "John";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    ProjectLeader = new ProjectLeader {Name = "Mads Bentsen John"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    ProjectLeader = new ProjectLeader {Name = "Navn1 Navn2 Navn3"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    ProjectLeader = new ProjectLeader {Name = "John Svend Bjerregaard"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    ProjectLeader = new ProjectLeader {Name = "Karl Vie Madsen"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    ProjectLeader = new ProjectLeader {Name = "Thor Johnson"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "ProjectLeaderName", Operator = FilterOperator.Contains, Value = nameToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1,3,5 },
                    (c1, c2) => c1.Id == c2 && c1.ProjectLeaderName.Contains(nameToFind, StringComparison.OrdinalIgnoreCase));
            });
        }


        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsTemporaryAddressFilterTrue_GetsAgendaPointsWithTemporaryAddressTrue()
        {
            var valueToFind = true;
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    TemporaryAddress = false
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    TemporaryAddress = false
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    TemporaryAddress = false
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    TemporaryAddress = true
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    TemporaryAddress = true
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TemporaryAddress", Operator = FilterOperator.IsEqualTo, Value = valueToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] {4, 5 },
                    (c1, c2) => c1.Id == c2 && c1.TemporaryAddress == valueToFind);
            });
        }
        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsTemporaryAddressFilterFalse_GetsAgendaPointsWithTemporaryAddressFalse()
        {
            var valueToFind = false;
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    TemporaryAddress = false
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    TemporaryAddress = false
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    TemporaryAddress = false
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    TemporaryAddress = true
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    TemporaryAddress = true
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TemporaryAddress", Operator = FilterOperator.IsEqualTo, Value = valueToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1,2,3 },
                    (c1, c2) => c1.Id == c2 && c1.TemporaryAddress == valueToFind);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterStreetTypeEqualTo_GetsAgendaPointsWithExactStreetType()
        {
            var streetTypeToFind = "Offentlig vej";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    StreetType = new StreetType {Type = "Offentlig vej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    StreetType = new StreetType {Type = "Privat vej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    StreetType = new StreetType {Type = "Offentlig motorvej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    StreetType = new StreetType {Type = "Offentlig vej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    StreetType = new StreetType {Type = "Privat jordvej"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "StreetType.Type", Operator = FilterOperator.IsEqualTo, Value = streetTypeToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1, 4 },
                    (c1, c2) => c1.Id == c2 && c1.StreetType.Type == streetTypeToFind);
            });
        }
        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterStreetTypeContains_GetsAgendaPointsWithStreetTypeContainingFilterString()
        {
            var streetTypeToFind = "offentlig";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    StreetType = new StreetType {Type = "Offentlig vej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    StreetType = new StreetType {Type = "Privat vej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    StreetType = new StreetType {Type = "Offentlig motorvej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    StreetType = new StreetType {Type = "Offentlig vej"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    StreetType = new StreetType {Type = "Privat jordvej"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "StreetType.Type", Operator = FilterOperator.Contains, Value = streetTypeToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1, 3, 4 },
                    (c1, c2) => c1.Id == c2 && c1.StreetType.Type.Contains(streetTypeToFind, StringComparison.OrdinalIgnoreCase));
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterTopicNameEqualTo_GetsAgendaPointsWithExactTopicName()
        {
            var topicNameToFind = "Park";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    Topic = new Topic {Name = "Park"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    Topic = new Topic {Name = "Kulturanlæg"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    Topic = new Topic {Name = "Rundkørsel"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    Topic = new Topic {Name = "Motorvejskryds"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    Topic = new Topic {Name = "Dyrepark"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TopicName", Operator = FilterOperator.IsEqualTo, Value = topicNameToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1 },
                    (c1, c2) => c1.Id == c2 && c1.TopicName == topicNameToFind);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterStreetTypeContains_GetsAgendaPointsWithTopicNameContainingFilterString()
        {
            var topicNameToFind = "Park";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    Topic = new Topic {Name = "Park"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    Topic = new Topic {Name = "Kulturanlæg"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    Topic = new Topic {Name = "Rundkørsel"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    Topic = new Topic {Name = "Motorvejskryds"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    Topic = new Topic {Name = "Dyrepark"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TopicName", Operator = FilterOperator.Contains, Value = topicNameToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1, 5 },
                    (c1, c2) => c1.Id == c2 && c1.TopicName.Contains(topicNameToFind, StringComparison.OrdinalIgnoreCase));
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterStatusEqualTo_GetsAgendaPointsWithExactStatus()
        {
            var StatusToFind = "Klar til dagsorden";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Under udarbejdelse"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Under udarbejdelse"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    AgendaPointStatus = new AgendaPointStatus {Status = StatusToFind}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    AgendaPointStatus = new AgendaPointStatus {Status = StatusToFind}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Ukendt"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "AgendaPointStatus.Status", Operator = FilterOperator.IsEqualTo, Value = StatusToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 3, 4 },
                    (c1, c2) => c1.Id == c2 && c1.AgendaPointStatus.Status == StatusToFind);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsFilterStatusContains_GetsAgendaPointsWithStatusContainingFilterString()
        {
            var StatusToFind = "under";
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Under udarbejdelse"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Under udarbejdelse"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Klar til dagsorden"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Klar til dagsorden"}
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    AgendaPointStatus = new AgendaPointStatus {Status = "Ukendt"}
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "AgendaPointStatus.Status", Operator = FilterOperator.Contains, Value = StatusToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1, 2 },
                    (c1, c2) => c1.Id == c2 && c1.AgendaPointStatus.Status.Contains(StatusToFind, StringComparison.OrdinalIgnoreCase));
            });
        }


        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsHasDocumentsFilterTrue_GetsAgendaPointsWithHasDocumentsTrue()
        {
            var valueToFind = true;
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    Documents = new List<AgendaPointDocument> {new AgendaPointDocument() }
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    Documents = new List<AgendaPointDocument> {new AgendaPointDocument() }
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "HasDocuments", Operator = FilterOperator.IsEqualTo, Value = valueToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 1, 5 },
                    (c1, c2) => c1.Id == c2 && c1.HasDocuments == valueToFind);
            });
        }
        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsHasDocumentsFilterFalse_GetsAgendaPointsWithHasDocumentsFalse()
        {
            var valueToFind = false;
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    Documents = new List<AgendaPointDocument> {new AgendaPointDocument() }
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    Documents = new List<AgendaPointDocument> {new AgendaPointDocument() }
                })
            };
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());
            var request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "HasDocuments", Operator = FilterOperator.IsEqualTo, Value = valueToFind });

            _adHelper.InRole(UserRole.Admin).Returns(true);

            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 2, 3, 4 },
                    (c1, c2) => c1.Id == c2 && c1.HasDocuments == valueToFind);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsOnlyNewAgendaPointsChecked_GetsAgendaPointsThatAreLessThanAYearOld()
        {
            var dateTimeNow = new DateTime(2017, 1, 1);
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    ModifiedOn = dateTimeNow.AddYears(-1).AddDays(-1)
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2,
                    ModifiedOn = dateTimeNow.AddYears(-1).AddDays(1)
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    // One with a null value
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                    ModifiedOn = dateTimeNow.AddYears(-1).AddMonths(1)
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    ModifiedOn = dateTimeNow.AddYears(1)
                })
            };
            _dateTime.Now.Returns(dateTimeNow);
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());


            var request = new DataSourceRequest {};

            _adHelper.InRole(UserRole.Admin).Returns(true);
            var httpCtx = ControllerContextHelpers.ConstructHttpRequestWithCookies(new HttpCookie("onlyNewPoints", "true"));
            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController, httpCtx);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 2, 4, 5 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void AgendaPoint_Read_GetAgendaPointsOnlyAgendaPointsNotAssociatedWithMeeting_GetsAgendaPointsThatArentAssociatedWithAMeeting()
        {
            var dateTimeNow = new DateTime(2017, 1, 1);
            // Arrange
            var agendaPointList = new List<AgendaPoint>
            {
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 1,
                    MeetingAgendaPoint = new MeetingAgendaPoint()
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 2
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 3,
                    MeetingAgendaPoint = new MeetingAgendaPoint()
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 4,
                }),
                ConstructProjectableAgendaPoint(new ProjectableAgendaPointParams
                {
                    Id = 5,
                    MeetingAgendaPoint = new MeetingAgendaPoint()
                })
            };
            _dateTime.Now.Returns(dateTimeNow);
            _agendaPointRepo.AsQueryable().Returns(agendaPointList.AsQueryable());


            var request = new DataSourceRequest { };

            _adHelper.InRole(UserRole.Admin).Returns(true);
            var httpCtx = ControllerContextHelpers.ConstructHttpRequestWithCookies(new HttpCookie("excludePoints", "true"));
            _agendaPointController.ControllerContext = ControllerContextHelpers.ConstructControllerContext(_agendaPointController, httpCtx);

            // Act
            _agendaPointController.WithCallTo(c => c.AgendaPoint_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexAgendaPointViewModel>).Should().Equal(
                    new[] { 2, 4 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        #endregion
    }
}

