﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Entrepreneur;
using Presentation.Web.Models.NextMeeting;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class NextMeetingControllerIntegrationTests
    {
        private readonly IGenericRepository<NextMeeting> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly NextMeetingController _nextMeetingController;

        public NextMeetingControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<NextMeeting>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _nextMeetingController = new NextMeetingController(_repo, _unitOfWork, _mapper, _adHelper);
        }

        [Fact]
        public void NextMeeting_Read_GetPage1PageSize10_GetsPageOfNextMeetings()
        {
            // Arrange
            var nextMeetingList = new List<NextMeeting>();
            for (int i = 0; i < 25; ++i)
            {
                nextMeetingList.Add(new NextMeeting());
            }
            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexNextMeetingViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void NextMeeting_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfNextMeetings()
        {
            // Arrange
            var nextMeetingList = new List<NextMeeting>();
            for (int i = 0; i < 25; ++i)
            {
                nextMeetingList.Add(new NextMeeting { Id = i });
            }
            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void NextMeeting_Read_GetNextMeetingsEqualToDeadline_GetsOnlyNextMeetingsWithExactDeadline()
        {
            // Arrange
            var deadlineWeWant = "Deadline";
            var nextMeetingList = new List<NextMeeting>{
                new NextMeeting {Id = 1, Deadline = "!Deadline"},
                new NextMeeting {Id = 2, Deadline = deadlineWeWant + " with a suffix"},
                new NextMeeting {Id = 3, Deadline = "with a prefix" + deadlineWeWant},
                new NextMeeting {Id = 4, Deadline = "not deadline"},
                new NextMeeting {Id = 5, Deadline = deadlineWeWant},
                new NextMeeting {Id = 6, Deadline = "20/20/2000"}
            };

            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Deadline", Operator = FilterOperator.IsEqualTo, Value = deadlineWeWant });

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Deadline == deadlineWeWant
                    );
            });
        }

        [Fact]
        public void NextMeeting_Read_GetNextMeetingsContainingPartOfDeadline_GetsNextMeetingsWithDeadlineContainingFilterString()
        {
            // Arrange
            var deadlineWeWant = "Deadline";
            var nextMeetingList = new List<NextMeeting>{
                new NextMeeting {Id = 1, Deadline = "!eadline"},
                new NextMeeting {Id = 2, Deadline = deadlineWeWant + " with a suffix"},
                new NextMeeting {Id = 3, Deadline = "with a prefix" + deadlineWeWant},
                new NextMeeting {Id = 4, Deadline = "enildaeD"},
                new NextMeeting {Id = 5, Deadline = deadlineWeWant},
                new NextMeeting {Id = 6, Deadline = "20/20/2000"}
            };

            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Deadline", Operator = FilterOperator.Contains, Value = deadlineWeWant });

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void NextMeeting_Read_GetNextMeetingsEqualToName_GetsOnlyNextMeetingsWithExactName()
        {
            // Arrange
            var nameWeWant = "A name";
            var nextMeetingList = new List<NextMeeting>{
                new NextMeeting {Id = 1, Name = "another name"},
                new NextMeeting {Id = 2, Name = nameWeWant + " with a suffix"},
                new NextMeeting {Id = 3, Name = "with a prefix" + nameWeWant},
                new NextMeeting {Id = 4, Name = "not the name"},
                new NextMeeting {Id = 5, Name = nameWeWant},
                new NextMeeting {Id = 6, Name = "aname"}
            };

            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void NextMeeting_Read_GetNextMeetingsContainingPartOfName_GetsNextMeetingsWithNameContainingFilterString()
        {
            // Arrange
            var nameWeWant = "A name";
            var nextMeetingList = new List<NextMeeting>{
                new NextMeeting {Id = 1, Name = "another name"},
                new NextMeeting {Id = 2, Name = nameWeWant + " with a suffix"},
                new NextMeeting {Id = 3, Name = "with a prefix" + nameWeWant},
                new NextMeeting {Id = 4, Name = "not the name"},
                new NextMeeting {Id = 5, Name = nameWeWant},
                new NextMeeting {Id = 6, Name = "aname"}
            };

            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void NextMeeting_Read_GetNextMeetingsIsActiveTrue_GetsNextMeetingsIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var nextMeetingList = new List<NextMeeting>
            {
                new NextMeeting {Id = 1, IsActive = !valueWeWant},
                new NextMeeting {Id = 2, IsActive = valueWeWant},
                new NextMeeting {Id = 3, IsActive = valueWeWant},
                new NextMeeting {Id = 4, IsActive = !valueWeWant},
                new NextMeeting {Id = 5, IsActive = !valueWeWant},
                new NextMeeting {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void NextMeeting_Read_GetNextMeetingsIsActiveFalse_GetsNextMeetingsIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var nextMeetingList = new List<NextMeeting>{
                new NextMeeting {Id = 1, IsActive = valueWeWant},
                new NextMeeting {Id = 2, IsActive = !valueWeWant},
                new NextMeeting {Id = 3, IsActive = !valueWeWant},
                new NextMeeting {Id = 4, IsActive = valueWeWant},
                new NextMeeting {Id = 5, IsActive = valueWeWant},
                new NextMeeting {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(nextMeetingList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _nextMeetingController.WithCallTo(c => c.NextMeeting_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexNextMeetingViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
