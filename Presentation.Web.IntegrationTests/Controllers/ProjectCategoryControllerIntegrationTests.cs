﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ProjectCategory;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class ProjectCategoryControllerIntegrationTests
    {
        private readonly IGenericRepository<ProjectCategory> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly ProjectCategoryController _projectCategoryController;

        public ProjectCategoryControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<ProjectCategory>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _projectCategoryController = new ProjectCategoryController(_repo, _unitOfWork, _mapper, _adHelper);
        }
        [Fact]
        public void ProjectCategory_Read_GetPage1PageSize10_GetsPageOfProjectCategories()
        {
            // Arrange
            var projectCategoryList = new List<ProjectCategory>();
            for (int i = 0; i < 25; ++i)
            {
                projectCategoryList.Add(new ProjectCategory());
            }
            _repo.AsQueryable().Returns(projectCategoryList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _projectCategoryController.WithCallTo(c => c.ProjectCategory_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexProjectCategoryViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void ProjectCategory_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfProjectCategories()
        {
            // Arrange
            var projectCategoryList = new List<ProjectCategory>();
            for (int i = 0; i < 25; ++i)
            {
                projectCategoryList.Add(new ProjectCategory { Id = i });
            }
            _repo.AsQueryable().Returns(projectCategoryList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _projectCategoryController.WithCallTo(c => c.ProjectCategory_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectCategoryViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }


        [Fact]
        public void ProjectCategory_Read_GetProjectCategoriesEqualToCategory_GetsOnlyProjectCategoriesWithExactCategory()
        {
            // Arrange
            var categoryWeWant = "A category";
            var projectCategoryList = new List<ProjectCategory>{
                new ProjectCategory {Id = 1, Category = "another category"},
                new ProjectCategory {Id = 2, Category = categoryWeWant + " with a suffix"},
                new ProjectCategory {Id = 3, Category = "with a prefix" + categoryWeWant},
                new ProjectCategory {Id = 4, Category = "not the category"},
                new ProjectCategory {Id = 5, Category = categoryWeWant},
                new ProjectCategory {Id = 6, Category = "acategory"}
            };

            _repo.AsQueryable().Returns(projectCategoryList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Category", Operator = FilterOperator.IsEqualTo, Value = categoryWeWant });

            // Act
            _projectCategoryController.WithCallTo(c => c.ProjectCategory_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectCategoryViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Category == categoryWeWant
                    );
            });
        }

        [Fact]
        public void ProjectCategory_Read_GetProjectCategoriesContainingPartOfCategory_GetsProjectCategoriesWithCategoryContainingFilterString()
        {
            // Arrange
            var categoryWeWant = "A category";
            var projectCategoryList = new List<ProjectCategory>{
                new ProjectCategory {Id = 1, Category = "another category"},
                new ProjectCategory {Id = 2, Category = categoryWeWant + " with a suffix"},
                new ProjectCategory {Id = 3, Category = "with a prefix" + categoryWeWant},
                new ProjectCategory {Id = 4, Category = "not the category"},
                new ProjectCategory {Id = 5, Category = categoryWeWant},
                new ProjectCategory {Id = 6, Category = "acategory"}
            };

            _repo.AsQueryable().Returns(projectCategoryList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Category", Operator = FilterOperator.Contains, Value = categoryWeWant });

            // Act
            _projectCategoryController.WithCallTo(c => c.ProjectCategory_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectCategoryViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void ProjectCategory_Read_GetProjectCategoriesIsActiveTrue_GetsProjectCategoriesIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var projectCategoryList = new List<ProjectCategory>
            {
                new ProjectCategory {Id = 1, IsActive = !valueWeWant},
                new ProjectCategory {Id = 2, IsActive = valueWeWant},
                new ProjectCategory {Id = 3, IsActive = valueWeWant},
                new ProjectCategory {Id = 4, IsActive = !valueWeWant},
                new ProjectCategory {Id = 5, IsActive = !valueWeWant},
                new ProjectCategory {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(projectCategoryList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _projectCategoryController.WithCallTo(c => c.ProjectCategory_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectCategoryViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void ProjectCategory_Read_GetProjectCategoriesIsActiveFalse_GetsProjectCategoriesIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var projectCategoryList = new List<ProjectCategory>{
                new ProjectCategory {Id = 1, IsActive = valueWeWant},
                new ProjectCategory {Id = 2, IsActive = !valueWeWant},
                new ProjectCategory {Id = 3, IsActive = !valueWeWant},
                new ProjectCategory {Id = 4, IsActive = valueWeWant},
                new ProjectCategory {Id = 5, IsActive = valueWeWant},
                new ProjectCategory {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(projectCategoryList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _projectCategoryController.WithCallTo(c => c.ProjectCategory_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectCategoryViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
