﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.ApplicationServices;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Models.LocalCommunity;
using Presentation.Web.Models.Search;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class SearchControllerIntegrationTests
    {
        private readonly IMapper _mapper;
        private readonly ISearchService _searchService;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepository;
        private readonly SearchController _searchController;

        public SearchControllerIntegrationTests()
        {
            _projectRepository = Substitute.For<IGenericRepository<Project>>();
            _agendaPointRepository = Substitute.For<IGenericRepository<AgendaPoint>>();
            _searchService = Substitute.For<ISearchService>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _searchController = new SearchController(_mapper, _searchService, _projectRepository, _agendaPointRepository);
        }

        #region AgendaPoint TestHelpers
        private class ProjectableAgendaPointParams
        {
            public int? Id { get; set; }
            public DbGeometry AddressLocation { get; set; }
            public string AddressStreetName { get; set; }
            public string AddressNumber { get; set; }
            public string AddressZipCode { get; set; }
            public string AddressCity { get; set; }
            public ProjectLeader ProjectLeader { get; set; }
            public int? ProjectLeaderId { get; set; }
            public bool? TemporaryAddress { get; set; }
            public string TemporaryAddressName { get; set; }
            public int? StreetTypeId { get; set; }
            public StreetType StreetType { get; set; }
            public int? ProjectId { get; set; }
            public Project Project { get; set; }
            public MeetingAgendaPoint MeetingAgendaPoint { get; set; }
            public string Description { get; set; }
            public string Conclusion { get; set; }
            public string Agreement { get; set; }
            public ICollection<Meeting> PreviousMeetings { get; set; }
            public ICollection<AgendaPointDocument> Documents { get; set; }
            public int? TopicId { get; set; }
            public Topic Topic { get; set; }
            public int? AgendaPointStatusId { get; set; }
            public AgendaPointStatus AgendaPointStatus { get; set; }
            public AreaType? Area { get; set; }
            public PartnerDiscussion CbmDiscussion { get; set; }
            public PartnerDiscussion CbaDiscussion { get; set; }
            public DateTime? CreatedOn { get; set; }
            public DateTime? ModifiedOn { get; set; }
            public string ModifiedBy { get; set; }
            public string CreatedBy { get; set; }
        }
        private AgendaPoint ConstructProjectableAgendaPoint(ProjectableAgendaPointParams p = null)
        {
            p = p ?? new ProjectableAgendaPointParams();
            return new AgendaPoint
            {
                Project = p.Project ?? null,
                Id = p.Id ?? default(int),
                CreatedOn = p.CreatedOn ?? default(DateTime),
                ModifiedOn = p.ModifiedOn ?? null,
                AddressStreetName = p.AddressStreetName ?? default(string),
                AddressLocation = p.AddressLocation ?? default(DbGeometry),
                AddressZipCode = p.AddressZipCode ?? default(string),
                AddressNumber = p.AddressNumber ?? default(string),
                AddressCity = p.AddressCity ?? default(string),
                Agreement = p.Agreement ?? default(string),
                Description = p.Description ?? default(string),
                StreetTypeId = p.StreetTypeId ?? default(int?),
                Conclusion = p.Conclusion ?? default(string),
                StreetType = p.StreetType ?? default(StreetType),
                TemporaryAddress = p.TemporaryAddress ?? default(bool),
                CbaDiscussion = p.CbaDiscussion ?? default(PartnerDiscussion),
                Area = p.Area ?? default(AreaType),
                TopicId = p.TopicId ?? default(int?),
                TemporaryAddressName = p.TemporaryAddressName ?? default(string),
                ProjectId = p.ProjectId ?? default(int?),
                AgendaPointStatusId = p.AgendaPointStatusId ?? default(int?),
                CbmDiscussion = p.CbmDiscussion ?? default(PartnerDiscussion),
                ProjectLeaderId = p.ProjectLeaderId ?? default(int?),
                AgendaPointStatus = p.AgendaPointStatus ?? default(AgendaPointStatus),
                ModifiedBy = p.ModifiedBy ?? default(string),
                CreatedBy = p.CreatedBy ?? default(string),
                ProjectLeader = p.ProjectLeader ?? new ProjectLeader(),
                MeetingAgendaPoint = p.MeetingAgendaPoint ?? null,
                Topic = p.Topic ?? new Topic(),
                Documents = p.Documents ?? new List<AgendaPointDocument>(),
                PreviousMeetings = p.PreviousMeetings ?? new List<Meeting>(),
            };
        }
        #endregion

        #region index httppost

        //Streetname

        [Fact]
        public void Index_Post_PopulatesViewModelForResultsView()
        {
            // Arrange
            var address = "address";
            var temporaryAddress = "temporary";
            var projectStatusId = 1;
            var agendaPointStatusId = 2;
            var topicIds = new [] {3};
            var projectId = 4;
            var meetingId = 5;
            var groupId = 6;
            var councilId = 7;
            var projectCategoryId = 8;
            var companyAdvisorId = 9;
            var companyEntrepreneurId = 10;
            var requestorId = 11;
            var dateStart = new DateTime(2001,1,1);
            var dateEnd = new DateTime(2002,2,2);
            var dateReview = DateSearchTypeViewModel.OneYearReview;
            var agendaPointTick = true;
            var projectsTick = true;

            var searchViewModel = new SearchViewModel
            {
                Address = address,
                TemporaryAddressName = temporaryAddress,
                TopicIds = topicIds,
                ProjectId = projectId,
                AgendaPointStatusId = agendaPointStatusId,
                RequestorId = requestorId,
                ProjectStatusId = projectStatusId,
                ProjectCategoryId = projectCategoryId,
                GroupId = groupId,
                MeetingId = meetingId,
                CouncilId = councilId,
                CompanyId = companyAdvisorId,
                DateSearchType = dateReview,
                EndDate = dateEnd,
                EntrepreneurCompanyId = companyEntrepreneurId,
                SearchInAgendaPoints = agendaPointTick,
                SearchInProjects = projectsTick,
                StartDate = dateStart
            };

            var expectedProjectSearchParameters = new ProjectSearchParameters
            {
                TopicIds = topicIds,
                TemporaryAddressName = temporaryAddress,
                ProjectId = projectId,
                GroupId = groupId,
                ProjectStatusId = projectStatusId,
                RequestorId = requestorId,
                CouncilId = councilId,
                ProjectCategoryId = projectCategoryId,
                Address = address,
                StartDate = dateStart,
                DateSearchType = _mapper.Map<DateSearchType>(dateReview),
                EntrepreneurCompanyId = companyEntrepreneurId,
                CompanyId = companyAdvisorId,
                EndDate = dateEnd
            };

            var expectedAgendaPointSearchParameters = new AgendaPointSearchParameters
            {
                TemporaryAddressName = temporaryAddress,
                TopicIds = topicIds,
                ProjectId = projectId,
                AgendaPointStatusId = agendaPointStatusId,
                Address = address,
                StartDate = dateStart,
                EndDate = dateEnd,
                MeetingId = meetingId
            };

            // Act
            _searchController.WithCallTo(c => c.IndexPost(searchViewModel))
                // Assert
                .ShouldRenderView("Results").WithModel<ResultsViewModel>(
                model =>
                {
                    model.ProjectSearchParameters.ShouldBeEquivalentTo(expectedProjectSearchParameters);
                    model.AgendaPointSearchParameters.ShouldBeEquivalentTo(expectedAgendaPointSearchParameters);
                    model.ShowAgendaPoints.Should().BeTrue();
                    model.ShowProjects.Should().BeTrue();
                    model.AgendaPointFilter.Should().NotBeNull();
                    model.ProjectFilter.Should().NotBeNull();
                });
        }




        //temporary address / projectnam
        //project status
        //agendapoint status
        //topic
        //project id
        //meetingadgendapoint id
        //group
        //council
        //projektkategori
        //company advisor
        //company entrepreneur
        //requestor
        //date	5 year	1 year
        //agendapoint tick
        //project tick



        #endregion
    }
}
