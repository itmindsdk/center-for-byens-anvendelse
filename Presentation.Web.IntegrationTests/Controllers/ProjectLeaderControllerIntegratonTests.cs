﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.ProjectLeader;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class ProjectLeaderControllerIntegratonTests
    {
        private readonly IGenericRepository<ProjectLeader> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly ProjectLeaderController _projectLeaderController;

        public ProjectLeaderControllerIntegratonTests()
        {
            _repo = Substitute.For<IGenericRepository<ProjectLeader>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _projectLeaderController = new ProjectLeaderController(_repo, _unitOfWork, _mapper, _adHelper);
        }


        #region testhelpers

        private class ProjectableProjectLeaderParams
        {
            public int? Id { get; set; }
            public string Name { get; set; }
            public string Initials { get; set; }
            public string Email { get; set; }
            public bool? IsDeleted { get; set; }
            public int? TeamId { get; set; }
            public Team Team { get; set; }
            public bool? IsActive { get; set; }
            public DateTime? CreatedOn { get; set; }
            public string CreatedBy { get; set; }
            public DateTime? ModifiedOn { get; set; }
            public string ModifiedBy { get; set; }
        }

        private ProjectLeader ConstructProjectableProjectLeader(ProjectableProjectLeaderParams p = null)
        {
            p = p ?? new ProjectableProjectLeaderParams();
            return new ProjectLeader
            {
                Id = p.Id ?? default(int),
                Name = p.Name,
                Team = p.Team ?? new Team(),
                IsActive = p.IsActive ?? default(bool),
                ModifiedOn = p.ModifiedOn,
                CreatedOn = p.CreatedOn ?? new DateTime(),
                Initials = p.Initials,
                ModifiedBy = p.ModifiedBy,
                CreatedBy = p.CreatedBy,
                Email = p.Email,
                IsDeleted = p.IsDeleted ?? default(bool),
                TeamId = p.TeamId ?? default(int)
            };

        }
        #endregion

        [Fact]
        public void ProjectLeader_Read_GetPage1PageSize10_GetsPageOfProjectLeaders()
        {
            // Arrange
            var projectLeaderList = new List<ProjectLeader>();
            for (int i = 0; i < 25; ++i)
            {
                projectLeaderList.Add(ConstructProjectableProjectLeader());
            }
            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().HaveCount(10);
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfProjectLeaders()
        {
            // Arrange
            var projectLeaderList = new List<ProjectLeader>();
            for (int i = 0; i < 25; ++i)
            {
                projectLeaderList.Add(ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = i
                }));
            }
            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersEqualToName_GetsOnlyProjectLeadersWithExactName()
        {
            // Arrange
            var nameWeWant = "Name";
            var projectLeaderList = new List<ProjectLeader>{
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Name = "emaN"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Name = nameWeWant + " with a suffix"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Name = "with a prefix" + nameWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Name = "ame"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Name = nameWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Name = "Nam"
                })
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersContainingPartOfName_GetsProjectLeadersWithNameContainingFilterString()
        {
            // Arrange
            var nameWeWant = "Name";
            var projectLeaderList = new List<ProjectLeader>{
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Name = "emaN"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Name = nameWeWant + " with a suffix"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Name = "with a prefix" + nameWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Name = "ame"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Name = nameWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Name = "Nam"
                })
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersEqualToInitials_GetsOnlyProjectLeadersWithExactInitials()
        {
            // Arrange
            var initialsWeWant = "KMM";
            var projectLeaderList = new List<ProjectLeader>{
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Initials = "SIM"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Initials = initialsWeWant + "WAS"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Initials = "WAP" + initialsWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Initials = "KM"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Initials = initialsWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                     Initials = "MM"
                })
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Initials", Operator = FilterOperator.IsEqualTo, Value = initialsWeWant });

            // Act
                _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Initials == initialsWeWant
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersContainingPartOfInitials_GetsProjectLeadersWithInitialsContainingFilterString()
        {
            // Arrange
            var initialsWeWant = "KMM";
            var projectLeaderList = new List<ProjectLeader>{
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Initials = "SIM"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Initials = initialsWeWant + "WAS"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Initials = "WAP" + initialsWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Initials = "KM"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Initials = initialsWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Initials = "MM"
                })
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Initials", Operator = FilterOperator.Contains, Value = initialsWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersEqualToEmail_GetsOnlyProjectLeadersWithExactEmail()
        {
            // Arrange
            var emailWeWant = "email@email.com";
            var projectLeaderList = new List<ProjectLeader>{
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Email = "email@email"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Email = emailWeWant + "suffix"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Email = "prefix" + emailWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Email = "mail@email.com"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Email = emailWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Email = "email@email.co"
                }),
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Email", Operator = FilterOperator.IsEqualTo, Value = emailWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Email == emailWeWant
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersContainingPartOfEmail_GetsProjectLeadersWithEmailContainingFilterString()
        {
            // Arrange
            var emailWeWant = "email@email.com";
            var projectLeaderList = new List<ProjectLeader>{
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Email = "email@email"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Email = emailWeWant + "suffix"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Email = "prefix" + emailWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Email = "mail@email.com"
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Email = emailWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Email = "email@email.co"
                }),
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Email", Operator = FilterOperator.Contains, Value = emailWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }


        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersEqualToTeamName_GetsOnlyProjectLeadersWithExactTeamName()
        {
            // Arrange
            var teamNameWeWant = "Team";
            var projectLeaderList = new List<ProjectLeader>
            {
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Team = new Team { Name = "eam"}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Team = new Team { Name = teamNameWeWant + "Suffix"}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Team = new Team { Name = "Prefix" + teamNameWeWant}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Team = new Team { Name = "Tea"}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Team = new Team { Name = teamNameWeWant}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Team = new Team { Name = "Not the team"}
                }),
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TeamName", Operator = FilterOperator.IsEqualTo, Value = teamNameWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.TeamName == teamNameWeWant
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersContainingPartOfTeamName_GetsProjectLeadersWithTeamNameContainingFilterString()
        {
            // Arrange
            var teamNameWeWant = "Team";
            var projectLeaderList = new List<ProjectLeader>
            {
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    Team = new Team { Name = "eam"}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    Team = new Team { Name = teamNameWeWant + "Suffix"}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    Team = new Team { Name = "Prefix" + teamNameWeWant}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    Team = new Team { Name = "Tea"}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    Team = new Team { Name = teamNameWeWant}
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    Team = new Team { Name = "Doesn't include the word we're looking for"}
                }),
            };

            _repo.AsQueryable().Returns(projectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "TeamName", Operator = FilterOperator.Contains, Value = teamNameWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 2, 3, 5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersIsActiveTrue_GetsProjectLeadersIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var ProjectLeaderList = new List<ProjectLeader>
            {
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    IsActive = !valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    IsActive = valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    IsActive = valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    IsActive = !valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    IsActive = !valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    IsActive = valueWeWant
                })
            };

            _repo.AsQueryable().Returns(ProjectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 2, 3, 6 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void ProjectLeader_Read_GetProjectLeadersIsActiveFalse_GetsProjectLeadersIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var ProjectLeaderList = new List<ProjectLeader>
            {
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 1,
                    IsActive = valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 2,
                    IsActive = !valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 3,
                    IsActive = !valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 4,
                    IsActive = valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 5,
                    IsActive = valueWeWant
                }),
                ConstructProjectableProjectLeader(new ProjectableProjectLeaderParams
                {
                    Id = 6,
                    IsActive = !valueWeWant
                })
            };

            _repo.AsQueryable().Returns(ProjectLeaderList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _projectLeaderController.WithCallTo(c => c.ProjectLeader_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexProjectLeaderViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
    }
}
