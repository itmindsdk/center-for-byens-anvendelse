﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.DomainModel;
using Core.DomainServices;
using FluentAssertions;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using NSubstitute;
using Presentation.Web.App_Start;
using Presentation.Web.Controllers;
using Presentation.Web.Helpers;
using Presentation.Web.Models.Company;
using Presentation.Web.Models.Shared;
using Presentation.Web.UnitTests;
using TestStack.FluentMVCTesting;
using Xunit;

namespace Presentation.Web.IntegrationTests.Controllers
{
    public class CompanyControllerIntegrationTests
    {
        private readonly IGenericRepository<Company> _repo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAdHelper _adHelper;
        private readonly CompanyController _companyController;
        public CompanyControllerIntegrationTests()
        {
            _repo = Substitute.For<IGenericRepository<Company>>();
            _unitOfWork = Substitute.For<IUnitOfWork>();
            _adHelper = Substitute.For<IAdHelper>();
            MappingConfig.Start();
            _mapper = Mapper.Instance;
            new UserMock().LogOn(); // setting user mock

            _companyController = new CompanyController(_repo, _unitOfWork, _mapper, _adHelper);
        }
        #region company_read
        [Fact]
        public void Company_Read_GetPage1PageSize10_GetsPageOfCompanies()
        {
            // Arrange
            var companyList = new List<Company>();
            for (int i = 0; i < 25; ++i){ companyList.Add(new Company());
            }
            _repo.AsQueryable().Returns(companyList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 1, PageSize = 10 };

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data as DataSourceResult).Total.Should().Be(25);
                (data.Data as IList<IndexCompanyViewModel>).Should().HaveCount(10);
            });
        }
        [Fact]
        public void Company_Read_GetLastPageResultsNotWholePage_GetsPageHalfFullOfCompanies()
        {
            // Arrange
            var companyList = new List<Company>();
            for (int i = 0; i < 25; ++i)
            {
                companyList.Add(new Company {Id = i});
            }
            _repo.AsQueryable().Returns(companyList.AsQueryable());

            DataSourceRequest request = new DataSourceRequest { Page = 3, PageSize = 10 };

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCompanyViewModel>).Should().Equal(
                    new[] { 20, 21, 22, 23, 24 },
                    (c1, c2) => c1.Id == c2);
            });
        }

        [Fact]
        public void Company_Read_GetCompaniesEqualToName_GetsOnlyCompanyWithExactName()
        {
            // Arrange
            var nameWeWant = "Acme";
            var companyList = new List<Company>{
                new Company {Id = 1, Name = "Emca"},
                new Company {Id = 2, Name = nameWeWant + " with a suffix"},
                new Company {Id = 3, Name = "With a prefix " + nameWeWant},
                new Company {Id = 4, Name = "Not the correct name at all"},
                new Company {Id = 5, Name = nameWeWant},
                new Company {Id = 6, Name = "Another"}
            };
            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.IsEqualTo, Value = nameWeWant });

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCompanyViewModel>).Should().Equal(
                        new[] { 5 },
                        (c1, c2) => c1.Id == c2 && c1.Name == nameWeWant
                    );
            });
        }

        [Fact]
        public void Company_Read_GetCompaniesContainingPartOfName_GetsCompaniesWithNameContainingFilterString()
        {
            // Arrange
            var nameWeWant = "Acme";
            var companyList = new List<Company>{
                new Company {Id = 1, Name = "Emca"},
                new Company {Id = 2, Name = nameWeWant + " with a suffix"},
                new Company {Id = 3, Name = "With a prefix " + nameWeWant},
                new Company {Id = 4, Name = "Not the correct name at all"},
                new Company {Id = 5, Name = nameWeWant},
                new Company {Id = 6, Name = "Another"}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Name", Operator = FilterOperator.Contains, Value = nameWeWant });

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCompanyViewModel>).Should().Equal(
                        new[] { 2,3,5 },
                        (c1, c2) => c1.Id == c2
                    );
            });
        }

        [Fact]
        public void Company_Read_GetCompaniesEqualToType_GetsCompaniesOfTypeInFilter()
        {
            // Arrange
            var typeWeWant = CompanyType.Advisor;
            var typeWeDontWant = CompanyType.Entrepreneur;
            var typeWeWantVM = CompanyTypeViewModel.Advisor;
            var companyList = new List<Company>{
                new Company {Id = 1, Type = typeWeDontWant},
                new Company {Id = 2, Type = typeWeDontWant},
                new Company {Id = 3, Type = typeWeWant},
                new Company {Id = 4, Type = typeWeDontWant},
                new Company {Id = 5, Type = typeWeWant},
                new Company {Id = 6, Type = typeWeDontWant}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "Type", Operator = FilterOperator.IsEqualTo, Value = typeWeWantVM});

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCompanyViewModel>).Should().Equal(
                        new[] { 3, 5 },
                        (c1, c2) => c1.Id == c2 && c1.Type == typeWeWantVM
                    );
            });
        }

        [Fact]
        public void Company_Read_GetCompaniesIsActiveTrue_GetsCompaniesIsActiveTrue()
        {
            // Arrange
            var valueWeWant = true;
            var companyList = new List<Company>{
                new Company {Id = 1, IsActive = !valueWeWant},
                new Company {Id = 2, IsActive = valueWeWant},
                new Company {Id = 3, IsActive = valueWeWant},
                new Company {Id = 4, IsActive = !valueWeWant},
                new Company {Id = 5, IsActive = !valueWeWant},
                new Company {Id = 6, IsActive = valueWeWant}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCompanyViewModel>).Should().Equal(
                        new[] { 2,3, 6},
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }

        [Fact]
        public void Company_Read_GetCompaniesIsActiveFalse_GetsCompaniesIsActiveFalse()
        {
            // Arrange
            var valueWeWant = false;
            var companyList = new List<Company>{
                new Company {Id = 1, IsActive = valueWeWant},
                new Company {Id = 2, IsActive = !valueWeWant},
                new Company {Id = 3, IsActive = !valueWeWant},
                new Company {Id = 4, IsActive = valueWeWant},
                new Company {Id = 5, IsActive = valueWeWant},
                new Company {Id = 6, IsActive = !valueWeWant}
            };

            _repo.AsQueryable().Returns(companyList.AsQueryable());
            DataSourceRequest request = new DataSourceRequest { Filters = new List<IFilterDescriptor>() };
            request.Filters.Add(new FilterDescriptor { Member = "IsActive", Operator = FilterOperator.IsEqualTo, Value = valueWeWant });

            // Act
            _companyController.WithCallTo(c => c.Company_Read(request)).ShouldReturnJson(data =>
            {
                // Assert
                (data.Data as IList<IndexCompanyViewModel>).Should().Equal(
                        new[] { 1, 4, 5 },
                        (c1, c2) => c1.Id == c2 && c1.IsActive == valueWeWant
                    );
            });
        }
        #endregion
    }
}
