﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Xunit;

namespace Core.ApplicationServices.UnitTests
{
    internal class AsyncSearchServiceUnitTests
    {
        private readonly SearchService _searchService;
        private readonly IGenericRepository<Project> _projectRepo;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;
        private readonly IGenericRepository<MeetingAgendaPoint> _meetingAgendaPointRepo;
        private readonly IDawaRepository _dawaRepository;

        public AsyncSearchServiceUnitTests()
        {
            _projectRepo = Substitute.For<IGenericRepository<Project>>();
            _agendaPointRepo = Substitute.For<IGenericRepository<AgendaPoint>>();
            _dawaRepository = Substitute.For<IDawaRepository>();
            _meetingAgendaPointRepo = Substitute.For<IGenericRepository<MeetingAgendaPoint>>();

            _searchService = new SearchService(_projectRepo, _agendaPointRepo);
        }

        #region GetProjectsAsync
        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public async Task GetProjectsAsync_ArgumentNull_ThrowArgumentNull()
        {
            // Arrange
            ProjectSearchParameters parameters = null;

            // Act
            // Assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => _searchService.GetProjectsAsync(parameters));
        }

        [Fact]
        public void GetProjectsAsync_EmptyParameters_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters();

            // Act
            var ret = _searchService.GetProjectsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation.")]
        public void GetProjectsAsync_AddressParameter_DawaCalled()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                Address = "query"
            };

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _dawaRepository.Received(1).GetAddressesAsync("q=" + parameters.Address + "*&fuzzy=true");
        }

        [Fact]
        public void GetProjectsAsync_AddressParameter_AddressExpressionCreated()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                Address = "query"
            };
            var dawaAddressId = new Guid();
            IEnumerable<Address> dawaAddresses = new List<Address>
            {
                new Address
                {
                    Id = dawaAddressId
                }
            };
            _dawaRepository.GetAddressesAsync(Arg.Any<string>())
                .Returns(Task.FromResult(dawaAddresses));

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Is<Expression<Func<Project, bool>>>(a =>
                ValidatePropertyConstantExpression("AddressId", dawaAddressId, a)));
        }

        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation.")]
        public void GetProjectsAsync_AddressParameterDawaReturns2Ids_OrElseAddressExpressionCreated()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                Address = "query"
            };
            var dawaAddressId = new Guid();
            IEnumerable<Address> dawaAddresses = new List<Address>
            {
                new Address
                {
                    Id = dawaAddressId
                },
                new Address
                {
                    Id = dawaAddressId
                }
            };
            _dawaRepository.GetAddressesAsync(Arg.Any<string>())
                .Returns(Task.FromResult(dawaAddresses));

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Is<Expression<Func<Project, bool>>>(a =>
                a.Body.NodeType == ExpressionType.OrElse));
        }

        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation.")]
        public void GetProjectsAsync_AddressParameterDawaReturnsNull_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                Address = "query"
            };
            _dawaRepository.GetAddresses(Arg.Any<string>())
                .Returns((IEnumerable<Address>)null);

            // Act
            var ret = _searchService.GetProjectsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetProjectsAsync_ProjectIdSet_ProjectIdExpressionCreated()
        {
            // Arrange
            const int projectId = 1;
            var parameters = new ProjectSearchParameters { ProjectId = projectId };

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Is<Expression<Func<Project, bool>>>(a =>
                ValidatePropertyConstantExpression("Id", projectId, a)));
        }

        [Fact]
        public void GetProjectsAsync_StatusSet_StatusExpressionCreated()
        {
            // Arrange
            const ProjectStatusType status = ProjectStatusType.Accepted;
            var parameters = new ProjectSearchParameters { ProjectStatusId = (int)status };

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Is<Expression<Func<Project, bool>>>(a =>
                ValidatePropertyConstantExpression("Status", status, a)));
        }

        [Fact]
        public void GetProjectsAsync_CouncilIdSet_CouncilIdExpressionCreated()
        {
            // Arrange
            const int councilId = 1;
            var parameters = new ProjectSearchParameters { CouncilId = councilId };

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Is<Expression<Func<Project, bool>>>(a =>
                ValidatePropertyConstantExpression("CouncilId", councilId, a)));
        }

        [Fact]
        public void GetProjectsAsync_GroupIdSet_GroupIdExpressionCreated()
        {
            // Arrange
            const int groupId = 1;
            var parameters = new ProjectSearchParameters { GroupId = groupId };

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Is<Expression<Func<Project, bool>>>(a =>
                ValidatePropertyConstantExpression("GroupId", groupId, a)));
        }

        [Fact]
        public void GetProjectsAsync_StartDate_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                StartDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetProjectsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetProjectsAsync_EndDate_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                EndDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetProjectsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetProjectsAsync_StartDateEndDateSet_DateCompareExpressionCreated()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                StartDate = new DateTime(2015, 10, 26),
                EndDate = new DateTime(2015, 10, 26)
            };

            // Act
            _searchService.GetProjectsAsync(parameters).Wait();

            // Assert
            _projectRepo.Received(1).GetAsync(Arg.Any<Expression<Func<Project, bool>>>());
        }
        #endregion

        #region GetAgendaPointsAsync
        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public async Task GetAgendaPointsAsync_ArgumentNull_ThrowArgumentNull()
        {
            // Arrange
            AgendaPointSearchParameters parameters = null;

            // Act
            // Assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => _searchService.GetAgendaPointsAsync(parameters));
        }

        [Fact]
        public void GetAgendaPointsAsync_EmptyParameters_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters();

            // Act
            var ret = _searchService.GetAgendaPointsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation.")]
        public void GetAgendaPointsAsync_AddressParameter_DawaCalled()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                Address = "query"
            };

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _dawaRepository.Received(1).GetAddressesAsync("q=" + parameters.Address + "*&fuzzy=true");
        }

        [Fact]
        public void GetAgendaPointsAsync_AddressParameter_AddressExpressionCreated()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                Address = "query"
            };
            var dawaAddressId = new Guid();
            IEnumerable<Address> dawaAddresses = new List<Address>
            {
                new Address
                {
                    Id = dawaAddressId
                }
            };
            _dawaRepository.GetAddressesAsync(Arg.Any<string>())
                .Returns(Task.FromResult(dawaAddresses));

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _agendaPointRepo.Received(1).GetAsync(Arg.Is<Expression<Func<AgendaPoint, bool>>>(a =>
                ValidatePropertyConstantExpression("AddressId", dawaAddressId, a)));
        }

        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation.")]
        public void GetAgendaPointsAsync_AddressParameterDawaReturns2Ids_OrElseAddressExpressionCreated()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                Address = "query"
            };
            var dawaAddressId = new Guid();
            IEnumerable<Address> dawaAddresses = new List<Address>
            {
                new Address
                {
                    Id = dawaAddressId
                },
                new Address
                {
                    Id = dawaAddressId
                }
            };
            _dawaRepository.GetAddressesAsync(Arg.Any<string>())
                .Returns(Task.FromResult(dawaAddresses));

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _agendaPointRepo.Received(1).GetAsync(Arg.Is<Expression<Func<AgendaPoint, bool>>>(a =>
                a.Body.NodeType == ExpressionType.OrElse));
        }

        [Fact, System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Dawa", Justification = "Abbreviation.")]
        public void GetAgendaPointsAsync_AddressParameterDawaReturnsNull_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                Address = "query"
            };
            _dawaRepository.GetAddresses(Arg.Any<string>())
                .Returns((IEnumerable<Address>)null);

            // Act
            var ret = _searchService.GetAgendaPointsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetAgendaPointsAsync_ProjectIdSet_ProjectIdExpressionCreated()
        {
            // Arrange
            const int projectId = 1;
            var parameters = new AgendaPointSearchParameters { ProjectId = projectId };

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _agendaPointRepo.Received(1).GetAsync(Arg.Is<Expression<Func<AgendaPoint, bool>>>(a =>
                ValidatePropertyConstantExpression("ProjectId", projectId, a)));
        }

        [Fact]
        public void GetAgendaPointsAsync_StatusSet_StatusExpressionCreated()
        {
            // Arrange
            const AgendaPointStatusType status = AgendaPointStatusType.Ready;
            var parameters = new AgendaPointSearchParameters { AgendaPointStatusId = (int)status };

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _agendaPointRepo.Received(1).GetAsync(Arg.Is<Expression<Func<AgendaPoint, bool>>>(a =>
                ValidatePropertyConstantExpression("Status", status, a)));
        }

        [Fact]
        public void GetAgendaPointsAsync_CouncilIdSet_CouncilIdExpressionCreated()
        {
            // Arrange
            const int topicId = 1;
            var parameters = new AgendaPointSearchParameters { TopicIds = new []{ topicId } };

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _agendaPointRepo.Received(1).GetAsync(Arg.Is<Expression<Func<AgendaPoint, bool>>>(a =>
                ValidatePropertyConstantExpression("TopicId", topicId, a)));
        }

        [Fact]
        public void GetAgendaPointsAsync_StartDate_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                StartDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetAgendaPointsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetAgendaPointsAsync_EndDate_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                EndDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetAgendaPointsAsync(parameters).Result;

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetAgendaPointsAsync_StartDateEndDateSet_DateCompareExpressionCreated()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                StartDate = new DateTime(2015, 10, 26),
                EndDate = new DateTime(2015, 10, 26)
            };

            // Act
            _searchService.GetAgendaPointsAsync(parameters).Wait();

            // Assert
            _agendaPointRepo.Received(1).GetAsync(Arg.Any<Expression<Func<AgendaPoint, bool>>>());
        }
        #endregion

        #region Assertion Helpers
        /// <summary>
        /// Validate an expression for a property and a constant value of type TConstant. True if the expression tree holds the property name in left branch and the constant value in right branch. Otherwise false.
        /// </summary>
        /// <typeparam name="TClass">The type on which the property exist.</typeparam>
        /// <typeparam name="TConstant">The constant type.</typeparam>
        /// <param name="expectedPropertyName">The expected property name.</param>
        /// <param name="expectedConstant">The expected constant value.</param>
        /// <param name="a">The expression to validate.</param>
        /// <returns>True if the expression tree holds the property name in left branch and the constant value in right branch. Otherwise false.</returns>
        private static bool ValidatePropertyConstantExpression<TClass, TConstant>(string expectedPropertyName, TConstant expectedConstant, Expression<Func<TClass, bool>> a)
        {
            var body = a.Body as BinaryExpression;
            if (body == null) return false;
            var left = body.Left as MemberExpression;
            if (left == null) return false;
            var right = body.Right as ConstantExpression;
            if (right == null) return false;

            var propertyName = left.Member.Name;
            var consant = right.Value;

            return (propertyName.Equals(expectedPropertyName) && consant.Equals(expectedConstant));
        }
        #endregion
    }
}
