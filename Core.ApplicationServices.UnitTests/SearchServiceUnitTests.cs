﻿#region Head
// <copyright file="SearchServiceUnitTests.cs" company="IT Minds" year="2015">
//     All rights reserved.
// </copyright>
// <license>
//     Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// </license>
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DomainModel;
using Core.DomainServices;
using NSubstitute;
using Xunit;

namespace Core.ApplicationServices.UnitTests
{
    public class SearchServiceUnitTests
    {
        private readonly SearchService _searchService;
        private readonly IGenericRepository<Project> _projectRepo;
        private readonly IGenericRepository<AgendaPoint> _agendaPointRepo;

        public SearchServiceUnitTests()
        {
            _projectRepo = Substitute.For<IGenericRepository<Project>>();
            _agendaPointRepo = Substitute.For<IGenericRepository<AgendaPoint>>();

            _searchService = new SearchService(_projectRepo, _agendaPointRepo);
        }

        #region GetProjects
        [Fact]
        public void GetProjects_ArgumentNull_ThrowArgumentNull()
        {
            // Arrange
            ProjectSearchParameters parameters = null;

            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => _searchService.GetProjects(parameters));
        }

        [Fact]
        public void GetProjects_EmptyParameters_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters();

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetProjects_StartDate_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                StartDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetProjects_EndDate_ReturnNull()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                EndDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetProjects_ProjectIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                ProjectId = 1
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<Project>>(ret);
        }

        [Fact]
        public void GetProjects_ProjectStatusHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                ProjectStatusId = (int)ProjectStatusType.Accepted
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<Project>>(ret);
        }

        [Fact]
        public void GetProjects_CouncilIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                CouncilId = 1
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<Project>>(ret);
        }

        [Fact]
        public void GetProjects_GroupIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                GroupId = 1
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<Project>>(ret);
        }

        [Fact]
        public void GetProjects_TopicIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new ProjectSearchParameters
            {
                TopicIds = new []{ 1 }
            };

            // Act
            var ret = _searchService.GetProjects(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<Project>>(ret);
        }
        #endregion

        #region GetAgendaPoints
        [Fact]
        public void GetAgendaPoints_ArgumentNull_ThrowArgumentNull()
        {
            // Arrange
            AgendaPointSearchParameters parameters = null;

            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => _searchService.GetAgendaPoints(parameters));
        }

        [Fact]
        public void GetAgendaPoints_EmptyParameters_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters();

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetAgendaPoints_StartDate_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                StartDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetAgendaPoints_EndDate_ReturnNull()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                EndDate = new DateTime(2015, 10, 26)
            };

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.Null(ret);
        }

        [Fact]
        public void GetAgendaPoints_ProjectIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                ProjectId = 1
            };

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<AgendaPoint>>(ret);
        }

        [Fact]
        public void GetAgendaPoints_MeetingIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                MeetingId = 1
            };

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<AgendaPoint>>(ret);
        }

        [Fact]
        public void GetAgendaPoints_AgendaPointStatusHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                AgendaPointStatusId = (int)AgendaPointStatusType.Approved
            };

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<AgendaPoint>>(ret);
        }

        [Fact]
        public void GetAgendaPoints_TopicIdHasValue_ReturnsNonNullIQueryable()
        {
            // Arrange
            var parameters = new AgendaPointSearchParameters
            {
                TopicIds = new []{ 1 }
            };

            // Act
            var ret = _searchService.GetAgendaPoints(parameters);

            // Assert
            Assert.NotNull(ret);
            Assert.IsAssignableFrom<IQueryable<AgendaPoint>>(ret);
        }
        #endregion

        #region Assertion Helpers
        /// <summary>
        /// Validate an expression for a property and a constant value of type TConstant. True if the expression tree holds the property name in left branch and the constant value in right branch. Otherwise false.
        /// </summary>
        /// <typeparam name="TClass">The type on which the property exist.</typeparam>
        /// <typeparam name="TConstant">The constant type.</typeparam>
        /// <param name="expectedPropertyName">The expected property name.</param>
        /// <param name="expectedConstant">The expected constant value.</param>
        /// <param name="a">The expression to validate.</param>
        /// <returns>True if the expression tree holds the property name in left branch and the constant value in right branch. Otherwise false.</returns>
        private static bool ValidatePropertyConstantExpression<TClass, TConstant>(string expectedPropertyName, TConstant expectedConstant, Expression<Func<TClass, bool>> a)
        {
            var body = a.Body as BinaryExpression;
            if (body == null) return false;
            var left = body.Left as MemberExpression;
            if (left == null) return false;
            var right = body.Right as ConstantExpression;
            if (right == null) return false;

            var propertyName = left.Member.Name;
            var consant = right.Value;

            return (propertyName.Equals(expectedPropertyName) && consant.Equals(expectedConstant));
        }
        #endregion
    }
}
